MODULE slatec_daxpy
CONTAINS

!DECK DAXPY
      SUBROUTINE DAXPY (N, DA, DX, INCX, DY, INCY)
!***BEGIN PROLOGUE  DAXPY
!***PURPOSE  Compute a constant times a vector plus a vector.
!***LIBRARY   SLATEC (BLAS)
!***CATEGORY  D1A7
!***TYPE      DOUBLE PRECISION (SAXPY-S, DAXPY-D, CAXPY-C)
!***KEYWORDS  BLAS, LINEAR ALGEBRA, TRIAD, VECTOR
!***AUTHOR  Lawson, C. L., (JPL)
!           Hanson, R. J., (SNLA)
!           Kincaid, D. R., (U. of Texas)
!           Krogh, F. T., (JPL)
!***DESCRIPTION
!
!                B L A S  Subprogram
!    Description of Parameters
!
!     --Input--
!        N  number of elements in input vector(s)
!       DA  double precision scalar multiplier
!       DX  double precision vector with N elements
!     INCX  storage spacing between elements of DX
!       DY  double precision vector with N elements
!     INCY  storage spacing between elements of DY
!
!     --Output--
!       DY  double precision result (unchanged if N .LE. 0)
!
!     Overwrite double precision DY with double precision DA*DX + DY.
!     For I = 0 to N-1, replace  DY(LY+I*INCY) with DA*DX(LX+I*INCX) +
!       DY(LY+I*INCY),
!     where LX = 1 if INCX .GE. 0, else LX = 1+(1-N)*INCX, and LY is
!     defined in a similar way using INCY.
!
!***REFERENCES  C. L. Lawson, R. J. Hanson, D. R. Kincaid and F. T.
!                 Krogh, Basic linear algebra subprograms for Fortran
!                 usage, Algorithm No. 539, Transactions on Mathematical
!                 Software 5, 3 (September 1979), pp. 308-323.
!***ROUTINES CALLED  (NONE)
!***REVISION HISTORY  (YYMMDD)
!   791001  DATE WRITTEN
!   890831  Modified array declarations.  (WRB)
!   890831  REVISION DATE from Version 3.2
!   891214  Prologue converted to Version 4.0 format.  (BAB)
!   920310  Corrected definition of LX in DESCRIPTION.  (WRB)
!   920501  Reformatted the REFERENCES section.  (WRB)
!***END PROLOGUE  DAXPY
      DOUBLE PRECISION DX(*), DY(*), DA
!***FIRST EXECUTABLE STATEMENT  DAXPY
      IF (N.LE.0 .OR. (.NOT. ABS(DA).GT.0.0D0)) RETURN
      !IF (INCX .EQ. INCY) IF (INCX-1) 5,20,60
      IF(INCX.NE.INCY .OR. (INCX-1).LT.0) THEN
!
!     Code for unequal or nonpositive increments.
!
        IX = 1
        IY = 1
        IF (INCX .LT. 0) IX = (-N+1)*INCX + 1
        IF (INCY .LT. 0) IY = (-N+1)*INCY + 1
        DO I = 1,N
          DY(IY) = DY(IY) + DA*DX(IX)
          IX = IX + INCX
          IY = IY + INCY
        END DO
        RETURN

      ELSE IF((INCX-1).EQ.0) THEN
!
!     Code for both increments equal to 1.
!
!     Clean-up loop so remaining vector length is a multiple of 4.
!
        M = MOD(N,4)
        IF (M .NE. 0) THEN
          DO I = 1,M
            DY(I) = DY(I) + DA*DX(I)
          END DO
          IF (N .LT. 4) RETURN
        END IF
        MP1 = M + 1
        DO I = MP1,N,4
          DY(I) = DY(I) + DA*DX(I)
          DY(I+1) = DY(I+1) + DA*DX(I+1)
          DY(I+2) = DY(I+2) + DA*DX(I+2)
          DY(I+3) = DY(I+3) + DA*DX(I+3)
        END DO
        RETURN

      ELSE !(INCX-1).GT.0
!
!     Code for equal, positive, non-unit increments.
!
        NS = N*INCX
        DO I = 1,NS,INCX
          DY(I) = DA*DX(I) + DY(I)
        END DO
        RETURN
      END IF
      END SUBROUTINE

END MODULE slatec_daxpy
