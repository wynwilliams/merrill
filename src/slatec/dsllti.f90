MODULE slatec_dsllti
CONTAINS

!DECK DSLLTI
      SUBROUTINE DSLLTI (N, B, X, NELT, IA, JA, A, ISYM, RWORK, IWORK)
         USE slatec_dllti2
!***BEGIN PROLOGUE  DSLLTI
!***PURPOSE  SLAP MSOLVE for LDL' (IC) Factorization.
!            This routine acts as an interface between the SLAP generic
!            MSOLVE calling convention and the routine that actually
!                           -1
!            computes (LDL')  B = X.
!***LIBRARY   SLATEC (SLAP)
!***CATEGORY  D2E
!***TYPE      DOUBLE PRECISION (SSLLTI-S, DSLLTI-D)
!***KEYWORDS  ITERATIVE PRECONDITION, LINEAR SYSTEM SOLVE, SLAP, SPARSE
!***AUTHOR  Greenbaum, Anne, (Courant Institute)
!           Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-60
!             Livermore, CA 94550 (510) 423-3141
!             seager@llnl.gov
!***DESCRIPTION
!       It is assumed that RWORK and IWORK have initialized with
!       the information required for DLLTI2:
!          IWORK(1) = NEL
!          IWORK(2) = Starting location of IEL in IWORK.
!          IWORK(3) = Starting location of JEL in IWORK.
!          IWORK(4) = Starting location of EL in RWORK.
!          IWORK(5) = Starting location of DINV in RWORK.
!       See the DESCRIPTION of DLLTI2 for details.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  DLLTI2
!***REVISION HISTORY  (YYMMDD)
!   871119  DATE WRITTEN
!   881213  Previous REVISION DATE
!   890915  Made changes requested at July 1989 CML Meeting.  (MKS)
!   890922  Numerous changes to prologue to make closer to SLATEC
!           standard.  (FNF)
!   890929  Numerous changes to reduce SP/DP differences.  (FNF)
!   910411  Prologue converted to Version 4.0 format.  (BAB)
!   910502  Corrected conversion error.  (FNF)
!   920511  Added complete declaration section.  (WRB)
!   921113  Corrected C***CATEGORY line.  (FNF)
!   930701  Updated CATEGORY section.  (FNF, WRB)
!***END PROLOGUE  DSLLTI
!     .. Scalar Arguments ..
      INTEGER ISYM, N, NELT
!     .. Array Arguments ..
      DOUBLE PRECISION A(NELT), B(*), RWORK(*), X(*)
      INTEGER IA(NELT), IWORK(*), JA(NELT)
!     .. Local Scalars ..
      INTEGER LOCDIN, LOCEL, LOCIEL, LOCJEL, NEL
!***FIRST EXECUTABLE STATEMENT  DSLLTI
      NEL = IWORK(1)
      LOCIEL = IWORK(3)
      LOCJEL = IWORK(2)
      LOCEL  = IWORK(4)
      LOCDIN = IWORK(5)
      CALL DLLTI2(N, B, X, NEL, IWORK(LOCIEL), IWORK(LOCJEL), &
           RWORK(LOCEL), RWORK(LOCDIN))
!
      RETURN
!------------- LAST LINE OF DSLLTI FOLLOWS ----------------------------
      END

END MODULE slatec_dsllti
