!> A Module containing routines for reading and writing mesh files.
module Mesh_IO
   use Utils, only: DP
   implicit none

   character(LEN = 1024):: datafile, stem, hystfile, loopfile, vboxfile, demagfile
   character(LEN = 1024):: stresshystfile, LLGfile
   character(LEN = 80):: Zstr
   integer:: currentmesh
!  integer zone_val  ! for use in the writing of multi zone files

contains

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Subroutines for Mesh IO related operations
   !    ->  BuildMeshIO                    :
   !    ->  ReadPatranMesh                 : read mesh from a patran file
   !    ->  ReadQDM                        :
   !    ->  ReadTecplotMesh                : read mesh in tecplot file format
   !    ->  WriteMag                       : writes out the magnetization
   !                                            (.dat) files
   !    ->  WriteMagForc                   :
   !    ->  WriteHyst                      : writes out m.h_est against
   !                                            |h_ext|
   !    ->  WriteStressHist                :
   !    ->  WriteLoopData                  : writes out field, magnetization, and
   !                                            other variables specified in a loop
   !    ->  WriteDemag
   !    ->  WriteVBox                      : write out the volume data associated with
   !                                              each element
   !    ->  ReadMagForc                    :
   !    ->  ReadMag                        :
   !    ->  WriteTecplot                   : writes out the TecPlot format
   !                                            for visualization
   !    ->  GenerateCubeMesh               :
   !    ->  GenerateSphereMesh             :
   !    ->  BuildUnitCube                  :
   !    ->  ReadTecPlotPath                : Read the multi-zone tecplot file
   !                                         `PathInFile` into the path variables.
   !    ->  ReadLoopData                   : Reads data from *.loop file
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   subroutine BuildMeshIO()
      use Tetrahedral_Mesh_Data, only: BuildTetrahedralMeshData
      use Material_Parameters, only: BuildMaterialParameters
      use Finite_Element_Matrices, only: BuildFiniteElement
      !use Energy_Calculator, only: BuildEnergyCalculator
      implicit none

      call BuildTetrahedralMeshData()
      call BuildFiniteElement()
      call BuildMaterialParameters()
      !call BuildEnergyCalculator()
   end subroutine BuildMeshIO

   !---------------------------------------------------------------------------
   ! ReadPatranMesh: Read meshfile containing a PATRAN Neutral File
   !---------------------------------------------------------------------------
   subroutine ReadPatranMesh(meshfile)
      use Tetrahedral_Mesh_Data
      implicit none

      character(len=*), intent(IN):: meshfile
      integer:: meshunit

      integer:: i
      integer, allocatable:: idx(:)

      ! Header values
      integer:: IT, ID, IV, KC, N1, N2, N3, N4, N5

      ! Packet 2 data card 1
      integer:: P2C2_nodes, P2C2_config, P2C2_ceid
      real(KIND = DP):: P2C2_th(3)

      character(len = 12) block_name

      character(len = 20), parameter:: HEADER_FMT = "(I2, 8I8)"
      integer:: ios

      !-------------------------------------------------------------------------
      !            Start reading MESH file (.pat format)
      !-------------------------------------------------------------------------
      !
      ! File format taken from
      !     Patran 2014.1 Reference Manual Part 1: Basic Functions, 
      !     The Neutral File, p 892
      ! found at
      !     https://simcompanion.mscsoftware.com/infocenter/
      !         index?page = content&id = DOC10756
      !         &cat = 2014.1_PATRAN_DOCS&actp = LIST
      !
      ! A Patran file is a series of packets. A packet contains several
      ! lines called "cards". The first card is a header with identifying
      ! information, including the number of subsequent cards/lines, and
      ! the rest are the relevant information information for the packet.
      !
      ! The main do loop should be iterating over packets, with the initial
      ! "read" reading a packet's header card and the "read"s within the
      ! SELECT CASE should be reading off the remaining cards in the packet.
      !

      NFIX = 0

      ! First we open the file
      open ( &
         NEWUNIT = meshunit, FILE = meshfile, &
         STATUS='OLD', ACTION='READ', IOSTAT = ios &
         )
      if (ios .ne. 0) then
         write (*, *) "Error opening file: ", trim(meshfile)
         stop
      end if

      do
         !
         ! We read the header card for the packet.
         ! This read should*always*be reading a header card. If it's not, 
         ! something is seriously wrong.
         !
         ! The header format is the same for every card, so we'll read it
         ! here, and use that information to figure out how it should be
         ! processed later.
         !
         ! IT here is the packet type, KC is the number of extra lines
         ! in the packet. N1-N5 are optional extra data used by the packet.
         !
         read (meshunit, HEADER_FMT, IOSTAT = ios) &
            IT, ID, IV, KC, N1, N2, N3, N4, N5

         ! Special case for card 99 and eof:
         ! On End card or end of file, exit the do loop.
         if (IT == 99 .or. ios < 0) exit

         ! Here we process each packet, reading the rest of its cards.
         !
         ! The Patran standard says these packets*should*appear in order, 
         ! if at all.
         select case (IT)

            !------------------------------------------------------------------
            ! Packet 25: Title Card
            !------------------------------------------------------------------
            ! The title card packet contains the following:
            !         25 ID IV KC
            !         TITLE
            !
            ! where:  ID = 0 (not applicable)
            !         IV = 0 (not applicable
            !         KC = 1
            !         TITLE = Identifying title ...
            !
            ! format: (I2, 8I8)
            !         (80A4)
            !------------------------------------------------------------------
         case (25)
            ! KC should be 1.
            ! Ignore this packet.
            do i = 1, KC
               read (meshunit, *)
            end do

            !------------------------------------------------------------------
            ! Packet 26: Summary Data
            !------------------------------------------------------------------
            ! The summary data packet contains the following:
            !         26 ID IV KC N1 N2 N3 N4 N5
            !         DATE TIME VERSION
            !
            ! where:  ID = 0 (not applicable)
            !         IV = 0 (not applicable)
            !         KC = 1
            !         N1 = number of nodes
            !         N2 = number of elements
            !         N3 = number of materials
            !         N4 = number of element properties
            !         N5 = number of coordinate frames
            !         DATE = dd-mm-yy
            !         TIME = hh:mm:ss
            !         VERSION = ??
            !
            ! format: (I2, 8I8)
            !         (80A4)
            !------------------------------------------------------------------
         case (26)
            NNODE = N1
            NTRI = N2

            ! Ignore the rest of this packet.
            do i = 1, KC
               read (meshunit, *)
            end do

            ! Allocate all arrays that depend on NNODE and NTRI
            call MeshAllocate(NNODE, NTRI)

            !------------------------------------------------------------------
            ! Packet 01: Node Data
            !------------------------------------------------------------------
            ! The node data packet contains the following:
            !         1 ID IV KC
            !         X Y Z
            !         ICF GTYPE NDF CONFIG CID PSP
            !
            ! where:  ID = node ID
            !         IV = 0 (not applicable)
            !         KC = number of lines in data card = 2
            !         X, Y, Z = X, Y, Z cartesian coordinate of the node
            !         ICF = 1 (referenced)
            !         GTYPE = G
            !         NDF = 2 or 3 for 2D or 3D model respectively
            !         CONFIG = 0 (not applicable)
            !         CID = 0 i.e. global cartesian coordinate system
            !         PSPC = 000000 (not used)
            !
            ! format: (I2, 8I8)
            !         (3E16.9) (Maybe actually (E15.8, 1X, E15.8, 1X, E15.8)
            !         (I1, 1A1, I8, I8, I8, 2X, 6I1)
            !------------------------------------------------------------------
         case (1)
            ! ID = Node ID

            ! Card 1: X Y Z data
            read (meshunit, "(3E16.9)") VCL(ID, 1), VCL(ID, 2), VCL(ID, 3)

            ! Ignore the rest of this packet
            do i = 2, KC
               read (meshunit, *)
            end do

            !------------------------------------------------------------------
            ! Packet 02: Element Data
            !------------------------------------------------------------------
            ! The element data packet contains the following:
            !         2 ID IV KC N1 N2
            !         NODES CONFIG PID CEID q1 q2 q3
            !         LNODES
            !         ADATA
            !
            ! where:  ID = element ID
            !         IV = shape (2 = bar, 3 = tri, 4 = quad, 
            !                     5 = tet, 7 = wedge, 8 = hex, 9 = pyra)
            !         KC = number of lines in data card
            !         N1 = 0 (not used)
            !         N2 = 0 (not used)
            !         NODES = number of nodes in the element
            !         CONFIG = 0 (not used)
            !         PID = element property ID
            !         CEID = 0 (not used)
            !         q1, q2, q3 = 0 (not used)
            !         LNODES = element corner nodes followed by
            !                  additional nodes
            !         ADATA : not used
            !
            ! format: (I2, 8I8)
            !         (I8, I8, I8, I8, 3E16.9)
            !         (10I8)
            !------------------------------------------------------------------
         case (2)
            ! ID = Element ID

            ! Card 1 contains number of nodes, element configuration, 
            ! property id etc.
            ! We'll put the property ID into TetSubDomains(ID)
            read (meshunit, "(4I8, 3E16.9)") &
               P2C2_nodes, P2C2_config, TetSubDomains(ID), P2C2_ceid, &
               P2C2_th

            ! Card 2 contains node ids. We assume a linear tetrahedron, 
            ! so we should only have 4 nodes.
            read (meshunit, "(4I8)") &
               TIL(ID, 1), TIL(ID, 2), TIL(ID, 3), TIL(ID, 4)
            TIL(ID, 5) = 0

            ! Ignore the rest of this packet
            do i = 3, KC
               read (meshunit, *)
            end do

            ! Named components, used for block ids

            !------------------------------------------------------------------
            ! Packet 21: Named Components
            !------------------------------------------------------------------
            ! The element data packet contains the following:
            !         21 ID IV KC
            !         NAME
            !         NTYPE(1) ID(1) NTYPE(2) ID(2) ... NTYPE(5) ID(5)
            !         NTYPE(1) ID(1) NTYPE(2) ID(2) ... NTYPE(5) ID(5)
            !         ...
            !
            ! where:  ID = component number (i.e a number if for the nodeset)
            !         IV = 2*number of data pairs.
            !               i.e. IV/2 is the number of elements referenced.
            !         KC = number of lines in data card
            !               KC = 1 + (IV+9)/10 for text files
            !         NAME = component name (often a nodeset name or number)
            !         NTYPE(i) = Element type.
            !               Node = 5
            !               Tetrahedron = 9
            !         ID(i) = Element ID.
            !
            ! format: (I2, 8I8)
            !         (A12)
            !         (10I8)
            !         (10I8)
            !         ...
            !------------------------------------------------------------------
         case (21)
            ! IV = 2*the number of data pairs
            ! ID = Component number

            ! Card 1 is component name.
            read (meshunit, "(A12)") block_name

            ! Warn about block ids and fixed nodes
            write (*, *)
            write (*, *) "Patran file contains block with name '", &
               trim(block_name), "' and id: ", ID
            !WRITE(*,*) "Note that all blocks apart from block id 0 &
            !    & are assumed to contain fixed magnetizations!"
            write (*, *) "Note that all blocks names are not currently &
                & allowd in MERRILL"

            ! Card 2 is a set of (NTYPE, ID) pairs, 5 pairs to a line.

            !
            !IGNORE THIS CARD because NodeBlockNumber used elsewhere(Sept 7 2021)
            ! IF(ALLOCATED(idx)) DEALLOCATE(idx)
            ! ALLOCATE(idx(IV))

            ! ! Read all lines/pairs in at once
            ! READ(meshunit, *) (idx(i), i = 1, IV)

            ! DO i = 1, IV/2
            !   IF(idx(2*(i-1)+2) .EQ. 5) THEN
            !     ! NTYPE = 5 for Nodes.
            !     ! sets the block number for each node of the mesh
            !     ! Block IDs seem to start at 0, we want them to start at 1.
            !     NodeBlockNumber(idx(2*(i-1)+1))=ID+1
            !   ELSE IF(idx(2*(i-1)+2) .EQ. 9) THEN
            !     ! NTYPE = 9 for Tetrahedra
            !     TetSubDomains(idx(2*(i-1)+1))=ID+1
            !   END IF
            ! END DO

            ! NFIX = NFIX+IV/2
            ! DEALLOCATE(idx)

            ! Unknown card, ignore
         case DEFAULT
            write (*, "(A, I2, A)") "ReadPatranMesh: Ignoring Packet Type ", IT, &
                & " in file '"//trim(meshfile)//"'"
            do i = 1, KC
               read (meshunit, *)
            end do

         end select

      end do

      close (meshunit)

      call BuildMeshIO()
   end subroutine ReadPatranMesh

   !---------------------------------------------------------------
   ! ReadQDM
   !---------------------------------------------------------------

   subroutine ReadQDM(qdmfile, ios)
      use Material_Parameters
      use Tetrahedral_Mesh_Data, only: QDMnodes, QDM, QDMAllocate
      implicit none

      integer i, NVAL
      character(1):: CH, pCH
      character(1):: DELIM = " "

      real(KIND = DP):: Qx, Qy, Qz, Qhz
      real(KIND = DP):: pass(5)
!    REAL(KIND = DP), ALLOCATABLE:: QDM(:,4)
      character(LEN = 200) ::  qdmfile
      integer:: qdmunit, ios

      ! Assume format is x y x QDM_hz
      ! count how many numbers there are in the first line
      open (NEWUNIT = qdmunit, FILE = qdmfile, STATUS='unknown')

      ! Read first record one character at a time and count delimeters
      NVAL = 0
      pch = DELIM

      do
         ! eor = 10 jumps to label 10 on end of row
         read (qdmunit, '(A)', ADVANCE='no', EOR = 10) ch

         ! increment counter only if the current in not a space and
         ! previous character was a space
         if ((ch /= DELIM) .and. (pch == DELIM)) then
            NVAL = NVAL+1
         end if
         !  set previous character to current character
         pch = ch
      end do
      ! read command skips here on EOR
10    continue

      rewind (qdmunit)
      close (qdmunit)
      ! NVAL now contains the number of columns in the first line of a file

      write (*, *) 'The QDM file contains', NVAL, 'columns'
      open (NEWUNIT = qdmunit, FILE = qdmfile, STATUS='unknown')

      QDMnodes = 0
      do
         read (qdmunit, *, iostat = ios)
         if (ios /= 0) exit
         QDMnodes = QDMnodes+1
      end do
      close (qdmunit)

      call QDMAllocate(QDMnodes)

      select case (NVAL)
      case (3)
         do i = 1, QDMnodes
            read (qdmunit, *, IOSTAT = ios) Qx, Qy, Qz, Qhz
            if (ios < 0) then
               goto 11

            end if
            QDM(i, 1) = Qx
            QDM(i, 2) = Qy
            QDM(i, 3) = Qz
            QDM(i, 4) = Qhz
         end do
11       if (ios < 0) write (*, *) 'The QDM file is not complete'
         close (qdmunit)

      case DEFAULT
         write (*, *) 'Error counting colums in file ', trim(qdmfile)
         close (qdmunit)
         stop

      end select

   end subroutine ReadQDM

   !----------------------------------------------------------------------------
   ! ReadTecplotMesh: Read meshfile containing a Tecplot ASCII File
   !                  - The numerical data is formatted in 10 column format
   !                  - Mx, My, Mz point data is optional (either all of the 3
   !                    components must be present OR absent)
   !                  - SD data is in integer format
   !                  - Header names can be in modern or old Tecplot format
   !----------------------------------------------------------------------------
   subroutine ReadTecplotMesh(meshfile)
      use Tetrahedral_Mesh_Data
      use strings
      implicit none

      character(len=*), intent(IN):: meshfile
!    CHARACTER(len = 5), INTENT(OUT):: packing
      integer:: meshunit

      integer, parameter:: MAX_TECPLOT_CHARS_PER_LINE = 32000
      character(len = MAX_TECPLOT_CHARS_PER_LINE):: line
      character(len = 1024):: token
      character(len=*), parameter:: separators = " ,"

      integer, parameter:: PARSE_SUCCESS = 1, PARSE_ERROR = -1

      integer:: NNODE_read, NTRI_read

      integer:: ierr

      integer:: ParserState, i, SDFLAG
      integer, parameter :: &
         PARSING_HEADERS = 1, PARSING_ZONE = 2, &
         ALLOCATING_MESH = 3, &
         PARSING_NODES = 4, PARSING_ELEMENTS = 5, &
         PARSING_DONE = 6, &
         PARSING_VAR1 = 7, PARSING_VAR2 = 8, &
         PARSING_VAR3 = 9, PARSING_VAR4 = 10, &
         PARSING_VAR5 = 11, PARSING_VAR6 = 12, &
         PARSING_VAR7 = 13

      logical:: MX_HDR = .false., MY_HDR = .false., MZ_HDR = .false.

      packing = "NOSET"  ! set packing to neither BLOCK nor POINT, and must be set in this subroutine or fail
      ! First we open the file

!    PRINT*, ' IN TECPLOT MESH'

      open ( &
         NEWUNIT = meshunit, FILE = meshfile, &
         STATUS='OLD', ACTION='READ', IOSTAT = ierr &
         )
      if (ierr .ne. 0) then
         write (*, *) "Error opening file: ", trim(meshfile)
         stop
      end if

      ParserState = PARSING_HEADERS
      NNODE_read = -1
      NTRI_read = -1
      ! Loop over lines
      do while (ParserState .ne. PARSING_DONE)
         line = ""
         read (meshunit, '(A)', iostat = ierr, iomsg = token) line
!      PRINT*, line(:10)
         if (ierr .ne. 0) then
            if (IS_IOSTAT_END(ierr)) then
               exit
            else
               write (*, *) token
               ERROR stop
            end if
         end if

         !
         ! Skip comments
         !
         call DropInitialSeparators(line)
         if (line(1:1) .eq. "#") then
            cycle
         end if

         !
         ! PARSING_HEADERS
         !
         ! Parsing Headers
         ! Expecting something like
         !   TITLE = "my fancy title"
         !   VARIABLES = "X", "Y", "Z", "Mx", "My", "Mz"
         ! terminated by a line beginning with ZONE
         !
         ! Maybe a little more complex than necessary, but it should
         ! allow for header items to be added by users in any order.
         !
         if (ParserState .eq. PARSING_HEADERS) then

            ! Loop over the tokens
            do
               ! Parse one token, expecting a keyword.
               call ParseToken(line, token)
!PRINT*, 'TOKEN = ', token
               if (len_trim(token) .gt. 0) then

                  ! Match token for further parsing
                  select case (lowercase(trim(token)))
                     ! Parse TITLE
                  case ("title")
                     call ParseEqOrDie(line, token)

                     ! Parse title value. Drop it, because we don't use it.
                     call ParseToken(line, token)

                     ! Parse VARIABLES
                  case ("variables")
!              PRINT*, 'IN VARIABLES'
                     call ParseEqOrDie(line, token)

                     ! The rest of the line is variable tokens.
                     ! Expect X, Y, Z, optional Mx, My, Mz, then EOF
                     call ParseToken(line, token)
                     if (trim(token) .ne. "X") then
                        write (*, *) 'ERROR: Expected first variable to be "X"'
                        ERROR stop
                     end if

                     call ParseToken(line, token)
                     if (trim(token) .ne. "Y") then
                        write (*, *) 'ERROR: Expected second variable to be "Y"'
                        ERROR stop
                     end if

                     call ParseToken(line, token)
                     if (trim(token) .ne. "Z") then
                        write (*, *) 'ERROR: Expected third variable to be "Z"'
                        ERROR stop
                     end if

                     ! Try parsing Mx My and Mz. If one is missing we raise an error
                     ! If the three comps are found or not found, we use the _HDR
                     ! variables to allow reading or not reading the magnetization data
                     call ParseToken(line, token)
                     if (len_trim(token) .gt. 0) then
                        if (trim(token) .ne. "Mx") then
                           MX_HDR = .false.
                        else
                           MX_HDR = .true.
                        end if

                        call ParseToken(line, token)
                        if (trim(token) .ne. "My") then
                           MY_HDR = .false.
                        else
                           MY_HDR = .true.
                        end if

                        call ParseToken(line, token)
                        if (trim(token) .ne. "Mz") then
                           MZ_HDR = .false.
                        else
                           MZ_HDR = .true.
                        end if

                        if (MX_HDR .and. MY_HDR .and. MZ_HDR) then
                           write (*, *) 'Found magnetization data in tecplot file &
                                       &but not using for mesh reading'
                        else if (.not. (MX_HDR .or. MY_HDR .or. MZ_HDR)) then
                           write (*, *) 'Magnetization data not found in tecplot file: skipping'
                        else
                           write (*, *) 'Missing a component of magnetization data'
                           write (*, *) 'Stopping mesh reading'
                           ERROR stop
                        end if

                        call ParseToken(line, token)
                        if (trim(token) .eq. "SD") then
                           write (*, *) 'SubDomain ID variable is present'
                        end if

                     end if
                     ! Parse ZONE.
                     ! Move on to the next phase.
                  case ("zone")
                     ! Done parsing header  ! Move on to parsing ZONE header.
                     ParserState = PARSING_ZONE

                     ! Break out of the token parsing loop
                     exit
                  end select
               else
                  ! No token parsed, end of line, break out of token parsing loop
                  exit
               end if

            end do  ! Parsing tokens
         end if  ! Parsing Headers

         !
         ! PARSING_ZONE
         !
         ! Parsing ZONE Header
         ! Expecting something like
         !   ZONE T="...", N = 1234
         !     E = 567 F = FEPOINT ET = TETRAHEDRON
         ! possibly spanning multiple lines, 
         ! terminated by the first numerical value of the actual data.
         !
         ! Again, maybe a little more complex than necessary, but it should
         ! allow for header items to be added by users in any order.
         !
         ! 10 May 2021 DavidC: Tecplot 2020 format has slightly different header
         ! names, such as ZONETYPE instead of ET, DATAPACKING instead of F, ...
         ! See: https://download.tecplot.com/360/current/360_data_format_guide.pdf
         !
         !
         if (ParserState .eq. PARSING_ZONE) then
!      PRINT*,' PARSING ZONE'
            ! Parse tokens
            do
               call ParseToken(line, token)
! PRINT*, 'token  =  ', ParserState,  token(:20)
               if (len_trim(token) .gt. 0) then

                  ! Match token for further parsing
                  select case (lowercase(trim(token)))
                     ! Parse zone title. Ignore.
                  case ("t")
                     call Skip2quotes(line)  ! skip the zone name

                  case ("f", "datapacking")
                     call ParseEqOrDie(line, token)
!PRINT*, 'IN  format'
                     call ParseToken(line, token)
                     if (lowercase(trim(token)) .eq. "fepoint" .or. &
                         lowercase(trim(token)) .eq. "point") packing = "point"
                     if (lowercase(trim(token)) .eq. "feblock" .or. &
                         lowercase(trim(token)) .eq. "block") packing = "block"

                     if (lowercase(trim(token)) .ne. "fepoint" .and. &
                         lowercase(trim(token)) .ne. "point" .and. &
                         lowercase(trim(token)) .ne. "feblock" .and. &
                         lowercase(trim(token)) .ne. "block") then
                        write (*, *) "ERROR: Unsupported value for F or DATAPACKING: &
                                    &expected either (FE)POINT or (FE)BLOCK"
                        ERROR stop
                     end if

                  case ("et", "zonetype")
                     call ParseEqOrDie(line, token)

                     call ParseToken(line, token)
                     if (lowercase(trim(token)) .ne. "tetrahedron" .and. &
                         lowercase(trim(token)) .ne. "fetetrahedron") then
                        write (*, *) "ERROR: Unsupported value for ET or ZONETYPE: &
                                    &expected (FE)TETRAHEDRON"
                        ERROR stop
                     end if

                  case ("n", "nodes")
                     call ParseEqOrDie(line, token)
                     call ParseToken(line, token)
                     if (len_trim(token) .eq. 0) then
                        write (*, *) "ERROR: Expected value for N or NODES."
                        ERROR stop
                     end if
                     read (token, *) NNODE
!PRINT*,' NNODE in tecplot  = ', NNODE

                  case ("e", "elements")
                     call ParseEqOrDie(line, token)
                     call ParseToken(line, token)
                     if (len_trim(token) .eq. 0) then
                        write (*, *) "ERROR: Expected value for E or ELEMENTS."
                        ERROR stop
                     end if
                     read (token, *) NTRI

                  case DEFAULT

                     !PRINT*, ' in case default with number ', token

                     ! Check if it's a number ([digit], ., or +/-)
                     if (scan(token(1:1), "0123456789.+-") .gt. 0) then
                        line = trim(token)//" "//trim(line)
!PRINT*, ' in case default with number ', token
                        ! Move parser along to node parsing
                        ParserState = ALLOCATING_MESH
                        exit
                     end if

                  end select

               else
                  ! No token parsed, end of line
                  exit
               end if
            end do
         end if

         !
         ! ALLOCATING_MESH
         !
         ! Allocating mesh based on N and E
         !
         if (ParserState .eq. ALLOCATING_MESH) then
            call MeshAllocate(NNODE, NTRI)
            NNODE_read = 1
            NTRI_read = 1

            if (lowercase(packing) .eq. "point") ParserState = PARSING_NODES
            if (lowercase(packing) .eq. "block") ParserState = PARSING_VAR1
         end if

         !
         ! PARSING_NODES
         !
         ! Parse node coordinate positions
         !

         if (ParserState .eq. PARSING_VAR1) then  ! must be in block format
            !read first  variable block (x coord of node location)
            if (NNODE_read .le. NNODE) then
               if ((NNODE+1 - NNODE_read) .ge. 10) then
                  !          print*, NNODE, NNODE_read
                  read (line, *) (VCL(i, 1), i = NNODE_read, NNODE_read+9)  ! read in 10 values per line
!           write(*, *) ( VCL(i, 1), i = NNODE_read, NNODE_read+9)  ! read in 10 values per line
                  NNODE_read = NNODE_read+10
!           STOP
               else
                  read (line, *) (VCL(i, 1), i = NNODE_read, NNODE)  ! read in remaining values
!           print*, 'END'
!           write(*,*) ( VCL(i, 1), i = NNODE_read, NNODE)
                  NNODE_read = NNODE+1
               end if
            else
!         PRINT*,' Read Var 1 x',NNODE_read
               ParserState = PARSING_VAR2
               NNODE_read = 1
            end if
         end if

         if (ParserState .eq. PARSING_VAR2) then  ! must be in block format
            !read second variable block (y coord of node location)
            if (NNODE_read .le. NNODE) then
               if ((NNODE+1 - NNODE_read) .ge. 10) then
                  read (line, *) (VCL(i, 2), i = NNODE_read, NNODE_read+9)  ! read in 10 values per line
!           write(*, *) ( VCL(i, 2), i = NNODE_read, NNODE_read+9)  ! read in 10 values per line
                  NNODE_read = NNODE_read+10
!           STOP
               else
                  read (line, *) (VCL(i, 2), i = NNODE_read, NNODE)  ! read in remaining values
                  NNODE_read = NNODE+1
               end if
            else
!         PRINT*,' Read Var 2 y',NNODE_read
               ParserState = PARSING_VAR3
               NNODE_read = 1
            end if
         end if

         if (ParserState .eq. PARSING_VAR3) then  ! must be in block format
            !read third variable block (z coord of node location)
            if (NNODE_read .le. NNODE) then
               if ((NNODE+1 - NNODE_read) .ge. 10) then
                  read (line, *) (VCL(i, 3), i = NNODE_read, NNODE_read+9)  ! read in 10 values per line
                  NNODE_read = NNODE_read+10
               else
                  read (line, *) (VCL(i, 3), i = NNODE_read, NNODE)  ! read in remaining values
                  NNODE_read = NNODE+1
               end if
            else
               ! PRINT*,' Read Var 3 z',NNODE_read
               ParserState = PARSING_VAR4
               NNODE_read = 1
            end if
         end if

         ! If Mx is present, then My and Mz must be as well. In that case we only
         ! have to check for PARSING_VAR4 and skip to VAR7 if Mx is missing
         ! Otherwise also read My and Mz
         if (ParserState .eq. PARSING_VAR4) then  ! must be in block format
            if (.not. MX_HDR) then
               ParserState = PARSING_VAR7
            else
               !read mx variable block but dont keep it
               if (NNODE_read .le. NNODE) then
                  if ((NNODE+1 - NNODE_read) .ge. 10) then
                     read (line, *)  ! read in 10 values per line
                     NNODE_read = NNODE_read+10
                  else
                     read (line, *)  ! read in remaining values
                     NNODE_read = NNODE+1
                  end if
               else
                  ! PRINT*,' Read Var 4 Mx',NNODE_read
                  ParserState = PARSING_VAR5
                  NNODE_read = 1
               end if
            end if
         end if

         if (ParserState .eq. PARSING_VAR5) then  ! must be in block format
            !read my variable block but dont keep it

            if (NNODE_read .le. NNODE) then
               if ((NNODE+1 - NNODE_read) .ge. 10) then
                  read (line, *)  ! read in 10 values per line
                  NNODE_read = NNODE_read+10
               else
                  read (line, *)  ! read in remaining values
                  NNODE_read = NNODE+1
               end if
            else
               ! PRINT*,' Read Var 5 My',NNODE_read
               ParserState = PARSING_VAR6
               NNODE_read = 1
            end if
         end if

         if (ParserState .eq. PARSING_VAR6) then  ! must be in block format
            ! read mz variable block but dont keep it
            if (NNODE_read .le. NNODE) then
               if ((NNODE+1 - NNODE_read) .ge. 10) then
                  read (line, *)  ! read in 10 values per line
                  NNODE_read = NNODE_read+10
               else
                  read (line, *)  ! read in remaining values
                  NNODE_read = NNODE+1
               end if
            else
               ! PRINT*,' Read Var 6 Mz',NNODE_read
               ParserState = PARSING_VAR7
               ! NNODE_read = 1 last node variableread so dont need to set
            end if
         end if

         if (ParserState .eq. PARSING_VAR7) then  ! must be in block format
            ! read in the sub-domain numbers into TetSubDomains(i)

            if (NTRI_read .le. NTRI) then
               if ((NTRI+1 - NTRI_read) .ge. 10) then
                  read (line, *) (TetSubDomains(i), i = NTRI_read, NTRI_read+9)  ! read in 10 values per line
                  NTRI_read = NTRI_read+10
               else
                  read (line, *) (TetSubDomains(i), i = NTRI_read, NTRI)  ! read in remaining values
                  NTRI_read = NTRI+1
               end if
            else
               ! PRINT*,' Read Var s SD',NTRI_read
               ParserState = PARSING_ELEMENTS
               NTRI_read = 1
               ! NNODE_read = 1  ! last node read so dont need to set
            end if
         end if

         if (ParserState .eq. PARSING_NODES) then  ! must be in point format
            if (NNODE_read .le. NNODE) then
               read (line, *) &
                  VCL(NNODE_read, 1), VCL(NNODE_read, 2), VCL(NNODE_read, 3)
               NNODE_read = NNODE_read+1
            else
               ParserState = PARSING_ELEMENTS
            end if
         end if

         !
         ! PARSING_ELEMENTS
         !
         ! Parse tetrahedron connectivities
         !
         if (ParserState .eq. PARSING_ELEMENTS) then
            if (NTRI_read .le. NTRI) then
               read (line, *) &
                  TIL(NTRI_read, 1), TIL(NTRI_read, 2), &
                  TIL(NTRI_read, 3), TIL(NTRI_read, 4)
               TIL(NTRI_read, 5) = 0

               NTRI_read = NTRI_read+1
            else
               ParserState = PARSING_DONE
            end if
         end if

      end do  ! Loop over lines

      close (meshunit)

      call BuildMeshIO()

   contains
      subroutine DropInitialSeparators(line)
         character(len=*), intent(INOUT):: line

         integer:: i

         ! Drop separators from start of line
         do i = 1, len(line)

            if (scan(separators, line(i:i)) .eq. 0) then
!        PRINT*, 'IN separators',separators, i, line(:50)
               line = line(i:)
!        PRINT*, 'TRUE',separators, i, line(:50)
               exit
            end if
         end do
      end subroutine DropInitialSeparators

      subroutine Skip2quotes(line)
         character(len=*), intent(INOUT):: line

         integer:: i, q_count, j

         q_count = 0
!      print*, 'searching for quotes'
! is there a quote on the line ?
         j = index(line, '"')
!       print*,j, line(:20)
         if (j .ne. 0) then !  a quote must exist
            q_count = q_count+1
!       PRINT*, q_count, 'A QUOTE IS PRESENT', line(:20)
            line = line(j+1:)
!       PRINT*, 'New line for  QUOTE', line(:20)

         end if

         j = index(line, '"')
         if (j .ne. 0) then !  a quote must exist
            q_count = q_count+1
            !     PRINT*, q_count, 'A QUOTE IS PRESENT', line(:20)
            line = line(j+1:)
            !      PRINT*, 'New line for  QUOTE', line(:20)

         end if

!    line = line(scan:)
!       IF(line(1:1) .EQ. '"') THEN
!        quoting = .TRUE.
!        line = line(2:)
!      END IF
         ! Drop separators from start of line
!      DO i = 1, LEN(line)

!        IF(SCAN('"', line(i:i)) .EQ. 0) THEN
!        PRINT*, 'IN separators',separators, i, line(:50)
!          line = line(i:)
!        PRINT*, 'TRUE',separators, i, line(:50)
!          EXIT
!       END IF
!      END DO
      end subroutine Skip2quotes

      subroutine ParseToken(line, token)
         character(len=*), intent(INOUT):: line
         character(len=*), intent(OUT)   :: token

         integer:: i
         logical:: quoting

         call DropInitialSeparators(line)

         ! add characters up until first separator
         token = ""
         quoting = .false.

         ! Try parse quote
         if (line(1:1) .eq. '"') then
            quoting = .true.
            line = line(2:)
         end if

         do i = 1, len(line)
            ! Scan up to delimiter
            if ( &
               (scan(separators//"=", line(i:i)) .ne. 0) &
               .or. &
               (quoting .and. (line(i:i) .eq. '"')) &
               ) then
               token = line(:i-1)
               line = line(i:)
               if (quoting) line = line(2:)
               exit
            end if
         end do
      end subroutine ParseToken

      subroutine ParseEq(line, ierr)
         character(len=*), intent(INOUT):: line
         integer, intent(OUT):: ierr

         integer:: i

         call DropInitialSeparators(line)

         ierr = PARSE_ERROR
         do i = 1, len(line)
            if (line(i:i) .eq. "=") then
               line = line(i+1:)
               ierr = PARSE_SUCCESS
               exit
            end if
         end do

      end subroutine ParseEq

      subroutine ParseEqOrDie(line, token)
         character(len=*), intent(INOUT):: line
         character(len=*), intent(IN):: token

         integer:: ierr

         call ParseEq(line, ierr)
         if (ierr .ne. PARSE_SUCCESS) then
            write (*, *) "ERROR: Expected = after ", trim(token)
            ERROR stop
         end if
      end subroutine ParseEqOrDie

   end subroutine ReadTecplotMesh

   !---------------------------------------------------------------
   ! WriteMagForc
   !---------------------------------------------------------------

   subroutine WriteMagForc(clockcount, Bb, Br, Bmax, Bmin, Bstep)
      use Tetrahedral_Mesh_Data
      use Material_Parameters
      implicit none

      integer i
      integer:: dataunit
      integer(KIND = 8)  :: clockcount
      real(KIND = DP):: Br, Bb, Bmax, Bmin, Bstep

      open (NEWUNIT = dataunit, FILE = datafile, STATUS='unknown')
      write (dataunit, 3001) 'CLOCK=', clockcount  ! miliseconds since Jan 1, 1970
      !print*, 'clock', clockcount
      write (dataunit, 3002) 'Bb', 'Br', 'Bmax', 'Bmin', 'BStep'
      write (dataunit, 3003) Bb, Br, Bmax, Bmin, Bstep
      do i = 1, NNODE
         write (dataunit, 3000) &
            VCL(i, 1), VCL(i, 2), VCL(i, 3), &
            m(i, 1), m(i, 2), m(i, 3)
      end do
      close (dataunit)

3000  format(7(f10.6, ' '))
3001  format(A6, I26)
3002  format(5a14)
3003  format(5(f14.4, ' '))
   end subroutine WriteMagForc

   !---------------------------------------------------------------
   ! WriteMag writes out the magnetization (.dat) files
   !---------------------------------------------------------------

   subroutine WriteMag()
      use Tetrahedral_Mesh_Data
      use Material_Parameters
      implicit none

      integer i
      integer:: dataunit

      open (NEWUNIT = dataunit, FILE = datafile, STATUS='unknown')
      do i = 1, NNODE
         write (dataunit, 3000) &
            VCL(i, 1), VCL(i, 2), VCL(i, 3), &
            m(i, 1), m(i, 2), m(i, 3)
      end do
      close (dataunit)

3000  format(7(f10.6, ' '))
   end subroutine WriteMag

   !---------------------------------------------------------------
   ! WriteHyst writes out m.h_est against |h_ext|
   !---------------------------------------------------------------

   subroutine WriteHyst()
      use Tetrahedral_Mesh_Data
      use Material_Parameters
      use Finite_Element_Matrices
      implicit none

      integer:: i, l
      real(KIND = DP):: mhdot, mag_av(3), mag_max, mag_maxv, mag_v(3), InterpolationSum
      logical:: exist2
      integer:: hystunit

      inquire (FILE = hystfile, EXIST = exist2)

      open (NEWUNIT = hystunit, FILE = hystfile, STATUS='unknown', POSITION='append')

      mhdot = 0  ! Moment in field direction
      mag_v(:) = 0
      mag_maxv = 0

      do i = 1, NNODE

! calculate the average magnetisation per node over neighbouring elements which can be of
! different material
         mag_av(:) = 0
         mag_max = 0
         InterpolationSum = 0  ! The summation of the interpolation matrix for a given node
         do l = CNR_IM(i), CNR_IM(i+1) - 1
            mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
            mag_max = mag_max+Ms(SDNR_IM(l))*InterpolationMatrix(l)
            InterpolationSum = InterpolationSum+InterpolationMatrix(l)
         end do
         mag_maxv = mag_maxv+mag_max*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
         mag_v(:) = mag_v(:) + mag_av(:)*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
         mhdot = mhdot + (mag_av(1)*Hz(1) + mag_av(2)*Hz(2) + mag_av(3)*Hz(3))*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
      end do

      if (exist2 .eqv. .false.) then
         write (hystunit, *) "Total Saturated Magnetization (Am^2) = ", mag_maxv
         write (hystunit, '(7(A16, :,","))') "mu0.H (Tesla)", "m.h < hat> (Am^2)", &
            "m.h/Ms", "<Mx>/<Ms>", "<My>/<Ms>", "<Mz>/<Ms>"
      end if

      write (hystunit, "(6(ES16.8, :,','))") extapp, mhdot, mhdot/(mag_maxv), mag_v(:)/mag_maxv  ! csv format
      close (hystunit)

   end subroutine WriteHyst

   !---------------------------------------------------------------
   ! WriteStressHyst
   !---------------------------------------------------------------

   subroutine WriteStressHyst()
      use Tetrahedral_Mesh_Data
      use Material_Parameters
      use Finite_Element_Matrices
      implicit none

      integer:: i, l
      real(KIND = DP):: mhdot, mag_av(3), mag_max, mag_maxv, mag_v(3), InterpolationSum
      real(KIND = DP):: shdot, LamdaSAV, SigmaSAV, StressAxisAV(3), stressnormAV
      logical:: exist2
      integer:: stresshystunit

      inquire (FILE = stresshystfile, EXIST = exist2)

      open (NEWUNIT = stresshystunit, FILE = stresshystfile, STATUS='unknown', POSITION='append')

      mhdot = 0  ! Moment in field direction
      shdot = 0  ! Moment in the frtess field direction
      mag_v(:) = 0
      mag_maxv = 0
      StressAxisAv(:) = 0
      stressnormAV = 0.
      LamdaSAV = 0
      SigmaSAV = 0

      do i = 1, NNODE

! calculate the average magnetisation per node over neighbouring elements which can be of
! different material
         mag_av(:) = 0
         mag_max = 0
         StressAxisAv(:) = 0
         LamdaSAV = 0
         SigmaSAV = 0

         InterpolationSum = 0  ! The summation of the interpolation matrix for a given node
         do l = CNR_IM(i), CNR_IM(i+1) - 1
            StressAxisAv(:) = StressAxisAv(:) + StressAxis(:, SDNR_IM(l))*InterpolationMatrix(l)
            LamdaSAV = LamdaSAV+LamdaS(SDNR_IM(l))*InterpolationMatrix(l)
            SigmaSAV = SigmaSAV+SigmaS(SDNR_IM(l))*InterpolationMatrix(l)
            mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
            mag_max = mag_max+Ms(SDNR_IM(l))*InterpolationMatrix(l)

            InterpolationSum = InterpolationSum+InterpolationMatrix(l)
         end do
         stressnormAV = sqrt(StressAxisAV(1)**2+StressAxisAV(2)**2+StressAxisAV(3)**2)  ! dont need to divide by Interpolatesum since will normalise anyway
         StressAxisAV(:) = StressAxisAV(:)/stressnormAV
         LamdaSAV = LamdaSAV/InterpolationSum
         SigmaSAV = SigmaSAV/InterpolationSum
         mag_maxv = mag_maxv+mag_max*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
         mag_v(:) = mag_v(:) + mag_av(:)*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
         mhdot = mhdot + (mag_av(1)*Hz(1) + mag_av(2)*Hz(2) + mag_av(3)*Hz(3))*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
         shdot = shdot + (mag_av(1)*StressAxisAV(1) + mag_av(2)*StressAxisAV(2) &
                          + mag_av(3)*StressAxisAV(3))*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
      end do

      if (exist2 .eqv. .false.) then
         write (stresshystunit, *) "Total Saturated Magnetization (Am^2) = ", mag_maxv
         write (stresshystunit, *) "Average magnetostriction (dimensionless) = ", LamdaSAV
         write (stresshystunit, *) "Average compressive stress (N/m^2) = ", SigmaSAV
         write (stresshystunit, *) "Stress Axis = (x, y, z)", StressAxisAV(:)
         write (stresshystunit, '(7(A20, :,","))') "SigmaS (N/m)", "m.stress < hat> (Am^2)", &
            "m.(stress axis)/Ms", "<Mx>/<Ms>", "<My>/<Ms>", "<Mz>/<Ms>"
      end if

      write (stresshystunit, "(6(ES20.8, :,','))") SigmaSAV, shdot, shdot/(mag_maxv), mag_v(:)/mag_maxv  ! csv format
      close (stresshystunit)

   end subroutine WriteStressHyst

   !---------------------------------------------------------------
   ! WriteLoopData writes out field, magnetization, and
   ! other variables specified in a loop
   !---------------------------------------------------------------

   subroutine WriteLoopData(arg_str, arg_val_str)
      use Tetrahedral_Mesh_Data
      use Material_Parameters
      use Finite_Element_Matrices
!    USE Loop_Parser
      use Strings
      implicit none

      character(len=*), optional:: arg_str(:), arg_val_str(:)
      real(KIND = DP), dimension(:), allocatable:: arg_val
      integer:: i, l, nvals
      real(KIND = DP):: mhdot, mag_av(3), mag_max, mag_maxv, mag_v(3), InterpolationSum, Ms_av
      logical:: exist2
      integer:: loopunit
!    CHARACTER (LEN = 20) ::   vi, vd, vs, vsi, vsd

      if (present(arg_val_str)) then
         nvals = size(arg_val_str)
         allocate (arg_val(nvals))

         ! Define the input values
         do i = 1, nvals
            read (arg_val_str(i), *) arg_val(i)
            !vs='$'//varstr(:len_trim(varstr))//'$'
            !call repsubstr()
            !arg_vals(i) = c
            !WRITE(*,*) arg_val_str(i)

         end do
      end if

      inquire (FILE = loopfile, EXIST = exist2)

      open (NEWUNIT = loopunit, FILE = loopfile, STATUS='unknown', POSITION='append')

      if (exist2 .eqv. .false.) then
         if (present(arg_str)) then
            write (loopunit, "(*(A16, :,','))") "B (Tesla)", "Bx", "By", "Bz", &
               "<Mx> (Am^2)", "<My> (Am^2)", "<Mz> (Am^2)", "Ms (A/m)", "Volume (m^3)", arg_str(:)
         else
            write (loopunit, "(*(A16, :,','))") "B (Tesla)", "Bx", "By", "Bz", &
               "<Mx> (Am^2)", "<My> (Am^2)", "<Mz> (Am^2)", "Ms (A/m)", "Volume (m^3)"
         end if

      end if

      !mhdot   = 0
      mag_v(:) = 0  ! Net x, y, z moment
      mag_maxv = 0  ! Net total moment
      Ms_av = 0 !  Net Ms (divide by total nodes for average)

      do i = 1, NNODE

! calculate the average magnetisation per node over neighbouring elements which can be of
! different material
         mag_av(:) = 0  ! Net x, y, z magnetization
         mag_max = 0  ! Net magnetization
         InterpolationSum = 0  ! The summation of the interpolation matrix for a given node
         do l = CNR_IM(i), CNR_IM(i+1) - 1
            mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
            mag_max = mag_max+Ms(SDNR_IM(l))*InterpolationMatrix(l)
            InterpolationSum = InterpolationSum+InterpolationMatrix(l)
            Ms_av = Ms_av+Ms(SDNR_IM(l))
         end do
         mag_maxv = mag_maxv+mag_max*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
         mag_v(:) = mag_v(:) + mag_av(:)*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
      end do

      if (present(arg_val_str)) then
         write (loopunit, "(*(ES16.8, :,','))") extapp, Hz(1), Hz(2), Hz(3), &
            mag_v(:), Ms_av/NNODE, total_volume/sqrt(Ls)**3, arg_val(:)
      else
         write (loopunit, "(*(ES16.8, :,','))") extapp, Hz(1), Hz(2), Hz(3), &
            mag_v(:), Ms_av/NNODE, total_volume/sqrt(Ls)**3
      end if

      close (loopunit)

   end subroutine WriteLoopData

   !---------------------------------------------------------------
   ! WriteDemag
   !---------------------------------------------------------------

   subroutine WriteDemag(extapp2, stem, loopcount)
      use Utils, only: MachEps
      use Tetrahedral_Mesh_Data
      use Material_Parameters
      use strings, only: compact, writenum, lowercase
      use Finite_Element_Matrices
      implicit none

      real(KIND = DP), intent(IN):: extapp2
      integer, intent(IN):: loopcount
      character(LEN=*), intent(IN):: stem
!    CHARACTER(LEN = 5), INTENT(IN):: packing

      integer:: i, j, l
      character(LEN = 80) ::  Nstr, Estr
      character(LEN = 200):: head
      integer:: demagunit
      real(KIND = DP):: hd_x, hd_y, hd_z, Ms_max, InterpolationSum
      real(KIND = DP), allocatable:: MS_av(:)

      if (allocated(MS_av)) deallocate (MS_av)

      allocate (MS_av(nnode))

      call compact(zstr)
      if (len(trim(Zstr)) == 0) Zstr = " "

      if (abs(extapp2 - (-1.0)) < MachEps) then
         call writenum(zone, Zstr, '(F10.3)')
         zone = zone+zoneinc
!    else
!      call writenum(extapp2, Zstr, '(F10.3)')
      end if

      call writenum(NNODE, Nstr, '(I8)')
      call writenum(NTRI, Estr, '(I8)')
      head = 'ZONE T="'//trim(Zstr)//'",  N='//trim(Nstr)//',  E='//trim(Estr)

      ! Find max Ms
      Ms_max = maxval(Ms)

      Ms_av(:) = 0.
      do i = 1, NNODE

! calculate the average magnetisation per node over neighbouring elements which can be of
! different material

         InterpolationSum = 0  ! The summation of the interpolation matrix for a given node
         do l = CNR_IM(i), CNR_IM(i+1) - 1
            Ms_av(i) = MS_av(i) + Ms(SDNR_IM(l))*InterpolationMatrix(l)
            InterpolationSum = InterpolationSum+InterpolationMatrix(l)
         end do

         MS_av(i) = MS_av(i)/InterpolationSum
!     IF(MS_av(i).ne. 0.)  print*, "MS= ",MS_av(i)
      end do

      if (lowercase(packing) == 'block') then

         if (loopcount == 1) then

            open ( &
               NEWUNIT = demagunit, FILE = stem(:len_trim(stem))//"_demag.tec", &
               STATUS='unknown' &
               )
            write (demagunit, *) 'TITLE = ', '"'//stem(:len_trim(stem))//'"'
            write (demagunit, *) 'VARIABLES = "X","Y","Z","Hd_x","Hd_y","Hd_z","Bv_x",&
                      &"Bv_y","Bv_z","M_x","M_y","M_z","POT","SD"'
            write (demagunit, *) trim(head)//', F = FEBLOCK, ET = TETRAHEDRON, VARLOCATION=([14]=CELLCENTERED)'

            do j = 1, 3
               write (demagunit, '(10E16.7)') (VCL(i, j), i = 1, NNODE)
            end do

            do j = 1, 3
               write (demagunit, '(10E16.7)') (DemagH(i, j), i = 1, nnode)
            end do

            if (allocated(B_vect)) then
               do j = 1, 3
                  write (demagunit, '(10E16.7)') (B_vect(i, j), i = 1, nnode)
               end do
            else
               do j = 1, 3
                  write (demagunit, '(10E16.7)') (0.0, i = 1, nnode)
               end do
            end if

            do j = 1, 3
               write (demagunit, '(10E16.7)') (m(i, j)*Ms_av(i), i = 1, nnode)
            end do

            write (demagunit, '(10E16.7)') (totphi(i), i = 1, nnode)

            write (demagunit, '(10I7)') (SubDomainIds(TetSubDomains(i)), i = 1, NTRI)

            do i = 1, NTRI
               write (demagunit, 3002) TIL(i, 1), TIL(i, 2), TIL(i, 3), TIL(i, 4)
            end do

         end if

         if (loopcount > 1) then

            open ( &
               NEWUNIT = demagunit, FILE = stem(:len_trim(stem))//"_demag.tec", &
               STATUS='unknown', POSITION='APPEND' &
               )
            write (demagunit, *) trim(head)//', F = FEBLOCK, ET = TETRAHEDRON',&
              &', VARSHARELIST =([1-3, 14]=1), CONNECTIVITYSHAREZONE = 1,  VARLOCATION=([14]=CELLCENTERED)'
            do j = 1, 3
               write (demagunit, '(10E16.7)') (DemagH(i, j), i = 1, nnode)
            end do

            if (allocated(B_vect)) then
               do j = 1, 3
                  write (demagunit, '(10E16.7)') (B_vect(i, j), i = 1, nnode)
               end do
            else
               do j = 1, 3
                  write (demagunit, '(10E16.7)') (0.0, i = 1, nnode)
               end do
            end if

            do j = 1, 3
               write (demagunit, '(10E16.7)') (m(i, j)*Ms_av(i), i = 1, nnode)
            end do

            write (demagunit, '(10E16.7)') (totphi(i), i = 1, nnode)
         end if

      else  ! must be in POINT format

         if (loopcount == 1) then

            open ( &
               NEWUNIT = demagunit, FILE = stem(:len_trim(stem))//"_demag.tec", &
               STATUS='unknown' &
               )
            write (demagunit, *) 'TITLE = ', '"'//stem(:len_trim(stem))//'"'
            write (demagunit, *) 'VARIABLES = "X","Y","Z","Hd_x","Hd_y","Hd_z", &
               & "Bv_x","Bv_y","Bv_z","M_x","M_y","M_z"'
            write (demagunit, *) trim(head)//', F = FEPOINT, ET = TETRAHEDRON'

            if (allocated(B_vect)) then
               do i = 1, NNODE
                  write (demagunit, 3001) &
                     VCL(i, 1), VCL(i, 2), VCL(i, 3), DemagH(i, :), B_vect(I, :), m(i, :)*Ms_av(i)
               end do
            else
               do i = 1, NNODE
                  write (demagunit, 3001) &
                     VCL(i, 1), VCL(i, 2), VCL(i, 3), DemagH(i, :), 0.0, 0.0, 0.0, m(i, :)*Ms_av(i)
               end do

            end if

            do i = 1, NTRI
               write (demagunit, 3002) TIL(i, 1), TIL(i, 2), TIL(i, 3), TIL(i, 4)
            end do

         end if

         if (loopcount > 1) then

            open ( &
               NEWUNIT = demagunit, FILE = stem(:len_trim(stem))//"_demag.tec", &
               STATUS='unknown', POSITION='APPEND' &
               )
            write (demagunit, *) trim(head)//', F = FEPOINT, ET = TETRAHEDRON',&
              &', VARSHARELIST =([1-3]=1), CONNECTIVITYSHAREZONE = 1'

            if (allocated(B_vect)) then
               do i = 1, NNODE
                  write (demagunit, 3003) DemagH(i, :), B_vect(i, :), m(i, :)*Ms_av(i)
               end do
            else
               do i = 1, NNODE
                  write (demagunit, 3003) DemagH(i, :), 0, 0, 0, m(i, :)*Ms_av(i)
               end do
            end if

         end if

      end if

      close (demagunit)

      deallocate (MS_av)

3001  format(12(ES16.7))
3002  format(4(i7))
3003  format(9(ES16.7))
   end subroutine WriteDemag

   !---------------------------------------------------------------
   ! WriteVBox write out the volume data associated with
   !                                              each element
   !---------------------------------------------------------------

   subroutine WriteVBox()
      use Tetrahedral_Mesh_Data
      use Material_Parameters
      use Finite_Element_Matrices
      implicit none

      integer:: i, l
      real(KIND = DP):: mhdot, mag_av(3), mag_max
      logical:: exist2
      integer:: vboxunit
      character(150) OutputString1, OutputString2

      inquire (FILE = vboxfile, EXIST = exist2)

      open (NEWUNIT = vboxunit, FILE = vboxfile, STATUS='unknown')

      if (exist2 .eqv. .false.) then
         write (vboxunit, "(7(A15, :,','))") &
            "x", "y", "z", "mx", "my", "mz", "vbox"
      end if

      mhdot = 0
      mag_av = 0
      mag_max = 0
      do i = 1, NNODE
         !mhdot = mhdot + (m(i, 1)*Hz(1) + m(i, 2)*Hz(2) + m(i, 3)*Hz(3))*vbox(i)

         !DO l = CNR_IM(i), CNR_IM(i+1)-1
         !  mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
         !  mag_max   = mag_max   + Ms(SDNR_IM(l))*InterpolationMatrix(l)
         !END DO

         write (vboxunit, "(*(ES16.8, :,','))") &
            VCL(i, 1), VCL(i, 2), VCL(i, 3), &
            m(i, 1), m(i, 2), m(i, 3), vbox(i)

!        write(vboxunit, *) TRIM(OutputString1)

         ! write(vboxunit, "((7(ES16.7), i7.4), :,',')") &
         !     VCL(i, 1), VCL(i, 2), VCL(i, 3), &
         !     m(i, 1), m(i, 2), m(i, 3), vbox(i), TetSubDomains(i)

      end do

      close (vboxunit)

      !3004 FORMAT(7(ES16.7), i7.4)

   end subroutine WriteVBox

   !---------------------------------------------------------------
   ! ReadMagFORC
   !---------------------------------------------------------------

   subroutine ReadMagFORC(dfile, pass, ios)
      use Material_Parameters
      use Tetrahedral_Mesh_Data, only: NNODE, m
      implicit none

      integer i, NVAL
      real(KIND = DP):: Br, Bb, Bmax, Bmin, Bstep
      character(1):: CH, pCH
      character(1):: DELIM = " "

      real(KIND = DP):: x, y, z, mx, my, mz
      real(KIND = DP):: pass(5)
      character(LEN = 200) ::  dfile
      character(LEN = 6) ::  header
      integer:: dunit, ios
      integer*8:: clockcount

      !we first need to know if the data file is of the format
      ! x y x mx my mz    or just mx my mz
      ! count how many numbers there are in the first line
      open (NEWUNIT = dunit, FILE = dfile, STATUS='unknown')

      ! check to see if the file has a header
      read (dunit, 3001) header, clockcount
      read (dunit, *)
      read (dunit, *) (pass(i), i = 1, 5)  ! Bb, Br, Bmax, Bmin,  Bstep
      !print*, pass(1), pass(2), pass(3), pass(4), pass(5)

      ! Read first record one character at a time and count delimeters
      NVAL = 0
      pch = DELIM

      do
         ! eor = 10 jumps to label 10 on end of row
         read (dunit, '(A)', ADVANCE='no', EOR = 10) ch

         ! increment counter only if the current in not a space and
         ! previous character was a space
         if ((ch /= DELIM) .and. (pch == DELIM)) then
            NVAL = NVAL+1
         end if
         !  set previous character to current character
         pch = ch
      end do
      ! read command skips here on EOR
10    continue

      rewind (dunit)
      close (dunit)
      ! NVAL now contains the number of columns in the first line of a file

      write (*, *) 'The magnetization file contains', NVAL, 'columns'
      open (NEWUNIT = dunit, FILE = dfile, STATUS='unknown')
      ! skip the 3 line header
      read (dunit, *)
      read (dunit, *)
      read (dunit, *)

      select case (NVAL)
      case (3)
         do i = 1, NNODE
            read (dunit, *, IOSTAT = ios) mx, my, mz
            if (ios < 0) then
               goto 11

            end if
            m(i, 1) = mx
            m(i, 2) = my
            m(i, 3) = mz
         end do
11       if (ios < 0) write (*, *) 'The last-state file is incomplete'
         close (dunit)

      case (6)
         do i = 1, NNODE
            read (dunit, *, IOSTAT = ios) x, y, z, mx, my, mz
            if (ios < 0) then
               goto 12
            end if

            m(i, 1) = mx
            m(i, 2) = my
            m(i, 3) = mz
         end do
12       if (ios < 0) write (*, *) 'The last-state file is incomplete'
         close (dunit)

      case DEFAULT
         write (*, *) 'Error counting colums in file ', trim(dfile)
         close (dunit)
         stop

      end select
3000  format(7(f10.6, ' '))
3001  format(A6, I16)
3002  format(5a14)
3003  format(5(f14.4, ' '))
   end subroutine ReadMagFORC

   !---------------------------------------------------------------
   ! ReadMag
   !---------------------------------------------------------------

   subroutine ReadMag(dfile, ios)
      use Material_Parameters
      use Tetrahedral_Mesh_Data, only: nnode, m
      implicit none

      integer i, NVAL
      character(1):: CH, pCH
      character(1):: DELIM = " "

      real(KIND = DP):: x, y, z, mx, my, mz
      real(KIND = DP):: pass(5)
      character(LEN = 200) ::  dfile
      integer:: dunit, ios

      !we first need to know if the data file is of the format
      ! x y x mx my mz    or just mx my mz
      ! count how many numbers there are in the first line
      open (NEWUNIT = dunit, FILE = dfile, STATUS='unknown')

      ! Read first record one character at a time and count delimeters
      NVAL = 0
      pch = DELIM

      do
         ! eor = 10 jumps to label 10 on end of row
         read (dunit, '(A)', ADVANCE='no', EOR = 10) ch

         ! increment counter only if the current in not a space and
         ! previous character was a space
         if ((ch /= DELIM) .and. (pch == DELIM)) then
            NVAL = NVAL+1
         end if
         !  set previous character to current character
         pch = ch
      end do
      ! read command skips here on EOR
10    continue

      rewind (dunit)
      close (dunit)
      ! NVAL now contains the number of columns in the first line of a file

      write (*, *) 'The magnetization file contains', NVAL, 'columns'
      open (NEWUNIT = dunit, FILE = dfile, STATUS='unknown')

      select case (NVAL)
      case (3)
         do i = 1, NNODE
            read (dunit, *, IOSTAT = ios) mx, my, mz
            if (ios < 0) then
               goto 11

            end if
            m(i, 1) = mx
            m(i, 2) = my
            m(i, 3) = mz
         end do
11       if (ios < 0) write (*, *) 'The last-state file is incomplete'
         close (dunit)

      case (6)
         do i = 1, NNODE
            read (dunit, *, IOSTAT = ios) x, y, z, mx, my, mz
            if (ios < 0) then
               goto 12
            end if

            m(i, 1) = mx
            m(i, 2) = my
            m(i, 3) = mz
         end do
12       if (ios < 0) write (*, *) 'The last-state file is incomplete'
         close (dunit)

      case DEFAULT
         write (*, *) 'Error counting colums in file ', trim(dfile)
         close (dunit)
         stop

      end select

   end subroutine ReadMag

   !---------------------------------------------------------------
   ! WriteTecplot  writes out the TecPlot format
   !                                            for visualization
   !---------------------------------------------------------------

   subroutine WriteTecplot(extapp, stem, loopcount)
      use Utils, only: MachEps
      use Tetrahedral_Mesh_Data
      use strings

      implicit none

      real(KIND = DP), intent(IN):: extapp
      integer, intent(IN):: loopcount
      character(LEN=*), intent(IN):: stem

      integer:: i, j
      character(LEN = 80) ::  Nstr, Estr
      character(LEN = 200):: head
      integer:: multunit

!   check that Zstr (zonename) has a value. If empty set to " "
      call compact(Zstr)
      if (len(trim(Zstr)) == 0) Zstr = " "
      if (abs(extapp - (-1.0)) < MachEps) then
         call writenum(zone, Zstr, '(F10.3)')
         zone = zone+zoneinc
!    else
!      call writenum(extapp, Zstr, '(F10.3)')
      end if

      call writenum(NNODE, Nstr, '(I8)')
      call writenum(NTRI, Estr, '(I8)')
      head = 'ZONE T="'//trim(Zstr)//'",  N='//trim(Nstr)//',  E='//trim(Estr)
!  print*, 'packing format in tecplot = ', packing

      if (lowercase(packing) == 'block') then

!    ***WRITE BLOCK FORMAT**
         if (loopcount == 1) then

            open ( &
               NEWUNIT = multunit, FILE = stem(:len_trim(stem))//"_mult.tec", &
               STATUS='unknown' &
               )
            write (multunit, *) 'TITLE = ', '"'//stem(:len_trim(stem))//'"'
            write (multunit, *) 'VARIABLES = "X","Y","Z","Mx","My","Mz", "SD"'
            write (multunit, *) trim(head)
            write (multunit, *) 'F = FEBLOCK, ET = TETRAHEDRON, VARLOCATION=([7]=CELLCENTERED)'

            do j = 1, 3
               write (multunit, '(10E16.7)') (VCL(i, j), i = 1, NNODE)
            end do

            do j = 1, 3
               write (multunit, '(10E16.7)') (m(i, j), i = 1, nnode)
            end do

            write (multunit, '(10I7)') (SubDomainIDs(TetSubDomains(i)), i = 1, NTRI)

            do i = 1, NTRI
               write (multunit, 3002) TIL(i, 1), TIL(i, 2), TIL(i, 3), TIL(i, 4)
            end do

         end if

         if (loopcount > 1) then

            open ( &
               NEWUNIT = multunit, FILE = stem(:len_trim(stem))//"_mult.tec", &
               STATUS='unknown', POSITION='APPEND' &
               )
            write (multunit, *) trim(head)
            write (multunit, *) 'F = FEBLOCK, ET = TETRAHEDRON, VARSHARELIST =([1-3, 7]=1), &
            & CONNECTIVITYSHAREZONE = 1, VARLOCATION=([7]=CELLCENTERED)'

            do j = 1, 3
               write (multunit, '(10E16.7)') (m(i, j), i = 1, nnode)
            end do
         end if

      else  ! if not block thenmust be point format

!    **** WRITE POINT FORMAT ****
!  Note that point format cannot contain element variables
! so cannot contain subdomain identifiers

         if (loopcount == 1) then

            open ( &
               NEWUNIT = multunit, FILE = stem(:len_trim(stem))//"_mult.tec", &
               STATUS='unknown' &
               )
            write (multunit, *) 'TITLE = ', '"'//stem(:len_trim(stem))//'"'
            write (multunit, *) 'VARIABLES = "X","Y","Z","Mx","My","Mz"'
            write (multunit, *) trim(head)
            write (multunit, *) 'F = FEPOINT, ET = TETRAHEDRON'
            do i = 1, NNODE
               write (multunit, 3001) &
                  VCL(i, 1), VCL(i, 2), VCL(i, 3), &
                  m(i, 1), m(i, 2), m(i, 3)
            end do
            do i = 1, NTRI
               write (multunit, 3002) TIL(i, 1), TIL(i, 2), TIL(i, 3), TIL(i, 4)
            end do

         end if

         if (loopcount > 1) then

            open ( &
               NEWUNIT = multunit, FILE = stem(:len_trim(stem))//"_mult.tec", &
               STATUS='unknown', POSITION='APPEND' &
               )
!      WRITE(multUnit, *) 'ZONE T="',loopcount, '"N=',NNODE, ',E=',NTRI
            write (multunit, *) trim(head)
            write (multunit, *) 'F = FEPOINT, ET = TETRAHEDRON, VARSHARELIST =([1-3]=1), CONNECTIVITYSHAREZONE = 1'

            do i = 1, NNODE
               write (multunit, 3003) m(i, 1), m(i, 2), m(i, 3)
            end do
         end if

      end if

      close (multunit)

3001  format(6(ES16.7))
3002  format(4(i7))
3003  format(3(ES16.7))
   end subroutine WriteTecplot

   subroutine GenerateCubeMesh(width, edge_length)
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices
      use Material_Parameters
      implicit none

      real(KIND = DP), intent(IN):: width, edge_length
      integer:: nnodes_x
      real(KIND = DP):: actual_edge_length

      ! number of nodes in one dim
      ! Target nodes is width/edge_length with some geometric factor
      ! for the shape of tets in the cubic cells.
      nnodes_x = ceiling(width/(edge_length*0.91*sqrt(2.0d0))) + 1
      call BuildUnitCube(nnodes_x)
      VCL(:, 1:3) = VCL(:, 1:3)*width

      call BuildMeshIO()
   end subroutine GenerateCubeMesh

   subroutine GenerateSphereMesh(width, edge_length)
      use Tetrahedral_Mesh_Data, only: VCL, NNODE
      !USE Finite_Element_Matrices
      !USE Material_Parameters
      implicit none

      real(KIND = DP), intent(IN):: width, edge_length
      integer:: nnodes_x
      integer:: i
      real(KIND = DP):: x, y, z, xp, yp, zp, box_width, tt

      ! Start by building a 2*unit cube mesh

      ! Using a very nody cube for more "spherical" symmetry.
      ! A coarser tetrahedralization causes issues where corners
      ! get flattened against the side of the sphere.

      ! number of nodes in one dim
      ! nnodes_x is the number of corner nodes in one dim
      ! Using edge_length/SQRT(3) because our most abundant edge
      ! is along the cube diagonal.
      ! Using+1 to ensure at least 2x2x2 corner nodes needed to build a single
      ! cube.
      nnodes_x = ceiling(width/(edge_length*sqrt(3.0d0))) + 1

      call BuildUnitCube(nnodes_x)
      VCL(:, 1:3) = 2*VCL(:, 1:3)

      ! Transform vertices from the surface of a 2*unit cube to a unit sphere, 
      ! then rescale to the actual cube/sphere diameter at that point
      do i = 1, NNODE
         x = VCL(i, 1)
         y = VCL(i, 2)
         z = VCL(i, 3)

         box_width = max(abs(x), abs(y), abs(z))

         tt = sqrt(x**2 + y**2 + z**2)
         if (tt .gt. 0.0d0) then
            ! tt here is a value such that (x, y, z) + tt*(x, y, z) is on a sphere
            ! of radius box_width.
            tt = (box_width**2)/tt-1

            ! Scale tt to keep "boxiness" in the sphere, 
            ! 0 <= box_width <= 1
            ! I did this by accident, I have no idea why it works, but it
            ! works pretty nicely! (sphere mesh is complete crap without this)
            tt = tt*box_width
         else
            tt = 0
         end if

         xp = x+tt*x
         yp = y+tt*y
         zp = z+tt*z

         VCL(i, 1:3) = (/xp, yp, zp/)*width/2
      end do

      call BuildMeshIO()

   end subroutine GenerateSphereMesh

   subroutine BuildUnitCube(nnodes_x)
      use Tetrahedral_Mesh_Data, only: NNODE, NTRI, VCL, TIL, MeshAllocate

      integer, intent(IN):: nnodes_x
      integer, allocatable:: vertex_index(:, :, :, :)
      real(KIND = DP):: actual_edge_length
      integer:: ncubes

      integer:: i, j, k
      integer:: nTIL, nVCL

      actual_edge_length = 1.0/(nnodes_x-1)
      ncubes = (nnodes_x-1)**3

      ! Nodes are on each corner, on each face and at the center of each cube.
      NNODE = (nnodes_x)**3+3*(nnodes_x)*(nnodes_x-1)**2+ncubes
      NTRI = 24*ncubes
      call MeshAllocate(NNODE, NTRI)

      ! Storing vertex indices instead of computing them, because
      ! it was easier during development. I wasn't sure how best to
      ! keep track of the face indices. It's probably something simple.
      !
      ! vertex_index(:,:,:, 1) = corner index
      ! vertex_index(:,:,:, 2) = center index
      ! vertex_index(:,:,:, 3) = xy face index
      ! vertex_index(:,:,:, 4) = yz face index
      ! vertex_index(:,:,:, 5) = xz face index
      allocate (vertex_index(nnodes_x, nnodes_x, nnodes_x, 5))
      vertex_index = -1

      nVCL = 1
      nTIL = 1
      do k = 1, nnodes_x
      do j = 1, nnodes_x
      do i = 1, nnodes_x
         ! Build node positions
         ! Always build the*next*node, a center node and the*current*cube
         ! Start loop on ghost cube, to ensure the previous nodes are always built
         vertex_index(i, j, k, 1) = nVCL
         nVCL = nVCL+1
         VCL(vertex_index(i, j, k, 1), 1:3) = &
            (/(i-1), (j-1), (k-1)/)*actual_edge_length &
            - 1.0/2

         ! k face
         if (i .ne. 1 .and. j .ne. 1) then
            vertex_index(i, j, k, 3) = nVCL
            nVCL = nVCL+1
            VCL(vertex_index(i, j, k, 3), 1:3) = &
               VCL(vertex_index(i, j, k, 1), 1:3) &
               - (/0.5, 0.5, 0.0/)*actual_edge_length
         end if

         ! i face
         if (j .ne. 1 .and. k .ne. 1) then
            vertex_index(i, j, k, 4) = nVCL
            nVCL = nVCL+1
            VCL(vertex_index(i, j, k, 4), 1:3) = &
               VCL(vertex_index(i, j, k, 1), 1:3) &
               - (/0.0, 0.5, 0.5/)*actual_edge_length
         end if

         ! j face
         if (i .ne. 1 .and. k .ne. 1) then
            vertex_index(i, j, k, 5) = nVCL
            nVCL = nVCL+1
            VCL(vertex_index(i, j, k, 5), 1:3) = &
               VCL(vertex_index(i, j, k, 1), 1:3) &
               - (/0.5, 0.0, 0.5/)*actual_edge_length
         end if

         ! Not building ghost cube, build actual cube
         if (i .ne. 1 .and. j .ne. 1 .and. k .ne. 1) then

            ! Add central vertex
            vertex_index(i, j, k, 2) = nVCL
            nVCL = nVCL+1
            VCL(vertex_index(i, j, k, 2), 1:3) = ( &
                                                 VCL(vertex_index(i, j, k, 1), 1:3) &
                                                 + VCL(vertex_index(i-1, j-1, k-1, 1), 1:3) &
                                                 )/2

            ! Build tets

            ! Back Face k = k-1
            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k-1, 3), &
                             vertex_index(i-1, j-1, k-1, 1), &
                             vertex_index(i, j-1, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k-1, 3), &
                             vertex_index(i, j-1, k-1, 1), &
                             vertex_index(i, j, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k-1, 3), &
                             vertex_index(i, j, k-1, 1), &
                             vertex_index(i-1, j, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k-1, 3), &
                             vertex_index(i-1, j, k-1, 1), &
                             vertex_index(i-1, j-1, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            ! Front Face k = k
            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 3), &
                             vertex_index(i, j-1, k, 1), &
                             vertex_index(i-1, j-1, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 3), &
                             vertex_index(i-1, j-1, k, 1), &
                             vertex_index(i-1, j, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 3), &
                             vertex_index(i-1, j, k, 1), &
                             vertex_index(i, j, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 3), &
                             vertex_index(i, j, k, 1), &
                             vertex_index(i, j-1, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            ! Left Face i = i-1
            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i-1, j, k, 4), &
                             vertex_index(i-1, j-1, k, 1), &
                             vertex_index(i-1, j-1, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i-1, j, k, 4), &
                             vertex_index(i-1, j-1, k-1, 1), &
                             vertex_index(i-1, j, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i-1, j, k, 4), &
                             vertex_index(i-1, j, k-1, 1), &
                             vertex_index(i-1, j, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i-1, j, k, 4), &
                             vertex_index(i-1, j, k, 1), &
                             vertex_index(i-1, j-1, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            ! Right Face i = i
            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 4), &
                             vertex_index(i, j-1, k-1, 1), &
                             vertex_index(i, j-1, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 4), &
                             vertex_index(i, j-1, k, 1), &
                             vertex_index(i, j, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 4), &
                             vertex_index(i, j, k, 1), &
                             vertex_index(i, j, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 4), &
                             vertex_index(i, j, k-1, 1), &
                             vertex_index(i, j-1, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            ! Top Face j = j-1
            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j-1, k, 5), &
                             vertex_index(i-1, j-1, k-1, 1), &
                             vertex_index(i-1, j-1, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j-1, k, 5), &
                             vertex_index(i-1, j-1, k, 1), &
                             vertex_index(i, j-1, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j-1, k, 5), &
                             vertex_index(i, j-1, k, 1), &
                             vertex_index(i, j-1, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j-1, k, 5), &
                             vertex_index(i, j-1, k-1, 1), &
                             vertex_index(i-1, j-1, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            ! Bottom Face j = j
            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 5), &
                             vertex_index(i-1, j, k-1, 1), &
                             vertex_index(i, j, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 5), &
                             vertex_index(i, j, k-1, 1), &
                             vertex_index(i, j, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 5), &
                             vertex_index(i, j, k, 1), &
                             vertex_index(i-1, j, k, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1

            TIL(nTIL, 1:4) = (/ &
                             vertex_index(i, j, k, 5), &
                             vertex_index(i-1, j, k, 1), &
                             vertex_index(i-1, j, k-1, 1), &
                             vertex_index(i, j, k, 2) &
                             /)
            nTIL = nTIL+1
         end if
      end do
      end do
      end do
   end subroutine BuildUnitCube

   !---------------------------------------------------------------
   !        ReadTecplotPath
   !---------------------------------------------------------------

   !> Read the multi-zone tecplot file `PathInFile` into the path variables.
   !> @todo DOCUMENT ME
   subroutine ReadTecplotPath()
      use Finite_Element_Matrices
      use Material_Parameters
      use Magnetization_Path
      use Tetrahedral_Mesh_Data, only: NNODE, NTRI
      use strings, only: lowercase

      implicit none
      integer i, pn, zonecnt, linecnt, ios, nodecnt, tetcnt, was4, dstop, result, RESULTF

      character(LEN = 200) ::  zonetest  ! read in first part of the line that should contain the word ZONE
      character(LEN = 300) ::  line
      character(LEN = 5):: pack  ! contains the file packing format

      double precision mnorm

      double precision dummy1, dummy2, dummy3
      integer:: dtil1, dtil2, dtil3, dtil4, nod, e1, e2, e3, e4, j

      integer:: PathInUnit

      open (NEWUNIT = PathInUnit, file = PathInFile, status='unknown')
      zonecnt = 0
      linecnt = 0

! search for the
! search for the word 'TETRAHEDRON' in each line of text read in
! since this always occurs in the last line before data
      i = 0
      result = 0
      RESULTF = 0
      nodecnt = 0
      tetcnt = 0
      was4 = 0
      dstop = 0
      ios = 0

      ! count the number of zones in a file
      do while (ios .ge. 0)
         linecnt = linecnt+1
         read (PathInUnit, IOSTAT = ios, FMT='(A200)') zonetest
!       print*,'line = ',zonetest
         if (ios /= 0) exit  ! needed to prevent read past EOF
         result = index(zonetest, 'TETRAHEDRON')
         if (result /= 0) zonecnt = zonecnt+1
      end do
      if (zonecnt < 2) then
         if (zonecnt == 0) then
            write (*, *) ' File ', PathInFile, ' contains no ZONE. '
         else
            write (*, *) ' File ', PathInFile, ' contains only one ZONE. '
         end if
         stop
         return
      end if

      PathN = zonecnt
      write (*, *) ' File ', PathInFile(:len_trim(PathInFile)), ' contains  ', PathN, ' ZONEs'
      rewind (PathInUnit)

! search for the word 'TETRAHEDRON' in each line of text read in
! since this always occurs in the last line before data
      i = 0
      result = 0
      do while (result .eq. 0)
         i = i+1
         read (PathInUnit, FMT='(A)') line
!      write(*,*) 'reading line',i, line  ! for testing only
         result = index(line, 'TETRAHEDRON')
      end do

! this next section counts the numbers of nodes and elements-HOWEVER-it can be sued to check that
! they mesh file you are using matches the same NODE and ELEMENT number as your data

!    ios = 0
!    DO WHILE((ios .GE. 0).AND.(dstop == 0))
!       READ(PathInUnit, IOSTAT = ios, FMT='(A)') line   !Read line
!       if (ios /= 0) exit  ! needed to prevent read past EOF
!       num = 0  ! it follows Fortran magic to count columns (num) in the input line
!       spc = 1  ! last char was space(or other char < 32)
!       Do i = 1, LEN(line)
!          select case(iachar(line(i:i)))
!          case(0:32)
!             spc = 1  ! last char was space(or other char < 32)
!          case(33:)
!             if(spc == 1) num = num+1  ! if previous char was space : increase column count
!             spc = 0
!          end select
!       End DO
!       if(num == 6) then
!          if(was4 == 0) then
!             nodecnt = nodecnt+1  ! 6 columns with no preceding 4 column line => one more node
!          else
!             dstop = 1   ! stop when first 6 column line occurs after a 4 column line
!          endif
!       else
!          if(num == 4) then       ! 4 columns   => one more tetrahedron
!             was4 = 1
!             tetcnt = tetcnt+1
!          else
!             dstop = 1  !stop if unexpected column number occurs (should never happen)
!          endif
!       endif
!    END DO

!    if (NNODE /= nodecnt .OR. NTRI /= tetcnt) then
!       write (*,*) 'STOPPING-mesh file size does not match your path data'
!       STOP
!       endif

      rewind (PathInUnit)

! rewind file an read in the magnetization states along path
! fist kip the header lines in file
! search for the word 'TETRAHEDRON' in each line of text read in
! since this always occurs in the last line before data

! read in the mesh data from tecplot file
!    REWIND(PathInUnit)
      close (PathInUnit)
      call ReadTecplotMesh(PathInFile)  !mm maybe wont wrk with file already open
!    REWIND(PathInUnit)  ! just in case

      open (NEWUNIT = PathInUnit, file = PathInFile, status='unknown')

      write (*, *) ' NNODE/NTRI :', NNODE, ' / ', NTRI

      call PathAllocate()
      write (*, *) ' Path allocated', PathN
      !     Reading in the first structure together with mesh

      i = 0
      result = 0
      RESULTF = 0
      pack = 'NoSet'
      do while (result .eq. 0)
         i = i+1
         read (PathInUnit, FMT='(A)') line
         RESULTF = index(line, "F=")
         if (RESULTF .ne. 0) then  ! read in format type
            read (line((RESULTF+4):), *) pack
!        print*,' ---->Data packing format in this file = ',pack
         end if
!      write(*,*) 'reading line',i, line  ! for testing only
         result = index(line, 'TETRAHEDRON')
      end do

      if (lowercase(pack) .eq. 'point') then
         call ReadPointpath()
      else if (lowercase(pack) .eq. 'block') then
         call ReadBlockPath()
      else
         write (*, *) ' STOPPING-cannot determine Pathfile format'
         stop
      end if

      close (PathInUnit)
      call PathRenewDist()

      write (*, *) 'TecPlot path read. Make sure material parameters are correctly assigned !'

   contains

      subroutine ReadPointpath()
         use Tetrahedral_Mesh_Data, only: m

         pn = 1

         do i = 1, NNODE
            !    print*, 'reading first node ',i
            read (PathInUnit, *) dummy1, dummy2, dummy3, PMag(pn, i, 1), PMag(pn, i, 2), PMag(pn, i, 3)

            mnorm = sqrt(PMag(pn, i, 1)**2+PMag(pn, i, 2)**2+PMag(pn, i, 3)**2)
            !         print*,'norm=',mnorm
            PMag(pn, i, 1) = PMag(pn, i, 1)/mnorm
            PMag(pn, i, 2) = PMag(pn, i, 2)/mnorm
            PMag(pn, i, 3) = PMag(pn, i, 3)/mnorm
            m(i, :) = PMag(pn, i, :)
         end do

         do i = 1, NTRI
            read (PathInUnit, *) dtil1, dtil2, dtil3, dtil4
         end do
!    print*, 'Read elements'
         !CALL BuildTetrahedralMeshData()
         !CALL BuildFiniteElement()
         !CALL BuildMaterialParameters()
         !CALL BuildEnergyCalculator()

         do pn = 2, PathN
            read (PathInUnit, *)
            read (PathInUnit, *)
            do i = 1, NNODE
               read (PathInUnit, *) PMag(pn, i, 1), PMag(pn, i, 2), PMag(pn, i, 3)
               mnorm = sqrt(PMag(pn, i, 1)**2+PMag(pn, i, 2)**2+PMag(pn, i, 3)**2)
               PMag(pn, i, 1) = PMag(pn, i, 1)/mnorm
               PMag(pn, i, 2) = PMag(pn, i, 2)/mnorm
               PMag(pn, i, 3) = PMag(pn, i, 3)/mnorm
            end do
!       print*, 'read nodes for zone',pn
         end do
      end subroutine ReadPointpath

      subroutine ReadBlockPath()
         use Tetrahedral_Mesh_Data, only: m
         pn = 1

         !     write(*,*) 'Reading Zone',pn
         ! skip the first three variables which are the mesh node locations, since there are
         !  10 values per line these will occupy 3*(int(NNODE/10) + 1) lines
         !   do i = 1, 3*(INT((NNODE-1)/10)+1)

         do i = 1, 3*ceiling(real(NNODE)/10.)
            read (PathInUnit, *)
         end do

         ! now read in the magnetization values  for mx
         if (NNODE .ge. 10) then
            do j = 1, (NNODE/10)
               nod = (j-1)*10+1
               read (PathInUnit, *) (PMag(pn, i, 1), i = nod, nod+9)

            end do
         end if
         if (mod(NNODE, 10) .ne. 0) then
            nod = (NNODE/10)*10+1
            read (PathInUnit, *) (PMag(pn, i, 1), i = nod, nnode)   ! read remainin numbers
         end if

         ! now read in the magnetization values  for my
         if (NNODE .ge. 10) then
            do j = 1, (NNODE/10)
               nod = (j-1)*10+1
               read (PathInUnit, *) (PMag(pn, i, 2), i = nod, nod+9)
            end do
         end if
         if (mod(NNODE, 10) .ne. 0) then
            nod = (NNODE/10)*10+1
            read (PathInUnit, *) (PMag(pn, i, 2), i = nod, nnode)   ! read remainin numbers
         end if

         ! now read in the magnetization values  for mz
         if (NNODE .ge. 10) then
            do j = 1, (NNODE/10)
               nod = (j-1)*10+1
               read (PathInUnit, *) (PMag(pn, i, 3), i = nod, nod+9)
            end do
         end if
         if (mod(NNODE, 10) .ne. 0) then
            nod = (NNODE/10)*10+1
            read (PathInUnit, *) (PMag(pn, i, 3), i = nod, nnode)   ! read remainin numbers
         end if

         ! ensure unit normalisation
         do i = 1, NNODE
            mnorm = sqrt(PMag(pn, i, 1)**2+PMag(pn, i, 2)**2+PMag(pn, i, 3)**2)
            PMag(pn, i, 1) = PMag(pn, i, 1)/mnorm
            PMag(pn, i, 2) = PMag(pn, i, 2)/mnorm
            PMag(pn, i, 3) = PMag(pn, i, 3)/mnorm
            m(i, :) = PMag(pn, i, :)
         end do

         do i = 1, ceiling(real(NTRI)/10.)
            read (PathInUnit, *)  ! read subdomain number and throw away
         end do

         do i = 1, NTRI
            read (PathInUnit, *) e1, e2, e3, e4  ! read elements and throw away
         end do

         do pn = 2, zonecnt

            read (PathInUnit, *)
            read (PathInUnit, *)

!          write(*,*) 'Reading Zone',pn

            ! now read in the magnetization values  for mx
            if (NNODE .ge. 10) then
               do j = 1, (NNODE/10)
                  nod = (j-1)*10+1
                  read (PathInUnit, *) (PMag(pn, i, 1), i = nod, nod+9)
               end do
            end if
            if (mod(NNODE, 10) .ne. 0) then
               nod = (NNODE/10)*10+1
               read (PathInUnit, *) (PMag(pn, i, 1), i = nod, nnode)   ! read remainin numbers
            end if

            ! now read in the magnetization values  for my
            if (NNODE .ge. 10) then
               do j = 1, (NNODE/10)
                  nod = (j-1)*10+1
                  read (PathInUnit, *) (PMag(pn, i, 2), i = nod, nod+9)
               end do
            end if
            if (mod(NNODE, 10) .ne. 0) then
               nod = (NNODE/10)*10+1
               read (PathInUnit, *) (PMag(pn, i, 2), i = nod, nnode)   ! read remainin numbers
            end if

            ! now read in the magnetization values  for mz
            if (NNODE .ge. 10) then
               do j = 1, (NNODE/10)
                  nod = (j-1)*10+1
                  read (PathInUnit, *) (PMag(pn, i, 3), i = nod, nod+9)
               end do
            end if
            if (mod(NNODE, 10) .ne. 0) then
               nod = (NNODE/10)*10+1
               read (PathInUnit, *) (PMag(pn, i, 3), i = nod, nnode)   ! read remainin numbers
            end if

            ! ensure unit normalisation
            do i = 1, NNODE
               mnorm = sqrt(PMag(pn, i, 1)**2+PMag(pn, i, 2)**2+PMag(pn, i, 3)**2)
               PMag(pn, i, 1) = PMag(pn, i, 1)/mnorm
               PMag(pn, i, 2) = PMag(pn, i, 2)/mnorm
               PMag(pn, i, 3) = PMag(pn, i, 3)/mnorm
            end do

         end do

      end subroutine ReadBlockPath

   end subroutine ReadTecplotPath

   !---------------------------------------------------------------
   ! ReadLoopData
   !---------------------------------------------------------------
   !> Reads data from *.loop file
   !> This version only reads the Field and 1 additonal column (Br)
   !> This is for SimpleFORCRestart only
   !> loopfile-name of file to be read
   !> DataVals-array of data from the loop file
   !> Header-vector of strings in the header line
   !>
   subroutine ReadLoopData(loopfile, DataVals, Header)

      use Strings, only: parse, value
      implicit none

      character(len=*), intent(IN):: loopfile!, cols(:)
      real(KIND = DP), intent(OUT), dimension(:, :), allocatable:: DataVals
      character(len = 100), intent(OUT), dimension(:), allocatable:: Header
      !INTEGER, INTENT(OUT):: ierr
      character(len = 1024):: line
      character(len = 200):: str_args(20)
      integer:: loopunit, nData, ios, nVars, nargs, i, j
      logical:: exist2

      inquire (FILE = loopfile, EXIST = exist2)

      if (exist2 .eqv. .false.) then

         ! crap out and return
         write (*, '(A30 A50)') 'Loop file does not exisit: ', loopfile
         stop

      else

         write (*, '(A20 A50)') 'Reading loop file: ', loopfile
         ! Read the file
         open (NEWUNIT = loopunit, FILE = loopfile, STATUS='old')

         ! Read the data lines (start at-1 to exclude header)
         nData = -1

         do
            ! eor = 10 jumps to label 10 on end of row
            read (loopunit, '(A)', iostat = ios) line

            if (ios .ne. 0) then
               exit
            end if

            ! increment counter
            nData = nData+1
            !WRITE(*,*) NData, ios, line

            ! Get the number of variable and the headers
            if (nData .eq. 0) then
               call parse(line, ',', str_args, nVars)

               allocate (Header(nVars))

               do i = 1, nVars
                  Header(i) = str_args(i)
               end do

            end if

         end do

         rewind (loopunit)

         write (*, '(A, I8, A, I2, A)') 'The loop file contains', nData, ' data rows, with ', nVars, ' variables'

         allocate (DataVals(nData, nVars))

         ! Re-read the file and extract the data

         do i = 0, nData
            read (loopunit, '(A)', iostat = ios) line

            if (ios .ne. 0) then
               exit
            end if

            if (i .ge. 1) then
               call parse(line, ',', str_args, nargs)

               if (nVars .ne. nargs) then
                  ! More crapping out and returning
                  write (*, *) 'Loop file data format is not self-consisitent for file: ', loopfile
                  stop

               else

                  do j = 1, nVars
                     call value(str_args(j), DataVals(i, j), ios)
                  end do

               end if

            end if  ! i >= 1

         end do

         close (loopunit)

         !TESTING
         ! WRITE(*,*) "file = ", loopfile
         ! WRITE(*,*) 'Header'
         ! WRITE(*,*) Header
         ! WRITE(*,*)
         ! WRITE(*,*) 'first three rows of variable 1 (i.e. H)'
         ! WRITE(*,*) DataVals(1, 1), DataVals(2, 1), DataVals(3, 1)
         ! WRITE(*,*)
         ! WRITE(*,*) 'first row of variable 1, 2, 3 & 4 (i.e. H, hx, hy, hz)'
         ! WRITE(*,*) DataVals(1, 1), DataVals(1, 2), DataVals(1, 3), DataVals(1, 4)
         ! WRITE(*,*)
         ! WRITE(*,*) 'Last row of variable 1, 2, 3 & 4 (i.e. H, hx, hy, hz)'
         ! WRITE(*,*) DataVals(nData, 1), DataVals(nData, 2), DataVals(nData, 3), DataVals(nData, 4)

      end if

   end subroutine ReadLoopData

end module Mesh_IO
