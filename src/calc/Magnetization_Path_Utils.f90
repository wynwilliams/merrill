!> Generic procedures used by Magnetization_Path and others
module Magnetization_Path_Utils
   use Utils, only: DP
   implicit none

contains 
   
   !> Calculates the norm of \c m1.
   !>
   !> Calculates the norm, \f$ d \f$ of the vector \f$ m_1 \f$ as
   !> \f[
   !>    d = \sqrt{ \langle m_1 | m_1 \rangle }
   !> \f]
   !> with the scalar product
   !> \f[
   !>    \langle m_1 | m_2 \rangle  = \frac{1}{V}  \int m_1 \cdot m_2 dV
   !> \f]
   !>
   !> @param[in] m1 The magnetization \f$ m_1 \f$.
   !> @returns The average magnetization over the volume.
   double precision function MagNorm(m1)
      use Tetrahedral_Mesh_Data, only: NNODE, total_volume, vbox
      implicit none

      integer i
      double precision m1(NNODE, 3)
      double precision sp, spsum

      spsum = 0.

      do i = 1, NNODE
         sp = m1(i, 1)*m1(i, 1) + m1(i, 2)*m1(i, 2) + m1(i, 3)*m1(i, 3)
         spsum = spsum+sp*vbox(i)
      end do
      MagNorm = sqrt(spsum/total_volume)
   end function MagNorm


   !> Calculates the vector distance between two magnetization states \f$m_1\f$
   !> \f$m_2\f$.
   !>
   !> The distance, \f$ d \f$, is calculated as
   !> \f[
   !>    d = \sqrt{ \langle m_1-m_2 | m_1-m_2 \rangle }
   !> \f]
   !> with the scalar product
   !> \f[
   !>    \langle m_1 | m_2 \rangle  = \frac{1}{V}  \int m_1 \cdot m_2\ dV
   !> \f]
   !> assuming  \f$ ||m_1|| = ||m_2|| = 1 \f$ pointwise, 
   !> yielding \f$ ||m_1-m_2||  =  \sqrt{ 2-2 \langle m_1 | m_2 \rangle } \f$
   !>
   !> @param[in] m1 The variable \f$m_1\f$.
   !> @param[in] m2 The variable \f$m_2\f$.
   !> @returns The distance \f$ \sqrt{ \langle m_1-m_2 | m_1-m_2 \rangle } \f$
   double precision function Distance(m1, m2)
      use Tetrahedral_Mesh_Data, only: NNODE
      implicit none

      double precision m1(NNODE, 3), m2(NNODE, 3), mm(NNODE, 3)

      mm(:, :) = m1(:, :) - m2(:, :)
      Distance = MagNorm(mm)
   end function Distance

   !> Calculates the scalar product
   !> \f[  \langle m_1 | m_2 \rangle  = \frac{1}{V}  \int m_1 \cdot m_2\ dV \f]
   !>
   !> @param[in] m1 The \f$m_1\f$ variable.
   !> @param[in] m2 The \f$m_2\f$ variable.
   !> @returns The scalar product \f$ \langle m_1 | m_2 \rangle \f$.
   double precision function ScalarProd(m1, m2)
      use Tetrahedral_Mesh_Data, only: NNODE, total_volume, vbox
      implicit none

      integer i
      double precision m1(NNODE, 3), m2(NNODE, 3)
      double precision sp, spsum

      spsum = 0.
      do i = 1, NNODE
         sp = m1(i, 1)*m2(i, 1) + m1(i, 2)*m2(i, 2) + m1(i, 3)*m2(i, 3)
         spsum = spsum+sp*vbox(i)
      end do
      ScalarProd = spsum/total_volume
   end function ScalarProd

end module
