!> Highest-level loop over HubertMinimizer, either along one axis or switching
module Minimizing_Loops
   use Utils, only: DP
   implicit none

   !> The energy log file name
   character(LEN = 1024):: logfile

   !> The unit for the energy log file
   ! ww set logunit to zero removed the = 0
   integer:: logunit

   !> If .TRUE., then run all the exchange calculators.
   logical:: CalcAllExchQ

contains

   !---------------------------------------------------------------
   !  TOP LEVEL ENERGY MINIMIZATION ROUTINE
   !---------------------------------------------------------------

   !----------------------------------------------------------------
   !     SPHERICAL POLARS along multi-orthogonal axis with the
   !     Polar angle along Z and Y on successive restarts
   !----------------------------------------------------------------

   !> Find the value of `M` which is a Local Energy Minimum for `E(M)`.
   !> Looping while switching between PolarZmin, PolarXmin, PolarXmin CartMinMult calls
   subroutine EnergyMinMult()

      use Material_Parameters, only: EnergyUnit, Kd, Ls, MaxRestarts, PathCoord
      use Tetrahedral_Mesh_Data, only: NNODE, total_volume
      !USE Finite_Element_Matrices, ONLY:
      !USE Hubert_Minimizer, ONLY:

      implicit none

      integer:: i, neval
      real(KIND = DP):: etot, Eold, Ediff
      !    REAL(KIND = DP):: XGUESS(2*NNODE), XX(2*NNODE), GRAD(2*NNODE)
      integer:: maxvar, restartcount

      ! SPECIFICATION of COORD SYSTEM
      character*5:: coord, opt_out
      character*1:: picker

      coord = "noset" ! will later be set to either 'xyz' or 'polar'
      opt_out = 'noset'
      ! set initial axis for minimization
      picker = "C"

      maxvar = 2*NNODE
      neval = 0
      EnergyUnit = Kd*total_volume/(Ls**(3.0/2.0))

      Eold = 999.
      Ediff = 999.
      restartcount = 0
      opt_out = "force"  !initial set to 'force' to force initial optimisation

      do while (abs(ediff) > 1.d-8 .and. restartcount .lt. MaxRestarts)
         print *, 'RESTART = ', restartcount
         restartcount = restartcount+1
         !      opt_out="force"  !initial set to 'force' to force initial optimisation
         if (restartcount .eq. MaxRestarts) print *, 'FINAL ENERGY ITERATION'
         do while (opt_out .eq. "force")
            if (pathcoord .eq. "Conly") picker = "C"
            !         print*, '1', opt_out
            if (opt_out .eq. 'force' .and. picker .eq. "Z") then
               coord = 'mult' !to enforce swap to next coord in Hubert_minimizer
               call PolarMin(picker, neval, etot, coord, opt_out)
               if (picker .ne. 'F') picker = "X" !TODO make an enum for next picker
            end if

            !         print*, '2', opt_out
            if (opt_out .eq. 'force' .and. picker .eq. "X") then
               coord = 'mult' !to enforce swap to next coord in Hubert_minimizer
               call PolarMin(picker, neval, etot, coord, opt_out)
               if (picker .ne. 'F') picker = "Y"
            end if

            !         print*, '3', opt_out
            if (opt_out .eq. 'force' .and. picker .eq. "Y") then
               coord = 'mult' !to enforce swap to next coord in Hubert_minimizer
               call PolarMin(picker, neval, etot, coord, opt_out)
               if (picker .ne. 'F') picker = "C"
            end if

            !          print*, '4', opt_out
            if (opt_out .eq. 'force' .and. (picker .eq. "C" .or. picker .eq. 'F')) then
               coord = 'xyz' !to enforce swap to next coord in Hubert_minimizer
               call CartminMult(neval, etot, coord, opt_out)
               picker = "Z"
            end if

            !         print*, '5', opt_out
            !         call Cartmin(neval, etot, coord, opt_out)
            ediff = Etot-Eold
            Eold = Etot

         end do

         if (opt_out .ne. 'force') print *, 'ediff = ', ediff

         !  NO RESTARTS NEEDESD SINCE METHOD INVOLVES FORCED RESTARTS ANYWAY-but
         ! to ediff check anyway

         !        if (MOD(restartcount, 3).eq.1) then  ! polar angle along Z
         !        call PolarZmin(neval, etot, coord, opt_out)
         !        endif  ! for polar angle along Z
         !
         !
         !       if (MOD(restartcount, 3).eq.2) then  ! polar angle along X
         !       call PolarXmin(neval, etot, coord, opt_out)
         !       endif  ! for polar angle along X
         !
         !
         !       if (MOD(restartcount, 3).eq.0) then  ! polar angle along Y
         !       call PolarYmin(neval, etot, coord, opt_out)
         !       endif  ! for polar angle along Y

      end do !  do while restart loop

      print *, '*** FINISHED MINIMIZATION RESTARTS'

      return
   end subroutine EnergyMinMult

   ! To call a single-direction minimizer directly from input script

   !----------------------------------------------------------------
   subroutine EnergyMinPol(axis)

      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer:: i, neval
      real(KIND = DP):: etot, Eold, Ediff
      real(KIND = DP):: XGUESS(2*NNODE), XX(2*NNODE), GRAD(2*NNODE)
      integer:: maxvar, restartcount

      ! SPECIFICATION of COORD SYSTEM
      character*5:: coord, opt_out
      character*1:: axis

      coord = "polar"
      opt_out = 'noset'
      maxvar = 2*NNODE
      neval = 0
      EnergyUnit = Kd*total_volume/(Ls**(3.0/2.0))

      Eold = 999.
      Ediff = 999.
      restartcount = 0
      do while (abs(ediff) > 1.d-8 .and. restartcount .lt. MaxRestarts)
         print *, 'RESTART = ', restartcount
         restartcount = restartcount+1
         if (restartcount .eq. MaxRestarts) print *, 'FINAL ENERGY ITERATION'

         call PolarMin(axis, neval, etot, coord, opt_out)

         ediff = Etot-Eold

         Eold = Etot

      end do !  do while restart loop

      print *, '*** FINISHED MINIMIZATION RESTARTS'

      return

   end subroutine EnergyMinPol

   !----------------------------------------------------------------
   !     CARTESIAN
   !----------------------------------------------------------------

   !> Find the value of `M` which is a Local Energy Minimum for `E(M)`.
   subroutine EnergyMinCart()

      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices
      use Hubert_Minimizer, only: HubertMinimize
      use Energy_Calculator, only: ET_GRAD_Cart

      implicit none

      integer:: i, neval
      real(KIND = DP):: etot, Eold, Ediff

      !REAL(KIND = DP):: XGUESS(2*NNODE), XX(2*NNODE), GRAD(2*NNODE)

      real(KIND = DP):: MGUESS(3*NNODE), MM(3*NNODE), GRADM(3*NNODE)

      integer:: maxvar, restartcount

      ! SPECIFICATION of COORD SYSTEM
      character*5:: coord, opt_out
      character*1:: axis
      axis = "C"
      !    print*, 'CARTESIAN'
      coord = "cart"
      maxvar = 3*NNODE
      neval = 0
      EnergyUnit = Kd*total_volume/(Ls**(3.0/2.0))

      Eold = 999.
      Ediff = 999.
      restartcount = 0
      do while (abs(ediff) > 1.d-8 .and. restartcount .lt. MaxRestarts)
         print *, 'RESTART = ', restartcount
         restartcount = restartcount+1
         if (restartcount .eq. MaxRestarts) print *, 'FINAL ENERGY ITERATION'

         do i = 1, NNODE
            MGUESS(3*i - 2) = m(i, 1)
            MGUESS(3*i - 1) = m(i, 2)
            MGUESS(3*i - 0) = m(i, 3)
         end do

         MM(:) = 0.
         GRADM(:) = 0.

         call HubertMinimize(axis, coord, opt_out, MAXVAR, MGUESS, MM, GRADM, ETOT, neval)

         ediff = Etot-Eold

         Eold = Etot
         ! results of minimisation returned in X

         !do i = 1, NNODE
         !  m(i, 1)=sin(XX(2*i-1))*cos(XX(2*i))
         !  m(i, 2)=sin(XX(2*i-1))*sin(XX(2*i))
         !  m(i, 3)=cos(XX(2*i-1))
         !enddo

         do i = 1, NNODE
            m(i, 1) = MM(3*i - 2)
            m(i, 2) = MM(3*i - 1)
            m(i, 3) = MM(3*i - 0)
         end do

      end do !  do while restart loop

      print *, '*** FINISHED MINIMIZATION RESTARTS'

      return
   end subroutine EnergyMinCart

   subroutine PolarMin(axis, neval, etot, coord, opt_out)
      use Hubert_Minimizer, only: HubertMinimize
      use Tetrahedral_Mesh_Data, only: NNODE, M
      implicit none

      integer:: i, neval
      real(KIND = DP):: etot, Eold, Ediff
      real(KIND = DP):: XGUESS(2*NNODE), XX(2*NNODE), GRAD(2*NNODE)
      integer:: maxvar, restartcount
      character*5:: coord, opt_out
      character, intent(in):: axis
      integer:: axis1, axis2, axis3

      select case (axis)
      case ('X')
         axis1 = 2
         axis2 = 3
         axis3 = 1
      case ('Y')
         axis1 = 3
         axis2 = 1
         axis3 = 2
      case ('Z')
         axis1 = 1
         axis2 = 2
         axis3 = 3

      end select

      maxvar = 2*NNODE  ! TODO outside

      ! coordinate conversion of m to X (magnetization vector in spherical polar)
      do i = 1, NNODE
         XGUESS(2*i - 1) = acos(m(i, axis3))
         XGUESS(2*i) = atan2(m(i, axis2), m(i, axis1))
      end do

      XX(:) = 0.
      GRAD(:) = 0.
      ! call minimizer: get new M (in the form of XX), gradients, energies
      call HubertMinimize(axis, coord, opt_out, MAXVAR, XGUESS, XX, GRAD, ETOT, neval)

      ! magentic field results of minimisation returned in XX

      ! coordinate conversion X to M (cartesian)
      do i = 1, NNODE
         m(i, axis1) = sin(XX(2*i - 1))*cos(XX(2*i))
         m(i, axis2) = sin(XX(2*i - 1))*sin(XX(2*i))
         m(i, axis3) = cos(XX(2*i - 1))
      end do

   end subroutine PolarMin

   subroutine CartminMult(neval, etot, coord, opt_out)
      use Tetrahedral_Mesh_Data, only: NNODE, m
      use Hubert_Minimizer, only: HubertMinimize
      use Energy_Calculator, only: ET_GRAD_Cart

      implicit none

      integer:: i, neval
      real(KIND = DP):: etot

      !REAL(KIND = DP):: XGUESS(2*NNODE), XX(2*NNODE), GRAD(2*NNODE)

      real(KIND = DP):: MGUESS(3*NNODE), MM(3*NNODE), GRADM(3*NNODE)

      integer:: maxvar

      ! SPECIFICATION of COORD SYSTEM
      character*5:: coord, opt_out
      character*1:: axis
      axis = "C"
      !    print*, 'CARTESIAN'

      coord = "xyz"
      maxvar = 3*NNODE

      !    neval = 0

      !    restartcount = 0

      do i = 1, NNODE
         MGUESS(3*i - 2) = m(i, 1)
         MGUESS(3*i - 1) = m(i, 2)
         MGUESS(3*i - 0) = m(i, 3)
      end do

      MM(:) = 0.
      GRADM(:) = 0.

      call HubertMinimize(axis, coord, opt_out, MAXVAR, MGUESS, MM, GRADM, ETOT, neval)

      do i = 1, NNODE
         m(i, 1) = MM(3*i - 2)
         m(i, 2) = MM(3*i - 1)
         m(i, 3) = MM(3*i - 0)
      end do

      !    print*,'*** FINISHED MINIMIZATION RESTARTS'
      return
   end subroutine CartminMult

   !----------------------------------------------------------------
   !     LLG_OPTIMIZE-- PLACE HOLDER COPY OF EMERNYMINCART NOT YET USED
   !----------------------------------------------------------------

   !> Find the value of `M` which is a Local Energy Minimum for `E(M)`.
   subroutine LLG_optimize()

      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices
      use Hubert_Minimizer
      use Energy_Calculator, only: ET_GRAD_Cart

      implicit none

      integer:: i, neval
      real(KIND = DP):: etot, Eold, Ediff

      !REAL(KIND = DP):: XGUESS(2*NNODE), XX(2*NNODE), GRAD(2*NNODE)

      real(KIND = DP):: MGUESS(3*NNODE), MM(3*NNODE), GRADM(3*NNODE)

      integer:: maxvar, restartcount

      ! SPECIFICATION of COORD SYSTEM
      character*5:: coord, opt_out
      character*1:: axis
      axis = "C"
      !    print*, 'CARTESIAN'
      coord = "cart"
      maxvar = 3*NNODE
      neval = 0
      EnergyUnit = Kd*total_volume/(Ls**(3.0/2.0))

      Eold = 999.
      Ediff = 999.
      restartcount = 0
      do while (abs(ediff) > 1.d-8 .and. restartcount .lt. MaxRestarts)
         print *, 'RESTART = ', restartcount
         restartcount = restartcount+1
         if (restartcount .eq. MaxRestarts) print *, 'FINAL ENERGY ITERATION'

         !do i = 1, NNODE
         !XGUESS(2*i-1)=acos(m(i, 3))
         !XGUESS(2*i)=atan2(m(i, 2), m(i, 1))
         !enddo

         do i = 1, NNODE
            MGUESS(3*i - 2) = m(i, 1)
            MGUESS(3*i - 1) = m(i, 2)
            MGUESS(3*i - 0) = m(i, 3)
         end do

         MM(:) = 0.
         GRADM(:) = 0.
         axis = 'C'
         call HubertMinimize(axis, coord, opt_out, MAXVAR, MGUESS, MM, GRADM, ETOT, neval)

         ediff = Etot-Eold

         Eold = Etot
         ! results of minimisation returned in X

         !do i = 1, NNODE
         !  m(i, 1)=sin(XX(2*i-1))*cos(XX(2*i))
         !  m(i, 2)=sin(XX(2*i-1))*sin(XX(2*i))
         !  m(i, 3)=cos(XX(2*i-1))
         !enddo

         do i = 1, NNODE
            m(i, 1) = MM(3*i - 2)
            m(i, 2) = MM(3*i - 1)
            m(i, 3) = MM(3*i - 0)
         end do

      end do !  do while restart loop

      print *, '*** FINISHED MINIMIZATION RESTARTS'

      return
   end subroutine LLG_optimize

   !-----------------------------------------------------------------------------------------------------
   ! SDEnergySurface(theta_min, theta_max, theta_delta, phi_min, phi_max, phi_delta, Surf_filename
   !        outputs SD magnetizatio direction and SD total energy and energy components
   !----------------------------------------------------------------------------------------------------

   !> Update all the magnetic fields and energies and return the effective field
   !> derived form total gradient.
   !> @param[out]   ETOT  The NORMALIZED total magnetic energy (note difference to enerymin routines that return non-normalized)
   !> @param[in]    theta_min   the min value of theta angle
   !> @param[in]    theta_max   the max value of theta angle
   !> @param[in]    theta_delta   the increment value of theta angle
   !> @param[in]    phi_min   the min value of phi angle
   !> @param[in]    phi_max  the min value of phi angle
   !> @param[in]    phi_delta   the increment value of phi angle
   !> @param[in]    Surf_filename   the name of the output file
   subroutine SDEnergySurface(theta_min, theta_max, theta_delta, phi_min, &
                   & phi_max, phi_delta, Surf_filename)

      use Utils, only: pi
      use Material_Parameters, only: AnisEnerg, BEnerg, DemagEnerg, Ls, StressEnerg
      use Tetrahedral_Mesh_Data, only: NNODE
      use Energy_Calculator, only: ET_GRAD_POLAR
      implicit none

      integer:: i, neval, surfunit
      double precision:: theta_min, theta_max, theta_delta, phi_min, x, y, z, &
              & phi_max, phi_delta, theta, phi, ETOT, ls_scale, theta_rad, phi_rad
      character(len = 55) ::  Surf_filename
      double precision:: Xtest(NNODE*2), grad_dummy(NNODE*2)

      theta = theta_min
      phi = phi_min
      neval = 1

      ! scaling value to report energiesin units of Joules
      ls_scale = sqrt(Ls)**3
      ! print*,'stem', Surf_filename, pi
      ! print*, theta, theta_min, theta_max, theta_delta

      open ( &
              NEWUNIT = surfunit, FILE = Surf_filename(:len_trim(Surf_filename)), &
              & STATUS='unknown')

      ! write headder to CSV file.
      write (surfunit, *) 'X, Y, Z, theta, phi, Total Energy, Anisotropy, Demag, External, Stress'

      ! calculate the direction consines
      do while (theta <= theta_max)
         do while (phi <= phi_max)
            phi_rad = phi*pi/180.0
            theta_rad = theta*pi/180.0
            do i = 1, NNODE
               Xtest(2*i) = phi_rad
               Xtest(2*i - 1) = theta_rad
            end do
            x = sin(theta_rad)*cos(phi_rad)
            y = sin(theta_rad)*sin(phi_rad)
            z = cos(theta_rad)
            call ET_GRAD_POLAR('Z', ETOT, grad_dummy, Xtest, neval)
            write (surfunit, 101) x, y, z, theta, phi, etot/ls_scale, AnisEnerg/ls_scale, DemagEnerg/ls_scale, &
                    & BEnerg/ls_scale, StressEnerg/ls_scale
            !  print*,' etot =', etot
            !      Xtest(:,1) = sin(theta*pi/180.)*cos(phi*pi/180.)
            !      Xtest(:,2) = sin(theta*pi/180.)*sin(phi*pi/180.)
            !      Xtest(:,3) = cos(theta*pi/180.)
            phi = phi+phi_delta
         end do
         phi = phi_min
         theta = theta+theta_delta
         !print*, 'theta=', theta
      end do

      ! CALL ET_GRAD(ETOT, grad_dummy, Xtest, neval)

      ! Formatting for CSV
101   format(1x, *(g0, ", "))

      return
   end subroutine SDEnergySurface

end module Minimizing_Loops

