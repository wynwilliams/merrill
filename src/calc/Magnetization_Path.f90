!> A module containing the NEB path calculation procedures.
module Magnetization_Path
   use Utils, only: DP
   implicit none
   save

   !> Number of structures along the path
   integer PathN

   !
   ! Path arrays and values
   !

   !> Path magnetization structures
   real(KIND = DP), allocatable:: PMag(:, :, :)

   !> Path tangent vectors
   real(KIND = DP), allocatable:: PTan(:, :, :)

   !> Path energy gradient vectors
   real(KIND = DP), allocatable:: PEGrad(:, :, :)

   !> Path nudged-elastic-band gradient vectors
   real(KIND = DP), allocatable:: PNEBGrad(:, :, :)

   !> Path energies
   real(KIND = DP), allocatable:: PEn(:)

   !> Path distances
   real(KIND = DP), allocatable:: PDist(:)

   !> @todo DOCUMENT ME
   real(KIND = DP), allocatable:: PEGradNorm(:)

   !> @todo DOCUMENT ME
   real(KIND = DP), allocatable:: PAlign(:)

   !> Cumulative path distances
   real(KIND = DP), allocatable:: CumulPDist(:)

   !> @todo DOCUMENT ME
   real(KIND = DP), allocatable:: PSpringForce(:)

   !> Flags for recording changes in Path
   !> (only changed nodes are recalculated)
   logical, allocatable:: PathChangedQ(:)

   !
   ! Path parameters
   !

   !> Path length
   real(KIND = DP):: PLength

   !> Geometric action along path
   real(KIND = DP):: PAction

   !> @todo DOCUMENT ME
   real(KIND = DP):: PDeltaGeodesic

   !> @todo DOCUMENT ME
   real(KIND = DP):: PHooke

   !> @todo DOCUMENT ME
   real(KIND = DP):: CurveWeight

   !> @todo DOCUMENT ME
   real(KIND = DP):: PTarget

   !> Distance between initial and final state of the path
   real(KIND = DP):: PathEndDist

   !> @todo DOCUMENT ME
   real(KIND = DP):: InitEVar(10)

   !> Reference position for initial path calculation
   integer:: InitRefPos

   !> @todo DOCUMENT ME
   logical:: PathLoggingQ

   !
   ! Log file names
   !

   !> @todo DOCUMENT ME
   character(LEN = 200):: PathInFile

   !> @todo DOCUMENT ME
   character(LEN = 200):: PathOutFile

   !> @todo DOCUMENT ME
   character(LEN = 200):: ZoneInFile

   !> @todo DOCUMENT ME
   character(LEN = 200):: PLogEnFile

   !> @todo DOCUMENT ME
   character(LEN = 200):: PLogGradFile

   !> @todo DOCUMENT ME
   character(LEN = 200):: PLogDistFile

   !> @todo DOCUMENT ME
   character(LEN = 200):: PLogFile

   !
   ! Log file units
   !

   !> @todo DOCUMENT ME
   integer:: PLogEnUnit = 0

   !> @todo DOCUMENT ME
   integer:: PLogGradUnit = 0

   !> @todo DOCUMENT ME
   integer:: PLogDistUnit = 0
   
   !> An interface for the energy_calculator.et_grad() subroutine.
   !> So that one of the ET_GRAD_... procedures of this shape can be 
   !> chosen as ET_GRAD for magnetization path
   interface
      subroutine ET_GRAD_PROCEDURE(ETOT, grad, X, neval)
         !>
         !> @param[out]   ETOT  The total magnetic energy.
         !> @param[out]   grad  The manetic energy gradient (in spherical polars)
         !> @param[in]    X     The initial guess for the LEM (in spherical polars)
         !> @param[inout] neval The number of times the function has been
         !>                     evaluated.
         !>

         use Utils
         implicit none

         real(KIND = DP), intent(OUT):: ETOT
         real(KIND = DP), intent(OUT):: grad(:)
         real(KIND = DP), intent(IN)  :: X(:)
         integer, intent(INOUT)     :: neval
      end subroutine ET_GRAD_PROCEDURE
   end interface
   procedure(ET_GRAD_PROCEDURE), pointer:: ET_GRAD => null()

contains

   !---------------------------------------------------------------
   !          InitializeMagnetizationPath
   !             Set reasonable defaults
   !---------------------------------------------------------------

   !> Initialize the Magnetization_Path module
   subroutine InitializeMagnetizationPath()
      USE Energy_Calculator, only: ET_GRAD_CART
      implicit none

      call DestroyMagnetizationPath()

      PLogEnUnit = 0
      PLogGradUnit = 0
      PLogDistUnit = 0

      PHooke = 0.
      CurveWeight = 0.

      ET_GRAD => ET_GRAD_CART  ! which energy fct is used ... change the choice here


   end subroutine InitializeMagnetizationPath

   !> Destroy and clean up the variables in the Magnetization_Path module
   subroutine DestroyMagnetizationPath()
      use Energy_Calculator, only : PMagInit
      if (allocated(PMag)) deallocate (PMag)
      if (allocated(PMagInit)) deallocate (PMagInit)
      if (allocated(PTan)) deallocate (PTan)
      if (allocated(PEGrad)) deallocate (PEGrad)
      if (allocated(PNEBGrad)) deallocate (PNEBGrad)
      if (allocated(PEn)) deallocate (PEn)
      if (allocated(PDist)) deallocate (PDist)
      if (allocated(PEGradNorm)) deallocate (PEGradNorm)
      if (allocated(PAlign)) deallocate (PAlign)
      if (allocated(CumulPDist)) deallocate (CumulPDist)
      if (allocated(PSpringForce)) deallocate (PSpringForce)
      if (allocated(PathChangedQ)) deallocate (PathChangedQ)

      ! TODO call deallocate of PMagInit in Energy_Calculator 

      call CloseIfOpen(PLogEnUnit)
      call CloseIfOpen(PLogGradUnit)
      call CloseIfOpen(PLogDistUnit)
   contains
      subroutine CloseIfOpen(unit)
         integer, intent(INOUT):: unit
         logical:: is_open

         is_open = .false.
         if (unit .ne. 0) inquire (UNIT = unit, OPENED = is_open)
         if (is_open) close (UNIT = unit)

         unit = 0
      end subroutine CloseIfOpen
   end subroutine DestroyMagnetizationPath

   !> Set the pointers to the necessary energy_calculator subroutines.
   !subroutine InitializeMagnetizationPathPointers( &
      !BuildEnergyCalculator_in, EnergyMin_in, ET_GRAD_in &
      !)
      !procedure(BuildEnergyCalculator):: BuildEnergyCalculator_in
      !procedure(EnergyMin):: EnergyMin_in
      !procedure(ET_GRAD_PROCEDURE):: ET_GRAD_in

      !BuildEnergyCalculator => BuildEnergyCalculator_in
      !EnergyMin => EnergyMin_in
      !ET_GRAD => ET_GRAD_in
   !end subroutine InitializeMagnetizationPathPointers

   !---------------------------------------------------------------
   !          PathAllocate
   !---------------------------------------------------------------

   !> Allocate the variables necessary for a the magnetization path in
   !> an NEB calculation.
   subroutine PathAllocate()
      use Tetrahedral_Mesh_Data, only: NNODE
      use Energy_Calculator, only : PMagInit

      implicit none

      if (allocated(PMag)) deallocate (PMag)
      if (allocated(PMagInit)) deallocate (PMagInit)
      if (allocated(PTan)) deallocate (PTan)
      if (allocated(PEGrad)) deallocate (PEGrad)
      if (allocated(PNEBGrad)) deallocate (PNEBGrad)
      if (allocated(PEGradNorm)) deallocate (PEGradNorm)
      if (allocated(PAlign)) deallocate (PAlign)
      if (allocated(PEn)) deallocate (PEn)
      if (allocated(PDist)) deallocate (PDist)
      if (allocated(CumulPDist)) deallocate (CumulPDist)
      if (allocated(PSpringForce)) deallocate (PSpringForce)
      if (allocated(PathChangedQ)) deallocate (PathChangedQ)

      if (allocated(PMag)) write (*, *) 'Alloc 1'
      if (allocated(PTan)) write (*, *) 'Alloc 2'
      if (allocated(PEGrad)) write (*, *) 'Alloc 3'
      if (allocated(PNEBGrad)) write (*, *) 'Alloc 4'
      if (allocated(PEGradNorm)) write (*, *) 'Alloc 5'
      if (allocated(PEn)) write (*, *) 'Alloc 6'
      if (allocated(PDist)) write (*, *) 'Alloc 7'
      if (allocated(CumulPDist)) write (*, *) 'Alloc 8'
      if (allocated(PAlign)) write (*, *) 'Alloc 9'
      if (allocated(PSpringForce)) write (*, *) 'Alloc 10'
      if (allocated(PathChangedQ)) write (*, *) 'Alloc 11'

      allocate (PMag(PathN, NNODE, 3), PTan(PathN, NNODE, 3))
      allocate (PMagInit (NNODE, 3))  ! to hold copy of initial slice at Energy_Calculators module
      allocate (PEGrad(PathN, NNODE, 3), PNEBGrad(PathN, NNODE, 3))
      allocate (PEn(PathN), PDist(PathN), CumulPDist(PathN))
      allocate (PEGradNorm(PathN), PAlign(PathN), PathChangedQ(PathN))
      allocate (PSpringForce(PathN))

      PMag(:, :, :) = 0.
      PMagInit(:, :) = 0.
      PTan(:, :, :) = 0.
      PEGrad(:, :, :) = 0.
      PNEBGrad(:, :, :) = 0.
      PEn(:) = 0.
      PDist(:) = 0.
      CumulPDist(:) = 0.
      PEGradNorm(:) = 0.
      PAlign(:) = 0.
      PSpringForce(:) = 0.

      PathChangedQ(:) = .true.

   end subroutine PathAllocate

   !> Calculate linear distances and cumulative distances
   !> along the path
   subroutine PathRenewDist()
      use Magnetization_Path_Utils, only: Distance

      implicit none
      integer i

      CumulPDist(1) = 0
      do i = 1, PathN-1
         ! PDist(i) =  Distance between structures i and i+1
         PDist(i) = Distance(PMag(i, :, :), PMag(i+1, :, :))

         ! Cumulative distance to structure i+1
         CumulPDist(i+1) = CumulPDist(i) + PDist(i)
      end do

      !  Total path length
      PLength = CumulPDist(PathN)

      !  Distance between start and end
      PathEndDist = Distance(PMag(1, :, :), PMag(PathN, :, :))

   end subroutine PathRenewDist

   !>  Calculate normalized tangent vectors, `PTan` as weighted finite
   !>  differences of the magnetization vectors.
   !>  All tangent vectors are projected into the S2^N tangent space
   subroutine PathTangents()
      use Tetrahedral_Mesh_Data, only: NNODE
      use Magnetization_Path_Utils, only: MagNorm
      implicit none
      integer pn
      double precision x1, x2, x3, Dm1(NNODE, 3), Dm2(NNODE, 3)

      ! Calculate normalized tangent vectors along the path
      PTan(1, :, :) = (PMag(2, :, :) - PMag(1, :, :))

      ! projects path tangent on magnetization tangent space
      call S2Project(PTan(1, :, :), PMag(1, :, :))
      x2 = MagNorm(PTan(1, :, :))
      PTan(1, :, :) = PTan(1, :, :)/x2

      PTan(PathN, :, :) = (PMag(PathN, :, :) - PMag(PathN-1, :, :))
      call S2Project(PTan(PathN, :, :), PMag(PathN, :, :))
      x2 = MagNorm(PTan(PathN, :, :))
      PTan(PathN, :, :) = PTan(PathN, :, :)/x2

      do pn = 2, PathN-1
         x1 = CumulPDist(pn-1)
         x2 = CumulPDist(pn)
         x3 = CumulPDist(pn+1)
         Dm1(:, :) = PMag(pn, :, :) - PMag(pn-1, :, :)
         Dm2(:, :) = PMag(pn+1, :, :) - PMag(pn, :, :)
         PTan(pn, :, :) = Dm1(:, :)*(x3-x2)/(x3-x1)/(x2-x1) + Dm2(:, :)*(x2-x1)/(x3-x1)/(x3-x2)
         call S2Project(PTan(pn, :, :), PMag(pn, :, :))
         x2 = MagNorm(PTan(pn, :, :))
         PTan(pn, :, :) = PTan(pn, :, :)/x2
         PSpringForce(pn) = PHooke*(PDist(pn) - PDist(pn-1))
      end do

   end subroutine PathTangents

   !> Calculate normalized tangent vectors, `PTan` using the method of
   !> G. Henkelman and H. Jonsson, (2000).
   !> All tangent vectors are projected into the S2^N tangent space
   subroutine HenkelmanTangents()
      use Tetrahedral_Mesh_Data, only: NNODE
      use Magnetization_Path_Utils, only: MagNorm
      implicit none
      integer pn
      double precision DEmax, DEMin, norm, Dm1(NNODE, 3), Dm2(NNODE, 3)

      PTan(1, :, :) = (PMag(2, :, :) - PMag(1, :, :))    ! Calculate normalized tangent vectors along the path
      call S2Project(PTan(1, :, :), PMag(1, :, :))  ! projects path tangent on magnetization tangent space
      norm = MagNorm(PTan(1, :, :))
      PTan(1, :, :) = PTan(1, :, :)/norm

      PTan(PathN, :, :) = (PMag(PathN, :, :) - PMag(PathN-1, :, :))
      call S2Project(PTan(PathN, :, :), PMag(PathN, :, :))
      norm = MagNorm(PTan(PathN, :, :))
      PTan(PathN, :, :) = PTan(PathN, :, :)/norm

      do pn = 2, PathN-1
         Dm1(:, :) = PMag(pn, :, :) - PMag(pn-1, :, :)   ! tau-
         Dm2(:, :) = PMag(pn+1, :, :) - PMag(pn, :, :)   ! tau+
         if ((PEn(pn+1) .ge. PEn(pn)) .and. (PEn(pn) .ge. PEn(pn-1))) then
            PTan(pn, :, :) = PMag(pn+1, :, :) - PMag(pn, :, :)
         else
            if ((PEn(pn+1) .le. PEn(pn)) .and. (PEn(pn) .le. PEn(pn-1))) then
               PTan(pn, :, :) = PMag(pn, :, :) - PMag(pn-1, :, :)
            else
               Dm1(:, :) = PMag(pn, :, :) - PMag(pn-1, :, :)   ! tau-
               Dm2(:, :) = PMag(pn+1, :, :) - PMag(pn, :, :)   ! tau+
               DEMax = abs(PEn(pn+1) - PEn(pn))
               DEMin = abs(PEn(pn-1) - PEn(pn))
               if (DEMax < DEMin) then
                  DEMax = DEMin
                  DEMin = abs(PEn(pn+1) - PEn(pn))
               end if
               if (PEn(pn+1) .ge. PEn(pn-1)) then
                  PTan(pn, :, :) = Dm2(:, :)*DEMax+Dm1(:, :)*DEMin
               else
                  PTan(pn, :, :) = Dm2(:, :)*DEMin+Dm1(:, :)*DEMax
               end if
            end if
         end if
         call S2Project(PTan(pn, :, :), PMag(pn, :, :))
         norm = MagNorm(PTan(pn, :, :))
         PTan(pn, :, :) = PTan(pn, :, :)/norm
         PSpringForce(pn) = PHooke*(PDist(pn) - PDist(pn-1))
      end do

   end subroutine HenkelmanTangents

   !---------------------------------------------------------------
   ! WRITETecplotPath
   !---------------------------------------------------------------

   !> Write the current magnetization path into a multi-zone tecplot file
   !> with filename `PathOutFile`, where `PathOutFile` is a global variable.
   subroutine WriteTecplotPath()

      use strings, only: lowercase
      use Tetrahedral_Mesh_Data, only: packing

      implicit none
      integer i, j, pn
      integer:: PathOutUnit

      open (NEWUNIT = PathOutUnit, file = PathOutFile, status='unknown')

      !    print*,' ABOUT TOWRITE OUTPIT IN FORMAT ',packing
      if (lowercase(packing) == 'block') then
         call WriteBlockPath()
      else
         if (lowercase(packing) == 'point') then
            call WritePointPath()
         else
            write (*, *) ' TecplotPathWrite format not known (must be "block"  or "point")'
            stop
         end if
      end if

      close (PathOutUnit)

   contains

      subroutine WriteBlockPath()
         use Tetrahedral_Mesh_Data, only: VCL, TetSubDomains, TIL, NNODE, NTRI, SubDomainIds
         pn = 1
         write (PathOutUnit, *) 'TITLE = ', '"'//PathOutFile(:len_trim(PathOutFile))//'"'
         write (PathOutUnit, *) 'VARIABLES = "X","Y","Z","Mx","My","Mz", "SD"'
         write (PathOutUnit, *) 'ZONE T="', pn, '"N=', NNODE, ',E=', NTRI
         write (PathOutUnit, *) 'F = FEBLOCK, ET = TETRAHEDRON, VARLOCATION=([7]=CELLCENTERED)'

         do pn = 1, PathN

            if (pn == 1) then
               do j = 1, 3
                  write (PathOutUnit, '(10E16.7)') (VCL(i, j), i = 1, NNODE)
               end do

               do j = 1, 3
                  write (PathOutUnit, '(10E16.7)') (PMag(pn, i, j), i = 1, nnode)
               end do

               write (PathOutUnit, '(10I7)') (SubDomainIDs(TetSubDomains(i)), i = 1, NTRI)

               do i = 1, NTRI
                  write (PathOutUnit, 3002) TIL(i, 1), TIL(i, 2), TIL(i, 3), TIL(i, 4)
               end do

            else
               write (PathOutUnit, *) 'ZONE T="', pn, '"N=', NNODE, ',E=', NTRI
               write (PathOutUnit, *) 'F = FEBLOCK, ET = TETRAHEDRON, VARSHARELIST =([1-3, 7]=1),  &
                 & CONNECTIVITYSHAREZONE = 1, VARLOCATION=([7]=CELLCENTERED)'

               do j = 1, 3
                  write (PathOutUnit, '(10E16.7)') (PMag(pn, i, j), i = 1, nnode)
               end do

            end if
         end do

3002     format(4(i7))
      end subroutine WriteBlockPath

      subroutine WritePointPath()
         use Tetrahedral_Mesh_Data, only: VCL, TIL, NNODE, NTRi

         write (PathOutUnit, *) 'TITLE = ', '"'//PathOutFile(:len_trim(PathOutFile))//'"'
         write (PathOutUnit, *) 'VARIABLES = "X","Y","Z","Mx","My","Mz"'

         do pn = 1, PathN

            if (pn == 1) then
               write (PathOutUnit, *) 'ZONE T="', pn, '"N=', NNODE, ',E=', NTRI
               write (PathOutUnit, *) 'F = FEPOINT, ET = TETRAHEDRON'
               do i = 1, NNODE
                  write (PathOutUnit, 3001) VCL(i, 1), VCL(i, 2), VCL(i, 3), &
                    & PMag(pn, i, 1), PMag(pn, i, 2), PMag(pn, i, 3)
               end do
               do i = 1, NTRI
                  write (PathOutUnit, 3002) TIL(i, 1), TIL(i, 2), TIL(i, 3), TIL(i, 4)
               end do
            else
               write (PathOutUnit, *) 'ZONE T="', pn, '"N=', NNODE, ',E=', NTRI
               write (PathOutUnit, *) 'F = FEPOINT, ET = TETRAHEDRON, VARSHARELIST =([1-3]=1), CONNECTIVITYSHAREZONE = 1'
               do i = 1, NNODE
                  write (PathOutUnit, 3003) PMag(pn, i, 1), PMag(pn, i, 2), PMag(pn, i, 3)
               end do
            end if
         end do

3001     format(6(f10.6, ' '))
3002     format(4(i7))
3003     format(3(f10.6, ' '))

      end subroutine WritePointPath

      !    RETURN
   end subroutine WriteTecplotPath

   !> Read the zone `izone` from the tecplot file `ZoneInFile`, where `izone`
   !> and `ZoneInFile` are global variables.
   subroutine ReadTecplotZone()
      use Finite_Element_Matrices
      use Material_Parameters
      use strings
      use Tetrahedral_Mesh_Data, only: NNODE, NTRI

      implicit none
      integer i, pn, zonecnt, linecnt, ios, nodecnt, tetcnt, was4, dstop
      integer result, RESULTN, RESULTE, RESULTF, RESULTT, nod, j
      integer:: ZoneInUnit

      character(LEN = 300) ::  zonetest  ! read first part of line that should contain the ZONE variable
      character(LEN = 300) ::  line
      character(LEN = 5):: pack  !local variable for packing format in this file (maybe different from mesh file)
      character(LEN = 1):: nextC

      double precision mnorm, x, y, z, mx, my, mz, e1, e2, e3, e4

      zonecnt = 0
      linecnt = 0
      i = 0
      result = 0
      RESULTT = 0  ! test next character
      RESULTN = 0
      RESULTE = 0
      nodecnt = 0
      tetcnt = 0
      was4 = 0
      dstop = 0
      ios = 0

      ! For future use, in case a more robust header reader is needed
      !    call readtecplotheader(ZoneInFile, new_NTRI, new_NNODE)

      open (NEWUNIT = ZoneInUnit, file = ZoneInFile, status='unknown')

      do while (ios .ge. 0)
         linecnt = linecnt+1
         read (ZoneInUnit, '(A)', IOSTAT = ios) zonetest
         if (ios /= 0) exit  ! needed to prevent read past EOF
         RESULTN = index(zonetest, "N=")
         read (zonetest((RESULTN+2):), *) nextC
         RESULTT = index(nextC, "(")
         if ((RESULTN .ne. 0) .and. (RESULTT .eq. 0)) then  ! read in number of nodes in this file
            read (zonetest((RESULTN+2):), *) nodecnt
            !      print*,' ---->Number of Nodes in this file = ',nodecnt
         end if
         RESULTE = index(zonetest, "E=")
         if (RESULTE .ne. 0) then  ! read in number of elements in this file
            read (zonetest((RESULTE+2):), *) tetcnt
            !      print*,' ---->Number of Elements in this file = ',tetcnt
         end if
         RESULTF = index(zonetest, "F=")
         if (RESULTF .ne. 0) then  ! read in number of elements in this file
            read (zonetest((RESULTF+4):), *) pack
            !      print*,' ---->Data packing format in this file = ',pack
         end if
         result = index(zonetest, "TETRAHEDRON")  ! for counting zones since only on TETRAHEDRON label per zone
         !      print*, RESULT, zonetest
         if (result .ne. 0) zonecnt = zonecnt+1
      end do

      if (NNODE /= nodecnt .or. NTRI /= tetcnt) then
         write (*, *) 'STOPPING-mesh file size does not match your zone data'
         write (*, *) 'Mesh Node and Element =', NNODE, NTRI
         write (*, *) 'ZONE Node and Element =', nodecnt, tetcnt
         stop
      end if
      !    NNODE = nodecnt
      !    NTRI = tetcnt

      if (zonecnt == 0) then
         write (*, *) ' File ', ZoneInFile(:len_trim(ZoneInFile)), ' STOPPING-input contains no ZONE. '
         stop
      end if

      write (*, *) ' File ', ZoneInFile(:len_trim(ZoneInFile)), ' contains  ', zonecnt, ' ZONEs'
      rewind (ZoneInUnit)
      if (izone .gt. zonecnt) then
         write (*, *) '*** ERROR**you asked for zone', izone, 'but file contains a max zone number', zonecnt
         stop
      end if

      was4 = 0
      dstop = 0
      ios = 0

      ! search for the word 'TETRAHEDRON' in each line of text read in
      ! since this always occurs in the last line before data
      i = 0
      result = 0
      do while (result .eq. 0)
         i = i+1
         read (ZoneInUnit, FMT='(A)') line
         !      write(*,*) 'reading line',i, line  ! for testing only
         result = index(line, 'TETRAHEDRON')
      end do

      ! this next section counts the numbers of nodes and elements-HOWEVER-it can be used to check that
      ! they mesh file you are using matches the same NODE and ELEMENT number as your data
      !
      ! comment out for now and look for a better way
      !    was4 = 0
      !   dstop = 0
      !   ios = 0
      !    DO WHILE((ios .GE. 0).AND.(dstop == 0))
      !      READ(ZoneInUnit, IOSTAT = ios, FMT='(A)') line   !Read line
      !      if (ios /= 0) exit  ! needed in case of only one zone-othewise attempts to read past EOF
      !      num = 0  ! it follows Fortran magic to count columns (num) in the input line
      !      spc = 1  ! last char was space(or other char < 32)
      !      Do i = 1, LEN(line)
      !        select case(iachar(line(i:i)))
      !          case(0:32)
      !             spc = 1  ! last char was space(or other char < 32)
      !          case(33:)
      !             if(spc == 1) num = num+1  ! if previous char was space : increase column count
      !             spc = 0
      !        end select
      !      End DO
      !      if(num == 6) then
      !        if(was4 == 0) then
      !          nodecnt = nodecnt+1  ! 6 columns with no preceding 4 column line => one more node
      !        else
      !          dstop = 1   ! stop when first 6 column line occurs after a 4 column line
      !        endif
      !      else
      !        if(num == 4) then       ! 4 columns   => one more tetrahedron
      !          was4 = 1
      !          tetcnt = tetcnt+1
      !        else
      !          dstop = 1  !stop if unexpected column number occurs (should never happen)
      !        endif
      !      endif
      !    END DO

      rewind (ZoneInUnit)

      ! search for the word 'TETRAHEDRON' in each line of text read in
      ! since this always occurs in the last line before data
      ! should work no matter what the packing format
      i = 0
      result = 0
      do while (result .eq. 0)
         i = i+1
         read (ZoneInUnit, FMT='(A)') line
         !      write(*,*) 'reading line',i, line  ! for testing only
         result = index(line, 'TETRAHEDRON')
      end do

      if (lowercase(pack) .eq. 'point') then
         call READPOINT()
      else if (lowercase(pack) .eq. 'block') then
         call READBLOCK()
      else
         write (*, *) ' STOPPING-cannot determine file format'
         stop
      end if

      close (ZoneInUnit)

   contains

      subroutine READBLOCK()
         use Tetrahedral_Mesh_Data, only: m, NNODE

         pn = 1

         if (izone .eq. 1) then
            write (*, *) 'Reading Zone', izone
            ! skip the first three variables which are the mesh node locations, since there are
            !  10 values per line these will occupy 3*(int(NNODE/10) + 1) lines
            !   do i = 1, 3*(INT((NNODE-1)/10)+1)
            do i = 1, 3*ceiling(real(NNODE)/10.)
               read (ZoneInUnit, *)
            end do

            ! now read in the magnetization values  for mx
            if (NNODE .ge. 10) then
               do j = 1, (NNODE/10)
                  nod = (j-1)*10+1
                  read (ZoneInUnit, *) (m(i, 1), i = nod, nod+9)
               end do
            end if
            if (mod(NNODE, 10) .ne. 0) then
               nod = (NNODE/10)*10+1
               read (ZoneInUnit, *) (m(i, 1), i = nod, nnode)   ! read remainin numbers
            end if

            ! now read in the magnetization values  for my
            if (NNODE .ge. 10) then
               do j = 1, (NNODE/10)
                  nod = (j-1)*10+1
                  read (ZoneInUnit, *) (m(i, 2), i = nod, nod+9)
               end do
            end if
            if (mod(NNODE, 10) .ne. 0) then
               nod = (NNODE/10)*10+1
               read (ZoneInUnit, *) (m(i, 2), i = nod, nnode)   ! read remainin numbers
            end if

            ! now read in the magnetization values  for mz
            if (NNODE .ge. 10) then
               do j = 1, (NNODE/10)
                  nod = (j-1)*10+1
                  read (ZoneInUnit, *) (m(i, 3), i = nod, nod+9)
               end do
            end if
            if (mod(NNODE, 10) .ne. 0) then
               nod = (NNODE/10)*10+1
               read (ZoneInUnit, *) (m(i, 3), i = nod, nnode)   ! read remainin numbers
            end if

            do i = 1, ceiling(real(NTRI)/10.)
               read (ZoneInUnit, *)  ! read subdomain number and throw away
            end do

         else  ! else read and throw away

            do i = 1, 6*ceiling(real(NNODE)/10.)
               read (ZoneInUnit, *)
            end do
            do i = 1, ceiling(real(NTRI)/10.)
               read (ZoneInUnit, *)  ! read subdomain number and throw away
            end do
         end if

         do i = 1, NTRI
            read (ZoneInUnit, *) e1, e2, e3, e4  ! read elements and throw away
         end do

         if (izone .gt. 1) then
            do pn = 2, zonecnt
               read (ZoneInUnit, *)
               read (ZoneInUnit, *)
               if (pn == izone) then
                  write (*, *) 'Reading Zone', izone

                  ! now read in the magnetization values  for mx
                  if (NNODE .ge. 10) then
                     do j = 1, (NNODE/10)
                        nod = (j-1)*10+1
                        read (ZoneInUnit, *) (m(i, 1), i = nod, nod+9)
                     end do
                  end if
                  if (mod(NNODE, 10) .ne. 0) then
                     nod = (NNODE/10)*10+1
                     read (ZoneInUnit, *) (m(i, 1), i = nod, nnode)   ! read remainin numbers
                  end if

                  ! now read in the magnetization values  for my
                  if (NNODE .ge. 10) then
                     do j = 1, (NNODE/10)
                        nod = (j-1)*10+1
                        read (ZoneInUnit, *) (m(i, 2), i = nod, nod+9)
                     end do
                  end if
                  if (mod(NNODE, 10) .ne. 0) then
                     nod = (NNODE/10)*10+1
                     read (ZoneInUnit, *) (m(i, 2), i = nod, nnode)   ! read remainin numbers
                  end if

                  ! now read in the magnetization values  for mz
                  if (NNODE .ge. 10) then
                     do j = 1, (NNODE/10)
                        nod = (j-1)*10+1
                        read (ZoneInUnit, *) (m(i, 3), i = nod, nod+9)
                     end do
                  end if
                  if (mod(NNODE, 10) .ne. 0) then
                     nod = (NNODE/10)*10+1
                     read (ZoneInUnit, *) (m(i, 3), i = nod, nnode)   ! read remainin numbers
                  end if

                  exit
               else

                  do i = 1, 3*ceiling(real(NNODE)/10.)
                     read (ZoneInUnit, *)
                  end do

               end if
            end do
         end if

         ! now normalise magnetization
         do i = 1, NNODE
            mnorm = sqrt(m(i, 1)**2+m(i, 2)**2+m(i, 3)**2)
            !                print*,'norm=',mnorm
            m(i, 1) = m(i, 1)/mnorm
            m(i, 2) = m(i, 2)/mnorm
            m(i, 3) = m(i, 3)/mnorm
         end do

      end subroutine READBLOCK

      subroutine READPOINT()
         use Tetrahedral_Mesh_Data, only: m
         pn = 1
         if (izone .eq. 1) then
            write (*, *) 'Reading Zone', izone
            do i = 1, NNODE
               read (ZoneInUnit, *) x, y, z, mx, my, mz
               mnorm = sqrt(mx**2 + my**2 + mz**2)
               !                print*,'norm=',mnorm
               m(i, 1) = mx/mnorm
               m(i, 2) = my/mnorm
               m(i, 3) = mz/mnorm
            end do
         else
            do i = 1, NNODE
               read (ZoneInUnit, *) x, y, z, mx, my, mz  ! read and throw away
            end do
         end if

         do i = 1, NTRI
            read (ZoneInUnit, *) e1, e2, e3, e4  ! read elements and throw away
         end do

         if (izone .gt. 1) then
            do pn = 2, zonecnt
               read (ZoneInUnit, *)
               read (ZoneInUnit, *)
               if (pn == izone) then
                  write (*, *) 'Reading Zone', izone
                  do i = 1, NNODE
                     read (ZoneInUnit, *) mx, my, mz
                     mnorm = sqrt(mx**2 + my**2 + mz**2)
                     m(i, 1) = mx/mnorm
                     m(i, 2) = my/mnorm
                     m(i, 3) = mz/mnorm
                  end do
                  exit
               else
                  do i = 1, NNODE
                     read (ZoneInUnit, *) mx, my, mz
                  end do
               end if
            end do
         end if
      end subroutine READPOINT

      !   CLOSE(ZoneInUnit)
   end subroutine ReadTecplotZone

   !---------------------------------------------------------------------------------------
   ! ReadTecplotHeadder: Read header from Tecplot ASCII File-a more robust header reader
   !---------------------------------------------------------------------------------------
   subroutine ReadTecplotHeader(tecfile, new_NTRI, new_NNODE)
      use Tetrahedral_Mesh_Data
      use strings
      implicit none

      character(len=*), intent(IN):: tecfile
      !    CHARACTER(len = 5), INTENT(OUT):: packing
      integer:: tecunit

      integer, parameter:: MAX_TECPLOT_CHARS_PER_LINE = 32000
      character(len = MAX_TECPLOT_CHARS_PER_LINE):: line
      character(len = 1024):: token
      character(len=*), parameter:: separators = " ,"

      integer, parameter:: PARSE_SUCCESS = 1, PARSE_ERROR = -1

      integer:: NNODE_read, NTRI_read, new_NNODE, new_NTRI

      integer:: ierr

      integer:: ParserState
      integer, parameter :: &
         PARSING_HEADERS = 1, PARSING_ZONE = 2, &
         HEADER_PARSED = 3, &
         PARSING_NODES = 4, PARSING_ELEMENTS = 5, &
         PARSING_DONE = 6, &
         PARSING_VAR1 = 7, PARSING_VAR2 = 8, &
         PARSING_VAR3 = 9, PARSING_VAR4 = 10, &
         PARSING_VAR5 = 11, PARSING_VAR6 = 12, &
         PARSING_VAR7 = 13

      packing = "NOSET"  ! set packing to neither BLOCK nor POINT, and must be set in this subroutine or fail
      ! First we open the file

      open ( &
         NEWUNIT = tecunit, FILE = tecfile, &
         STATUS='OLD', ACTION='READ', IOSTAT = ierr &
         )
      rewind (tecunit)  ! make sure your at the beginning of the file
      if (ierr .ne. 0) then
         write (*, *) "Error opening file: ", trim(tecfile)
         stop
      end if

      ParserState = PARSING_HEADERS
      NNODE_read = -1
      NTRI_read = -1
      ! Loop over lines

      do while (ParserState .ne. PARSING_DONE)
         line = ""
         read (tecunit, '(A)', iostat = ierr, iomsg = token) line
         if (ierr .ne. 0) then
            if (IS_IOSTAT_END(ierr)) then
               exit
            else
               write (*, *) token
               ERROR stop
            end if
         end if

         !
         ! Skip comments
         !
         call DropInitialSeparators(line)
         if (line(1:1) .eq. "#") then
            cycle
         end if

         !
         ! PARSING_HEADERS
         !
         ! Parsing Headers
         ! Expecting something like
         !   TITLE = "my fancy title"
         !   VARIABLES = "X", "Y", "Z", "Mx", "My", "Mz"
         ! terminated by a line beginning with ZONE
         !
         ! Maybe a little more complex than necessary, but it should
         ! allow for header items to be added by users in any order.
         !
         if (ParserState .eq. PARSING_HEADERS) then

            ! Loop over the tokens
            do
               ! Parse one token, expecting a keyword.
               call ParseToken(line, token)

               if (len_trim(token) .gt. 0) then

                  ! Match token for further parsing
                  select case (lowercase(trim(token)))
                     ! Parse TITLE
                  case ("title")
                     call ParseEqOrDie(line, token)

                     ! Parse title value. Drop it, because we don't use it.
                     call ParseToken(line, token)

                     ! Parse VARIABLES
                  case ("variables")
                     !              PRINT*, 'IN VARIABLES'
                     call ParseEqOrDie(line, token)

                     ! The rest of the line is variable tokens.
                     ! Expect X, Y, Z, optional Mx, My, Mz, then EOF
                     call ParseToken(line, token)
                     if (trim(token) .ne. "X") then
                        write (*, *) 'ERROR: Expected first variable to be "X"'
                        ERROR stop
                     end if

                     call ParseToken(line, token)
                     if (trim(token) .ne. "Y") then
                        write (*, *) 'ERROR: Expected second variable to be "Y"'
                        ERROR stop
                     end if

                     call ParseToken(line, token)
                     if (trim(token) .ne. "Z") then
                        write (*, *) 'ERROR: Expected third variable to be "Z"'
                        ERROR stop
                     end if

                     ! Try parse Mx. If we get it, parse My and Mz.
                     call ParseToken(line, token)
                     if (len_trim(token) .gt. 0) then
                        if (trim(token) .ne. "Mx") then
                           write (*, *) 'ERROR: Expected fourth variable to be "Mx"'
                           ERROR stop
                        end if

                        call ParseToken(line, token)
                        if (trim(token) .ne. "My") then
                           write (*, *) 'ERROR: Expected fourth variable to be "My"'
                           ERROR stop
                        end if

                        call ParseToken(line, token)
                        if (trim(token) .ne. "Mz") then
                           write (*, *) 'ERROR: Expected fourth variable to be "Mz"'
                           ERROR stop
                        end if

                        call ParseToken(line, token)
                        if (trim(token) .eq. "SD") then
                           write (*, *) 'SubDomain ID variable is present'

                        end if

                     end if
                     ! Parse ZONE.
                     ! Move on to the next phase.
                  case ("zone")
                     ! Done parsing header  ! Move on to parsing ZONE header.
                     ParserState = PARSING_ZONE

                     ! Break out of the token parsing loop
                     exit
                  end select
               else
                  ! No token parsed, end of line, break out of token parsing loop
                  exit
               end if

            end do  ! Parsing tokens
         end if  ! Parsing Headers

         !
         ! PARSING_ZONE
         !
         ! Parsing ZONE Header
         ! Expecting something like
         !   ZONE T="...", N = 1234
         !     E = 567 F = FEPOINT ET = TETRAHEDRON
         ! possibly spanning multiple lines, 
         ! terminated by the first numerical value of the actual data.
         !
         ! Again, maybe a little more complex than necessary, but it should
         ! allow for header items to be added by users in any order.
         !
         if (ParserState .eq. PARSING_ZONE) then
            !      PRINT*,' PARSING ZONE'
            ! Parse tokens
            do
               call ParseToken(line, token)
               ! PRINT*, 'token  =  ', ParserState,  token(:20)
               if (len_trim(token) .gt. 0) then

                  ! Match token for further parsing
                  select case (lowercase(trim(token)))
                     ! Parse zone title. Ignore.
                  case ("t")
                     call Skip2quotes(line)  ! skip the zone name

                  case ("f")
                     call ParseEqOrDie(line, token)
                     call ParseToken(line, token)
                     if (lowercase(trim(token)) .eq. "fepoint") packing = "point"
                     if (lowercase(trim(token)) .eq. "feblock") packing = "block"

                     if (lowercase(trim(token)) .ne. "fepoint" &
                         .and. &
                         lowercase(trim(token)) .ne. "feblock") then
                        write (*, *) "ERROR: Unsupported value for F."
                        write (*, *) "ERROR: Expected either F = FEPOINT or F = FEBLOCK"
                        ERROR stop
                     end if

                  case ("et")
                     call ParseEqOrDie(line, token)

                     call ParseToken(line, token)
                     if (lowercase(trim(token)) .ne. "tetrahedron") then
                        write (*, *) "ERROR: Unsupported value for ET."
                        write (*, *) "ERROR: Expected ET = TETRAHEDRON"
                        ERROR stop
                     end if

                  case ("n")
                     call ParseEqOrDie(line, token)
                     call ParseToken(line, token)
                     if (len_trim(token) .eq. 0) then
                        write (*, *) "ERROR: Expected value for N."
                        ERROR stop
                     end if
                     read (token, *) new_NNODE
                  case ("e")
                     call ParseEqOrDie(line, token)
                     call ParseToken(line, token)
                     if (len_trim(token) .eq. 0) then
                        write (*, *) "ERROR: Expected value for E."
                        ERROR stop
                     end if
                     read (token, *) new_NTRI

                  case DEFAULT

                     !PRINT*, ' in case default with number ', token

                     ! Check if it's a number ([digit], ., or +/-)
                     if (scan(token(1:1), "0123456789.+-") .gt. 0) then
                        line = trim(token)//" "//trim(line)
                        !PRINT*, ' in case default with number ', token
                        ! Move parser along to node parsing
                        ParserState = PARSING_DONE
                        exit
                     end if

                  end select

               else
                  ! No token parsed, end of line
                  exit
               end if
            end do
         end if

         print *, 'HEADER NEW NTRI, NNODE', new_NTRI, new_NNODE
         !
         ! ALLOCATING_MESH
         !
         ! Allocating mesh based on N and E
         !

         !
         ! PARSING_NODES
         !
         ! Parse node coordinate positions
         !

      end do  ! Loop over lines

      close (tecunit)

   contains
      subroutine DropInitialSeparators(line)
         character(len=*), intent(INOUT):: line

         integer:: i

         ! Drop separators from start of line
         do i = 1, len(line)

            if (scan(separators, line(i:i)) .eq. 0) then
               !        PRINT*, 'IN separators',separators, i, line(:50)
               line = line(i:)
               !        PRINT*, 'TRUE',separators, i, line(:50)
               exit
            end if
         end do
      end subroutine DropInitialSeparators

      subroutine Skip2quotes(line)
         character(len=*), intent(INOUT):: line

         integer:: q_count, j

         q_count = 0
         !      print*, 'searching for quotes'
         ! is there a quote on the line ?
         j = index(line, '"')
         !       print*,j, line(:20)
         if (j .ne. 0) then !  a quote must exist
            q_count = q_count+1
            !       PRINT*, q_count, 'A QUOTE IS PRESENT', line(:20)
            line = line(j+1:)
            !       PRINT*, 'New line for  QUOTE', line(:20)

         end if

         j = index(line, '"')
         if (j .ne. 0) then !  a quote must exist
            q_count = q_count+1
            !     PRINT*, q_count, 'A QUOTE IS PRESENT', line(:20)
            line = line(j+1:)
            !      PRINT*, 'New line for  QUOTE', line(:20)

         end if

         !    line = line(scan:)
         !       IF(line(1:1) .EQ. '"') THEN
         !        quoting = .TRUE.
         !        line = line(2:)
         !      END IF
         ! Drop separators from start of line
         !      DO i = 1, LEN(line)

         !        IF(SCAN('"', line(i:i)) .EQ. 0) THEN
         !        PRINT*, 'IN separators',separators, i, line(:50)
         !          line = line(i:)
         !        PRINT*, 'TRUE',separators, i, line(:50)
         !          EXIT
         !       END IF
         !      END DO
      end subroutine Skip2quotes

      subroutine ParseToken(line, token)
         character(len=*), intent(INOUT):: line
         character(len=*), intent(OUT)   :: token

         integer:: i
         logical:: quoting

         call DropInitialSeparators(line)

         ! add characters up until first separator
         token = ""
         quoting = .false.

         ! Try parse quote
         if (line(1:1) .eq. '"') then
            quoting = .true.
            line = line(2:)
         end if

         do i = 1, len(line)
            ! Scan up to delimiter
            if ( &
               (scan(separators//"=", line(i:i)) .ne. 0) &
               .or. &
               (quoting .and. (line(i:i) .eq. '"')) &
               ) then
               token = line(:i-1)
               line = line(i:)
               if (quoting) line = line(2:)
               exit
            end if
         end do
      end subroutine ParseToken

      subroutine ParseEq(line, ierr)
         character(len=*), intent(INOUT):: line
         integer, intent(OUT):: ierr

         integer:: i

         call DropInitialSeparators(line)

         ierr = PARSE_ERROR
         do i = 1, len(line)
            if (line(i:i) .eq. "=") then
               line = line(i+1:)
               ierr = PARSE_SUCCESS
               exit
            end if
         end do

      end subroutine ParseEq

      subroutine ParseEqOrDie(line, token)
         character(len=*), intent(INOUT):: line
         character(len=*), intent(IN):: token

         integer:: ierr

         call ParseEq(line, ierr)
         if (ierr .ne. PARSE_SUCCESS) then
            write (*, *) "ERROR: Expected = after ", trim(token)
            ERROR stop
         end if
      end subroutine ParseEqOrDie

   end subroutine ReadTecplotHeader

   !> Projects the vector `vec` on the S2^N tangent space of `mag`.
   !> Assumes `mag(i, :)` are unit vectors  i.e. in S2.
   !> The result is pointwise normal to mag
   !>
   !> @param[inout] vec The vector to project
   !> @param[in]    mag The space to project on to.
   !>                   Assumes `mag` contains only unit vectors.
   subroutine S2Project(vec, mag)
      use Tetrahedral_Mesh_Data, only: NNODE
      implicit none

      integer i
      double precision vec(NNODE, 3), mag(NNODE, 3)
      double precision sp

      do i = 1, NNODE
         sp = vec(i, 1)*mag(i, 1) + vec(i, 2)*mag(i, 2) + vec(i, 3)*mag(i, 3)
         vec(i, :) = vec(i, :) - sp*mag(i, :)
      end do

   end subroutine S2Project

   !> For t in [0, 1] calculates the magnetization state mt
   !> that interpolates at t between the magnetization states m1, m2
   !>
   !> @param[in]  m1 The magnetization at t = 0.
   !> @param[in]  m2 The magnetization at t = 1.
   !> @param[out] mt The interpolated magnetization.
   !> @param[in]  t  The interpolation parameter. Must be in [0, 1].
   subroutine PathInterpolate(m1, m2, mt, t)
      use Tetrahedral_Mesh_Data, only: NNODE
      use Material_Parameters, only: warnflag
      implicit none

      integer i
      double precision mt(:, :), m1(:, :), m2(:, :)
      double precision phi, stp, a, b, sp, t
      double precision sp_eps

      sp_eps = 1e-10

      do i = 1, NNODE
         ! scalar product m1.m2
         sp = m1(i, 1)*m2(i, 1) + m1(i, 2)*m2(i, 2) + m1(i, 3)*m2(i, 3)

         if (warnflag .eq. 1 .and. ((sp < -1.0) .or. (sp > 1.0))) then
            write (*, *) "WARNING: 'sp' value is out of range: ", sp
            ! Some code here to clamp 'sp' to [-1, 1]??
         end if

         if (abs(sp-1.0) < sp_eps) then  !  parallel : no change
            ! 1-eps < sp < 1+eps => -eps < sp-1 < eps
            !                    => |eps-1| < eps
            mt(i, 1) = m1(i, 1)
            mt(i, 2) = m1(i, 2)
            mt(i, 3) = m1(i, 3)
         else
            if (abs(sp+1.0) < sp_eps) then  ! antiparallel : choose the closer one
               ! -1-eps < sp < -1+eps => -eps < sp+1 < eps
               !                      => |sp+1| < eps
               mt(i, 1) = sign(m1(i, 1), 0.500003-t)
               mt(i, 2) = sign(m1(i, 2), 0.500003-t)
               mt(i, 3) = sign(m1(i, 3), 0.500003-t)
            else
               ! use the analytical linear interpolation in rotation angle
               phi = acos(sp)
               stp = sin(t*phi)
               a = cos(t*phi) - stp/tan(phi)
               b = stp/sin(phi)
               mt(i, 1) = a*m1(i, 1) + b*m2(i, 1)
               mt(i, 2) = a*m1(i, 2) + b*m2(i, 2)
               mt(i, 3) = a*m1(i, 3) + b*m2(i, 3)
            end if
         end if
      end do

      return
   end subroutine PathInterpolate
   !----------------!

   !> Interpolates the current path over `NewPathN` equidistant points.
   !> The initial and final states remain unchanged
   !>
   !> @param[in] NewPathN The number of path points to refine the path to
   subroutine RefinePathTo(NewPathN)
      use Tetrahedral_Mesh_Data, only: NNODE
      implicit none

      integer i, NewPathN, uind, mind, lind
      double precision, allocatable ::  NewPMag(:, :, :)
      double precision ndist, t

      call PathRenewDist()

      allocate (NewPMag(NewPathN, NNODE, 3))
      !         Initial and final states remain unchanged
      NewPMag(1, :, :) = PMag(1, :, :)
      NewPMag(NewPathN, :, :) = PMag(PathN, :, :)

      do i = 2, NewPathN-1
         ndist = PLength*(i-1)/(NewPathN-1)
         !               search insert position in CumulPDist such that
         !               CumulPDist(lind)<= ndist <=  CumulPDist(uind) and uind = lind+1
         !               By halving interval lengths -> O( Log2(Newpath) )

         lind = 1; uind = PathN
         do while (uind-lind > 1)
            mind = (uind+lind)/2
            if (CumulPDist(mind) <= ndist) then
               lind = mind
            else
               uind = mind
            end if
         end do
         !               t in (0, 1) is the correct position between lind/uind
         t = (ndist-CumulPDist(lind))/(CumulPDist(uind) - CumulPDist(lind))
         call PathInterpolate(PMag(lind, :, :), PMag(uind, :, :), NewPMag(i, :, :), t)  ! interpolate between old magnetizations

      end do

      PathN = NewPathN              ! Assign new path length
      call PathAllocate()         ! Deallocates old path allocates new sizes
      PMag(:, :, :) = NewPMag(:, :, :)  ! Assigns new path
      call PathRenewDist()            ! New calculation of distances
      PathChangedQ(:) = .true.
      deallocate (NewPMag)
   end subroutine RefinePathTo

   !> Calls the energy evaluation for position `pos` and
   !> calculates the gradient of the energy at `pos`
   !>
   !> @param[in] pos The position to calculate the energy and gradient at.
   subroutine PathEnergyAt(pos)
      use Tetrahedral_Mesh_Data, only: m, gradc, NNODE
      use Finite_Element_Matrices, only: FEMTolerance
      implicit none

      integer pos, neval

      double precision grad(3*NNODE), X(3*NNODE)

      !external  ET_GRAD

      neval = -100  ! Negative neval for ET_GRAD  -> keeps m(:,:) unchanged!!
      FEMTolerance = 1.d-10         ! Controlling tolerance of linear CG solver
      if (pos < 1 .or. pos > PathN) return

      m(:, :) = PMag(pos, :, :)

      call ET_GRAD(PEn(pos), grad, X, neval)

      PEGrad(pos, :, :) = gradc(:, :)

   end subroutine PathEnergyAt

   !> Calculates the energy for the structure at index `idx`.
   !>
   !> @param[in] idx The structure index to calculate the energy for
   !> @returns The energy at structure `idx`.
   function structureEnergy(idx) result(structEnergy)
      use Tetrahedral_Mesh_Data, only: m, NNODE
      use Finite_Element_Matrices, only: FEMTolerance
      use Material_Parameters
      implicit none

      ! Arguments and return values
      integer, intent(in)  :: idx
      double precision              :: structEnergy(NMaterials+1)

      ! Local variables
      integer                              :: neval
      double precision                     :: energy
      double precision, dimension(3*NNODE):: grad
      double precision, dimension(3*NNODE):: x
      integer:: i

      !external  ET_GRAD

      ! Function body
      neval = -100     ! negative value hack (see above).
      femTolerance = 1.d-10  ! CG solver tolerance (see above).

      m(:, :) = pmag(idx, :, :)

      ! x here is a dummy since we copy the magnetisation at index 'idx' over
      ! to m-subsequent energy functional calls in ET_GRAD*then use this
      ! value to compute energy (since neval is negative x is never used)

      call ET_GRAD(energy, grad, x, neval)
      structEnergy(1) = energy

      do i = 1, NMaterials
         structEnergy(i+1) = (AnisEnergSD(1, i) + AnisEnergSD(2, i) + BEnergSD(i) &
           & + DemagEnergSD(i) + ExchangeESD(i) + StressEnergSD(i))
         !WRITE(*,'(A32, I16)') "Component PATH Energies for SubDomain ID", SubDomainIds(i)
         !WRITE(*, '(A)') "Energies in units of J:"
         !WRITE(*, '(A16, ES16.8)') "E-Anis",  (AnisEnergSD(1, i) + AnisEnergSD(2, i)) /(SQRT(Ls)**3)
         !WRITE(*, '(A16, ES16.8)') "E-Ext",   BEnergSD(i)/(SQRT(Ls)**3)
         !WRITE(*, '(A16, ES16.8)') "E-Demag", DemagEnergSD(i)/(SQRT(Ls)**3)
         !WRITE(*, '(A16, ES16.8)') "E-Exch",  ExchangeESD(i)/(SQRT(Ls)**3)
         !WRITE(*,*)
      end do

      !structEnergy = energy
   end function structureEnergy

   !> Calculates the energy for the structure at index `idx`.
   !>
   !> @param[in] idx The structure index to calculate the energy for
   !> @returns The energy at structure `idx`.
   function structureEnergyALL(idx) result(structEnergyALL)
      use Material_Parameters
      use Finite_Element_Matrices, only: FEMTolerance
      use Tetrahedral_Mesh_Data, only: NNODE, m
      implicit none

      ! Arguments and return values
      integer, intent(in)  :: idx
      double precision              :: structEnergyALL(6, NMaterials+1)

      ! Local variables
      integer                              :: neval
      double precision                     :: energy, tot_energy, tot_demag, tot_exch, tot_anis &
        & , tot_BE, tot_stress
      double precision, dimension(3*NNODE):: grad
      double precision, dimension(3*NNODE):: x
      integer:: i

      !external  ET_GRAD

      ! Function body
      neval = -100     ! negative value hack (see above).
      femTolerance = 1.d-10  ! CG solver tolerance (see above).

      m(:, :) = pmag(idx, :, :)

      ! x here is a dummy since we copy the magnetisation at index 'idx' over
      ! to m-subsequent energy functional calls in ET_GRAD*then use this
      ! value to compute energy (since neval is negative x is never used)

      call ET_GRAD(energy, grad, x, neval)
      structEnergyAll(:, :) = 0.0

      tot_energy = 0
      tot_anis = 0
      tot_BE = 0
      tot_demag = 0
      tot_exch = 0
      tot_stress = 0
      do i = 1, NMaterials

         structEnergyALL(1, i+1) = (AnisEnergSD(1, i) + AnisEnergSD(2, i) + BEnergSD(i) &
           & + DemagEnergSD(i) + ExchangeESD(i) + StressEnergSD(i))

         structEnergyALL(2, i+1) = DemagEnergSD(i)
         structEnergyALL(3, i+1) = ExchangeESD(i)
         structEnergyALL(4, i+1) = AnisEnergSD(1, i) + AnisEnergSD(2, i)
         structEnergyALL(5, i+1) = BEnergSD(i)
         structEnergyALL(6, i+1) = StressEnergSD(i)

         tot_energy = tot_energy+structEnergyALL(1, i+1)
         tot_demag = tot_demag+DemagEnergSD(i)
         tot_exch = tot_exch+ExchangeESD(i)
         tot_anis = tot_anis+AnisEnergSD(1, i) + AnisEnergSD(2, i)
         tot_BE = tot_BE+BEnergSD(i)
         tot_stress = tot_stress+StressEnergSD(i)

         !WRITE(*,'(A32, I16)') "Component PATH Energies for SubDomain ID", SubDomainIds(i)
         !WRITE(*, '(A)') "Energies in units of J:"
         !WRITE(*, '(A16, ES16.8)') "E-Anis",  (AnisEnergSD(1, i) + AnisEnergSD(2, i)) /(SQRT(Ls)**3)
         !WRITE(*, '(A16, ES16.8)') "E-Ext",   BEnergSD(i)/(SQRT(Ls)**3)
         !WRITE(*, '(A16, ES16.8)') "E-Demag", DemagEnergSD(i)/(SQRT(Ls)**3)
         !WRITE(*, '(A16, ES16.8)') "E-Exch",  ExchangeESD(i)/(SQRT(Ls)**3)
         !WRITE(*,*)
      end do

      structEnergyALL(1, 1) = tot_energy
      structEnergyALL(2, 1) = tot_demag
      structEnergyALL(3, 1) = tot_exch
      structEnergyALL(4, 1) = tot_anis
      structEnergyALL(5, 1) = tot_BE
      structEnergyALL(6, 1) = tot_stress
      !structEnergy = energy
   end function structureEnergyALL

   !> Calculates the energy for the current magnetisation, `m`.
   !>
   !> @returns The energy for the current magnetization
   function currentMagEnergy() result(currEnergy)
      use Finite_Element_Matrices, only: FEMTolerance
      use Tetrahedral_Mesh_Data, only: NNODE
      implicit none

      ! Return values
      double precision:: currEnergy

      ! Local variables
      integer                              :: neval
      double precision                     :: energy, dgscale
      double precision, dimension(2*NNODE):: grad
      double precision, dimension(2*NNODE):: x

      !external ET_GRAD

      ! Function body
      neval = -100          ! negative value hack.
      dgscale = 0.0
      femTolerance = 1.d-10  ! CG solver tolerance.

      ! Assume that global m(:,:) contains magnetisation
      call ET_GRAD(energy, grad, x, neval)

      currEnergy = energy
   end function currentMagEnergy

   !> Calls pathenergyat for each changed position, `pos`, and
   !> calculates energy at that position.
   subroutine CalcPathAction()
      use slatec_avint
      !  SUBROUTINE AVINT (X, Y, N, XLO, XUP, ANS, IERR)
      !  Integration routine Davis & RabinowitzT
      use Utils, only: NONZERO
      use Material_Parameters, only: kd
      use Magnetization_Path_Utils, only: MagNorm, ScalarProd
      use Tetrahedral_Mesh_Data, only: NNODE, total_volume
      implicit none

      integer pos, IERR
      real(KIND = DP) sp, PCurv(NNODE, 3), EnScale
      logical:: RecalculateQ

      EnScale = Kd*total_volume  ! Energy scale to transform into units of Kd V
      ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls

      RecalculateQ = .true.

      do while (RecalculateQ)
         call PathRenewDist()  ! Gets distances renewed
         RecalculateQ = .false.
         do pos = 1, PathN
            if (PathChangedQ(pos)) then
               call PathEnergyAt(pos)  ! if magnetization has changed calculate new energy and gradient
               call S2Project(PEGrad(pos, :, :), PMag(pos, :, :))  ! project gradient in S2^N tangent space
               PEGradNorm(pos) = MagNorm(PEGrad(pos, :, :))
               PathChangedQ(pos) = .false.
            end if
         end do

         ! Check if end energies are not local minima
         !  in this case take the neighboring state as new end state

         !******** NOTE**********
         ! This search to enure path endpoints are minimum energies of the path
         ! sometimes causes an error when trigerred for low-reolution meshes
         ! Since its easy enough to sub-sample the path, its best to deactivate
         ! this automatic search for now.

         ! If(PEn(1)>1.03*PEn(2)) then
         !    PMag(1, :,:)=PMag(2, :,:)
         !    call PathInterpolate(PMag(1, :,:), PMag(3, :,:), PMag(2, :,:), 0.5d0)  ! interpolate new 2. state
         !    write(*,*) '--> New Minimum at path position 1'
         !    PathChangedQ(1)=.TRUE.
         !    PathChangedQ(2)=.TRUE.
         !    RecalculateQ=.TRUE.
         ! End If
         ! If(PEn(PathN)>1.03*PEn(PathN-1)) then
         !    PMag(PathN, :,:)=PMag(PathN-1, :,:)
         !    call PathInterpolate(PMag(PathN, :,:), PMag(PathN-2, :,:), PMag(PathN-1, :,:), 0.5d0)  ! interpolate new-2. state
         !    write(*,*) '--> New Minimum at path position ',PathN
         !    PathChangedQ(PathN)=.TRUE.
         !    PathChangedQ(PathN-1)=.TRUE.
         !    RecalculateQ=.TRUE.
         ! End If
      end do

      call HenkelmanTangents()  ! Gets  tangents renewed by Henkelman method using new energies

      do pos = 1, PathN
         if (PEGradNorm(pos) > 0.) then
            sp = ScalarProd(PTan(pos, :, :), PEGrad(pos, :, :))/PEGradNorm(pos)
         else
            sp = 1.
         end if
         PAlign(pos) = acos(abs(sp))  ! PAlign is angular deviation from gradient to tangent

         PNEBGrad(pos, :, :) = PEGrad(pos, :, :) - (sp-PSpringForce(pos))*PTan(pos, :, :)
      end do
      ! Integration of energy-gradient norm along the path yields PAction
      !           write (*,*) (CumulPDist(pos), pos = 1, PathN)
      !           write (*,*) (PEGradNorm(pos), pos = 1, PathN)

      call AVINT(CumulPDist, PEGradNorm, PathN, CumulPDist(1), CumulPDist(PathN), PAction, IERR)
      do pos = 2, PathN-1
         if (NONZERO(CurveWeight)) then
            !  curvature at pos
            PCurv(:, :) = (PTan(pos+1, :, :) - PTan(pos-1, :, :))/(PDist(pos-1) + PDist(pos))
            ! project curvature in S2^N tangent space
            call S2Project(PCurv(:, :), PMag(pos, :, :))
            PNEBGrad(pos, :, :) = PNEBGrad(pos, :, :) - PEGradNorm(pos)*CurveWeight*PCurv(:, :)
         end if
         if (NONZERO(PHooke)) then
            PTarget = PAction+PSpringForce(pos)**2/PHooke/2
         else
            PTarget = PAction
         end if
      end do
      call AVINT(CumulPDist, PAlign, PathN, CumulPDist(1), CumulPDist(PathN), PDeltaGeodesic, IERR)
      ! average angular gradient deviation from tangent along path
      PDeltaGeodesic = PDeltaGeodesic/PLength
      if (PathLoggingQ) then
         write (PLogEnUnit, *) (PEn(pos)/EnScale, pos = 1, PathN)
         write (PLogGradUnit, *) (PEGradNorm(pos)/EnScale, pos = 1, PathN)
         write (PLogDistUnit, *) (CumulPDist(pos), pos = 1, PathN)
      end if

   end subroutine CalcPathAction

   !> Estimate the variability of the energy near position `pos` in the path, 
   !> and set `InitAlpha` to a reasonable value using it.
   !>
   !> This is used to obtain an estimate for `InitAlpha` which must be chose to
   !> be large enough to constrain the distance but small enough to realistically
   !> reflect the magnetic energy
   !>
   !> @param[in] pos The position in the path to estimate the energy variability.
   subroutine EnergyVariability(pos)
      use Tetrahedral_Mesh_Data, only: m, NNODE, ModifyMag
      use Finite_Element_Matrices, only: FEMTolerance
      use Energy_Calculator, only: AddInitPathEnergyQ, InitAlpha, InitDelta
      implicit none

      integer pos, n, i, neval

      real(KIND = DP) angle, EZero, ModEnergy, rvar, uvar
      double precision grad(3*NNODE), X(3*NNODE)
      character(LEN = 20):: str

      AddInitPathEnergyQ = .false.

      write (*, *) "INFO: EnergyVariability(pos)"

      if (pos < 1 .or. pos > PathN) return
      m(:, :) = PMag(pos, :, :)
      neval = -100  ! Negative neval for ET_GRAD  -> keeps m(:,:) unchanged!!
      FEMTolerance = 1.d-10         ! Controlling tolerance of linear CG solver

      do n = 1, NNODE
         X(3*n - 2) = m(n, 1)
         X(3*n - 1) = m(n, 2)
         X(3*n - 0) = m(n, 3)
      end do

      call ET_GRAD(EZero, grad, X, neval)

      rvar = 0.
      uvar = 0.
      do i = 1, 3
         angle = 1.0*i
         m(:, :) = PMag(pos, :, :)
         str = 'random'
         call ModifyMag(str, angle)
         call ET_GRAD(ModEnergy, grad, X, neval)
         InitEVar(i) = ModEnergy-EZero
         rvar = rvar + (InitEVar(i)/angle/angle)**2

         m(:, :) = PMag(pos, :, :)
         str = 'uniform'
         call ModifyMag(str, angle)
         call ET_GRAD(ModEnergy, grad, X, neval)
         InitEVar(i+3) = ModEnergy-EZero
         uvar = uvar + (InitEVar(i+3)/angle)**2
      end do

      ! approximate energy variation per degree angular distance
      InitAlpha = sqrt(uvar) + sqrt(rvar)
      write (*, *) 'QQ EV sqrt(uvar) =', sqrt(uvar)
      write (*, *) 'QQ EV sqrt(rvar) =', sqrt(rvar)

      ! The more states the path has the better each distance should be kept
      ! set at Energy_Calculator module data:
      InitAlpha = 4*PathN*PathN*InitAlpha
      InitDelta = 1.5

      write (*, *) 'QQ EV InitAlpha =', InitAlpha

   end subroutine EnergyVariability

   !> Make an initial magnetization path
   !>
   !> assumes that PathN is set and Initial and final states are loaded
   !> 1) Estimates InitAlpha using EnergyVariability(pos)
   !> 2) Minimizes modified energy along the path
   !>     to force states of prescribed distances
   subroutine MakeInitialPath()
      use Tetrahedral_Mesh_Data, only: m, ModifyMag
      use Material_Parameters, only: MaxEnergyEval, MaxRestarts
      use Magnetization_Path_Utils, only: Distance
      use Minimizing_Loops, only: EnergyMinMult
      ! set, update these quantities in energy calculator module data
      ! before they are used by calling EnergyMin() with AddinitPathEnergyQ=.true. , 
      ! all only used here in initial step
      use Energy_Calculator, only: AddInitPathEnergyQ, InitDelta, InitAlpha, PMagInit 
      implicit none

      integer i, TMPRestart, TMPEval

      real(KIND = DP) angle

      character(LEN = 20):: str

      PathEndDist = Distance(PMag(1, :, :), PMag(PathN, :, :))
      write (*, *) 'QQ MIP PathEndDist=', PathEndDist

      TMPRestart = MaxRestarts  ! Stores value of MaxRestarts
      MaxRestarts = 1           ! No restarts for intermediate Minima
      TMPEval = MaxEnergyEval  ! Stores value of MaxEnergyEval
      ! MaxEnergyEval = 1000      ! No more than so many energy evaluations for intermediate states

      call EnergyVariability(PathN)

      InitRefPos = 1
      AddInitPathEnergyQ = .true.
      ! copy PMag(InitRefPos, :,:) to EnergyCalculator module data :
      PMagInit(:,:) = Pmag(InitRefPos, :, :)
      do i = PathN-1, 2, -1
         if (i .eq. (PathN-1)) write (*, *) 'Note that first and last Path image is fixed'
         write (*, *) 'Path NUMBER = ', i
         InitDelta = PathEndDist/(PathN-1)*(i-1)
         m(:, :) = PMag(i+1, :, :)
         str = 'random'
         angle = 2.0
         call ModifyMag(str, angle)
         call EnergyMinMult()
         PMag(i, :, :) = m(:, :)
         ! if PMag(InitREfPos = 1, :, :) changed copy to PmagInit... 
         ! TODO not sure if needed
         if (i == InitRefPos) then 
            PMagInit(:,:) = Pmag(InitRefPos, :, :)
         endif 
         PathChangedQ(i) = .true.
      end do
      AddInitPathEnergyQ = .false.

      call PathRenewDist()

      MaxRestarts = TMPRestart  ! Restores value of MaxRestarts
      MaxEnergyEval = TMPEval   ! Restores value of MaxEnergyEval

   end subroutine MakeInitialPath

   !> Minimize the path
   !>
   !> Uses an adaptive minimization routine to change the path
   !> towards minimal geometric action along the pseudo gradient
   !> given by Henkelmann&Jonsson(2000) NEB
   subroutine PathMinimize()

      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices
      use Magnetization_Path_Utils, only: MagNorm

      implicit none

      integer:: CREEPCNT, MAXCREEP, FINISHED, FTrailLength
      integer:: nstart, i, ResetCnt, NEval, pos, pn

      real(KIND = DP), allocatable:: POldMag(:, :, :), POldNEBGrad(:, :, :)

      double precision:: ALPHA, DALPHA, OldTarget, DeltaF, GSQUARE, &
         TOLERANCE, GTOL, BestF, AlphaScale, norm, mnorm
      double precision, allocatable:: FTrail(:)

      allocate (POldMag(PathN, NNODE, 3), POldNEBGrad(PathN, NNODE, 3))

      MAXCREEP = 4         ! Controls the number of creep steps
      DALPHA = 2.6         ! Controls the alpha acceleration/deceleration

      FTrailLength = 13    ! Length of trailing action value for finishing
      allocate (FTrail(FTrailLength))
      TOLERANCE = 1.e-8    ! Finished if consecutive actions with distance FTrailLength are < TOLERANCE
      GTOL = 1.e-10        ! Finished if Average pseudo-gradient < GTOL
      nstart = 1
      FTrail(:) = 0.0
      BestF = 0.
      NEval = 0
      ResetCnt = 0
      AlphaScale = 0.001

      ! ensure vectors from initial path are normlized

      pn = 1
      do i = 1, NNODE
         mnorm = sqrt(PMag(pn, i, 1)**2+PMag(pn, i, 2)**2+PMag(pn, i, 3)**2)
         PMag(pn, i, 1) = PMag(pn, i, 1)/mnorm
         PMag(pn, i, 2) = PMag(pn, i, 2)/mnorm
         PMag(pn, i, 3) = PMag(pn, i, 3)/mnorm
         m(i, :) = PMag(pn, i, :)
      end do

      do pn = 2, PathN

         ! ensure unit normalisation
         do i = 1, NNODE
            mnorm = sqrt(PMag(pn, i, 1)**2+PMag(pn, i, 2)**2+PMag(pn, i, 3)**2)
            PMag(pn, i, 1) = PMag(pn, i, 1)/mnorm
            PMag(pn, i, 2) = PMag(pn, i, 2)/mnorm
            PMag(pn, i, 3) = PMag(pn, i, 3)/mnorm
         end do
      end do

10    call CalcPathAction()
      NEval = NEval+1

      OldTarget = PTarget
      POldMag(:, :, :) = PMag(:, :, :)
      POldNEBGrad(:, :, :) = PNEBGrad(:, :, :)

   if (modulo(NEval, 10) .eq. 0) write (*, '(I5, 6F15.6)') NEval, ALPHA, PLength, DeltaF/TOLERANCE, PDeltaGeodesic, PAction, PTarget
    if (modulo(NEval, 100) .eq. 1) write (*, '(A5, 6A15)') 'NEval', 'ALPHA', 'PLength', 'dS/TOL', 'D-Geodesic', 'PAction', 'PTarget'
      if (PathLoggingQ .and. (modulo(NEval, 500) .eq. 0)) then
         PathOutFile = PLogFile
         call WRITETecplotPath()
      end if

      FTrail(nstart) = PTarget
      nstart = nstart+1
      if (nstart > FTrailLength) nstart = 1
      ALPHA = 1.

      FINISHED = 0
30    CREEPCNT = 0
      do while (CREEPCNT < MAXCREEP)

         do pos = 2, PathN-1
            PMag(pos, :, :) = PMag(pos, :, :) - ALPHA*AlphaScale*PNEBGrad(pos, :, :)
            do i = 1, NNODE
               norm = sqrt(PMag(pos, i, 1)**2+PMag(pos, i, 2)**2+PMag(pos, i, 3)**2)
               PMag(pos, i, :) = PMag(pos, i, :)/norm
            end do
         end do

         PathChangedQ(:) = .true.

         call CalcPathAction()
         NEval = NEval+1

   if (modulo(NEval, 10) .eq. 0) write (*, '(I5, 6F15.6)') NEval, ALPHA, PLength, DeltaF/TOLERANCE, PDeltaGeodesic, PAction, PTarget
    if (modulo(NEval, 100) .eq. 1) write (*, '(A5, 6A15)') 'NEval', 'ALPHA', 'PLength', 'dS/TOL', 'D-Geodesic', 'PAction', 'PTarget'
         if (PathLoggingQ .and. (modulo(NEval, 500) .eq. 0)) then
            PathOutFile = PLogFile
            call WRITETecplotPath()
         end if

         FTrail(nstart) = PTarget
         nstart = nstart+1
         if (nstart > FTrailLength) nstart = 1

         GSQUARE = 0
         do i = 1, PathN
            GSQUARE = GSQUARE+MagNorm(PNEBGrad(i, :, :))**2
         end do
         GSQUARE = GSQUARE/PathN

         DeltaF = abs(FTrail(nstart) - PTarget)/FTrailLength  ! Average step difference between trailing F and new F

         if ((NEval > FTrailLength*10) .and. (DeltaF < TOLERANCE)) then
            write (*, *) 'Change in action DeltaS negligible:', DeltaF
            goto 100  ! FINISHED Delta F negligible
         end if
         if (NEval .ge. MaxPathEval) then
            write (*, *) 'MAX Path Evaluations reached!! DeltaS:', DeltaF
            goto 100  ! FINISHED TOO MANY PATH EVALUATIONS
         end if
         if (OldTarget < PTarget) then
            CREEPCNT = 0
            ALPHA = ALPHA/DALPHA/DALPHA
            !         Write(*,*) 'QQ HM-> ALPHA = ',ALPHA
            if (ALPHA < 1.e-3) then
               write (*, '(I5, 6F15.6, A10)') NEval, ALPHA, PLength, DeltaF/TOLERANCE, PDeltaGeodesic, PAction, PTarget, ' <Reset>'
               ResetCnt = ResetCnt+1
               BestF = OldTarget
               call RefinePathTo(PathN)  ! Resets the path to equidistant structures (smoothing  kinks?)
               PathChangedQ(:) = .true.
               if (ResetCnt > 20) then
                  write (*, *) '+++++   FAILED TO CONVERGE +++++'
                  goto 100
               end if
               goto 10 !    !!  ALPHA TOO SMALL: RESTART From worse state
            else
               PMag(:, :, :) = POldMag(:, :, :)
               PNEBGrad(:, :, :) = POldNEBGrad(:, :, :)
            end if

         else
            CREEPCNT = CREEPCNT+1
            !         Write(*,*) 'QQ HM     CREEP #',CREEPCNT
            OldTarget = PTarget
            POldMag(:, :, :) = PMag(:, :, :)
            POldNEBGrad(:, :, :) = PNEBGrad(:, :, :)
            if (GSQUARE < GTOL) then
               write (*, *) 'PSEUDO-GRADIENT Negligible:', GSQUARE
               goto 100  ! FINISHED Grad = 0
            end if
         end if
      end do
      ALPHA = DALPHA*ALPHA
      ResetCnt = 0
      !     Write(*,*) 'QQ HM-> ALPHA = ',ALPHA
      goto 30

100   write (*, *)
      write (*, *) '   MINIMIZATION FINISHED '
      write (*, *)
      write (*, '(A43, I5)') '   ||                 CalcPathAction Calls:', NEval
      write (*, '(A43, F15.6, A2)') '   ||                               Action:', PAction
      write (*, '(A43, F12.2)') '   ||                           DeltaS/TOL:', DeltaF/TOLERANCE
      write (*, '(A43, F12.2)') '   ||                    sqrt(grad^2/GTOL):', sqrt(GSQUARE/GTOL)

      deallocate (POldMag, POldNEBGrad)

      return

   end subroutine PathMinimize

end module Magnetization_Path

