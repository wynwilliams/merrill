
! never used EnergyCalculator object concept, being separated out from code in use ...

   !> Destroy and clean up the variables in this module.
   subroutine DestroyEnergyCalculator()
    integer:: i
    close (logunit)

    if (allocated(ExtraEnergyCalculators)) then
       do i = 1, size(ExtraEnergyCalculators)
          call ExtraEnergyCalculators(i)%Destroy()
       end do
       deallocate (ExtraEnergyCalculators)
    end if
 contains
    subroutine CloseIfOpen(unit)
       integer, intent(INOUT):: unit
       logical:: is_open

       is_open = .false.
       if (unit .ne. 0) inquire (UNIT = unit, OPENED = is_open)
       if (is_open) close (unit)

       unit = 0
    end subroutine CloseIfOpen
 end subroutine DestroyEnergyCalculator


!> Build all the objects necessary to use the Energy_Calculator module.
   subroutine BuildEnergyCalculator()
    implicit none

    integer:: i

    do i = 1, size(ExtraEnergyCalculators)
       if (associated(ExtraEnergyCalculators(i)%Build)) then
          call ExtraEnergyCalculators(i)%Build()
       end if
    end do
 end subroutine BuildEnergyCalculator

 !> Add a calculator to the ExtraEnergyCalculators array.
 !> eg
 !> @code
 !>     TYPE(EnergyCalculator):: my_calculator
 !>
 !>     ALLOCATE(my_calculator%h(NNODE, 3))
 !>     my_calculator%Initialize => my_calculator_initialize
 !>     my_calculator%Destroy    => my_calculator_destroy
 !>     my_calculator%Run        => my_calculator_run
 !>
 !>     CALL AddEnergyCalculator("amazing calculator", my_calculator)
 !> @endcode
 subroutine AddEnergyCalculator(name, calculator)
    use iso_c_binding
    implicit none

    character(len=*), intent(IN):: name
    type(EnergyCalculator), intent(INOUT):: calculator

    type(EnergyCalculator), allocatable:: tmp_eec(:)

    integer:: i

    ! Warn if name is already taken
    do i = 1, size(ExtraEnergyCalculators)
       if (trim(ExtraEnergyCalculators(i)%Name) .eq. trim(name)) then
          write (*, *) "WARNING: ", trim(name), " is already a calculator!"
       end if
    end do

    ! Allocate larger array and copy to it
    allocate (tmp_eec(size(ExtraEnergyCalculators) + 1))
    do i = 1, size(ExtraEnergyCalculators)
       tmp_eec(i) = ExtraEnergyCalculators(i)
    end do
    call move_alloc(tmp_eec, ExtraEnergyCalculators)

    ! Initialize and add the new calculator
    i = size(ExtraEnergyCalculators)
    ExtraEnergyCalculators(i) = calculator
    ExtraEnergyCalculators(i)%Name = name
    if (associated(ExtraEnergyCalculators(i)%Initialize)) then
       call ExtraEnergyCalculators(i)%Initialize()
    end if

 end subroutine AddEnergyCalculator

 !> Remove a calculator of a given name
 !> eg
 !> @code
 !>  CALL RemoveEnergyCalculator("my_derived_calculator")
 !> @endcode
 subroutine RemoveEnergyCalculator(name)
    implicit none

    character(len=*), intent(IN):: name

    type(EnergyCalculator), allocatable:: tmp_eec(:)

    integer:: i

    ! Pop off all calculators named name

    i = 0
    do
       i = i+1

       ! If we've gotten to the end, break the loop
       if (i .gt. size(ExtraEnergyCalculators)) then
          exit
       end if

       if (trim(ExtraEnergyCalculators(i)%Name) .eq. trim(name)) then
          write (*, *) "Removing calculator: ", trim(name)

          call ExtraEnergyCalculators(i)%Destroy()

          ! Pop off calculator i
          allocate (tmp_eec(size(ExtraEnergyCalculators) - 1))
          tmp_eec(1:i-1) = ExtraEnergyCalculators(1:i-1)
          tmp_eec(i:) = ExtraEnergyCalculators(i:size(tmp_eec))
          call move_alloc(tmp_eec, ExtraEnergyCalculators)

          ! Do loop again with the same value of i
          i = i-1
       end if

    end do

 end subroutine RemoveEnergyCalculator
