!> The LLG_Solver module contains the LLG fucntion (in )

module LLG_Solver
   use Utils

   implicit none
   real(KIND=kind(1.0d0)) :: Ttime, TOUT, RTOL, ATOL, AG, ETOT
   integer :: neval
   double precision, allocatable :: YDOT(:), rel_tols(:), abs_tols(:)

   real(KIND=DP), allocatable ::  heff(:), Y(:), mold_llg(:, :), mdot(:, :)

   ! INTERFACE
   !   !> Interface for the energy_calculator.et_grad procedure, passed to
   !   !> the HubertMinimize() function.
   !   SUBROUTINE ET_GRAD_PROCEDURE(ETOT, grad, X, neval)
   !     !>
   !     !> An interface for the energy_calculator.et_grad() subroutine.
   !     !>
   !     !> @param[out]   ETOT  The total magnetic energy.
   !     !> @param[out]   grad  The manetic energy gradient (in spherical polars)
   !     !> @param[in]    X     The initial guess for the LEM
   !     !>                     (in spherical polars)
   !     !> @param[inout] neval The number of times the function has been
   !     !>                     evaluated.
   !     !>
   !     USE Utils
   !     REAL(KIND=DP), INTENT(OUT) :: ETOT
   !     REAL(KIND=DP), INTENT(OUT) :: grad(:)
   !     REAL(KIND=DP), INTENT(IN)  :: X(:)
   !     INTEGER, INTENT(INOUT)     :: neval
   !   END SUBROUTINE ET_GRAD_PROCEDURE
   ! END INTERFACE

contains

!> Initialize the variables in this module
   subroutine InitializeLLG_Solver()

      implicit none

      call DestroyLLG_Solver()

   end subroutine InitializeLLG_Solver

!Destroy and clean up the variables in this module.

   subroutine DestroyLLG_Solver()

   end subroutine DestroyLLG_Solver

!>===========================================

   !---------------------------------------------------------------
   ! LLG  : LLG_ODE_Solver
   !---------------------------------------------------------------

   !> Performs integration of the Landau Lifshitz Gilbert ODE
   !>
   !> @param[in] LLGType The type of ODE integration to perform. Current one of
   !>                    Euler, RK4 or RKadapt.
   !> @param[in] DeltaT  The steplegth (in reducded time) on the indepedent
   !>                    variable (time).
   !> @param[in] start_t The start time, the initial value of the independent
   !>                    variable.
   !> @param[in] MaxDelta  The maximum allowable value of the steplength.
   !> @param[in] Delta_min The minimum alloabele value of the steplength.
   !> @param[in] abs_tols  A scalar value for desired accuracy, used in dermining
   !>                      the best adaptive steplength.
   !> @param[in] ag        The LLG damping parameter (usual valie in SI units = 1).
   !> @param[in] converge  A scalar convergence value (set to maxdiff) which exits
   !>                      when succesive iterations have changes in M/DeltaT< converge.
   subroutine LLG_ODE_solver(LLGtype, DeltaT, start_t, MaxDelta, &
     &  DELTAT_min, Abs_tol, ag, converge, LLGoutInterval)

      use MESH_IO
      use Finite_Element_Matrices
      use Tetrahedral_Mesh_Data
      use Energy_Calculator
      use Material_Parameters

      implicit none
      integer:: i, neval, itask, istate, mxstep, llg_counter, NEQ
      real(KIND=DP) :: m_length, maxdiff, magtotx, magtoty, magtotz, change, rtime, ag
      double precision :: start_t, End_t, MaxDelta, DELTAT_min, abs_tol, TimeStep, DeltaT, converge
      character(LEN=*), parameter :: FMT1 = "(F15.4, E15.4, E15.4, F17.10, 3F15.10, F15.10)"
      character(len=20) :: LLGtype
      double precision :: TypicalEnergy, invTE, LLGoutInterval, writeouttime, time_in, time_out

      allocate (heff(NNODE*3), mold_llg(NNODE, 3), mdot(NNODE, 3))

      Ttime = 0 ! initial time
      writeouttime = 0.
      llg_counter = 1
      End_t = MaxDelta

      if (stem /= "") then
         write (ZStr, '(F14.4)') Ttime! The zone name
         call Writetecplot(Zoneflag, stem, llg_counter)
         writeouttime = 0.
      end if
      !ag=1. ! the damping factor in LLG equation.
      maxdiff = 1.0d0

      llg_counter = 0
      !print*, 'converge= ',converge

      ! ==========  Energy Scaling =======================

      TypicalEnergy = 0.
      ! Calculate typical energy scale
      ! (order of magnitude estimate of micromagnetic energy)
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = &
            (sqrt(Ls)**3) &
            *sqrt(Kd*sqrt(Aex(maxloc(Ms, 1))*abs(K1(maxloc(Ms, 1))))) &
            *(total_volume/(sqrt(Ls)**3))**(5./6.)
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = Kd*total_volume
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = maxval(Aex)*Ls*total_volume**(1./3.)
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = maxval(abs(K1))*total_volume
      end if
      invTE = 100./TypicalEnergy
      ! =====================================================

      write (*, *) "    Comp Time  :Real Time (secs):  maxdiff   : Reduced Energy : average Mx  :  average My  :  average Mz"
      ! ===============================================
      ! ==========  start timestep loop ===============
      do while ((maxdiff > converge))
         maxdiff = 0.0d0
         llg_counter = llg_counter + 1
         time_in = Ttime

         !   ! normalize length of m
         do i = 1, NNODE
            ! print*,' normalize'
            m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
            if (m_length /= 0.) then
               m(i, 1) = m(i, 1)/m_length
               m(i, 2) = m(i, 2)/m_length
               m(i, 3) = m(i, 3)/m_length

            end if
         end do

         neval = 0

         if (LLGtype .eq. 'euler') then
            call eulersolver(Ttime, DELTAT, mDOT)
            TimeStep = DeltaT
         end if

         if (LLGtype .eq. 'rk4') then
            call RK4solver(Ttime, DELTAT, mDOT)
            TimeStep = DeltaT
         end if

         if (LLGtype .eq. 'rkadapt') then
            TimeStep = DELTAT

            call RKadapt(Start_t, End_t, Abs_tol, DELTAT, DELTAT_min)
            neval = neval + 1 !increment the counter
            Ttime = End_t
            start_t = End_t
            end_t = start_t + MaxDelta
            TimeStep = MaxDelta

         end if

         do i = 1, NNODE
            !print*, 'normalize the magnetization vector '
            m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
            if (m_length /= 0.) m(i, :) = m(i, :)/m_length
         end do

         ! initialize the max difference in any value of m(:,:) betwwen interations
         maxdiff = 0.0d0
         ! initilaze the average magetization components
         magtotx = 0.0d0
         magtoty = 0.0d0
         magtotz = 0.0d0

         do i = 1, NNODE

            magtotx = magtotx + m(i, 1)
            magtoty = magtoty + m(i, 2)
            magtotz = magtotz + m(i, 3)

! calculate the maximum change at any node
            change = (sqrt((m(i, 1) - mold_llg(i, 1))**2 &
          & + (m(i, 2) - mold_llg(i, 2))**2 + (m(i, 3) - mold_llg(i, 3))**2))/TimeStep
            if (change > maxdiff) then
               maxdiff = change
            end if

            mold_llg(i, 1) = m(i, 1)
            mold_llg(i, 2) = m(i, 2)
            mold_llg(i, 3) = m(i, 3)

         end do

         magtotx = magtotx/NNODE
         magtoty = magtoty/NNODE
         magtotz = magtotz/NNODE
         !        WRITE(*,*) "SOL",Ttime, maxdiff,magtotx,magtoty,magtotz
         if (mod(llg_counter, 100) == 1) then
            !   cpuold=cputime_now
            !   elapseDOld= elapsed_now
            ! call CPU_TIME(cputime_now)
            ! call SYSTEM_CLOCK(COUNT=elapsed_now)

            call reducedtime(Ttime, rtime)

            write (*, FMT1) Ttime, rtime, maxdiff, ETOT*invTE, magtotx, magtoty, magtotz
         end if

         if (mod(llg_counter, 1000) == 0) then
            write (*, *) "    Comp Time  :Real Time (secs):  maxdiff   : Reduced Energy : average Mx  :  average My  :  average Mz"

            call reducedtime(Ttime, rtime)

            write (*, FMT1) Ttime, rtime, maxdiff, ETOT*invTE, magtotx, magtoty, magtotz
         end if

         !   write intermdiate output tecplot file if filename  exists (stem/="")  and if
         !   the writeouttime > LLGoutInterval
         time_out = Ttime
         writeouttime = writeouttime + (time_out - time_in)
         if ((stem /= "") .and. (writeouttime .ge. LLGoutInterval)) then
            write (ZStr, '(F14.4)') Ttime! The zone name
            call Writetecplot(Zoneflag, stem, llg_counter)
            writeouttime = 0.
         end if

      end do ! for the do while

      deallocate (heff)
      deallocate (mdot)
      deallocate (mold_llg)
      return
   end subroutine LLG_ODE_solver

   !---------------------------------------------------------------
   ! LLG  : Eulersolver
   !---------------------------------------------------------------

   !> Performs simple Euler integration of the LLG ODE.
   !>
   !> @param[in] DeltaT      Scalar,  the steplength (in reducded time) on the
   !>                        indepedent variable (time).
   !> @param[inout] Ttime    The curent value of the independet vairable.
   !> @param[out] mdot       Array, The current right value of the LLG right hand side.
   subroutine eulersolver(Ttime, DELTAT, mDOT)
      use Material_Parameters
      use Energy_Calculator
      use Tetrahedral_Mesh_Data, only: NNODE, m
      implicit none
      integer::  i
      double precision, intent(IN) :: DELTAT
      double precision, intent(OUT) :: mDOT(NNODE, 3)
      double precision, intent(INOUT) :: Ttime

      ! CALL EFFECTIVE_field(ETOT, neval)
      ! call calc_LLG(ag,mDOT)
      call EFFECTIVE_field(ETOT, neval, ag, mdot)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, 1) = m(i, 1) + DELTAT*(mdot(i, 1))
         m(i, 2) = m(i, 2) + DELTAT*(mdot(i, 2))
         m(i, 3) = m(i, 3) + DELTAT*(mdot(i, 3))
      end do
      Ttime = Ttime + DeltaT
      return
   end subroutine eulersolver

   !---------------------------------------------------------------
   ! LLG  : RK4solver
   !---------------------------------------------------------------

   !> Performs 4th order Runge-Kutta integration of the LLG ODE.
   !>
   !> @param[in] DeltaT      Scalar,  the steplength (in reducded time) on the
   !>                        indepedent variable (time).
   !> @param[inout] Ttime    The curent value of the independet vairable.
   !> @param[out] mdot       Array, The current right value of the LLG right hand side.
   subroutine RK4solver(Ttime, DELTAT, mDOT) ! 4th order Runge-Kutta
      use Material_Parameters
      use Energy_Calculator
      use Tetrahedral_Mesh_Data, only: m, NNODE
      implicit none
      integer::  i
      double precision, intent(IN) :: DELTAT
      double precision, intent(OUT) :: mDOT(NNODE, 3)
      double precision, intent(INOUT) :: Ttime
      double precision :: mDOT2(NNODE, 3), mDOT3(NNODE, 3), m0(NNODE, 3)
      double precision :: m_length, DT1, DT2, DT3

      DT1 = DELTAT*0.5
      DT2 = DELTAT + DT1
      DT3 = DELTAT/6.

      do i = 1, NNODE
         m0(i, :) = m(i, :)
      end do
      !     print*,'m in', m0(1,:)
!   step1
      call EFFECTIVE_field(ETOT, neval, ag, mdot)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + DT1*(mdot(i, :))
         m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
      end do

!   step2
      call EFFECTIVE_field(ETOT, neval, ag, mdot2)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + DT1*(mdot2(i, :))
         m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
      end do

!   step3
      call EFFECTIVE_field(ETOT, neval, ag, mdot3)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + DELTAT*(mdot3(i, :))
         m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
         mdot3(i, :) = mdot2(i, :) + mdot3(i, :)
      end do

!   step4
      call EFFECTIVE_field(ETOT, neval, ag, mdot2)     !re-use mdot2
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + DT3*(mdot(i, :) + mdot2(i, :) + 2*mdot3(i, :))
         m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
      end do

      Ttime = Ttime + DeltaT

      return
   end subroutine RK4solver

   !---------------------------------------------------------------
   ! LLG  : RKadapt
   !---------------------------------------------------------------

   !> Performs adaptive timestep Runge-Kutta integration of the LLG ODE.
   !> This updates mdot, The current right value of the LLG right hand side.
   !>
   !> @param[in] start_t The start time, the initial value of the independent
   !>                    variable.
   !> @param[in] end_t   The end time, the final time value of the independent
   !>                    variable where integration stops.
   !> @param[in] abs_tols  A scalar value for desired accuracy, used in dermining
   !>                      the best adaptive steplength.
   !> @param[in] DeltaT    Scalar,  the steplength (in reducded time) on the
   !>                      indepedent variable (time).
   !> @param[in] DeltaT_min  The minim allowed value of the steplenth.
   !> @param[out] mdot       Array, The current right value of the LLG right hand side.
   subroutine RKadapt(Start_t, End_t, Abs_tol, DELTAT, DELTAT_min)
      use Material_Parameters
      use Energy_Calculator
      use Tetrahedral_Mesh_Data, only: m, NNODe
      implicit none
      double precision, intent(IN) :: Start_t, End_t, Abs_tol, DELTAT, DELTAT_min

      double precision, parameter :: TINY = 1.0d-30
      integer, parameter :: MAXSTP = 10000
      integer :: nstp
      integer :: nok, nbad, kount, i
      double precision :: t_step, DeltaDid, DeltaNext, Ttime, xsav, m_length, x
      double precision, dimension(NNODE, 3) :: mdot, mscal, m0, m_start
      x = Start_t
      t_step = sign(DELTAT, End_t - Start_t)
      nok = 0
      nbad = 0
      kount = 0
      m0(:, :) = m(:, :)
      m_start(:, :) = m(:, :)

      do nstp = 1, MAXSTP

         !   call derivs(x,y,dydx)
         !   yscal(:)=abs(y(:))+abs(h*dydx(:))+TINY
         call EFFECTIVE_field(ETOT, neval, ag, mdot)
         ! print*,'m,mdot', m(1,:),mdot(1,:)
         do i = 1, NNODE
            mscal(i, :) = t_step*mdot(i, :)
            m_length = sqrt(mscal(i, 1)**2 + mscal(i, 2)**2 + mscal(i, 3)**2)
            if (m_length /= 0.) mscal(i, :) = mscal(i, :)/m_length
            mscal(i, :) = abs(m(i, :)) + abs(mscal(i, :)) + TINY
         end do

         if ((x + t_step - End_t)*(x + t_step - Start_t) > 0.0) t_step = End_t - x
         !print*, 'TAKE A STEP'
         call OneRKstep(mdot, x, t_step, Abs_tol, mscal, DeltaDid, DeltaNext)
         !call OneBSstep(mdot,x,t_step,Abs_tol,mscal,DeltaDid,DeltaNext)
         !     print*, 'ONE STEP TAKEN'
         if (DeltaDid == t_step) then
            nok = nok + 1
         else
            nbad = nbad + 1
         end if
         if ((x - End_t)*(End_t - Start_t) >= 0.0) then
            m_start(:, :) = m(:, :)

            return
         end if
         if (abs(DeltaNext) < DELTAT_min) then
            write (*, *) 'stepsize smaller than minimum in RKadapt - programme stopping'
            stop
         end if
         t_step = DeltaNext
      end do
      write (*, *) 'too many steps in RKadapt'
      stop

   end subroutine RKadapt

   subroutine OneRKStep(mdot, x, step_try, Abs_tol, mscal, DeltaDid, DeltaNext)
      use Material_Parameters
      use Energy_Calculator
      use Tetrahedral_Mesh_Data, only: NNODE, m
      implicit none
      !double precision, DIMENSION(:), INTENT(INOUT) :: y
      double precision, dimension(NNODE, 3), intent(IN) :: mdot, mscal
      double precision, intent(INOUT) :: x
      double precision, intent(IN) :: step_try, Abs_tol
      double precision, intent(OUT) :: DeltaDid, DeltaNext

      integer :: ndum
      double precision :: errmax, h, htemp, xnew
      double precision, dimension(NNODE, 3) :: merr, mtemp
      double precision, parameter :: SAFETY = 0.9, PGROW = -0.2, PSHRNK = -0.25,&
         & ERRCON = 1.89e-4

      h = step_try
      do
         !      print*, 'call CKRK'
         call CKRKs(mdot, x, h, mtemp, merr)
         errmax = maxval(abs(merr(:, :)/mscal(:, :)))/Abs_tol
         !      print*, 'errmax',  errmax
         if (errmax <= 1.0) exit
         htemp = SAFETY*h*(errmax**PSHRNK)
         h = sign(max(abs(htemp), 0.1*abs(h)), h)
         xnew = x + h
         if (xnew == x) then
            write (*, *) 'stepsize underflow in RKadapt, programme stopping'
            stop
         end if
      end do
      if (errmax > ERRCON) then
         DeltaNext = SAFETY*h*(errmax**PGROW)
      else
         DeltaNext = 5.0*h
      end if
      DeltaDid = h
      x = x + h
      m(:, :) = mtemp(:, :)
   end subroutine OneRKStep

   subroutine CKRKs(mDOT, Ttime, DELTAT, mout, merr)
      use Material_Parameters
      use Energy_Calculator
      use Tetrahedral_Mesh_Data, only: NNODE, m
      !USE nrtype; USE nrutil, ONLY : assert_eq
      implicit none
      integer :: i
      double precision, intent(IN) :: Ttime, DELTAT
      double precision, dimension(NNODE, 3), intent(OUT) :: mout, merr
      double precision, dimension(NNODE, 3) :: mdot, mdot2, mdot3, mdot4, mdot5, mdot6, mtemp, m0
      double precision :: m_length
      double precision, parameter :: A2 = 0.2, A3 = 0.3, A4 = 0.6, A5 = 1.0, &
         A6 = 0.875, B21 = 0.2, B31 = 3.0/40.0, B32 = 9.0/40.0, &
         B41 = 0.3, B42 = -0.9, B43 = 1.2, B51 = -11.0/54.0, &
         B52 = 2.5, B53 = -70.0/27.0, B54 = 35.0/27.0, &
         B61 = 1631.0/55296.0, B62 = 175.0/512.0, &
         B63 = 575.0/13824.0, B64 = 44275.0/110592.0, &
         B65 = 253.0/4096.0, C1 = 37.0/378.0, &
         C3 = 250.0/621.0, C4 = 125.0/594.0, &
         C6 = 512.0/1771.0, DC1 = C1 - 2825.0/27648.0, &
         DC3 = C3 - 18575.0/48384.0, DC4 = C4 - 13525.0/55296.0, &
         DC5 = -277.0/14336.0, DC6 = C6 - 0.25
      ! here requires another call to external field - not ino riginal

      do i = 1, NNODE ! store the original magnetization and use m for intermediate updates
         m0(i, :) = m(i, :)
      end do

!   ytemp=y+B21*h*dydx
      call EFFECTIVE_field(ETOT, neval, ag, mdot)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + B21*DELTAT*(mdot(i, :)) ! step= .21xDETLTAT
         m_length = sqrt(mtemp(i, 1)**2 + mtemp(i, 2)**2 + mtemp(i, 3)**2)
         if (m_length /= 0.) mtemp(i, :) = mtemp(i, :)/m_length
      end do

      ! call derivs(x+A2*h,ytemp,mdot2)
      ! ytemp=y+h*(B31*mdot+B32*mdot2)
      call EFFECTIVE_field(ETOT, neval, ag, mdot2)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + DELTAT*(B31*mdot(i, :) + B32*mdot2(i, :))
         m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
      end do

      ! call derivs(x+A3*h,ytemp,mdot3)
      ! ytemp=y+h*(B41*mdot+B42*mdot2+B43*mdot3)
      call EFFECTIVE_field(ETOT, neval, ag, mdot3)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + DELTAT*(B41*mdot(i, :) + B42*mdot2(i, :) + &
           &  B43*mdot3(i, :))
         m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
      end do

      ! call derivs(x+A4*h,ytemp,mdot4)
      ! ytemp=y+h*(B51*mdot+B52*ak2+B53*mdot3+B54*mdot4)
      call EFFECTIVE_field(ETOT, neval, ag, mdot4)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + DELTAT*(B51*mdot(i, :) + B52*mdot2(i, :) + &
             &   B53*mdot3(i, :) + B54*mdot4(i, :))
         m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
      end do

      ! call derivs(x+A5*h,ytemp,mdot5)
      ! ytemp=y+h*(B61*mdot+B62*ak2+B63*mdot3+B64*mdot4+B65*mdot5)
      call EFFECTIVE_field(ETOT, neval, ag, mdot5)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         m(i, :) = m0(i, :) + DELTAT*(B61*mdot(i, :) + B62*mdot2(i, :) + &
             &   B63*mdot3(i, :) + B64*mdot4(i, :) + B65*mdot5(i, :))
         m_length = sqrt(m(i, 1)**2 + m(i, 2)**2 + m(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
      end do

      ! call derivs(x+A6*h,ytemp,mdot6)
      ! yout=y+h*(C1*mdot+C3*ak3+C4*ak4+C6*mdot6)
      ! yerr=h*(DC1*mdot+DC3*mdot3+DC4*mdot4+DC5*mdot5+DC6*mdot6)
      call EFFECTIVE_field(ETOT, neval, ag, mdot6)
      ! print*,'m,mdot', m(1,:),mdot(1,:)
      do i = 1, NNODE
         mout(i, :) = m0(i, :) + DELTAT*(C1*mdot(i, :) + C3*mdot3(i, :) + &
             &   C4*mdot4(i, :) + C6*mdot6(i, :))
         m_length = sqrt(mout(i, 1)**2 + mout(i, 2)**2 + mout(i, 3)**2)
         if (m_length /= 0.) m(i, :) = m(i, :)/m_length
         merr(i, :) = DELTAT*(DC1*mdot(i, :) + DC3*mdot3(i, :) +  &
             & DC4*mdot4(I, :) + DC5*mdot5(i, :) + DC6*mdot6(i, :))

      end do

   end subroutine CKRKs

   subroutine reducedtime(Ttime, rtime)
      use Material_Parameters
      use Energy_Calculator
      double precision gam, rtime, k1max, msmax, gmag, Hk, Ttime
      !pi=4.0d0*atan(1.0d0)
      !gam=2.210173d5 ! old val
      ! gyromag ratio of electron (SI units of rad. s^-1 .Tesla)
      gmag = 1.76085963d11 !(from wikipedia)

      ! timestep scaling

      k1max = maxval(abs(K1))
      Msmax = maxval(ms)
      Hk = 2*K1max/Msmax

      rtime = Ttime/gmag
      if (k1max /= 0.) then
         rtime = Ttime/(gmag*Hk)
      end if

! !     reduce size of time step for k1=0
! !     and reset rtime in SI only
!       IF (k1max==0.) THEN
!       mcon(4)=(mcon(4)*1.d-3)
!       IF (mcon(6)/=1.) THEN
!       rtime=rtime*1.d4
!       ENDIF
!       ENDIF

      !     WRITE(*,*) 'Reduced time step (seconds)',rtime

      return
   end subroutine reducedtime

end module LLG_Solver
