!> The Energy_Calculator module contains the calculators for the various
!> magnetic energies, including the demagnetizing, anisotropy and exchange
!> energies.
module Energy_Calculator
   use Utils, only: DP
   use Tetrahedral_Mesh_Data, only: NNODE
   implicit none

   !> The energy log file name
   character(LEN = 1024):: logfile

   !> The unit for the energy log file
   ! ww set logunit to zero removed the = 0
   integer:: logunit

   !> If .TRUE., then the energy should be logged to the energy log file.
   logical:: EnergyLogging

   !> If .TRUE., then run all the exchange calculators.
   logical:: CalcAllExchQ

   !> The number of bad tetrahedra in the mesh
   integer:: NBadTets

   ! ! Set from Magnetization_Path, read here for initial energy calculation 

   !> flag to add energy relative to magnetization state 1 on initial round
   logical:: AddInitPathEnergyQ = .false.

   !> Alpha for initial path determination: typical energy variation
   real(KIND = DP):: InitAlpha

   !> Intended distance from initial state during creation of initial path
   real(KIND = DP):: InitDelta
   
   ! Log file header flag
   integer:: firstrow = 0

   ! read-only local copy of PMag(InitRefPos, :,:), usually InitRefPos = 1
   real (KIND = DP), dimension(:,:), allocatable:: PMagInit  

   !real (DP), dimension(:), allocatable:: totphi, phi1, phi2

contains

   !> Initialize the variables in this module
   subroutine InitializeEnergyCalculator()
      implicit none

      !call DestroyEnergyCalculator()

      !    logunit = 0

      ! (no logfile is written)
      EnergyLogging = .false.

      ! To speed up calculation : .false. suppresses calculation of
      !   unused exchange energies
      CalcAllExchQ = .false.

      NBadTets = 0

      ! allocate (ExtraEnergyCalculators(0))

      !CALL InitializeMagnetizationPathPointers( &
      !   BuildEnergyCalculator, EnergyMinMult, ET_GRAD_CART &
      !   ) 
   end subroutine InitializeEnergyCalculator



   
   !---------------------------------------------------------------
   ! BEM  : ET_GRAD(ETOT, G, X, neval)
   !        Only remaining energy and gradient function
   !---------------------------------------------------------------
   !> Update all the magnetic fields and energies and return the energy
   !> gradient in spherical polar form (with the radial component dropped).
   !>
   !> @param[in]    axis  character 'X', 'Y', or 'Z', picks axis: chooses permutation of axis labels in coordinate conversions
   !> @param[out]   ETOT  The total magnetic energy
   !> @param[out]   grad  The magnetic energy gradient (in spherical polars)
   !> @param[in]    X     The initial guess for the LEM
   !> @param[inout] neval The number of times this function has been evaluated
   !>
   !> @todo This subroutine should be renamed ET_GRAD_PolZ for consistency with
   !>       other polar functions.
   subroutine ET_GRAD_POLAR(axis, ETOT, grad, X, neval)
      use Utils, only: NONZERO
      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices
      use Magnetization_Path_Utils, only: Distance

      implicit none

      real(KIND = DP), intent(OUT):: etot
      real(KIND = DP), intent(OUT):: grad(:)
      real(KIND = DP), intent(IN)  :: X(:)
      integer, intent(INOUT):: neval

      integer:: i, N, l
      integer(KIND = 8), save:: global_neval = 0
      real(KIND = DP):: phi, theta, edist, dist, UsedExchangeEn
      real(KIND = DP):: EnScale
      real(KIND = DP):: mag_av(3), mag_maxv, mag_max
      character, intent(in):: axis
      integer:: axis1, axis2, axis3

      integer:: WhichExchangeHere

      select case (axis)
      case ('X')
         axis1 = 2
         axis2 = 3
         axis3 = 1
      case ('Y')
         axis1 = 3
         axis2 = 1
         axis3 = 2
      case ('Z')
         axis1 = 1
         axis2 = 2
         axis3 = 3
      case default
         write (*, *) "Warning: ET_GRAD_POLAR was called with axis ", axis, " requested."

      end select

      global_neval = global_neval+1

      EnScale = Kd*total_volume  ! Energy scale to transform into units of Kd V
      ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
      if (.not. NONZERO(EnScale)) EnScale = 1

      ! increment the counter for number of energy evalulations
      neval = neval+1

      MeanMag(:) = 0.
      mag_maxv = 0.
      WhichExchangeHere = WhichExchange

      ! when calling with negative neval the cartesian magnetization
      !   is not changed!!
      ! TODO encapsulate function
      if (neval > 0) then
         ! this is used for Magnetization paths--> PathEnergyAt(pos) !!
         do n = 1, NNODE
            m(n, axis1) = sin(X(2*n - 1))*cos(X(2*n))  ! difference in permutation
            m(n, axis2) = sin(X(2*n - 1))*sin(X(2*n))
            m(n, axis3) = cos(X(2*n - 1))
            mag_av(:) = 0
            mag_max = 0
            ! calculate the average magnetisation per node over neighbouring elements which cam be of
            ! different material
            !       print*, 'min max loop' ,CNR_IM(n), CNR_IM(n+1)-1
            do l = CNR_IM(n), CNR_IM(n+1) - 1
               mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
               mag_max = mag_max+Ms(SDNR_IM(l))*InterpolationMatrix(l)
            end do
            mag_maxv = mag_maxv+mag_max*vbox(n)
            MeanMag(:) = MeanMag(:) + mag_av(:)*vbox(n)
         end do
         MeanMag(:) = MeanMag(:)/mag_maxv
      end if

      ! Fill phi from M using FEM-BEM methods
      call CalcPhi()

      ! Calculates demag and Laplace exchange (gradient+energy)
      call CalcDemagEx()
      ! Calculates anisotropy, strain and external field (gradient+energy)
      call CalcAnisExt()

      if (CalcAllExchQ .eqv. .true.) then  ! calculate  all exchange energies
         call ExEnergyGrad3(X)  !  Test alternative exchange energy -> ExEnerg2
         if (WhichExchange == 3 .and. NBadTets > 0) then
            WhichExchangeHere = 2
            write (*, *) 'Ex changed 3 -> 2, NBad =', NBadTets
         end if
         call ExGrad()
         call ExGrad4()
      else  ! calculate  only the requested exchange energy (for faster execution)
         select case (WhichExchangeHere)
         case (2)
            call ExGrad()
         case (3)
            call ExEnergyGrad3(X)  !  Use alternative exchange energy -> ExEnerg2
            if (WhichExchange == 3 .and. NBadTets > 0) then
               WhichExchangeHere = 2
               call ExGrad()
               write (*, *) 'Ex changed 3 -> 2, NBad =', NBadTets
            end if
         case (4)
            call ExGrad4()
         end select
      end if

      !do i = 1, size(ExtraEnergyCalculators)
      !   call ExtraEnergyCalculators(i)%Run()
      !end do

      !
      !      1)   Collect total energy
      !
      UsedExchangeEn = 0
      select case (WhichExchangeHere)
      case (1)
         UsedExchangeEn = ExchangeE
      case (2)
         UsedExchangeEn = ExEnerg2
      case (3)
         UsedExchangeEn = ExEnerg3
      case (4)
         UsedExchangeEn = ExEnerg4
      end select

      etot = AnisEnerg+UsedExchangeEn+DemagEnerg+BEnerg+StressEnerg

      !do i = 1, size(ExtraEnergyCalculators)
      !   etot = etot+ExtraEnergyCalculators(i)%Energy
      !end do

      if (AddInitPathEnergyQ) then
         edist = InitDelta-Distance(PMagInit, m(:, :))
         edist = InitAlpha*edist*edist
         etot = etot+edist
      end if

      if (EnergyLogging) then
         if (AddInitPathEnergyQ) then 
            if (firstrow == 0) then
               write (logunit, *) "FIRST ROW"
               write (logunit, '(2A12, 1A20, 10A28)') &
                  "Global-N-Eval", "N-Eval", "E-Anis", "E-Stress", "E-Exch", &
                  "E-ext", "E-Demag", "E-distance", &
                  "E-Tot", "Mx", "My", "Mz"
               firstrow = 1
            end if
            write (logunit, '(2I12, TR2, 10E28.20)') &
               global_neval, neval, AnisEnerg/EnScale, StressEnerg/EnScale, &
               UsedExchangeEn/EnScale, BEnerg/EnScale, DemagEnerg/EnScale, edist/EnScale, &
               etot/EnScale, &
               MeanMag(1), MeanMag(2), MeanMag(3)
         else
            write (logunit, '(2I12, TR2, 12E28.20)') &
               global_neval, neval, AnisEnerg/EnScale, StressEnerg/EnScale, ExchangeE/EnScale, &
               ExEnerg2/EnScale, ExEnerg3/EnScale, ExEnerg4/EnScale, &
               BEnerg/EnScale, DemagEnerg/EnScale, etot/EnScale, &
               MeanMag(1), MeanMag(2), MeanMag(3)
         end if
      end if

      !
      !      2)   Collect total gradient
      !
      do i = 1, NNODE
         gradc(i, :) = hanis(i, :) + hstress(i, :) + hdemag(i, :) + hext(i, :)
         select case (WhichExchangeHere)
         case (1)
            gradc(i, :) = gradc(i, :) + hexch(i, :)
         case (2)
            gradc(i, :) = gradc(i, :) + hexch2(i, :)
         case (3)
            !                   gradc(i, :)= gradc(i, :)+hexch3(i, :)
            continue
         case (4)
            gradc(i, :) = gradc(i, :) + hexch4(i, :)
         end select
      end do

      !do i = 1, size(ExtraEnergyCalculators)
      !   gradc = gradc+ExtraEnergyCalculators(i)%h
      !end do

      if (AddInitPathEnergyQ) then !! InitPathEnergy gradient should be added!!
         dist = Distance(PMagInit, m(:, :))
         do i = 1, NNODE
            gradc(i, :) = gradc(i, :) &
                          + 2*InitAlpha*(InitDelta/dist-1) &
                          * PMagInit(i, :) &
                          *vbox(i)/total_volume  !! InitPathEnergy gradient!!
         end do
      end if

      !
      !      2)   Transform gradient to polar magnetizations
      !
      do i = 1, NNODE
         phi = X(2*i)
         theta = X(2*i - 1)
         grad(2*i - 1) = gradc(i, axis1)*cos(theta)*cos(phi)   &  !difference in permutation
         &   + gradc(i, axis2)*cos(theta)*sin(phi)  &
             &   - gradc(i, axis3)*sin(theta)
         grad(2*i) = -gradc(i, axis1)*sin(theta)*sin(phi) &
             &        + gradc(i, axis2)*sin(theta)*cos(phi)
         if (WhichExchangeHere == 3) then
            grad(2*i - 1) = grad(2*i - 1) + Aex(1)*ls*DExTheta(i)
            grad(2*i) = grad(2*i) + Aex(1)*ls*DExPhi(i)
         end if

         ! projection on m
         ! sp = gradc(i, 1)*m(i, 1)+gradc(i, 2)*m(i, 2)+gradc(i, 3)*m(i, 3)
         ! make gradc perpendicular to m
         ! gradc(i, :)=gradc(i, :)-sp*m(i, :)
      end do
      !
      !      3)   Set gradient at fixed magnetizations to zero
      !
      !print*,'nfix in enrgymin=',nfix
      if (NFIX > 0) then   !  In case of fixed nodes

         do i = 1, NNODE
            if (NodeBlockNumber(i) .eq. 1) then  ! All free nodes are in block number 0 !!
               grad(2*i) = 0      ! set gradient = 0 for fixed nodes
               grad(2*i - 1) = 0
            end if
         end do
      end if

      return
   end subroutine ET_GRAD_POLAR

   !---------------------------------------------------------------
   ! BEM  : ET_GRAD_Cart(ETOT, G, X, neval)
   !        Only remaining energy and gradient function
   !---------------------------------------------------------------

   !> Update all the magnetic fields and energies and return the energy
   !> gradient in spherical polar form (with the radial component dropped).
   !>
   !> @param[out]   ETOT  The total magnetic energy
   !> @param[out]   grad  The magnetic energy gradient (in spherical polars)
   !> @param[in]    X     The initial guess for the LEM
   !> @param[inout] neval The number of times this function has been evaluated
   subroutine ET_GRAD_Cart(ETOT, grad, X, neval)
      use Utils, only: NONZERO
      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices
      !use Magnetization_Path, only: & !TODO these variables (read only) to live in this module and be set from above!
      !   AddInitPathEnergyQ, PMag, InitAlpha, InitDelta, InitRefPos, Distance, &
      !   firstrow
      use Magnetization_Path_Utils, only: Distance

      implicit none

      real(KIND = DP), intent(OUT):: etot
      real(KIND = DP), intent(OUT):: grad(:)
      real(KIND = DP), intent(IN)  :: X(:)
      integer, intent(INOUT):: neval

      integer:: i, N, l
      integer(KIND = 8), save:: global_neval = 0
      real(KIND = DP):: edist, dist, UsedExchangeEn
      real(KIND = DP):: EnScale
      real(KIND = DP):: mag_av(3), mag_maxv, mag_max

      integer:: WhichExchangeHere

      global_neval = global_neval+1

      EnScale = Kd*total_volume  ! Energy scale to transform into units of Kd V
      ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
      if (.not. NONZERO(EnScale)) EnScale = 1

      ! increment the counter for number of energy evalulations
      neval = neval+1

      MeanMag(:) = 0.
      mag_maxv = 0.
      WhichExchangeHere = WhichExchange

      ! when calling with negative neval the cartesian magnetization
      !   is not changed!!
      if (neval > 0) then
         ! this is used for Magnetization paths--> PathEnergyAt(pos) !!
         !m(:) = X(:)
         do n = 1, NNODE
            !m(n, 1)=sin(X(2*n-1))*cos(X(2*n))
            !m(n, 2)=sin(X(2*n-1))*sin(X(2*n))
            !m(n, 3)=cos(X(2*n-1))

            m(n, 1) = X(3*n - 2)
            m(n, 2) = X(3*n - 1)
            m(n, 3) = X(3*n - 0)

            mag_av(:) = 0
            mag_max = 0
            ! calculate the average magnetisation per node over neighbouring elements which cam be of
            ! different material
            do l = CNR_IM(n), CNR_IM(n+1) - 1
               mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
               mag_max = mag_max+Ms(SDNR_IM(l))*InterpolationMatrix(l)
            end do
            mag_maxv = mag_maxv+mag_max*vbox(n)
            MeanMag(:) = MeanMag(:) + mag_av(:)*vbox(n)
         end do
         MeanMag(:) = MeanMag(:)/mag_maxv
      end if

      ! Fill phi from M using FEM-BEM methods
      call CalcPhi()

      ! Calculates demag and Laplace exchange (gradient+energy)
      call CalcDemagEx()
      ! Calculates anisotropy and external field (gradient+energy)
      call CalcAnisExt()
      ! Calculates exchange
      call ExGrad()

      !     if(CalcAllExchQ .eqv. .true.) then  ! calculate  all exchange energies
      !       !call ExEnergyGrad3(X)  !  Test alternative exchange energy -> ExEnerg2
      !       if (WhichExchange == 3 .and. NBadTets > 0) then
      !         WhichExchangeHere = 2
      !         write(*,*) 'Ex changed 3 -> 2, NBad =',NBadTets
      !       endif
      !       call ExGrad()
      !       call ExGrad4()
      !     else  ! calculate  only the requested exchange energy (for faster execution)
      !       select case(WhichExchangeHere)
      !       case(2)
      !         call ExGrad()
      !       case(3)
      !         !call ExEnergyGrad3(X)  !  Use alternative exchange energy -> ExEnerg2
      !         if (WhichExchange == 3 .and. NBadTets > 0) then
      !           WhichExchangeHere = 2
      !           call ExGrad()
      !           write(*,*) 'Ex changed 3 -> 2, NBad =',NBadTets
      !         endif
      !       case(4)
      !         call ExGrad4()
      !       end select
      !     endif

      !do i = 1, size(ExtraEnergyCalculators)
      !   call ExtraEnergyCalculators(i)%Run()
      !end do

      !
      !      1)   Collect total energy
      !
      !     UsedExchangeEn = 0
      !     select case(WhichExchangeHere)
      !       case(1)
      !         UsedExchangeEn = ExchangeE
      !       case(2)
      !         UsedExchangeEn = ExEnerg2
      !       case(3)
      !         UsedExchangeEn = ExEnerg3
      !       case(4)
      !         UsedExchangeEn = ExEnerg4
      !     end select

      etot = AnisEnerg+ExchangeE+DemagEnerg+BEnerg+StressEnerg

      !do i = 1, size(ExtraEnergyCalculators)
      !   etot = etot+ExtraEnergyCalculators(i)%Energy
      !end do

      if (AddInitPathEnergyQ) then
         edist = InitDelta-Distance(PMagInit, m(:, :))
         edist = InitAlpha*edist*edist
         etot = etot+edist
      end if

      if (EnergyLogging) then
         if (AddInitPathEnergyQ) then  ! TODO move writing of header line !!
            if (firstrow == 0) then
               write (logunit, *) "FIRST ROW"
               write (logunit, '(2A12, 1A20, 10A28)') &
                  "Global-N-Eval", "N-Eval", "E-Anis", "E-Stress", "E-Exch", &
                  "E-ext", "E-Demag", "E-distance", &
                  "E-Tot", "Mx", "My", "Mz"
               firstrow = 1
            end if
            write (logunit, '(2I12, TR2, 10E28.20)') &
               global_neval, neval, AnisEnerg/EnScale, StressEnerg/EnScale, &
               UsedExchangeEn/EnScale, BEnerg/EnScale, DemagEnerg/EnScale, edist/EnScale, &
               etot/EnScale, &
               MeanMag(1), MeanMag(2), MeanMag(3)
         else
            write (logunit, '(2I12, TR2, 12E28.20)') &
               global_neval, neval, AnisEnerg/EnScale, StressEnerg/EnScale, ExchangeE/EnScale, &
               ExEnerg2/EnScale, ExEnerg3/EnScale, ExEnerg4/EnScale, &
               BEnerg/EnScale, DemagEnerg/EnScale, etot/EnScale, &
               MeanMag(1), MeanMag(2), MeanMag(3)
         end if
      end if

      !
      !      2)   Collect total gradient
      !
      !    Print*, ' whichexchange = ', WhichExchangeHere
      do i = 1, NNODE
         gradc(i, :) = hanis(i, :) + hstress(i, :) + hdemag(i, :) + hext(i, :)
         !      select case(WhichExchangeHere)
         !        case(1)
         gradc(i, :) = gradc(i, :) + hexch(i, :)
         !        case(2)
         !          gradc(i, :)= gradc(i, :)+hexch2(i, :)
         !        case(3)
         !                   gradc(i, :)= gradc(i, :)+hexch3(i, :)
         continue
         !        case(4)
         !          gradc(i, :)= gradc(i, :)+hexch4(i, :)
         !      end select
      end do

      !do i = 1, size(ExtraEnergyCalculators)
      !   gradc = gradc+ExtraEnergyCalculators(i)%h
      !end do

      if (AddInitPathEnergyQ) then !! InitPathEnergy gradient should be added!!
         dist = Distance(PMagInit, m(:, :))
         do i = 1, NNODE
            gradc(i, :) = gradc(i, :) &
                          + 2*InitAlpha*(InitDelta/dist-1) &
                          *PMagInit( i, :) &
                          *vbox(i)/total_volume  !! InitPathEnergy gradient!!
         end do
      end if

      !
      !      2)   Transform gradient to polar magnetizations
      !

      !grad(:) = gradc(:)

      do i = 1, NNODE
         grad(3*i - 2) = gradc(i, 1)
         grad(3*i - 1) = gradc(i, 2)
         grad(3*i - 0) = gradc(i, 3)
      end do

      !     DO i = 1, NNODE
      !       phi = X(2*i)
      !       theta = X(2*i-1)
      !       grad(2*i-1)= gradc(i, 1)*cos(theta )*cos(phi)   &
      !         &   + gradc(i, 2)*cos(theta )*sin(phi)  &
      !         &   - gradc(i, 3) * sin(theta )
      !       grad(2*i)= -gradc(i, 1)*sin(theta )*sin(phi ) &
      !         &        + gradc(i, 2)*sin(theta )*cos(phi)
      !       if (WhichExchangeHere == 3) then
      !         grad(2*i-1)=grad(2*i-1) +Aex(1)*ls*DExTheta(i)
      !         grad(2*i)= grad(2*i) +Aex(1)*ls*DExPhi(i)
      !       endif
      !
      !       ! projection on m
      !       ! sp = gradc(i, 1)*m(i, 1)+gradc(i, 2)*m(i, 2)+gradc(i, 3)*m(i, 3)
      !       ! make gradc perpendicular to m
      !       ! gradc(i, :)=gradc(i, :)-sp*m(i, :)
      !     END DO
      !
      !      3)   Set gradient at fixed magnetizations to zero
      !
      !print*,'nfix in enrgymin=',nfix
      if (NFIX > 0) then   !  In case of fixed nodes
         do i = 1, NNODE
            if (NodeBlockNumber(i) .eq. 1) then  ! All free nodes are in block number 0 !!
               grad(3*i - 0) = 0      ! set gradient = 0 for fixed nodes
               grad(3*i - 1) = 0
               grad(3*i - 2) = 0
            end if
         end do
      end if

      return
   end subroutine ET_GRAD_Cart

   !---------------------------------------------------------------
   ! BEM  :  GradientTest()
   !---------------------------------------------------------------

   !> A function for testing the gradients
   !> @todo DOCUMENT ME
   subroutine GradientTest()

      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer:: i, neval, ind, l
      real(KIND = DP):: G(NNODE*2), G2(NNODE*2), X(NNODE*2), etot
      real(KIND = DP):: eref, delta
      real(KIND = DP):: mag_av(3), mag_max, mag_maxv

      ! Controlling tolerance of linear CG solver
      FEMTolerance = 1.d-10   ! via global FEMTolerance

      neval = 1
      MeanMag(:) = 0.
      ! Theta (co-lat) indicies = 2n-1, phi (azimuth)  indicies = 2n

      do i = 1, NNODE
         X(2*i - 1) = acos(m(i, 3))
         X(2*i) = atan2(m(i, 2), m(i, 1))
         mag_av(:) = 0
         mag_max = 0
         ! calculate the average magnetisation per node over neighbouring elements which cam be of
         ! different material
         do l = CNR_IM(i), CNR_IM(i+1) - 1
            mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
            mag_max = mag_max+Ms(SDNR_IM(l))*InterpolationMatrix(l)
         end do
         mag_maxv = mag_maxv+mag_max*vbox(i)
         MeanMag(:) = MeanMag(:) + m(i, :)*vbox(i)
      end do

      MeanMag(:) = MeanMag(:)/mag_maxv

      call ET_GRAD_POLAR('Z', eref, G, X, neval)

      delta = 0.001
      do ind = 50, min(NNode, 100)
         X(2*ind) = X(2*ind) + delta
         m(ind, 1) = sin(X(2*ind-1))*cos(X(2*ind))
         m(ind, 2) = sin(X(2*ind-1))*sin(X(2*ind))
         m(ind, 3) = cos(X(2*ind-1))

         call ET_GRAD_POLAR('Z', ETOT, G2, X, neval)

         write (*, *) "phi:   dE, DE/d:", ind, G(2*ind), (etot-eref)/delta
         X(2*ind) = X(2*ind) - delta
         X(2*ind-1) = X(2*ind-1) + delta
         m(ind, 1) = sin(X(2*ind-1))*cos(X(2*ind))
         m(ind, 2) = sin(X(2*ind-1))*sin(X(2*ind))
         m(ind, 3) = cos(X(2*ind-1))

         call ET_GRAD_POLAR('Z', ETOT, G2, X, neval)

         write (*, *) "theta: dE, DE/d:", ind, G(2*ind-1), (etot-eref)/delta
         X(2*ind-1) = X(2*ind-1) - delta
         m(ind, 1) = sin(X(2*ind-1))*cos(X(2*ind))
         m(ind, 2) = sin(X(2*ind-1))*sin(X(2*ind))
         m(ind, 3) = cos(X(2*ind-1))

      end do

      return
   end subroutine GradientTest

   !---------------------------------------------------------------
   ! BEM  :  ReportEnergy()
   !---------------------------------------------------------------

   !> Print out info about the current energies
   subroutine ReportEnergy(energy_filename)
      use Utils, only: MERRILL_VERSION_STRING, NONZERO
      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer:: i, neval, energywriteunit
      real(KIND = DP):: G(NNODE*2), X(NNODE*2), etot
      real(KIND = DP):: EnScale, TypEn, KdV
      real(KIND = DP):: MeanEdgeLength
      logical:: TmpQ

      character(len = 1024), optional, intent(IN):: energy_filename

      if (present(energy_filename)) then
         open ( &
             NEWUNIT = energywriteunit, FILE = energy_filename(:len_trim(energy_filename)), &
             & STATUS='UNKNOWN')
         ! write(*,*) 'Writing energies to file', energy_filename(:LEN_TRIM(energy_filename))
         ! write(*,*) ' to unit ',energywriteunit
      else
         energywriteunit = 6
      end if

      !EnScale = Kd*total_volume_MagOnly  ! Energy scale to transform into units of Kd V
      Enscale = Kd*total_volume
      ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
      TmpQ = CalcAllExchQ
      CalcAllExchQ = .true.

      ! Controlling tolerance of linear CG solver
      FEMTolerance = 1.d-10   ! via global FEMTolerance

      neval = 1
      MeanMag = 0
      ! Theta (co-lat) indicies = 2n-1, phi (azimuth)  indicies = 2n
      do i = 1, NNODE
         X(2*i - 1) = acos(m(i, 3))
         X(2*i) = atan2(m(i, 2), m(i, 1))
         ! MeanMag(:) = MeanMag(:) + m(i, :)*vbox(i)
         MeanMag(:) = MeanMag(:) + m(i, :)*vbox_MagOnly(i)
      end do
      MeanMag(:) = MeanMag(:)/total_volume
      !    MeanMag(:)=MeanMag(:)/total_volume_MagOnly
        !!! Below only works for spherical polars
      call ET_GRAD_POLAR('Z', etot, G, X, neval)

      !    KdV = Kd*total_volume/(SQRT(Ls)**3)
      KdV = Kd*total_volume_MagOnly/(sqrt(Ls)**3)
      if (.not. NONZERO(KdV)) KdV = 1
      TypEn = sqrt(Kd*sqrt(Aex(1)*abs(K1(1)))) &
         *(total_volume_MagOnly/(sqrt(Ls)**3))**(5./6.)
      !  *(total_volume/(SQRT(Ls)**3))**(5./6.)
      if (.not. (NONZERO(TypEn))) TypEn = Kd*total_volume_MagOnly/(sqrt(Ls)**3)
      if (.not. (NONZERO(TypEn))) TypEn = Aex(1)*total_volume_MagOnly**(1./3.)/sqrt(Ls)
      if (.not. (NONZERO(TypEn))) TypEn = abs(K1(1))*total_volume_MagOnly/(sqrt(Ls)**3)

      !    IF(.NOT. (NONZERO(TypEn))) TypEn = Kd*total_volume/(SQRT(Ls)**3)
      !    IF(.NOT. (NONZERO(TypEn))) TypEn = Aex(1)*total_volume**(1./3.)/SQRT(Ls)
      !    IF(.NOT. (NONZERO(TypEn))) TypEn = ABS(K1(1))*total_volume/(SQRT(Ls)**3)

      ! Get average edge length
      MeanEdgeLength = 0
      do i = 1, NTRI
         MeanEdgeLength = MeanEdgeLength &
                          + norm2(VCL(TIL(i, 1), 1:3) - VCL(TIL(i, 2), 1:3)) &
                          + norm2(VCL(TIL(i, 1), 1:3) - VCL(TIL(i, 3), 1:3)) &
                          + norm2(VCL(TIL(i, 1), 1:3) - VCL(TIL(i, 4), 1:3)) &
                          + norm2(VCL(TIL(i, 2), 1:3) - VCL(TIL(i, 3), 1:3)) &
                          + norm2(VCL(TIL(i, 2), 1:3) - VCL(TIL(i, 4), 1:3)) &
                          + norm2(VCL(TIL(i, 3), 1:3) - VCL(TIL(i, 4), 1:3))
      end do
      MeanEdgeLength = MeanEdgeLength/(6*NTRI)

      write (energywriteunit, *)

      write (energywriteunit, '(A)') MERRILL_VERSION_STRING//" (Williams, Nagy, Paterson, Fabian, O Conbhui, Ridley, Cortes)"
      write (energywriteunit, *)

      write (energywriteunit, '(A)') "Mesh data:"
      write (energywriteunit, '(A16, I16)') "Nodes", NNODE
      write (energywriteunit, '(A16, I16)') "Tetrahedra", NTRI
      write (energywriteunit, '(A16, I16)') "Boundary Nodes", BNODE
      write (energywriteunit, '(A16, I16)') "Boundary Faces", BFCE
      write (energywriteunit, '(A16, ES16.8)') "Volume", total_volume
      write (energywriteunit, *)

      write (energywriteunit, '(A)') "Material data:"
      do i = 1, NMaterials
         write (energywriteunit, '(A16, I16)') "SubDomain ID", SubDomainIds(i)
         write (energywriteunit, '(A16, ES16.8)') "Ms", Ms(i)
         write (energywriteunit, '(A16, ES16.8)') "K1", K1(i)
         write (energywriteunit, '(A16, ES16.8)') "K2", K2(i)
         write (energywriteunit, '(A16, ES16.8)') "LamdaS", LamdaS(i)
         write (energywriteunit, '(A16, ES16.8)') "SigmaS", SigmaS(i)
         write (energywriteunit, '(A16, 3ES16.8)') "Compressional Stress Axis", StressAxis(:, i)
         write (energywriteunit, '(A16, ES16.8)') "Aex", Aex(i)
         write (energywriteunit, '(A16, ES16.8)') "Vol", (sum(vol(:), TetSubDomains .eq. i))/(sqrt(Ls)**3)
         select case (anisform(i))
         case (ANISFORM_CUBIC)
            write (energywriteunit, '(A16, A16)') "Anisotropy", "CUBIC"
            write (energywriteunit, '(A16, 3ES16.8)') "Axes", CubicAxes(:, 1, i)
            write (energywriteunit, '(A16, 3ES16.8)') "", CubicAxes(:, 2, i)
            write (energywriteunit, '(A16, 3ES16.8)') "", CubicAxes(:, 3, i)
         case (ANISFORM_UNIAXIAL)
            write (energywriteunit, '(A16, A16)') "Anisotropy", "UNIAXIAL"
            write (energywriteunit, '(A16, 3ES16.8)') "Axis", EasyAxis(:, i)
         end select
         write (energywriteunit, *)
      end do

      write (energywriteunit, '(A16, ES16.7)') "MagVol^(1/3) (m)", (total_volume_MagOnly**(1./3.))/sqrt(Ls)
      write (energywriteunit, '(A16, ES16.7)') "QHardness", QHardness
      write (energywriteunit, '(A16, ES16.7)') "Exch Len (nm)", LambdaEx*1.d9
      write (energywriteunit, '(A16, ES16.7)') "Avg Edge", MeanEdgeLength
      write (energywriteunit, '(A16, ES16.7)') "Avg Edge (nm)", MeanEdgeLength/sqrt(Ls)*1.d9
      write (energywriteunit, '(A16, ES16.7)') "Kd", Kd
      write (energywriteunit, '(A16, ES16.7)') "Kd V magonly (J)", Kd*total_volume_MagOnly/(sqrt(Ls)**3)
      write (energywriteunit, *)

      write (energywriteunit, '(A)') "External field direction (x, y, z) and strength B (T):"
      write (energywriteunit, '(4A14)') "hx", "hy", "hz", "B (T)"
      write (energywriteunit, '(4ES14.6)') hz(1), hz(2), hz(3), extapp
      write (energywriteunit, *)

      !EnScale = Kd V
      write (energywriteunit, '(A)') "Energies in units of Kd V:"
      write (energywriteunit, '(A16, ES16.8)') "E-Anis", AnisEnerg/EnScale
      write (energywriteunit, '(A16, ES16.8)') "E-Stress", StressEnerg/EnScale
      write (energywriteunit, '(A16, ES16.8)') "E-Ext", BEnerg/EnScale
      write (energywriteunit, '(A16, ES16.8)') "E-Demag", DemagEnerg/EnScale
      write (energywriteunit, '(A16, ES16.8)') "E-Exch", ExchangeE/EnScale
      write (energywriteunit, *)

      write (energywriteunit, '(A16, ES16.8)') "E-Exch2", ExEnerg2/EnScale
      write (energywriteunit, '(A16, ES16.8)') "E-Exch3", ExEnerg3/EnScale
      write (energywriteunit, '(A16, ES16.8)') "E-Exch4", ExEnerg4/EnScale
      write (energywriteunit, *)
      write (energywriteunit, '(A16, ES16.8)') "E-Tot", etot/EnScale
      write (energywriteunit, *)

      write (energywriteunit, '(A)') "Energies in units of J:"
      write (energywriteunit, '(A16, ES16.8)') "E-Anis", AnisEnerg/(sqrt(Ls)**3)
      write (energywriteunit, '(A16, ES16.8)') "E-Stress", StressEnerg/(sqrt(Ls)**3)
      write (energywriteunit, '(A16, ES16.8)') "E-Ext", BEnerg/(sqrt(Ls)**3)
      write (energywriteunit, '(A16, ES16.8)') "E-Demag", DemagEnerg/(sqrt(Ls)**3)
      write (energywriteunit, '(A16, ES16.8)') "E-Exch", ExchangeE/(sqrt(Ls)**3)
      write (energywriteunit, *)

      write (energywriteunit, '(A16, ES16.8)') "E-Exch2", ExEnerg2/(sqrt(Ls)**3)
      write (energywriteunit, '(A16, ES16.8)') "E-Exch3", ExEnerg3/(sqrt(Ls)**3)
      write (energywriteunit, '(A16, ES16.8)') "E-Exch4", ExEnerg4/(sqrt(Ls)**3)
      write (energywriteunit, *)
      write (energywriteunit, '(A16, ES16.8)') "E-Tot", etot/(sqrt(Ls)**3)
      !do i = 1, size(ExtraEnergyCalculators)
      !   write (energywriteunit, *) &
      !      trim(ExtraEnergyCalculators(i)%Name), ":", &
      !      ExtraEnergyCalculators(i)%Energy/EnScale
      !end do
      write (energywriteunit, *)

      if (NMaterials .gt. 1) then
         do i = 1, NMaterials
            write (energywriteunit, '(A32, I16)') "Component Energies for SubDomain ID", SubDomainIds(i)
            write (energywriteunit, '(A)') "Energies in units of J:"
            write (energywriteunit, '(A16, ES16.8)') "E-Anis", (AnisEnergSD(1, i) + AnisEnergSD(2, i))/(sqrt(Ls)**3)
            write (energywriteunit, '(A16, ES16.8)') "E-Ext", BEnergSD(i)/(sqrt(Ls)**3)
            write (energywriteunit, '(A16, ES16.8)') "E-Demag", DemagEnergSD(i)/(sqrt(Ls)**3)
            write (energywriteunit, '(A16, ES16.8)') "E-Exch", ExchangeESD(i)/(sqrt(Ls)**3)
            write (energywriteunit, '(A32, I16)') "Moments (Am^2) for SubDomain ID", SubDomainIds(i)
            write (energywriteunit, '(A16, 3ES16.8)') "Mag_net", Mag_netSD(1, i), Mag_netSD(2, i), Mag_netSD(3, i)
            write (energywriteunit, *)
         end do
      end if

      write (energywriteunit, '(A)') "Average magnetization:"
      write (energywriteunit, '(A16, E16.8)') "<Mx>", MeanMag(1)
      write (energywriteunit, '(A16, E16.8)') "<My>", MeanMag(2)
      write (energywriteunit, '(A16, E16.8)') "<Mz>", MeanMag(3)
      write (energywriteunit, '(A16, E16.8)') "<M>", norm2(MeanMag)
      write (energywriteunit, *)

      write (energywriteunit, '(A)') "Typical energy scale Kd^(1/2) (A K1)^(1/4) V^(5/6):"
      write (energywriteunit, '(A25, ES16.8)') "Typical Energy (J)   ", TypEn
      write (energywriteunit, '(A25, ES16.8)') "Typical Energy (Kd V)", TypEn/KdV
      write (energywriteunit, *)

      CalcAllExchQ = TmpQ

      close (energywriteunit)
   end subroutine ReportEnergy

#if MERRILL_USE_H2LIB
   subroutine CalcPhi()
      use Utils, only: NONZERO
      use Material_Parameters, only: Ms
      use Tetrahedral_Mesh_Data, only: NNODE, BNODE, m, phi1, phi2, totphi
      use Finite_Element_Matrices, only: CNR4, RNR4, BoundaryNodeIndex, &
                                         SDNR4, &
                                         FAX, FAY, FAZ
      use iso_c_binding, only: c_ptr, c_double
      use H2LibFEMBEM, only: SolveFEMH2LibPNM, SolveFEMH2LibPDM, matmulH2AB
      implicit none

      integer i, j

      real(KIND = DP):: Ms_max, rMs_max
      real(KIND = DP), dimension(BNODE):: phi2Boundary, phi1Boundary
      real(KIND = DP), dimension(NNODE):: divM
        !!!REAL(KIND = DP):: DemagEnergSD(NMaterials), ExchangeESD(NMaterials)

      ! Find reciprocal max Ms
      Ms_max = maxval(Ms)
      if (NONZERO(Ms_max)) then
         rMs_max = 1/Ms_max
      else
         Ms_max = 1
         rMs_max = 1
      end if

      ! Calculate f = div . m
      ! We do
      !   div . m/Ms_max
      ! to account for relative multiphase values of Ms, but without having
      ! to multiply by the full value of Ms, for numerical stability.
      ! ww. 19.04.2021 div is calculated across col-row node pairs. It will receive a contribution
      ! for each node pair of an element side, and from each subdomain.
      ! if the side length comes from a zero Ms element, it will have zero contribution here.
      divM = 0

      ! Fill RHS div M of
      ! \nabla^2 \phi_1 = \nabla  M
      do i = 1, NNODE
         do j = CNR4(i), CNR4(i+1) - 1
            divM(i) = divM(i) &
                      + rMs_max*Ms(SDNR4(j))*( &
                      m(RNR4(j), 1)*FAX(j) &
                      + m(RNR4(j), 2)*FAY(j) &
                      + m(RNR4(j), 3)*FAZ(j) &
                      )
            !      print*,'I=',i, ' J=',j, ' Ms=',Ms(SDNR4(j)), '  f=',f(i)
         end do
      end do

      divM(NNODE) = 0

      phi1 = 0  ! TODO not needed because intent(out), deallocated on entry

      ! Solves PNM*phi_1 = divM with operators PoissonNeumannMatrix set up earlier
      ! from mesh geometry, including fixed node/Boundary condition on last node
      ! phi1(NNODE) = 0.0
      call SolveFEMH2LibPNM(phi1, divM)

      !write(*,*) "H2 phi1",  phi1(16), phi1(NNODE-1), phi1(NNODe)
        !!!!!!!!!!!!!!!!!!!
      !     calculate phi2
      !
      ! the value of phi2 is calculated for each node
      ! by a discretized version of \Int phi1/(4pi |r|^3) (r.n) dtheta dphi
      ! this is Fredkin & Koehler eq (15): phi2 = A*phi1

      phi2 = 0

      ! The  matrix-vector-mul, replaced with same from H2Lib
      ! need to filter for those phi1, phi2 at boundaryNodeIndex only
      phi2Boundary = 0

      do i = 1, BNODE
         phi1Boundary(i) = phi1(BoundaryNodeIndex(i))
      end do

      ! the operation phi2(i) = BA(i, j)*phi1(j)
      call matmulH2AB(phi1Boundary, phi2Boundary)

      ! Solve
      !   grad^2 phi2 = 0
      !   with BC: phi2 set on boundary above
      call SolveFEMH2LibPDM(phi2, phi2Boundary)

      ! Now set totphi from phi1 and phi2
      ! totphi used for its orginal purpose again: holding phi = phi1+phi2
      ! fill totphi is sparsified indexing scheme
      totphi = phi1+phi2

   end subroutine
#else

   !---------------------------------------------------------------
   ! BEM  : ENERGY CALCULATIONS
   !---------------------------------------------------------------

   !> Solve FEM/BEM equations for magnetic scalar potentials phi2, phi1, phi = phi1+phi2
   !> Given the current magnetic vector potential field M
   !>
   !> 1. Calculate Phi1: solve Poisson-Neumann equation in sparse matrix form, DCG
   !> 2. Fill boundary node values of phi2 from a dense matrix multiplication with those of phi1
   !> 3. Calculate Phi2: solve Poisson-Dirichlet equation in sparse matrix form, DCG
   !> 4. Have totphi, totphi = phi1+phi2
   !> In:
   !> Out: (to ET_GRAD_ methods:)  Energy, for inspection and convergence detection.
   subroutine CalcPhi()
      use Utils, only: NONZERO
      use Material_Parameters, only: Ms, hdemag, DemagH, DemagEnergSD, ExchangeESD, hexch, Aex, DemagEnerg, ExchangeE, ls, mu
      use Tetrahedral_Mesh_Data, only: NNODE, totphi, phi1, BNODE, m, phi2, NodeOnBoundary, vbox
      use Finite_Element_Matrices, only: nze_pdm, PNM_RWORK, PDM_IWORK, CNR4, RNR4, BA, BoundaryNodeIndex, SDNR4, &
                                       YA4, cnr_pdm, nze_pdm, rnr_pdm, FEMTolerance, PoissonDirichletMatrix, PoissonNeumannMatrix, &
                                         nze_pnm, rnr_em, RNR_PNM, CNR_PNM, pdm_rwork, pnm_iwork, ExchangeMatrix, sdnr_em, cnr_em, &
                                         FAX, FAY, FAZ, BAcptr
      use DCG_Preconditioned, only: DCGpreconditioned

      implicit none

      real(KIND = DP):: TOL, ERR  ! TODO perhaps get from Utils or this module
      integer i, j, l, sd_new, sd
      integer ITER, ISYM, ITOL, IERR, IUNIT, ITMAX  ! for slatec DCG call

      real(KIND = DP):: Ms_max, rMs_max, s
      real(KIND = DP), dimension(BNODE):: phi2Boundary, phi1Boundary
      real(KIND = DP), dimension(NNODE):: divM
        !!!REAL(KIND = DP):: DemagEnergSD(NMaterials), ExchangeESD(NMaterials)

      ! calculate phi1, i.e. f
      !   grad^2 phi1 = div m
      !   (div m) . n = 0 on the boundary
      ! Add div m to force vector
      ! Add m.n Neumann condition to force vector
      ! totphi isn't used until later, so we'll use it to store f  ! TODO just using a new matrix, divM
      ! f = div m

      ! Find reciprocal max Ms
      Ms_max = maxval(Ms)
      if (NONZERO(Ms_max)) then
         rMs_max = 1/Ms_max
      else
         Ms_max = 1
         rMs_max = 1
      end if

      ! Calculate f = div . m
      ! We do
      !   div . m/Ms_max
      ! to account for relative multiphase values of Ms, but without having
      ! to multiply by the full value of Ms, for numerical stability.
      ! ww. 19.04.2021 div is calculated across col-row node pairs. It will receive a contribution
      ! for each node pair of an element side, and from each subdomain.
      ! if the side length comes from a zero Ms element, it will have zero contribution here.
      divM = 0

      do i = 1, NNODE
         do j = CNR4(i), CNR4(i+1) - 1
            divM(i) = divM(i) &
                      + rMs_max*Ms(SDNR4(j))*( &
                      m(RNR4(j), 1)*FAX(j) &
                      + m(RNR4(j), 2)*FAY(j) &
                      + m(RNR4(j), 3)*FAZ(j) &
                      )
         end do
      end do

      ! Set PNM fixed boundary condition RHS value

      divM(NNODE) = 0

      ! Solve grad^2 phi1 = f for phi1
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !     solve linear equations DSICCG values
      ! variables for slatec routine
      ISYM = 0
      ITOL = 1
      TOL = FEMTolerance
      ITMAX = 4000
      IUNIT = 0
      ! Same DCG call as in DSICCG with precomputed RWORK and IWORK
      ! PoissonNeumannMatrix
      ! solve Ax = b
      ! here matrix A-PoissonNeumannMatrix
      ! vector b-RHS, f divM
      ! solution vector x-to be stored in phi1, phi1 also initial guess for solution

      call DCGpreconditioned( &
         NNODE, divM, phi1, nze_pnm, RNR_PNM, CNR_PNM, PoissonNeumannMatrix, &
         ISYM, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
         PNM_RWORK, size(PNM_RWORK), PNM_IWORK, size(PNM_IWORK) &
         )
      if (IERR /= 0) write (*, *) 'GMRES error in phi 1:', IERR

      !write(*,*) "orig phi1", phi1(16), phi1(NNODE-1), phi1(NNODe)

      !     calculate phi2
      !     again, totphi isn't being used, so we'll use that. !TODO nope
      !
      ! the value of phi2 is calculated for each node
      ! by a discretized version of \Int phi1/(4pi |r|^3) (r.n) dtheta dphi
      ! this is Fredkin & Koehler eq (15): phi2 = A*phi1
      phi2 = 0

      ! The slow, dense matrix-vector multiplication

      do i = 1, BNODE
         phi2(BoundaryNodeIndex(i)) = sum(phi1(BoundaryNodeIndex(:))*BA(:, i))
      end do

      ! For integrate_{tet} grad(u_i) . grad(v_j) dV with v = 0 at the boundary, 
      ! move known values of u_i to the RHS.
      !
      ! PoissonDirichletMatrix is built with rows and columns containing boundary
      ! values set to zero, with a 1 on the diagonal.  When multiplied with a
      ! force vector, the resulting value is missing contributions from boundary
      ! nodes. These need to be added back in. Since the value of u_i is known
      ! there, they can be added to the RHS and PoissonDirichletMatrix remains
      ! symmetric.
      !
      ! We want to locate row/column positions containing known boundary values, 
      ! but for rows that aren't for a v_i on the boundary since v_i is zero
      ! at boundary nodes.
      ! Where NodeOnBoundary(j) == .TRUE.  column j contains u_i on the boundary.
      ! Where NodeOnBoundary(RNR_PNM(l)) == .FALSE., row RNR_PNM(l) isn't using
      ! v_i on the boundary.
      !
      ! Where we find one of these, add the integral for the (RNR(l), j)
      ! position, integral_{tet} grad(u(j)) . grad(v(RNR(l))) dV, to the RHS.
      ! Minus sign since it's+integral ... on the LHS.
      !

      s = 0
      j = 0
      do i = 1, NNODE
         do l = CNR_PNM(i), CNR_PNM(i+1) - 1
            if (NodeOnBoundary(i) .and. .not. NodeOnBoundary(RNR_PNM(l))) then
               ! We don't need to worry about totphi(i) referencing a previously
               ! updated totphi, since totphi(i) are boundary values and
               ! totphi(RNR_PNM(l)) aren't.
               phi2(RNR_PNM(l)) = phi2(RNR_PNM(l)) - PoissonNeumannMatrix(l)*phi2(i)

            end if
         end do
      end do

      ! Solve
      !   grad^2 phi2 = 0
      !   phi2 set on boundary above
      ! Same DCG call as in DSICCG with precomputed RWORK and IWORK for
      ! PoissonDirichletMatrix
      ! phi2 : in, RHS, with values filled above to give Dirichlet BC
      ! totphi: used to store phi2 out
      totphi = 0

      call DCGpreconditioned( &
         NNODE, phi2, totphi, nze_pdm, RNR_PDM, CNR_PDM, PoissonDirichletMatrix, &
         ISYM, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
         PDM_RWORK, size(PDM_RWORK), PDM_IWORK, size(PDM_IWORK))

      if (IERR /= 0) write (*, *) 'GMRES error in phi 2:', IERR

      ! Now set totphi from phi1 and phi2
      ! totphi used for its orginal purpose again: holding phi = phi1+phi2

      totphi = phi1+totphi
   end subroutine
#endif

   !> Calculate the Demagnetizing and Exchange energies and gradients.
   !> 5. Calculate demag field H from phi, calculate demag energy, exchange energy, gradient of ex energy
   !> In:
   !> Out: (to ET_GRAD_ methods:)  Energy, for inspection and convergence detection.
   subroutine CalcDemagEx()
      use Utils, only: NONZERO
      use Material_Parameters, only: Ms, hdemag, DemagH, DemagEnergSD, ExchangeESD, hexch, Aex, DemagEnerg, ExchangeE, ls, mu
      use Tetrahedral_Mesh_Data, only: NNODE, totphi, m, vbox
      use Finite_Element_Matrices, only: CNR4, RNR4, SDNR4, RNR_EM, &
                                         YA4, &
                                         ExchangeMatrix, sdnr_em, cnr_em

      implicit none

      integer i, j, sd

      real(KIND = DP):: Ms_max, rMs_max

      Ms_max = maxval(Ms)
      if (NONZERO(Ms_max)) then
         rMs_max = 1/Ms_max
      else
         Ms_max = 1
         rMs_max = 1
      end if

      DemagEnergSD(:) = 0
      ExchangeESD(:) = 0

      ! directly calculate energy and gradient
      hdemag = 0
      demagH = 0
      DemagEnerg = 0
      hexch = 0
      ExchangeE = 0

      do i = 1, NNODE
         ! Gradient of the demag energy
         do j = CNR4(i), CNR4(i+1) - 1
            hdemag(i, :) = hdemag(i, :) &
                           + totphi(RNR4(j)) &
                           *Ms(SDNR4(j)) &
                           *YA4(j, :)
            !write(*,*) hdemag(i, :)
         end do

         ! The demag field DemagH
         ! The calculation below determines the demag field but doesn't multiply
         ! by absolute value of Ms because that would result in node values
         ! where Ms is zero having zero Ms. The potential already contains relative values
         ! of Ms through the force vector in the Neuman boundary conditions.
         ! to get the actual demag field we must multiply DemagH by Ms_max and divide by Vbox(i)
         ! an easy test is to look at uniforly magnetised sphere, and DemagH = -Ms/3
         ! i.e. dividing DemagH by Ms_max (or just not multiplying by it in the final expression
         ! below, will yeild a value of-1/3 (antiparallel to m). Units of H are in A/m

         ! TODO H2Lib route version?
         do j = CNR4(i), CNR4(i+1) - 1
            DemagH(i, :) = DemagH(i, :) &
                           + totphi(RNR4(j)) &
                           *YA4(j, :)
         end do

         !  print*, 'DEMAG at', i, DemagH(i, :)
         ! note that the vbox here is the volume box volume associated with each node irrespective of the
         ! Ms value form contributing elements. We need to ensure that zero Ms elements do not contribue to the
         ! box volume for meshes that are on a boundary to a non magnetic element.
         DemagH(i, :) = -Ms_max*DemagH(i, :)/vbox(i)

         !    print*,'demagH',   &
         !     SQRT(demagH(i, 1)**2+demagH(i, 2)**2+demagH(i, 3)**2), &
         !      demagH(i, :), total_volume

         hdemag(i, :) = Ms_max*mu*hdemag(i, :)
         !      print*,' demag field at node', i, hdemag(i, :)

         DemagEnerg = DemagEnerg &
             & + (hdemag(i, 1)*m(i, 1) + hdemag(i, 2)*m(i, 2) + hdemag(i, 3)*m(i, 3))/2.0

         ! Assign energy to element blocks: look at how many elements attached to each node

         sd = SDNR4(CNR4(i))
         !DO j = CNR4(i), CNR4(i+1)-1
         !sd_new = SDNR4(j)
         !if(sd_new .NE. sd) then
         !   print*,'ERROR-node belongs to more than one block'
         !endif
         !end do
         DemagEnergSD(sd) = DemagEnergSD(sd) &
             & + (hdemag(i, 1)*m(i, 1) + hdemag(i, 2)*m(i, 2) + hdemag(i, 3)*m(i, 3))/2.

         !   print*, ' demag energies, tot sd ',DemagEnerg, DemagEnergSD(:)

         ! Gradient of the exchange energy
         do j = CNR_EM(i), CNR_EM(i+1) - 1
            hexch(i, :) = hexch(i, :) &
                          + m(RNR_EM(j), :) &
                          *ExchangeMatrix(j) &
                          *Aex(SDNR_EM(j))
         end do
         hexch(i, :) = 2*hexch(i, :)*ls

         ExchangeE = ExchangeE &
             & + (hexch(i, 1)*m(i, 1) + hexch(i, 2)*m(i, 2) + hexch(i, 3)*m(i, 3))/2.

         ExchangeESD(sd) = ExchangeESD(sd) &
             & + (hexch(i, 1)*m(i, 1) + hexch(i, 2)*m(i, 2) + hexch(i, 3)*m(i, 3))/2.
      end do

   end subroutine CalcDemagEx

   !> New Exchange energy  calculation using
   !> @code
   !>     Eex = (grad theta)^2+sin^2 theta (grad phi)^2
   !> @endcode
   subroutine ExEnergy3()

      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer i, j, s
      real(KIND = DP) vv, exen, th0
      real(KIND = DP) dph(3), dth(3), phi, theta, mavg(3), pavg

      !     calculate exchange energy based on gradient phi and theta
      !                    Eex = (grad theta)^2+sin^2 theta (grad phi)^2

      exen = 0.

      do i = 1, NTRI
         vv = vol(i)      ! represented volume per tetrahedron
         mavg(:) = m(Til(i, 1), :) + m(Til(i, 2), :) + m(Til(i, 3), :) + m(Til(i, 4), :)
         ! average angle phi for backrotation around z-axis
         pavg = atan2(mavg(2), mavg(1))
         th0 = 0.
         dph(:) = 0
         dth(:) = 0
         do j = 1, 4
            s = TIL(i, j)
            ! back rotation of m by-pavg
            ! avoids discontinuity for angles phi near-pi
            phi = atan2(m(s, 2)*cos(pavg) - m(s, 1)*sin(pavg), &
                &   m(s, 1)*cos(pavg) + m(s, 2)*sin(pavg))
            ! Note that back rotation around z doesn't change any gradient !!
            theta = acos(m(s, 3))
            dph(1) = dph(1) + phi*b(i, j)
            dph(2) = dph(2) + phi*c(i, j)
            dph(3) = dph(3) + phi*d(i, j)
            dth(1) = dth(1) + theta*b(i, j)
            dth(2) = dth(2) + theta*c(i, j)
            dth(3) = dth(3) + theta*d(i, j)
            th0 = th0+theta
         end do
         exen = exen + (dth(1)**2+dth(2)**2+dth(3)**2)/(36*vv)
         exen = exen + (dph(1)**2+dph(2)**2+dph(3)**2)*(sin(th0*0.25))**2/(36*vv)
      end do
      ExEnerg3 = exen !  set global variable in module Material_Parameters
      return
   end subroutine ExEnergy3

   !> New Exchange energy  gradient calculation using
   !> @code
   !>    Eex = (grad theta)^2+sin^2 theta (grad phi)^2
   !> @endcode
   !>
   !> Calculate exchange energy based on gradient phi and theta
   !> @code
   !>    Eex = (grad theta)^2+sin^2 theta (grad phi)^2
   !> @endcode
   !> this works only when the magnetizations at all tetrahedra
   !> vertices lie in one common half space
   !> tetrahedra where this does not apply are counted in NBadTets
   !> if bad tetrahedra occur this hinders nucleation because the
   !> exchange energy is then substantially overestimated
   subroutine ExEnergyGrad3(X)
      use Utils, only: NONZERO
      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer i, s, tt
      real(KIND = DP):: vv, exen, th0, ddp, ddt, sqdenom, rtdenom
      real(KIND = DP):: tmp, mtt(3), rm(3, 3), cp, sp, ct, st
      real(KIND = DP):: dph(3), dth(3), phi, theta, mavg(3), pavg, tavg
      real(KIND = DP):: X(:)

      exen = 0.
      DExPhi(:) = 0.
      DExTheta(:) = 0.
      NBadTets = 0

      do tt = 1, NTRI
         vv = vol(tt)     ! represented volume per tetrahedron
         mavg(:) = 0.
         do i = 1, 4
            mavg(:) = mavg(:) + m(TIL(tt, i), :)  ! calculate average magnetization
         end do
         tmp = sqrt(mavg(1)*mavg(1) + mavg(2)*mavg(2) + mavg(3)*mavg(3))
         if (NONZERO(tmp)) then
            mavg(:) = mavg(:)/tmp
         else
            mavg = 1/sqrt(3.0)
         end if
         do i = 1, 4
            sp = &
               mavg(1)*m(TIL(tt, i), 1) &
               + mavg(2)*m(TIL(tt, i), 2) &
               + mavg(3)*m(TIL(tt, i), 3)
            if (sp < 0) NBadTets = NBadTets+1
         end do
         pavg = atan2(mavg(2), mavg(1))  ! optimal direction to rotate x-axis to
         tavg = acos(mavg(3))
         ct = cos(tavg); st = sin(tavg)  ! direction cosines
         cp = cos(pavg); sp = sin(pavg)
         ! rotation matrix for optimal rotation
         rm(1, 1) = cp*ct; rm(1, 2) = sp*ct; rm(1, 3) = st; 
         ! first  - pavg around z then-tavg around y
         rm(2, 1) = -sp; rm(2, 2) = cp; rm(2, 3) = 0; 
         rm(3, 1) = -cp*st; rm(3, 2) = -sp*st; rm(3, 3) = ct; 
         th0 = 0.
         dph(:) = 0.
         dth(:) = 0.
         do i = 1, 4
            s = TIL(tt, i)
            ! rotate m to optimal coord. system
            mtt(:) = rm(:, 1)*m(s, 1) + rm(:, 2)*m(s, 2) + rm(:, 3)*m(s, 3)
            phi = atan2(mtt(2), mtt(1))
            theta = acos(mtt(3))
            dph(1) = dph(1) + phi*b(tt, i)  ! phi gradient in original coord. system
            dph(2) = dph(2) + phi*c(tt, i)  ! is phi grad divided by st
            dph(3) = dph(3) + phi*d(tt, i)
            dth(1) = dth(1) + theta*b(tt, i)
            dth(2) = dth(2) + theta*c(tt, i)
            dth(3) = dth(3) + theta*d(tt, i)
            th0 = th0+theta
         end do
         do i = 1, 4
            s = TIL(tt, i)
            phi = atan2(m(s, 2), m(s, 1))
            theta = acos(m(s, 3))
            ddp = 2*( &
                  b(tt, i)*dph(1) + c(tt, i)*dph(2) + d(tt, i)*dph(3) &
                  )*(sin(th0*0.25))**2/(36*vv)
            ddt = 2*(b(tt, i)*dth(1) + c(tt, i)*dth(2) + d(tt, i)*dth(3))/(36*vv)
            ddt = ddt + (dph(1)**2+dph(2)**2+dph(3)**2)*sin(th0*0.5)*0.25/(36*vv)
            rtdenom = cos(pavg-phi)*st*sin(theta) - ct*cos(theta)
            rtdenom = sqrt(1-rtdenom*rtdenom)
            sqdenom = st*st*cos(theta)**2+st*ct*cos(pavg-phi)*sin(2*theta)
            sqdenom = sqdenom + (1-st*st**cos(pavg-phi)**2)*sin(theta)**2
            tmp = cp*st*cos(theta)*cos(phi) + cos(theta)*sin(phi)*sp*st+ct*sin(theta)
            if (NONZERO(sqdenom)) then
               tmp = tmp*sin(theta)/sqdenom
            else
               tmp = 0
            end if
            !if(tmp /= tmp) tmp = 0.
            !if(ISNAN(tmp)) tmp = 0.
            DExPhi(s) = DExPhi(s) + ddp*tmp
            if (NONZERO(rtdenom)) then
               tmp = st*sin(pavg-phi)*sin(theta)/rtdenom
            else
               tmp = 0
            end if
            !if(tmp /= tmp) tmp = 0.
            !if(ISNAN(tmp)) tmp = 0.
            DExPhi(s) = DExPhi(s) + ddt*tmp
            if (NONZERO(sqdenom)) then
               tmp = -st*sin(pavg-phi)/sqdenom
            else
               tmp = 0
            end if
            !if(tmp /= tmp) tmp = 0.
            !if(ISNAN(tmp)) tmp = 0.
            DExTheta(s) = DExTheta(s) + ddp*tmp
            tmp = cos(pavg-phi)*cos(theta)*st+ct*sin(theta)
            if (NONZERO(rtdenom)) then
               tmp = tmp/rtdenom
            else
               tmp = 0
            end if
            !if(tmp /= tmp) tmp = 0.
            !if(ISNAN(tmp)) tmp = 0.
            DExTheta(s) = DExTheta(s) + ddt*tmp
         end do
         exen = exen + (dth(1)**2+dth(2)**2+dth(3)**2)/(36*vv)
         exen = exen + (dph(1)**2+dph(2)**2+dph(3)**2)*(sin(th0*0.25))**2/(36*vv)
      end do
      !  set global variable in module Material_Parameters
      ExEnerg3 = Aex(1)*ls*exen
      ! Do i = 1, NNODE  ! transform from polar gradients to cartesian gradients
      !   phi = X(2*i)
      !   theta = X(2*i-1)
      !   dmp = -sin(theta) *sin(phi)
      !   dmt = cos(theta) *cos(phi)
      !   if(abs(dmp)> MachEps)  hexch3(i, 1)=Aex*ls*DExPhi(i)/dmp
      !   if(abs(dmt)> MachEps)  hexch3(i, 1)=hexch3(i, 1)+Aex*ls*DExTheta(i)/dmt
      !   dmp = sin(theta) *cos(phi)
      !   dmt = cos(theta) *sin(phi)
      !   if(abs(dmp)> MachEps)  hexch3(i, 2)=Aex*ls*DExPhi(i)/dmp
      !   if(abs(dmt)> MachEps)  hexch3(i, 2)=hexch3(i, 2)+Aex*ls*DExTheta(i)/dmt
      !   dmt = -sin(theta)
      !   if(abs(dmt)> MachEps)  hexch3(i, 3)=Aex*ls*DExTheta(i)/dmt
      ! End do
      return
   end subroutine ExEnergyGrad3

   !> New Exchange energy calculation
   !>
   !> calculate exchange energy based on linear angular rotation along edges
   !> each edge in tetrahedron TIL(i, :) has associated volume  vv = vol(i)/6
   !> each edge contributes multiple times for each tetrahedron it belongs to
   !> the exchange between nodes s and t is estimated as
   !> @code
   !>   A*vv/len^2*arccos( m(s, :).m(t, :))^2
   !> @endcode
   !> len is the distance between the nodes
   !> @code
   !>   len^2 = (VCL(s, :)-VCL(t, :)).(VCL(s, :)-VCL(t, :))
   !> @endcode
   subroutine ExEnerg()
      use Utils, only: MachEps, NONZERO
      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer i, j, k, s, tt
      real(KIND = DP) vv, sp, len2, exen, ac, tmp

      exen = 0.

      do i = 1, NTRI
         vv = vol(i)/2. ! represented volume per tetrahedral edge = 3*vol/6
         ! The factor 3 occurs because for each of the 4 nodes
         ! the gradient has three terms d/dx, d/dy, d/dz represented by
         ! the three edges starting at the node
         tmp = 0.
         if (vv > MachEps) then
            do j = 1, 3
               s = TIL(i, j)
               do k = j+1, 4
                  tt = TIL(i, k)
                  len2 = (VCL(s, 1) - VCL(tt, 1))*(VCL(s, 1) - VCL(tt, 1)) +  &
                      &(VCL(s, 2) - VCL(tt, 2))*(VCL(s, 2) - VCL(tt, 2)) +  &
                      &(VCL(s, 3) - VCL(tt, 3))*(VCL(s, 3) - VCL(tt, 3))
                  !            scalar product of node magnetizations
                  sp = m(s, 1)*m(tt, 1) + m(s, 2)*m(tt, 2) + m(s, 3)*m(tt, 3)
                  if (-1 .le. sp .and. sp .le. 1) then
                     ac = acos(sp)
                     if (NONZERO(len2)) then
                        ac = ac*ac/len2
                     else
                        ac = 0
                     end if
                     !if(ac /= ac)   ac = 0. !  Catch NaN
                     !if(ISNAN(ac))   ac = 0. !  Catch NaN
                     tmp = tmp+ac     !  correct phi^2
                     !if(tmp /= tmp) then
                  else
                     !if(ISNAN(tmp)) then
                     tmp = 1
                     vv = 0d0
                     !  !            write(*,*) 'ExchangeEnergy2 NaN in Tet:',i, j, k, vv
                     !endif
                  end if
               end do
            end do
            exen = exen+vv*tmp
         end if
      end do
      ! set global variable in module Material_Parameters
      ExEnerg2 = Aex(1)*ls*exen
      return
   end subroutine ExEnerg

   !> New Exchange gradient calculation.
   !>
   !> calculate exchange energy based on linear angular rotation along edges
   !> each edge in tetrahedron `TIL(i, :)` has associated volume `vv = vol(i)/6`
   !> each edge contributes multiple times for each tetrahedron it belongs to
   !> the exchange between nodes `s` and `t` is estimated as
   !> @code
   !>    A*vv/len^2*arccos( m(s, :).m(t, :))^2
   !> @endcode
   !> len is the distance between the nodes
   !> @code
   !>    len^2 = (VCL(s, :)-VCL(t, :)).(VCL(s, :)-VCL(t, :))
   !> @endcode
   subroutine ExGrad()
      use Utils, only: MachEps, NONZERO
      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer i, j, k, s, tt
      real(KIND = DP) vv, sp, len2, exen, ac, tmp, rt

      exen = 0.
      hexch2(:, :) = 0.0  !  contains afterwards the GRADIENT not the exchange field
      do i = 1, NTRI
         vv = vol(i)/6.  ! volume per edge of the tetrahedron
         tmp = 0.
         if (vv > MachEps) then
            do j = 1, 3
               s = TIL(i, j)
               do k = j+1, 4
                  tt = TIL(i, k)
                  len2 = (VCL(s, 1) - VCL(tt, 1))*(VCL(s, 1) - VCL(tt, 1)) +  &
                      &(VCL(s, 2) - VCL(tt, 2))*(VCL(s, 2) - VCL(tt, 2)) +  &
                      &(VCL(s, 3) - VCL(tt, 3))*(VCL(s, 3) - VCL(tt, 3))
                  !            scalar product of node magnetizations
                  sp = m(s, 1)*m(tt, 1) + m(s, 2)*m(tt, 2) + m(s, 3)*m(tt, 3)
                  if (-1 .le. sp .and. sp .le. 1) then
                     ac = acos(sp)
                     if (NONZERO(len2)) then
                        ac = ac*ac/len2
                     else
                        ac = 0
                     end if
                     !if(ac /= ac)   ac = 0. !  Catch NaN
                     !if(ISNAN(ac))   ac = 0. !  Catch NaN
                     tmp = tmp+ac     !  correct phi^2
                     !if(tmp /= tmp) then
                  else
                     !if(ISNAN(tmp)) then
                     tmp = 1
                     vv = 0d0
                     !  !            write(*,*) 'ExchangeEnergy2 NaN in Tet:',i, j, k, vv
                     !endif
                  end if
                  rt = sqrt(max(1-sp*sp, real(0, KIND = DP)))  ! in that case acos(sp)=0 anyway
                  if ( &
                     NONZERO(len2) .and. NONZERO(rt) &
                     .and. &
                     -1 .lt. sp .and. sp .lt. 1 &
                     ) then
                     ac = -6*vv/len2*acos(sp)/rt
                  else
                     ac = 0
                  end if
                  !if(ac /= ac) ac = 0.  ! NaN here implies vv = 0
                  !if(ISNAN(ac)) ac = 0.  ! NaN here implies vv = 0
                  hexch2(s, :) = hexch2(s, :) + ac*m(tt, :)
                  hexch2(tt, :) = hexch2(tt, :) + ac*m(s, :)
               end do
            end do
            exen = exen+3*vv*tmp
         end if
      end do
      ! use only gradient components perpendicular to m
      do i = 1, NNODE
         hexch2(i, :) = Aex(1)*ls*hexch2(i, :)
      end do
      ! set global variable in module Material_Parameters
      ExEnerg2 = Aex(1)*ls*exen
      return
   end subroutine ExGrad

   !> New Exchange gradient calculation.
   !>
   !> calculate exchange energy based on linear angular rotation
   !> from tetrahedron centroid to its 4 vertices.
   !>
   !> Each of the four lines in tetrahedron `TIL(i, :)`
   !> has associated volume `vv = vol(i)/4`
   !> each line contributes three times (as x, y, z coordinate direction)
   !> the exchange between nodes `t` and centroid `s`
   !> is estimated as
   !> @code
   !>    A*vv/len^2*arccos( mq.m(t, :))^2
   !> @endcode
   !> `len` is the distance between `s` and `t`.
   subroutine ExGrad4()
      use Utils, only: MachEps, NONZERO
      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer i, j, k, ind(4)
      real(KIND = DP) vv, sp, exen, ac, tmp, rt, sprt
      real(KIND = DP) mq(3), mqs(3), rq(3), dst(4), rs(4, 3)

      exen = 0.
      hexch4(:, :) = 0.0  !  contains afterwards the GRADIENT not the exchange field
      do i = 1, NTRI
         vv = 0.75*vol(i)  ! 3/4 volume per line inside the tetrahedron
         if (vv > MachEps) then
            tmp = 0.
            ind(:) = TIL(i, 1:4)
            rs(:, :) = VCL(ind(:), 1:3)
            rq(:) = (rs(1, :) + rs(2, :) + rs(3, :) + rs(4, :))*0.25
            dst(:) = sqrt((rq(1) - rs(:, 1))**2 + (rq(2) - rs(:, 2))**2 + (rq(3) - rs(:, 3))**2)
            mqs(:) = m(ind(1), :) + m(ind(2), :) + m(ind(3), :) + m(ind(4), :)
            rt = sqrt(mqs(1)*mqs(1) + mqs(2)*mqs(2) + mqs(3)*mqs(3))
            if (rt < MachEps) then
               rt = 1.
               mqs(1) = 1.
            end if
            if (NONZERO(rt)) then
               mq(:) = mqs(:)/rt
            else
               mq(:) = mqs(:)
            end if
            do j = 1, 4
               sp = m(ind(j), 1)*mq(1) + m(ind(j), 2)*mq(2) + m(ind(j), 3)*mq(3)
               if (-1 .lt. sp .and. sp .lt. 1) then
                  ac = acos(sp)
               else
                  ac = 0
               end if
               !tmp = ac*ac/(dst(j)*dst(j))*vv
               tmp = (dst(j)*dst(j))*vv
               if (NONZERO(tmp)) then
                  tmp = ac*ac/tmp
               else
                  tmp = 0
               end if
               !if(tmp /= tmp)   tmp = 0. !  Catch NaN
               !if(ISNAN(tmp))   tmp = 0. !  Catch NaN
               exen = exen+tmp
               sprt = sqrt(max(1-sp*sp, real(0, KIND = DP)))
               if (NONZERO(sprt)) then
                  tmp = -2*vv*ac/sprt
               else
                  tmp = 0
               end if
               do k = 1, 4
                  if (j == k) then
                     if (NONZERO(dst(k))) then
                        hexch4(ind(k), :) = hexch4(ind(k), :) + mq(:)*tmp/(dst(k)*dst(k))
                     end if
                  end if

                  if (NONZERO(dst(j))) then
                     hexch4(ind(k), :) = hexch4(ind(k), :) &
                                         + (m(ind(j), :) - sp*mq(:))/rt*tmp/(dst(j)*dst(j))
                  end if
               end do
            end do
         end if
      end do
      do i = 1, NNODE
         hexch4(i, :) = Aex(1)*ls*hexch4(i, :)
      end do
      ! set global variable in module Material_Parameters
      ExEnerg4 = Aex(1)*ls*exen
      return
   end subroutine ExGrad4

   !> Calculate the Anisotropy end External/Zeeman energy and gradient
   subroutine CalcAnisExt()

      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer:: i, l
      real(KIND = DP):: a(3), m2(3), ga(3) !!, AnisEnergSD(2, NMaterials), BEnergSD(NMaterials)
      real(KIND = DP):: alp_a, alp_b, alp_u, in_rt2, in_rt3, R, S
      real(KIND = DP):: InterpolationSumSD(NMaterials)

      integer:: sd

      ! Calculate anisotropy energy and gradient (wrt m) in correct units (J)

      ! Initialize energies to 0
      AnisEnerg = 0
      AnisEnerg1 = 0
      AnisEnerg2 = 0
      AnisEnergSD(:, :) = 0
      BEnergSD(:) = 0
      StressEnerg = 0
      StressEnergSD(:) = 0

      ! Initialize gradient field to 0
      ! hanis contains the GRADIENT, not the effective field of
      ! the anisotropy energy
      hanis = 0
      hstress = 0

      do i = 1, NNODE
         do l = CNR_IM(i), CNR_IM(i+1) - 1
            sd = SDNR_IM(l)

            select case (anisform(sd))
            case (ANISFORM_CUBIC)

               ! Rotated magnetizations
               a(:) = &
                  m(RNR_IM(l), 1)*CubicAxes(1, :, sd) &
                  + m(RNR_IM(l), 2)*CubicAxes(2, :, sd) &
                  + m(RNR_IM(l), 3)*CubicAxes(3, :, sd)
               m2(:) = a(:)**2

               ! Rotated gradient
               ga(1) = ( &
                       2*K1(sd)*a(1)*(m2(2) + m2(3)) &
                       + 2*K2(sd)*a(1)*m2(2)*m2(3) &
                       )
               ga(2) = ( &
                       2*K1(sd)*a(2)*(m2(1) + m2(3)) &
                       + 2*K2(sd)*a(2)*m2(1)*m2(3) &
                       )
               ga(3) = ( &
                       2*K1(sd)*a(3)*(m2(2) + m2(1)) &
                       + 2*K2(sd)*a(3)*m2(1)*m2(2) &
                       )

               ! Backrotated gradient
               ! Multply by transpose of rotation matrix as inverse
               hanis(i, :) = hanis(i, :) &
                             + ( &
                             ga(1)*CubicAxes(:, 1, sd) &
                             + ga(2)*CubicAxes(:, 2, sd) &
                             + ga(3)*CubicAxes(:, 3, sd) &
                             )*InterpolationMatrix(l)

               ! K1 Energy in rotated coordinates
               AnisEnerg1 = AnisEnerg1 &
                            + K1(sd)*(m2(1)*m2(2) + m2(1)*m2(3) + m2(3)*m2(2)) &
                            *InterpolationMatrix(l)
               AnisEnerg2 = AnisEnerg2 &
                            + K2(sd)*(m2(1)*m2(2)*m2(3)) &
                            *InterpolationMatrix(l)

               AnisEnergSD(1, sd) = AnisEnergSD(1, sd) &
                                    + K1(sd)*(m2(1)*m2(2) + m2(1)*m2(3) + m2(3)*m2(2)) &
                                    *InterpolationMatrix(l)

               AnisEnergSD(2, sd) = AnisEnergSD(2, sd) &
                                    + K2(sd)*(m2(1)*m2(2)*m2(3)) &
                                    *InterpolationMatrix(l)

               !print*,'Anis Energy',SD, AnisEnergSD(sd)
               ! END CASE(ANISFORM_CUBIC)

            case (ANISFORM_MONOCLINIC)

               ! Rotated magnetizations
               a(:) = &
                  m(RNR_IM(l), 1)*CubicAxes(1, :, sd) &
                  + m(RNR_IM(l), 2)*CubicAxes(2, :, sd) &
                  + m(RNR_IM(l), 3)*CubicAxes(3, :, sd)
               m2(:) = a(:)**2

               ! direction cosines along monoclinic directions, a [1-1 0]), b [1 1 0],u [1 1 1]
               in_rt2 = 2**(-.5)
               in_rt3 = 3**(-(1.0/3.0))
               alp_a = in_rt2*(a(1) - a(2))
               alp_b = in_rt2*(a(1) + a(2))
               alp_u = in_rt3*(a(1) + a(2) + a(3))

               ! Rotated gradient
               ga(1) = ( &
                       2*Ka(sd)*alp_a*in_rt2 &
                       + 2*Kb(sd)*alp_b*in_rt2 &
                       - 2*Ku(sd)*alp_u*in_rt3 &
                       + 4*Kaa(sd)*(alp_a**3)*in_rt2 &
                       + 4*Kbb(sd)*(alp_b**3)*in_rt2 &
                       + 2*Kab(sd)*((alp_a**2)*alp_b + (alp_b**2)*alp_a)*in_rt2 &
                       )

               ga(2) = ( &
                       -2*Ka(sd)*alp_a*in_rt2 &
                       + 2*Kb(sd)*alp_b*in_rt2 &
                       - 2*Ku(sd)*alp_u*in_rt3 &
                       - 4*Kaa(sd)*(alp_a**3)*in_rt2 &
                       + 4*Kbb(sd)*(alp_b**3)*in_rt2 &
                       + 2*Kab(sd)*((alp_a**2)*alp_b - (alp_b**2)*alp_a)*in_rt2 &
                       )
               ga(3) = ( &
                       -2*Ku(sd)*alp_u*in_rt3 &
                       )

               ! Backrotated gradient
               ! Multply by transpose of rotation matrix as inverse
               hanis(i, :) = hanis(i, :) &
                             + ( &
                             ga(1)*CubicAxes(:, 1, sd) &
                             + ga(2)*CubicAxes(:, 2, sd) &
                             + ga(3)*CubicAxes(:, 3, sd) &
                             )*InterpolationMatrix(l)

               ! K1 Energy in rotated coordinates
               AnisEnerg1 = AnisEnerg1 &
                            + (Ka(sd)*(alp_a)**2+Kb(sd)*(alp_b)**2-Ku(sd)*(alp_u)**2) &
                            *InterpolationMatrix(l)

               AnisEnerg2 = AnisEnerg2 &
                            + (Kaa(sd)*(alp_a**4) + Kbb(sd)*(alp_b**4) + Kab(sd)*(alp_a**2)*(alp_b**2)) &
                            *InterpolationMatrix(l)

               AnisEnergSD(1, sd) = AnisEnergSD(1, sd) &
                                    + (Ka(sd)*(alp_a)**2+Kb(sd)*(alp_b)**2-Ku(sd)*(alp_u)**2) &
                                    *InterpolationMatrix(l)

               AnisEnergSD(2, sd) = AnisEnergSD(2, sd) &
                                    + (Kaa(sd)*(alp_a**4) + Kbb(sd)*(alp_b**4) + Kab(sd)*(alp_a**2)*(alp_b**2)) &
                                    *InterpolationMatrix(l)

               ! END CASE(ANISFORM_MONOCLINIC)

            case (ANISFORM_MONOCLINIC_PYRR)

               ! mononclinic uniaxial for pyrrhotite expressed in terms of direction cosines along x, y, z
               ! Rotated magnetizations
               a(:) = &
                  m(RNR_IM(l), 1)*CubicAxes(1, :, sd) &
                  + m(RNR_IM(l), 2)*CubicAxes(2, :, sd) &
                  + m(RNR_IM(l), 3)*CubicAxes(3, :, sd)
               m2(:) = a(:)**2

               !============ Original==============================
               ! Rotated gradient
               ! ga(1) = 0.

               ! ga(2) = ( &
               !   + 2*K1p(sd)*(1-m2(3))*a(2) &
               !   + 4*K2p(sd)*((1-m2(3))**2)*m2(2)*a(2) &
               !   + 2*K5p(sd)*(1-m2(3))*m2(3)*a(2) &
               ! )

               ! ga(3) = ( &
               !   - 2*K1p(sd)*m2(2)*a(3) &
               !   - 4*K2p(sd)*(1-m2(3))*a(3)*(m2(2)**2) &
               !   + 2*K3p(sd)*a(3) &
               !   + 4*K4p(sd)*m2(3)*a(3) &
               !   + 2*K5p(sd)*(1-m2(3))*a(3)*m2(2) &
               ! )

               ! ============ corrected =========================
               ga(1) = 0.

               ga(2) = ( &
                       +2*K1p(sd)*a(2) &
                       + 4*K2p(sd)*m2(2)*a(2) &
                       + 2*K5p(sd)*m2(3)*a(2) &
                       )

               ga(3) = ( &
                       +2*K3p(sd)*a(3) &
                       + 4*K4p(sd)*m2(3)*a(3) &
                       + 2*K5p(sd)*m2(2)*a(3) &
                       )
               !==============================================

               ! Backrotated gradient
               ! Multply by transpose of rotation matrix as inverse
               hanis(i, :) = hanis(i, :) &
                             + ( &
                             ga(1)*CubicAxes(:, 1, sd) &
                             + ga(2)*CubicAxes(:, 2, sd) &
                             + ga(3)*CubicAxes(:, 3, sd) &
                             )*InterpolationMatrix(l)

               !============ Original==============================
               !K1 Energy in rotated coordinates
               ! AnisEnerg1 = AnisEnerg1 &
               !   + ( K1p(sd)*(1-m2(3))*m2(2)+ K2p(sd)*((1-m2(3))**2)*m2(2)**2+K3p(sd)*m2(3) &
               !     + K4p(sd)*m2(3)**2+K5p(sd)*(1-m2(3))*m2(3)*m2(2) ) &
               !      * InterpolationMatrix(l)

               ! AnisEnergSD(1, sd) = AnisEnergSD(1, sd) &
               !   + ( K1p(sd)*(1-m2(3))*m2(2)+ K2p(sd)*((1-m2(3))**2)*m2(2)**2+K3p(sd)*m2(3) &
               !     + K4p(sd)*m2(3)**2+K5p(sd)*(1-m2(3))*m2(3)*m2(2) ) &
               !      * InterpolationMatrix(l)

               ! ============ corrected =========================
               AnisEnerg1 = AnisEnerg1 &
                            + (K1p(sd)*m2(2) + K2p(sd)*m2(2)**2+K3p(sd)*m2(3) &
                               + K4p(sd)*m2(3)**2+K5p(sd)*m2(3)*m2(2)) &
                            *InterpolationMatrix(l)

               AnisEnergSD(1, sd) = AnisEnergSD(1, sd) &
                                    + (K1p(sd)*m2(2) + K2p(sd)*m2(2)**2+K3p(sd)*m2(3) &
                                       + K4p(sd)*m2(3)**2+K5p(sd)*m2(3)*m2(2)) &
                                    *InterpolationMatrix(l)

               !END CASE(ANISFORM_MONOCLINIC)

            case (ANISFORM_HEX_PYRR)

               ! mononclinic hexagonal for pyrrhotite expressed in terms of direction cosines along x, y, z
               ! Rotated magnetizations
               a(:) = &
                  m(RNR_IM(l), 1)*CubicAxes(1, :, sd) &
                  + m(RNR_IM(l), 2)*CubicAxes(2, :, sd) &
                  + m(RNR_IM(l), 3)*CubicAxes(3, :, sd)
               m2(:) = a(:)**2

               S = (1-m2(3))
               R = 3*a(2) - (4*(a(2)**3)/S)
               ga(1) = 0.

               ga(2) = ( &
                       +2*K1p(sd)*R*(3 - (12*m2(2)/S)) &
                       + 4*K2p(sd)*m2(2)*a(2) &
                       + 2*K5p(sd)*m2(3)*a(2) &
                       )

               ga(3) = ( &
                       -K1p(sd)*16*R*a(3)*(a(2)**3)/(S**2) &
                       + 2*K3p(sd)*a(3) &
                       + 4*K4p(sd)*m2(3)*a(3) &
                       + 2*K5p(sd)*m2(2)*a(3) &
                       )

               ! Backrotated gradient
               ! Multply by transpose of rotation matrix as inverse
               hanis(i, :) = hanis(i, :) &
                             + ( &
                             ga(1)*CubicAxes(:, 1, sd) &
                             + ga(2)*CubicAxes(:, 2, sd) &
                             + ga(3)*CubicAxes(:, 3, sd) &
                             )*InterpolationMatrix(l)

               AnisEnerg1 = AnisEnerg1 &
                            + (K1p(sd)*R**2 + K2p(sd)*m2(2)**2+K3p(sd)*m2(3) &
                               + K4p(sd)*m2(3)**2+K5p(sd)*m2(3)*m2(2)) &
                            *InterpolationMatrix(l)

               AnisEnergSD(1, sd) = AnisEnergSD(1, sd) &
                                    + (K1p(sd)*R**2 + K2p(sd)*m2(2)**2+K3p(sd)*m2(3) &
                                       + K4p(sd)*m2(3)**2+K5p(sd)*m2(3)*m2(2)) &
                                    *InterpolationMatrix(l)

               !END CASE(ANISFORM_HEX_PYRR)

            case (ANISFORM_UNIAXIAL)

               ! Uniaxial gradient
               hanis(i, :) = hanis(i, :) &
                             - 2*K1(sd)*( &
                             EasyAxis(1, sd)*m(RNR_IM(l), 1) &
                             + EasyAxis(2, sd)*m(RNR_IM(l), 2) &
                             + EasyAxis(3, sd)*m(RNR_IM(l), 3) &
                             )*EasyAxis(:, sd)*InterpolationMatrix(l)

               ! Energy
               AnisEnerg1 = AnisEnerg1 &
                            + K1(sd)*( &
                            1 - ( &
                            EasyAxis(1, sd)*m(RNR_IM(l), 1) &
                            + EasyAxis(2, sd)*m(RNR_IM(l), 2) &
                            + EasyAxis(3, sd)*m(RNR_IM(l), 3) &
                            )**2 &
                            )*InterpolationMatrix(l)

               AnisEnergSD(1, sd) = AnisEnergSD(1, sd) &
                                    + K1(sd)*( &
                                    1 - ( &
                                    EasyAxis(1, sd)*m(RNR_IM(l), 1) &
                                    + EasyAxis(2, sd)*m(RNR_IM(l), 2) &
                                    + EasyAxis(3, sd)*m(RNR_IM(l), 3) &
                                    )**2 &
                                    )*InterpolationMatrix(l)
               ! END CASE(ANISFORM_UNIAXIAL)

            end select
         end do
      end do

      AnisEnerg = AnisEnerg1+AnisEnerg2

      !    Calculate magnetostrictive strain energy and gradient (wrt m) in correct
      !    units (J)
      !    print*,'stress = lambda, sigma',LamdaS(sd), SigmaS(sd), LamdaS(sd)*SigmaS(sd)
      StressEnerg = 0.
      hstress = 0.
      if (SigmaS(SD) .ge. 0) then
            !! Compressional regime
         do i = 1, NNODE
            do l = CNR_IM(i), CNR_IM(i+1) - 1
               sd = SDNR_IM(l)
               !Uniaxial gradient
               hstress(i, :) = hstress(i, :) &
                               + 3.*LamdaS(sd)*SigmaS(sd)*( &
                               StressAxis(1, sd)*m(RNR_IM(l), 1) &
                               + StressAxis(2, sd)*m(RNR_IM(l), 2) &
                               + StressAxis(3, sd)*m(RNR_IM(l), 3) &
                               )*StressAxis(:, sd)*InterpolationMatrix(l)

               !Energy
               StressEnerg = StressEnerg &
                             + (3./2.)*LamdaS(sd)*SigmaS(sd)*( &
                             ( &
                             StressAxis(1, sd)*m(RNR_IM(l), 1) &
                             + StressAxis(2, sd)*m(RNR_IM(l), 2) &
                             + StressAxis(3, sd)*m(RNR_IM(l), 3) &
                             )**2 &
                             )*InterpolationMatrix(l)

               StressEnergSD(sd) = StressEnergSD(sd) &
                                   + (3./2.)*LamdaS(sd)*SigmaS(sd)*( &
                                   ( &
                                   StressAxis(1, sd)*m(RNR_IM(l), 1) &
                                   + StressAxis(2, sd)*m(RNR_IM(l), 2) &
                                   + StressAxis(3, sd)*m(RNR_IM(l), 3) &
                                   )**2 &
                                   )*InterpolationMatrix(l)

            end do
         end do

      else

            !! Tensional regime
         do i = 1, NNODE
            do l = CNR_IM(i), CNR_IM(i+1) - 1
               sd = SDNR_IM(l)
               !Uniaxial gradient
               hstress(i, :) = hstress(i, :) &
                               - 3.*LamdaS(sd)*abs(SigmaS(sd))*( &
                               StressAxis(1, sd)*m(RNR_IM(l), 1) &
                               + StressAxis(2, sd)*m(RNR_IM(l), 2) &
                               + StressAxis(3, sd)*m(RNR_IM(l), 3) &
                               )*StressAxis(:, sd)*InterpolationMatrix(l)

               !Energy
               StressEnerg = StressEnerg &
                             + (3./2.)*LamdaS(sd)*abs(SigmaS(sd))*( &
                             1 - ( &
                             StressAxis(1, sd)*m(RNR_IM(l), 1) &
                             + StressAxis(2, sd)*m(RNR_IM(l), 2) &
                             + StressAxis(3, sd)*m(RNR_IM(l), 3) &
                             )**2 &
                             )*InterpolationMatrix(l)

               StressEnergSD(sd) = StressEnergSD(sd) &
                                   + (3./2.)*LamdaS(sd)*abs(SigmaS(sd))*( &
                                   1 - ( &
                                   StressAxis(1, sd)*m(RNR_IM(l), 1) &
                                   + StressAxis(2, sd)*m(RNR_IM(l), 2) &
                                   + StressAxis(3, sd)*m(RNR_IM(l), 3) &
                                   )**2 &
                                   )*InterpolationMatrix(l)

            end do
         end do

      end if

      BEnerg = 0.
      hext = 0.
      do i = 1, NNODE
         do l = CNR_IM(i), CNR_IM(i+1) - 1  ! loop over the different sub domains
            ! Uniform field energy GRADIENT
            hext(i, :) = hext(i, :) - Ms(SDNR_IM(l))*extapp*hz(:)*InterpolationMatrix(l)

            ! Energy
            sd = SDNR_IM(l)
            BEnergSD(sd) = BEnergSD(sd) &
                           - Ms(SDNR_IM(l))*extapp*( &
                           hz(1)*m(RNR_IM(l), 1) &
                           + hz(2)*m(RNR_IM(l), 2) &
                           + hz(3)*m(RNR_IM(l), 3) &
                           )*InterpolationMatrix(l)
            BEnerg = BEnerg &
                     - Ms(SDNR_IM(l))*extapp*( &
                     hz(1)*m(RNR_IM(l), 1) &
                     + hz(2)*m(RNR_IM(l), 2) &
                     + hz(3)*m(RNR_IM(l), 3) &
                     )*InterpolationMatrix(l)
         end do
      end do

      Mag_netSD(:, :) = 0.
      InterpolationSumSD(:) = 0.

      do i = 1, NNODE
         do l = CNR_IM(i), CNR_IM(i+1) - 1

            sd = SDNR_IM(l)
            Mag_netSD(:, sd) = Mag_netSD(:, sd) &
                               + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
            InterpolationSumSD(sd) = InterpolationSumSD(sd) + InterpolationMatrix(l)

         end do

      end do

      ! renormalize to Am2 units
      do i = 1, NMaterials
         Mag_netSD(:, i) = Mag_netSD(:, i)*sum(vol(:), TetSubDomains .eq. i)/(sqrt(Ls)**3)/InterpolationSumSD(i)
      end do

   end subroutine CalcAnisExt

   !> Calculate the Anisotropy end External/Zeeman energy and gradient
   subroutine CalcQDM()
      use Utils, only: pi
      use Material_Parameters
      use Tetrahedral_Mesh_Data
      use Finite_Element_Matrices

      implicit none

      integer:: i, l, k
      real(KIND = DP):: x, y, z, qx, qy, qz, rdist, nrx, nry, nrz  ! node locations of magnetic region, and surface nodes, and distance between them
      real(KIND = DP):: Qhz, StrayHz, ndotm, dipole_eng, Hz_diff
      real(KIND = DP):: InterpolationSumSD(NMaterials)

      ! Calculate anisotropy energy and gradient (wrt m) in correct units (J)

      ! Initialize energies to 0
      AnisEnerg = 0
      AnisEnerg1 = 0
      AnisEnerg2 = 0
      AnisEnergSD(:, :) = 0
      BEnergSD(:) = 0
      StressEnerg = 0
      StressEnergSD(:) = 0

      ! Initialize gradient field to 0
      ! hanis contains the GRADIENT, not the effective field of
      ! the anisotropy energy
      hanis = 0
      hstress = 0

      do k = 1, QDMnodes
         qx = QDM(k, 1)
         qy = QDM(k, 2)
         qz = QDM(k, 3)
         Qhz = QDM(k, 4)

         Dipole_eng = 0.
         hext = 0.
         StrayHz = 0.
         Hz_diff = 0.

         do i = 1, NNODE
            x = VCL(i, 1)
            y = VCL(i, 2)
            z = VCL(i, 3)

            rdist = sqrt((qx-x)**2 + (qy-y)**2 + (qz-z)**2)
            !unit vector along rdist
            nrx = (qx-x)/rdist
            nry = (qy-y)/rdist
            nrz = (qz-z)/rdist
            do l = CNR_IM(i), CNR_IM(i+1) - 1  ! loop over the different sub domains
               ! Uniform field energy GRADIENT
               ! hext(i, :) = hext(i, :) - Ms(SDNR_IM(l))*extapp*hz(:)*InterpolationMatrix(l)

               ! Stray field at QDM node k due to vbox node i
               ndotm = (nrx*m(RNR_IM(l), 1) &
                        + nry*m(RNR_IM(l), 2) &
                        + nrz*m(RNR_IM(l), 3) &
                        )*InterpolationMatrix(l)  ! interpolation matrix includes the vbox volume

               StrayHz = StrayHz + (1.0/4.0*pi)*Ms(SDNR_IM(l))*(3*nrz*ndotm-m(RNR_IM(l), 3))/rdist**3
               ! BEnerg = BEnerg &
               !   - Ms(SDNR_IM(l))*extapp*( &
               !       hz(1)*m(RNR_IM(l), 1) &
               !     + hz(2)*m(RNR_IM(l), 2) &
               !     + hz(3)*m(RNR_IM(l), 3) &
               !   )*InterpolationMatrix(l)  ! interpolation matrix includes the vbox volume
            end do  ! end of loop over neighbouring elements.

         end do  ! end of sum over all nodes-yielding the stay field at QDMnode k

         Hz_Diff = Hz_Diff + (Qhz-StrayHz)**2

      end do

      ! renormalize to Am2 units
      do i = 1, NMaterials
         Mag_netSD(:, i) = Mag_netSD(:, i)*sum(vol(:), TetSubDomains .eq. i)/(sqrt(Ls)**3)/InterpolationSumSD(i)
      end do

   end subroutine CalcQDM

   !---------------------------------------------------------------
   ! BEM  : EFFECTIVE_field(ETOT, neval, ag, mdot)
   !        Only remaining energy and gradient function
   !---------------------------------------------------------------

   !> Update all the magnetic fields and energies and return the effective field
   !> derived form total gradient.
   !> @param[out]   ETOT  The NORMALIZED total magnetic energy (note difference to enerymin routines that return non-normalized)
   !> @param[out]   mdot  theleft had side if the LLG equatiosn (equates to dm/dt)
   !> @param[in]    ag    the LLG daming parameter
   !> @param[inout] neval The number of times this function has been evaluated
   ! SUBROUTINE EFFECTIVE_field(ETOT, heff, X, neval)
   subroutine EFFECTIVE_field(ETOT, neval, ag, mdot)
      use Utils, only: NONZERO
      use Material_Parameters
      use Tetrahedral_Mesh_Data, only: NNODE, heffc, MeanMag, m, gradc, NFIX, total_volume, vbox, NodeBlockNumber
      use Finite_Element_Matrices
      use Magnetization_Path_Utils, only: Distance

      implicit none

      real(KIND = DP), intent(OUT):: etot

      integer, intent(INOUT):: neval
      double precision, intent(IN):: ag
      double precision, intent(OUT):: mDOT(NNODE, 3)

      integer:: i, N, l
      integer(KIND = 8), save:: global_neval = 0
      real(KIND = DP):: edist, UsedExchangeEn
      real(KIND = DP):: EnScale, ms_average
      real(KIND = DP):: mag_av(3), mag_maxv, mag_max, k1max, msmax

      integer:: WhichExchangeHere

      ! print*, 'in effective field'
      global_neval = global_neval+1

      EnScale = Kd*total_volume  ! Energy scale to transform into units of Kd V
      ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
      if (.not. NONZERO(EnScale)) EnScale = 1

      ! increment the counter for number of energy evalulations
      neval = neval+1

      MeanMag(:) = 0.
      mag_maxv = 0.
      WhichExchangeHere = WhichExchange

      ! when calling with negative neval the cartesian magnetization
      !   is not changed!!
      if (neval > 0) then
         ! this is used for Magnetization paths--> PathEnergyAt(pos) !!
         !m(:) = X(:)

         do n = 1, NNODE  ! OUTER LOOP

            ! DO i = 1, NNODE  ! NORMALIZE
            !        m_length = SQRT(m(i, 1)**2+m(i, 2)**2+m(i, 3)**2)
            !       IF (m_length /= 0.) THEN
            !          m(i, 1)=m(i, 1)/m_length
            !          m(i, 2)=m(i, 2)/m_length
            !          m(i, 3)=m(i, 3)/m_length
            !       ENDIF
            !     END DO
            !m(1, :) = 0.

            mag_av(:) = 0
            mag_max = 0
            ! calculate the average magnetisation per node over neighbouring elements which cam be of
            ! different material
            do l = CNR_IM(n), CNR_IM(n+1) - 1
               mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
               mag_max = mag_max+Ms(SDNR_IM(l))*InterpolationMatrix(l)
            end do
            mag_maxv = mag_maxv+mag_max*vbox(n)
            MeanMag(:) = MeanMag(:) + mag_av(:)*vbox(n)
         end do   ! END OUTER LOOP

         MeanMag(:) = MeanMag(:)/mag_maxv
      end if

      ! print*, 'in effective field 2'

      ! Calculates demag and Laplace exchange (gradient+energy)
      call CalcDemagEx()
      !  print*, 'in effective field 3'
      ! Calculates anisotropy and external field (gradient+energy)
      call CalcAnisExt()
      !      print*, 'in effective field 4'
      ! Calculates exchange
      call ExGrad()
      !   print*, 'in effective field 5'

      !do i = 1, size(ExtraEnergyCalculators)
      !   call ExtraEnergyCalculators(i)%Run()
      !end do

      !
      !      1)   Collect total energy

      etot = AnisEnerg+ExchangeE+DemagEnerg+BEnerg+StressEnerg

      !do i = 1, size(ExtraEnergyCalculators)
      !   etot = etot+ExtraEnergyCalculators(i)%Energy
      !end do

      if (AddInitPathEnergyQ) then
         edist = InitDelta-Distance(PMagInit, m(:, :))
         edist = InitAlpha*edist*edist
         etot = etot+edist
      end if

      if (EnergyLogging) then
         if (AddInitPathEnergyQ) then
            if (firstrow == 0) then
               write (logunit, *) "FIRST ROW"
               write (logunit, '(2A12, 1A20, 10A28)') &
                  "Global-N-Eval", "N-Eval", "E-Anis", "E-Stress", "E-Exch", &
                  "E-ext", "E-Demag", "E-distance", &
                  "E-Tot", "Mx", "My", "Mz"
               firstrow = 1
            end if
            write (logunit, '(2I12, TR2, 10E28.20)') &
               global_neval, neval, AnisEnerg/EnScale, StressEnerg/EnScale, &
               UsedExchangeEn/EnScale, BEnerg/EnScale, DemagEnerg/EnScale, edist/EnScale, &
               etot/EnScale, &
               MeanMag(1), MeanMag(2), MeanMag(3)
         else
            write (logunit, '(2I12, TR2, 12E28.20)') &
               global_neval, neval, AnisEnerg/EnScale, StressEnerg/EnScale, ExchangeE/EnScale, &
               ExEnerg2/EnScale, ExEnerg3/EnScale, ExEnerg4/EnScale, &
               BEnerg/EnScale, DemagEnerg/EnScale, etot/EnScale, &
               MeanMag(1), MeanMag(2), MeanMag(3)
         end if
      end if

      !
      !      2)   Collect total gradient
      !
      !    Print*, ' whichexchange = ', WhichExchangeHere
      do i = 1, NNODE
         gradc(i, :) = hanis(i, :) + hstress(i, :) + hdemag(i, :) + hext(i, :)
         !gradc(i, :) = hext(i, :)

         !      select case(WhichExchangeHere)
         !        case(1)
         gradc(i, :) = gradc(i, :) + hexch(i, :)  !ie alwasy use case 1
         !        case(2)
         !          gradc(i, :)= gradc(i, :)+hexch2(i, :)
         !        case(3)
         !                   gradc(i, :)= gradc(i, :)+hexch3(i, :)
         !        case(4)
         !          gradc(i, :)= gradc(i, :)+hexch4(i, :)
         !      end select

      end do

      !do i = 1, size(ExtraEnergyCalculators)
      !   gradc = gradc+ExtraEnergyCalculators(i)%h
      !end do

      ! I dont think LLG will work with NEB path-comment out until validated
      ! if(AddInitPathEnergyQ) then !! InitPathEnergy gradient should be added!!
      !   dist = Distance(PMag(InitRefPos, :,:), m(:,:))
      !   DO i = 1, NNODE
      !     gradc(i, :) = gradc(i, :) &
      !       + 2*InitAlpha*(InitDelta/dist-1) &
      !         *PMag(InitRefPos, i, :) &
      !         *vbox(i)/total_volume  !! InitPathEnergy gradient!!
      !   END DO
      ! endif

      heffc(:, :) = 0

      ! convert gradients to effective fields. Effective field = - grad [ (mag energy)/ (mag polarization*vol)], units of Am^-1
      ! OR ELSE in units of TESLA if Effective field = - grad [ (mag energy)/ (magnetisation*vol)]
      ! here we have EFFCTIVE FIELD in units of TESLA, , as these are the unit of external field tht we normall use
      ! NB Ms cannot take a zero non-mag value

      Msmax = maxval(ms)

      do i = 1, NNODE

         ! calulate ms_average*boxvol, note that boxvol is included in the Interpolation
         ms_average = 0
         do l = CNR_IM(i), CNR_IM(i+1) - 1
            ms_average = ms_average+Ms(SDNR_IM(l))*InterpolationMatrix(l)
         end do
         !   heffc(i, :) = - gradc(i, :)/(Msmax*vbox(i))  ! units of TESLA
         heffc(i, :) = -gradc(i, :)/ms_average  ! units of TESLA

      end do

      k1max = maxval(abs(K1))  ! this is just for normalization, so dont need to calculate 'average' K per node
      ! divide by Hk = 2K/M   which also hase units of TESLA

      if (k1max /= 0.) then
         do i = 1, NNODE
            heffc(i, :) = (msmax*heffc(i, :))/(2*abs(k1max))  ! dimensionless
         end do
      end if

      !     3)   Set gradient at fixed magnetizations to zero

      if (NFIX > 0) then   !  In case of fixed nodes
         do i = 1, NNODE
            if (NodeBlockNumber(i) .eq. 1) heffc(i, :) = 0    ! All free nodes are in block number 0 !!
         end do
      end if

      do i = 1, NNODE
         !       Landau-LIFshitz Gilbert
         mdot(i, 1) = (-(m(i, 2)*heffc(i, 3) - m(i, 3)  &
             &  *heffc(i, 2)) - ag*(m(i, 2)*(m(i, 1)*heffc(i, 2) - m(i, 2)*heffc(i, 1)) &
             &  - m(i, 3)*(m(i, 3)*heffc(i, 1) - m(i, 1)*heffc(i, 3))))/(1+ag**2)

         mdot(i, 2) = (-(m(i, 3)*heffc(i, 1) - m(i, 1)  &
             &  *heffc(i, 3)) - ag*(m(i, 3)*(m(i, 2)*heffc(i, 3) - m(i, 3)*heffc(i, 2)) &
             &  - m(i, 1)*(m(i, 1)*heffc(i, 2) - m(i, 2)*heffc(i, 1))))/(1+ag**2)

         mdot(i, 3) = (-(m(i, 1)*heffc(i, 2) - m(i, 2)  &
             &  *heffc(i, 1)) - ag*(m(i, 1)*(m(i, 3)*heffc(i, 1) - m(i, 1)*heffc(i, 3)) &
             &  - m(i, 2)*(m(i, 2)*heffc(i, 3) - m(i, 3)*heffc(i, 2))))/(1+ag**2)
         !  print*, i, 'heffc, mdot', heffc(i, :), m(i, :)
      end do

      !  ETOT = ETOT/EnScale  ! scale Etot so it can be used in LLG for output

      return
   end subroutine EFFECTIVE_field

   subroutine calc_LLG(ag, mDOT)

      use Material_Parameters
      use Tetrahedral_Mesh_Data, only: NNODE, heffc, m
      implicit none

      double precision, intent(IN):: ag
      double precision, intent(OUT):: mDOT(NNODE, 3)
      integer i

      do i = 1, NNODE
         !       Landau-LIFshitz Gilbert
         mdot(i, 1) = (-(m(i, 2)*heffc(i, 3) - m(i, 3)  &
             &  *heffc(i, 2)) - ag*(m(i, 2)*(m(i, 1)*heffc(i, 2) - m(i, 2)*heffc(i, 1)) &
             &  - m(i, 3)*(m(i, 3)*heffc(i, 1) - m(i, 1)*heffc(i, 3))))/(1+ag**2)

         mdot(i, 2) = (-(m(i, 3)*heffc(i, 1) - m(i, 1)  &
             &  *heffc(i, 3)) - ag*(m(i, 3)*(m(i, 2)*heffc(i, 3) - m(i, 3)*heffc(i, 2)) &
             &  - m(i, 1)*(m(i, 1)*heffc(i, 2) - m(i, 2)*heffc(i, 1))))/(1+ag**2)

         mdot(i, 3) = (-(m(i, 1)*heffc(i, 2) - m(i, 2)  &
             &  *heffc(i, 1)) - ag*(m(i, 1)*(m(i, 3)*heffc(i, 1) - m(i, 1)*heffc(i, 3)) &
             &  - m(i, 2)*(m(i, 2)*heffc(i, 3) - m(i, 3)*heffc(i, 2))))/(1+ag**2)

      end do

      return
   end subroutine calc_LLG

   !-----------------------------------------------------------------------------------------------------
   ! SDEnergySurface(theta_min, theta_max, theta_delta, phi_min, phi_max, phi_delta, Surf_filename
   !        outputs SD magnetizatio direction and SD total energy and energy components
   !----------------------------------------------------------------------------------------------------

   !> Update all the magnetic fields and energies and return the effective field
   !> derived form total gradient.
   !> @param[out]   ETOT  The NORMALIZED total magnetic energy (note difference to enerymin routines that return non-normalized)
   !> @param[in]    theta_min   the min value of theta angle
   !> @param[in]    theta_max   the max value of theta angle
   !> @param[in]    theta_delta   the increment value of theta angle
   !> @param[in]    phi_min   the min value of phi angle
   !> @param[in]    phi_max  the min value of phi angle
   !> @param[in]    phi_delta   the increment value of phi angle
   !> @param[in]    Surf_filename   the name of the output file
   subroutine SDEnergySurface(theta_min, theta_max, theta_delta, phi_min, &
           & phi_max, phi_delta, Surf_filename)

      use Utils, only: pi
      use Material_Parameters, only: AnisEnerg, BEnerg, DemagEnerg, Ls, StressEnerg
      use Tetrahedral_Mesh_Data, only: NNODE
      implicit none

      integer:: i, neval, surfunit
      double precision:: theta_min, theta_max, theta_delta, phi_min, x, y, z, &
          & phi_max, phi_delta, theta, phi, ETOT, ls_scale, theta_rad, phi_rad
      character(len = 55) ::  Surf_filename
      double precision:: Xtest(NNODE*2), grad_dummy(NNODE*2)

      theta = theta_min
      phi = phi_min
      neval = 1

      ! scaling value to report energiesin units of Joules
      ls_scale = sqrt(Ls)**3
      ! print*,'stem', Surf_filename, pi
      ! print*, theta, theta_min, theta_max, theta_delta

      open ( &
          NEWUNIT = surfunit, FILE = Surf_filename(:len_trim(Surf_filename)), &
          & STATUS='unknown')

      ! write headder to CSV file.
      write (surfunit, *) 'X, Y, Z, theta, phi, Total Energy, Anisotropy, Demag, External, Stress'

      ! calculate the direction consines
      do while (theta <= theta_max)
         do while (phi <= phi_max)
            phi_rad = phi*pi/180.0
            theta_rad = theta*pi/180.0
            do i = 1, NNODE
               Xtest(2*i) = phi_rad
               Xtest(2*i - 1) = theta_rad
            end do
            x = sin(theta_rad)*cos(phi_rad)
            y = sin(theta_rad)*sin(phi_rad)
            z = cos(theta_rad)
            call ET_GRAD_POLAR('Z', ETOT, grad_dummy, Xtest, neval)
            write (surfunit, 101) x, y, z, theta, phi, etot/ls_scale, AnisEnerg/ls_scale, DemagEnerg/ls_scale, &
                & BEnerg/ls_scale, StressEnerg/ls_scale
            !  print*,' etot =', etot
            !      Xtest(:,1) = sin(theta*pi/180.)*cos(phi*pi/180.)
            !      Xtest(:,2) = sin(theta*pi/180.)*sin(phi*pi/180.)
            !      Xtest(:,3) = cos(theta*pi/180.)
            phi = phi+phi_delta
         end do
         phi = phi_min
         theta = theta+theta_delta
         !print*, 'theta=', theta
      end do

      ! CALL ET_GRAD(ETOT, grad_dummy, Xtest, neval)

      ! Formatting for CSV
101   format(1x, *(g0, ", "))

      return
   end subroutine SDEnergySurface

end module Energy_Calculator

