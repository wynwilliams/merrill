!> Using H2Lib objects and functions to carry out the
!> Finite Element Method - Boundary Element Method calculation, obtaining
!> field \phi (value at each node) from a given magnetic vector potential field M.
!> See eg Fredkin & Koehler 1990, Lindholm 1984, Hertel 2019.
!>
!> Functions to generate matrices PNM, AB, PDM from mesh geometry during startup
!>
!> Functions for FEM-BEM steps matrix-vector calculations
!> 1. Solve PNM * \phi_1 = div M for \phi_1.
!> 2. Solve \phi_2^b = AB * \phi^b_1 at boundary nodes.
!> 3. Solve PDM * \phi_2 = \vec{0} for rest of \phi_2, given \phi_2^b values on boundary.
!>
!> Note: only compiled with Cmake option -DMERRILL_USE_H2LIB=ON (default)
module H2LibFEMBEM

   use iso_c_binding, only: c_ptr, c_int, c_double, c_bool
   implicit none

contains
   subroutine SolveFEMH2LibPNM(phi1, b)
      !> This performs the FEM conjugate gradient solver step to get \phi1
      !> from a given div M, with the geometry's fixed stiffness matrices.
      !> This replaces slatec dsicgg call to conjugate gradient solver
      !> and preparation .
      use Tetrahedral_Mesh_Data, only: NNODE
      use H2Lib_avector, only: f_avector, f_new_avector, f_new_pointer_avector, f_del_avector
      use H2Lib_sparsematrix, only: f_addeval_sparsematrix_avector, f_solve_cg_sparsematrix_avector
      use Finite_Element_Matrices, only: PNM, PNM_FIXED
      use iso_c_binding, only: c_f_pointer, c_loc
      integer(c_int) :: steps !max number of CG solver steps
      real(c_double)  :: eps !solver accuracy
      integer(c_int) :: iter ! returned by cg solver

      real(c_double), dimension(:), intent(inout), target :: phi1 !solution of A phi1 = b, and initial guess
      real(c_double), dimension(:), intent(in), target :: b !RHS of A phi1 = b
      type(c_ptr) :: fixed_values_avector, b_avector ! pointer to b as H2Lib avector
      type(c_ptr) :: phi1_avector
      type(f_avector), pointer :: sln_fptr
      real(c_double), dimension(1), target :: fixed_values
      real(c_double), dimension(:), pointer :: phi1_fptr

      ! make b into an avector object matching sparsity, fixed nodes structure of A
      b_avector = f_new_pointer_avector(c_loc(b(1:NNODE - 1)), NNODE - 1)
      ! subtract A_f * d from RHS to impose the fixed node constraint, here d is value vector (0.0)
      ! of single fixed node and A_f is NNODE-1 x 1 matrix
      fixed_values(1) = 0
      fixed_values_avector = f_new_pointer_avector(c_loc(fixed_values), 1)
      call f_addeval_sparsematrix_avector(-1.0_c_double, PNM_fixed, fixed_values_avector, b_avector)

      ! avector to store solution for all but fixed node
      phi1_avector = f_new_pointer_avector(c_loc(phi1(1:NNODE - 1)), NNODE - 1)

      steps = 2500
      eps = 1e-12
      ! do the conjugate gradient solver
      iter = f_solve_cg_sparsematrix_avector(PNM, b_avector, phi1_avector, Eps, steps)

      !get data out of the avector c struct
      call c_f_pointer(phi1_avector, sln_fptr)
      call c_f_pointer(sln_fptr%v, phi1_fptr, [NNODE - 1])
      ! fill output fortran array
      phi1(1:NNODE - 1) = phi1_fptr
      phi1(NNODE) = 0_c_double

      call f_del_avector(fixed_values_avector)
      call f_del_avector(b_avector)
      call f_del_avector(phi1_avector)

   end subroutine

   ! TODO could be a pure function
   !> Carries out the matrix-vector multiplication \phi_2 = AB * \phi_1
   !> over surface nodes to obtain values of \phi_2 at surface.
   !> using the AB object saved at this module's data.  Using H2matrix
   !> format from H2Lib.
   !> intent(in) : surfacephi1, values of scalar field phi1 at surface nodes.
   !> intent(out) : surfacephi2,  values of scalar field phi2 at surface nodes.
   !>
   !> Both vectors Fortran array of c-interoperable real type, length BNODE,
   !> indexed by surface node in same order as in tetrahedralmeshdata boundary_node_index .
   subroutine matmulH2AB(surfacephi1, surfacephi2)
      use Tetrahedral_Mesh_Data, only: BNODE
      use H2Lib_avector, only: f_avector, f_new_avector, f_new_pointer_avector, f_clear_avector, &
                              & f_del_avector
      use H2Lib_h2matrix, only: f_addevaltrans_h2matrix_avector
      use Finite_Element_Matrices, only: BA_h2
      use iso_c_binding, only: c_double, c_ptr, c_loc, c_f_pointer
      real(c_double), dimension(:), intent(in), target :: surfacephi1
      type(f_avector), pointer :: phi2_fptr !fptr to avector struct
      real(c_double), dimension(:), intent(out), target :: surfacephi2
      real(c_double), dimension(:), pointer :: surfacephi2_fptr
        !!!!!! Solving matrix vector multiplication \phi_2 = KM \phi_1 !!!!!!

      integer(c_int) :: nvertices
      type(c_ptr) :: phi2, phi1 !avectors

      ! Prepare vector phi2 to recieve solution
      ! phi2,  vertices basis goes with linear basis
      Nvertices = BNODE
      phi2 = f_new_avector(BNODE)
      call f_clear_avector(phi2)

      !prepare vector phi1
      !phi1 input data: previously calculated values of phi1 on surface nodes only
      !convert from fortran array to avector ptr
      phi1 = f_new_pointer_avector(c_loc(surfacephi1), BNODE)
      !write (*,*) "surfacephi1", surfacephi1(1), surfacephi1(2), surfacephi1(3)

      ! reference the module's AB matrix h2matrix , previously filled
      call f_addevaltrans_h2matrix_avector(1.0_c_double, BA_H2, phi1, phi2)
      ! transfer data from resulting avector to output fortran array
      call c_f_pointer(phi2, phi2_fptr)
      call c_f_pointer(phi2_fptr%v, surfacephi2_fptr, [BNODE])
      surfacephi2 = surfacephi2_fptr

      ! TODO consider reusing storage location as argument
      ! or module data instead of
      ! creating, deallocating every time
      call f_del_avector(phi1)
      call f_del_avector(phi2)

   end subroutine

   subroutine SolveFEMH2LibPDM(phi2, phi2boundary)
      !> This performs the FEM conjugate gradient solver step to get \phi1
      !> from a given M, with the geometry's fixed stiffness matrices.
      !> This replaces slatec dsicgg call to conjugate gradient solver
      !> and preparation .
      !> Inout: phi2, with entries corresponding to boundary nodes set and bulk nodes to fill
      !> In: phi2boundary, fixed values at boundary nodes as Dirchlet BC
      use Tetrahedral_Mesh_Data, only: BNODE, NNODE
      use H2Lib_avector, only: f_avector, f_new_avector, f_new_pointer_avector, f_del_avector
      use H2Lib_sparsematrix, only: f_sparsematrix, f_addeval_sparsematrix_avector, f_solve_cg_sparsematrix_avector
      use Finite_Element_Matrices, only: PDM, PDM_fixed, PDM_isdof
      use iso_c_binding, only: c_f_pointer, c_loc
      integer(c_int) :: steps !max number of CG solver steps
      real(c_double)  :: eps !solver accuracy
      integer(c_int) :: iter ! returned by cg solver

      real(c_double), dimension(:), intent(inout), target :: phi2 !solution of A phi2 = b, and initial guess
      real(c_double), dimension(:), intent(in), target :: phi2boundary !RHS of A phi1 = b
      real(c_double), dimension(:), allocatable, target :: b
      type(c_ptr) :: b_avector ! pointer to b (RHS) as H2Lib avector
      real(c_double), dimension(:), allocatable, target :: phi2bulk
      type(c_ptr) :: phi2bulk_avector, phi2boundary_avector
      type(f_avector), pointer :: b_avector_fptr
      type(f_avector), pointer :: sln_fptr
      real(c_double), dimension(:), pointer :: phi2bulk_fptr, b_data_fptr

      ! get nfix ndof

      ! make phi2boundary into an avector object
      phi2boundary_avector = f_new_pointer_avector(c_loc(phi2boundary), BNODE)

      ! vector to hold solution phi2 values for bulk nodes
      allocate (phi2bulk(NNODE - BNODE))
      phi2bulk = 0.0
      phi2bulk_avector = f_new_pointer_avector(c_loc(phi2bulk), NNODE - BNODE)

      !construct the RHS vector b= 0 - A_f * phi2boundary (imposing Dirichlet BC)
      allocate (b(NNODE - BNODE))
      b = 0
      b_avector = f_new_pointer_avector(c_loc(b), NNODE - BNODE)

      call f_addeval_sparsematrix_avector(-1.0_c_double, PDM_fixed, phi2boundary_avector, b_avector)

      ! Use as RHS in conjugate gradient solver

      steps = 2500
      eps = 1e-12

      ! do the conjugate gradient solver
      call c_f_pointer(b_avector, b_avector_fptr)
      !call c_f_pointer(b_avector_fptr%v, b_data_fptr, [NNODE-BNODE])

      iter = f_solve_cg_sparsematrix_avector(PDM, b_avector, phi2bulk_avector, Eps, steps)

      !get data out of the c struct
      call c_f_pointer(phi2bulk_avector, sln_fptr)
      call c_f_pointer(sln_fptr%v, phi2bulk_fptr, [NNODE - BNODE])

      ! Assemble full phi2 vector over all node indices by filling
      ! boundary node values and bulk node values.  My fortran function.
      phi2 = assemble_free_fixed(phi2boundary, phi2bulk_fptr, PDM_isdof, BNODE, NNODE - BNODE)

      ! TODO consider reusing storage location as argument
      ! or module data instead of
      ! creating, deallocating every time
      call f_del_avector(b_avector) ! holds array b, too
      call f_del_avector(phi2bulk_avector) !holds phi2bulk
      call f_del_avector(phi2boundary_avector) !including deallocating the array from intent(in)?

   end subroutine

   !> Utility function
   !> Assemble an array of length N+M from vectors of length N and M,
   !> by taking from values1 (boundary) when bool array mask is 0
   pure function assemble_free_fixed(values1, values2, mask, N, M) result(result_)
      integer, intent(in) :: N, M
      real(kind=c_double), dimension(N), intent(in) :: values1
      real(kind=c_double), dimension(M), intent(in) ::  values2
      real(kind=c_double), dimension(N + M)  :: result_
      logical(kind=c_bool), dimension(N + M), intent(in) :: mask
      integer :: i, j, k
      j = 1
      k = 1
      do i = 1, N + M
         if (mask(i)) then !bulk
            result_(i) = values2(j)
            j = j + 1
         else !surface
            result_(i) = values1(k)
            k = k + 1
         end if
      end do
   end function

end module
