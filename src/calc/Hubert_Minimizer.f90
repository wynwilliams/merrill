!> A Module containing the Hubert Minimizer routine.
module Hubert_Minimizer
   use Utils, only: DP
   implicit none

   !> Use Conjugent Gradient steps?
   logical:: ConGradQ

   ! Conversion tolerances

   !> Finished if consecutive Energies with distance
   !> `FTrailLength` are < `FTolerance`.
   real(KIND = DP):: FTolerance

   !> Finished if Average gradient^2 < `GTolerance`
   real(KIND = DP):: GTolerance

   !> Scales the gradient value to a step size
   real(KIND = DP):: AlphaScale

   !> Sets the minimum alpha step size
   real(KIND = DP):: MinAlpha

   !> Controls the alpha acceleration/deceleration
   real(KIND = DP):: DAlpha

contains

   !> Initialize the variables in the hubert_minimizer module.
   subroutine InitializeHubertMinimizer()
      FTolerance = 1.e-8
      GTolerance = 1.e-14

      ConGradQ = .true.

      AlphaScale = 0.01
      MinAlpha = 1e-3
      DAlpha = 2.6

   end subroutine InitializeHubertMinimizer

   !> Run the Hubert Minimizer to find the LEM of ET_GRAD.
   !>
   !> @param[in] ET_GRAD The function to calculate magnetic energies and
   !>                    gradients.
   !> @param[in]    N      The size of the temporary arrays. N = 3*NNODE.or 2*NNODE
   !>                      for cartesian or Spherical
   !> @param[in]    XGUESS The initial guess for the LEM.
   !> @param[inout] X      The working array for holding the magnetization
   !>                      (cartesian or spherical). This will contain the LEM
   !>                      when the subroutine finishes, assuming convergance.
   !> @param[inout] G      The working array for the energy gradients.
   !> @param[out]   FVALUE The magnetic energy
   !> @param[inout] neval  The number of times ET_GRAD has been called.
   !> @param[in] coord     The coordinate system to be used during minimisation
   subroutine HubertMinimize(axis, coord, opt_out, N, XGUESS, X, G, FVALUE, neval)
      use Utils, only: NONZERO
      use Material_Parameters, only: Ls, Kd, Aex, K1, Ms, MaxEnergyEval, switch_count, switch_per
      use Tetrahedral_Mesh_Data, only: total_volume, NNODE, MeanMag, m
      use Finite_Element_Matrices, only: FEMTolerance
      use Energy_Calculator, only: ET_GRAD_POLAR, ET_GRAD_CART
      use System_Parameters, only: TypicalEnergy, invTE

      implicit none

!       ET_GRAD_CART and ET_GRAD have the same form but will have different array sizes
!        PROCEDURE(ET_GRAD_PROCEDURE):: ET_GRAD_CART
      !PROCEDURE(ET_GRAD_PROCEDURE):: ET_GRAD_BOTH

      ! SPECIFICATIONS FOR ARGUMENTS
      integer:: N, neval

      ! SPECIFICATIONS FOR LOCAL VARIABLES

      integer:: CREEPCNT, MAXCREEP, FINISHED, &
                 FTrailLength, nstart, i, ResetCnt, switch_crit1, switch_crit2

      real(KIND = DP):: XNEW(N), GRNEW(N), FVALUE, XGUESS(N), X(N), G(N)

      !  For conjugate gradient steps
      real(KIND = DP):: S(N), GOLD(N), beta, GO2, SPG

      real(KIND = DP):: ALPHA, &
                       FNEW, DeltaF, GSQUARE, FTOLMin, &
                       FEMTolMax, VECNORMVAL
      real(KIND = DP), allocatable:: FTrail(:)

      ! SPECIFICATIONS FOR INTRINSICS
      intrinsic ABS, MAX, MIN

      ! SPECIFICATION of COOR SYSTEM
      character*5:: coord, opt_out
      character*1:: axis

!        opt_out='noset' ! optimisation ending type not set yet-moved into energymin calls
      X = XGUESS
      !  FOR X
      !  DETERMINE INITIAL ENERGY: FVALUE
      !             AND GRADIENT : G

      MAXCREEP = 4         ! Controls the number of creep steps

      FTrailLength = 13    ! Length of trailing energy value for finishing
      allocate (FTrail(FTrailLength))
      nstart = 1
      FTrail(:) = 0.0
      GOLd(:) = 0.
      S(:) = 0.
      beta = 0.

      FEMTolMax = 1.d-10         ! Controlling tolerance of linear CG solver
      FEMTolerance = FEMTolMax  ! via global FEMTolerance
      FTOLMin = FEMTolMax       ! should speed up minimization
      ResetCnt = 0
      FNEW = 0
      GSQUARE = 0.
      ! ConGradQ =.true.  ! Now global variable set in BEMScript.f90

      ! Calculate typical energy scale
      ! (order of magnitude estimate of micromagnetic energy)
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = &
            (sqrt(Ls)**3) &
            *sqrt(Kd*sqrt(Aex(maxloc(Ms, 1))*abs(K1(maxloc(Ms, 1))))) &
            *(total_volume/(sqrt(Ls)**3))**(5./6.)
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = Kd*total_volume
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = maxval(Aex)*Ls*total_volume**(1./3.)
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = maxval(abs(K1))*total_volume
      end if
!         print*,'opt_out = ', opt_out
      if (opt_out .ne. 'force') write (*, *) " Typical Energy: ", TypicalEnergy

      invTE = 100./TypicalEnergy

10    GOLD(:) = G(:)

      if (coord .eq. "xyz" .or. coord .eq. "cart" .or. axis == 'C') then
         ! Normalize X
         do i = 1, NNODE
            VECNORMVAL = sqrt(X(3*i - 2)**2+X(3*i - 1)**2+X(3*i - 0)**2)
            X(3*i - 2) = X(3*i - 2)/VECNORMVAL
            X(3*i - 1) = X(3*i - 1)/VECNORMVAL
            X(3*i - 0) = X(3*i - 0)/VECNORMVAL
         end do
         call ET_GRAD_Cart(fvalue, G, X, neval)
      else

         call ET_GRAD_POLAR(axis, fvalue, G, X, neval)
      end if
      ! Energy fvalue and gradient G are used to inform next magnetic field

      !DO i = 1, N
      !  write(*,*) G(i)
      !ENDDO

      FValue = FValue*invTE
      G(:) = G(:)*invTE

      GO2 = GSQUARE
      GSQUARE = 0
      SPG = 0.
      do i = 1, N
         GSQUARE = GSQUARE+G(i)*G(i)
         SPG = SPG+GOLD(i)*G(i)
      end do
      GSQUARE = GSQUARE
      if (NONZERO(GO2)) then
         ! for Polak–Ribière CG with reset
         beta = max(real(0.0, KIND = kind(GO2)), (GSQUARE-SPG)/GO2)
      else
         beta = 0.
      end if
      S(:) = G(:) + beta*S(:)  ! updated conjugate gradient direction

      FTrail(nstart) = FValue
      nstart = nstart+1
      if (nstart > FTrailLength) nstart = 1
      ALPHA = 1.0

      if (modulo(NEval, 100) .eq. 0) then
         write (*, '(I5, A5, 4F15.6)') NEval, axis, ALPHA, DeltaF/FTolerance, &
            sqrt(MeanMag(1)**2+MeanMag(2)**2+MeanMag(3)**2), FNEW
      end if
      if (modulo(NEval, 1000) .eq. 1) then
         write (*, '(A5, A5, 4A15)') 'NEval', 'Axis', 'ALPHA', 'dEn/TOL', 'Mag', 'Energy'
      end if

      !20 FINISHED = 0
      FINISHED = 0
30    CREEPCNT = 0
      do while (CREEPCNT < MAXCREEP)

         !write(*,*) "alpha: ", alpha

         if (ConGradQ) then
            XNEW(:) = X(:) - ALPHA*AlphaScale*S(:)
         else
            XNEW(:) = X(:) - ALPHA*AlphaScale*G(:)
         end if

         !write(*,*) "xnew: ", xnew(1)

!   enable cartesian by uncommenting the following
         if (coord .eq. "xyz" .or. coord .eq. "cart" .or. axis == 'C') then
            ! NORMALIZE XNEW

            do i = 1, NNODE
               VECNORMVAL = sqrt(XNEW(3*i - 2)**2+XNEW(3*i - 1)**2+XNEW(3*i - 0)**2)
               XNEW(3*i - 2) = XNEW(3*i - 2)/VECNORMVAL
               XNEW(3*i - 1) = XNEW(3*i - 1)/VECNORMVAL
               XNEW(3*i - 0) = XNEW(3*i - 0)/VECNORMVAL
            end do
            call ET_GRAD_CART(FNEW, GRNEW, XNEW, neval)
         else

            call ET_GRAD_POLAR(axis, FNEW, GRNEW, XNEW, neval)
            !write(*,*) "FNEW: ", FNEW
         end if
         FNEW = FNEW*invTE
         GRNEW(:) = GRNEW(:)*invTE

         FTrail(nstart) = FNEW
         nstart = nstart+1
         if (nstart > FTrailLength) nstart = 1

         if (modulo(NEval, 100) .eq. 0) then
            write (*, '(I5, A5, 4F15.6)') NEval, axis, ALPHA, DeltaF/FTolerance, &
               sqrt(MeanMag(1)**2+MeanMag(2)**2+MeanMag(3)**2), FNEW
         end if
         if (modulo(NEval, 1000) .eq. 1) then
            write (*, '(A5, A5, 4A15)') 'NEval', 'Axis', 'ALPHA', 'dEn/TOL', &
               'Mag', 'Energy'
         end if

         ! Average step difference between trailing F and new F
         DeltaF = abs(FTrail(nstart) - FNEW)/FTrailLength

         if (DeltaF < FTolerance) then
            write (*, *) 'Delta F negligible:', DeltaF
            opt_out = "noset" ! i.e NOT a forced end for polar axis reset
            goto 100  ! FINISHED Delta F negligible
         end if
         if (NEval .ge. MaxEnergyEval) then
            write (*, *) 'MAX Energy Evaluations reached !  Delta F:', DeltaF
            opt_out = "noset" ! i.e NOT a forced end for polar axis reset
            goto 100  ! FINISHED TOO MANY ENERGY CALLS
         end if
!  FORCE change in polar angle for coordinate system if using 'mult' option
!   initially along Z then change to X then Y. Note that fmultiswitching caresian coord is xyz, 
! for cartesian only then coord is eq to 'cart' and we don't force switching
!            print*, 'switch count, per',Switch_Count, Switch_Per

! switch polar angle after Number of Energy Evaluations = Switch_per % of MaxEnergyEval (input parameter default 3%)
         switch_crit1 = (modulo(NEval, int(MaxEnergyEval*Switch_Per/100.0)))

! switch every Switch_Count energy evaluations ( defaults to 300)
         switch_crit2 = (modulo(NEval, Switch_Count))

         if ((switch_crit1 .eq. 0 .or. switch_crit2 .eq. 0) .and. (coord .eq. 'mult' .or. coord .eq. 'xyz')) then  ! i.e every 3% of maxenenergyvals

            opt_out = 'force' ! use coord variable to show optimisation had forced end to switch polar angle
            goto 100  ! forced change in polar axis
         end if

         if (FVALUE < FNEW) then
            CREEPCNT = 0
            ALPHA = ALPHA/Dalpha/DAlpha
            !       Write(*,*) 'QQ HM-> ALPHA = ',ALPHA
            if (ALPHA < MinAlpha) then
               ResetCnt = ResetCnt+1
               write (*, '(I5, A5, 4F15.6, A11, I3)') &
                  NEval, axis, ALPHA, DeltaF/FTolerance, &
                  sqrt(MeanMag(1)**2+MeanMag(2)**2+MeanMag(3)**2), &
                  FNEW, ' ->/```** magic: ', ResetCnt
               if (ResetCnt > 0) then

!   enable cartesian by uncommenting the following
                  if (coord .eq. "xyz" .or. coord .eq. "cart") then
                     do i = 1, NNODE
                        m(i, 1) = X(3*i - 2)
                        m(i, 2) = X(3*i - 1)
                        m(i, 3) = X(3*i - 0)
                     end do
                  else

                     ! Try magic !
                     !       Write(*,*)   '**  --> MAGIC '

                     ! Reset angles
                     ! (Implicit random perturbation !?
                     ! Remove gimbal lock effects?)
                     do i = 1, NNODE
                        m(i, 1) = sin(X(2*i - 1))*cos(X(2*i))
                        m(i, 2) = sin(X(2*i - 1))*sin(X(2*i))
                        m(i, 3) = cos(X(2*i - 1))
                        X(2*i - 1) = acos(m(i, 3))
                        X(2*i) = atan2(m(i, 2), m(i, 1))
                     end do
!   enable cartesian by uncommenting the following line
                  end if

               end if
               if (ResetCnt > 20) then
                  opt_out = 'force' !means it will loop through . Change to 'noset' to cause to terminate minimisation on fail
                  ! axis='F'! to force switch to cartesian for next iteration
                  write (*, *) '+++++   FAILED TO CONVERGE  +++++'
                  write (*, *) '+++++   FORCING POLAR AXIS SWITCH, CONTINUING TO MINIMIZE  +++++'
                  goto 100
               end if

               goto 10 !    !!  ALPHA TOO SMALL: RESTART
            end if
         else
            CREEPCNT = CREEPCNT+1
            FVALUE = FNEW
            X(:) = XNEW(:)
            GOLD(:) = G(:)
            G(:) = GRNEW(:)

            GO2 = GSQUARE
            GSQUARE = 0
            SPG = 0.
            do i = 1, N
               GSQUARE = GSQUARE+GRNEW(i)*GRNEW(i)
               SPG = SPG+GOLD(i)*GRNEW(i)
            end do
            ! for Polak–Ribière CG with reset
            beta = max(real(0.0, KIND = kind(GO2)), (GSQUARE-SPG)/GO2)
            S(:) = G(:) + beta*S(:)  ! updated conjugate gradient direction

            if (GSQUARE/N < GTolerance) then
               write (*, *) 'GRADIENT Negligible:', GSQUARE
               goto 100  ! FINISHED Grad = 0
            end if
         end if
      end do
      ALPHA = DAlpha*ALPHA
      ResetCnt = 0
      !     Write(*,*) 'QQ HM-> ALPHA = ',ALPHA
      goto 30

100   continue
      if (opt_out .ne. 'force') then
         write (*, *)
         write (*, *) 'MINIMIZATION FINISHED '
         write (*, *)
         write (*, *) '               ||      Energy Calls:', neval
         write (*, *) '               ||      Final Energy:', FVALUE
         write (*, *) '               ||        DeltaF/TOL:', DeltaF/FTolerance
         write (*, *) '               || sqrt(grad^2/GTOL):', GSQUARE/GTolerance
      end if

      return

   end subroutine HubertMinimize

end module Hubert_Minimizer
