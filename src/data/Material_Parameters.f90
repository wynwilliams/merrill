!> The Material_Parameters module contains variables and functions defining the
!> various physical constants for magnetic materials, along with the
!> energies and H-fields.
module Material_Parameters

   use Utils, only: DP
   !USE Tetrahedral_Mesh_Data !TODO place in each subroutine

   implicit none
   save

   !        Input from   .consts file variables

   ! Temperature T and Curie Temperature TC
   real(KIND=DP) T, TC

   real(KIND=DP) extappst, extappfin, extappstep
   real(KIND=DP) hz(3)
   character(LEN=16) ::  solver
   integer MF, JACFLG, JPRE, MAXORD, IZONE

   integer :: rest, MaxRestarts, MaxEnergyEval, MaxPathEval, WhichExchange

   ! paremeters controlling the switching between axis for multi-axis minimization
   ! using enerymin or energyminmult and during initialpath calculations
   ! in these types of minimization the proceeds from Polar axis Z--> Y --> X --> C
   ! where C is the cartesian system
   ! switch_count forces switching every switch_count number of energy evaluations
   ! switch_per
   integer :: switch_count, switch_per
   character*5 :: PathCoord

   !        Material parameters

   integer :: NMaterials
   ! For each material:
   ! Ms: M_s saturation magnetization
   ! Ku: K_u uniaxial anisotropy constant
   real(KIND=DP), target, allocatable :: Ms(:), K1(:), K2(:), Aex(:)
   real(KIND=DP), target, allocatable :: Ka(:), Kaa(:), Kb(:), Kbb(:), Kab(:), Ku(:)
   real(KIND=DP), target, allocatable :: K1p(:), K2p(:), K3p(:), K4p(:), K5p(:)
   ! material parameters for magnetostrictive  strain energy, compressive stress (SigmaS)
   ! and isotropic magnetostriction (LamdaS)
   real(KIND=DP), target, allocatable :: SigmaS(:), LamdaS(:)
   real(KIND=DP) :: Ls ! inverse length scale in 1/m
   real(KIND=DP) :: mu ! permeability
   real(KIND=DP) :: Kd, LambdaEx, QHardness, EnergyUnit

   ! supress warning messages
   integer:: warnflag

   ! Anisotropy forms

   integer, allocatable :: anisform(:)

   integer, parameter :: &
      ANISFORM_CUBIC = 1, &
      ANISFORM_UNIAXIAL = 2, &
      ANISFORM_MONOCLINIC = 3, &
      ANISFORM_MONOCLINIC_PYRR = 4, &
      ANISFORM_HEX_PYRR = 5

   ! Easy Axes for anisotropy
   ! EasyAxis(3,NMaterials)  -> Uniaxial Anisotropy
   !   EasyAxis((x,y,z), sd)
   ! CubicAxes(3,3,NMaterials) -> Cubic Anisotropy
   !   CubicAxes((x,y,z), n, sd) where n enumerates axis n

   real(KIND=DP), allocatable :: EasyAxis(:, :), CubicAxes(:, :, :), StressAxis(:, :)

   !        Time bookkeeping

   real(KIND=DP) cputime_now, cputime_start, cpuold
   integer elapsed_start, elapsed_now, elapsedold

   !        Problem size (usually = NNODES or 2*NNODES )

   ! External field strength
   real(KIND=DP) extapp

   !        Effective field contributions

   real(KIND=DP) magx, magy, magz, hznorm, eanorm

   real(KIND=DP) &
      DemagEnerg, BEnerg, &
      AnisEnerg, AnisEnerg1, AnisEnerg2, &
      StressEnerg, &
      ExchangeE, ExEnerg2, ExEnerg3, ExEnerg4

   real(KIND=DP), allocatable :: &
      DemagEnergSD(:), ExchangeESD(:), BEnergSD(:), StressEnergSD(:)

   real(KIND=DP), allocatable :: &
      Mag_netSD(:, :)

   real(KIND=DP), allocatable :: &
      AnisEnergSD(:, :)

   real(KIND=DP), allocatable :: &
      htot(:, :), &
      hanis(:, :), hdemag(:, :), hext(:, :), DemagH(:, :), &
      hexch(:, :), hexch2(:, :), hexch3(:, :), hexch4(:, :), &
      hstress(:, :)

   real(KIND=DP), allocatable :: DExPhi(:), DExTheta(:)

contains

   !---------------------------------------------------------------
   !            Evaluate program arguments
   !---------------------------------------------------------------

   ! Initialize all variables in this module
   subroutine InitializeMaterialParameters()

      call DestroyMaterialParameters()

      ! Unused
      ! mcon(6), T, ag, extappst ,extappfin, extappstep
      ! solver
      ! MF,JACFLG,JPRE,MAXORD
      ! magx,magy,magz,
      ! cputime_now,cputime_start , cpuold
      ! elapsed_start, elapsed_now, elapsedold

      ! warnings switched ON by default
      warnflag = 1
      ! External field
      hz = 0.0

      ! Minimizer parameters
      PathCoord = "noset"
      switch_count = 300
      switch_per = 101 !default switch percentage is never used since its greater than MaxEnergyEvals.
      rest = 0
      MaxRestarts = 4
      MaxEnergyEval = 100000
      MaxPathEval = 10000
      WhichExchange = 1 ! Laplace

      !
      ! Initializing multiphase dependent variables
      !

      ! We want Magnetite(t) to work as expected before BuildMaterialParameters
      ! is called.

      ! Material parameters
      NMaterials = 1

      ! Saturation Magnetization, Anisotropy, Exchange constants.
      allocate (Ms(1), K1(1), K2(1), Aex(1))
      allocate (Ka(1), Kb(1), Ku(1), Kaa(1), Kbb(1), Kab(1))
      allocate (K1p(1), K2p(1), K3p(1), K4p(1), K5p(1))
      allocate (SigmaS(1), LamdaS(1))
      allocate (DemagEnergSD(1), ExchangeESD(1), BEnergSD(1), StressEnergSD(1))
      allocate (AnisEnergSD(2, 1))
      allocate (Mag_netSD(3, 1))

      Ms = 0.0
      K1 = 0.0
      K2 = 0.0
      Aex = 0.0

      Ka = 0.0
      Kb = 0.0
      Ku = 0.0
      Kaa = 0.0
      Kbb = 0.0
      Kab = 0.0

      K1p = 0.0
      K2p = 0.0
      K3p = 0.0
      K4p = 0.0
      K5p = 0.0

      SigmaS = 0.0
      LamdaS = 0.0

      ! Anisotropy Axes
      allocate (anisform(1), EasyAxis(3, 1), CubicAxes(3, 3, 1))

      ! Compression Stress Axes
      allocate (StressAxis(3, 1))

      ! Anisotropy forms
      anisform(:) = ANISFORM_CUBIC

      ! Uniaxial axis
      EasyAxis(:, 1) = (/1.0, 0.0, 0.0/)

      ! Cubic Axes
      CubicAxes(:, :, :) = 0.0
      CubicAxes(1, 1, :) = 1.0
      CubicAxes(2, 2, :) = 1.0
      CubicAxes(3, 3, :) = 1.0

      !Default Stress axes along X
      StressAxis(:, 1) = (/1.0, 0.0, 0.0/)

      ! Default length scale in microns. Ls = length scale squared.
      Ls = 1e12
      ! Permeability of free space 4 pi 10^-7
      mu = 4*(4*atan(real(1, KIND=DP)))*1e-7

      Kd = 0.0
      LambdaEx = 0.0
      QHardness = 0.0
      EnergyUnit = 0.0

      ! !        Problem size (usually = NNODES or 2*NNODES )

      ! External field strength
      extapp = 0.0

      ! !        Effective field contributions

      ! REAL(KIND=DP)  hznorm,eanorm

      AnisEnerg = 0.0
      StressEnerg = 0.0
      DemagEnerg = 0.0
      BEnerg = 0.0
      ExchangeE = 0.0
      ExEnerg2 = 0.0
      ExEnerg3 = 0.0
      ExEnerg4 = 0.0

      DemagEnergSD = 0.0
      BEnergSD = 0.0
      ExchangeESD = 0.0
      StressEnergSD = 0.0
      AnisEnergSD(:, :) = 0.0
      Mag_netSD(:, :) = 0.0

   end subroutine InitializeMaterialParameters

   subroutine DestroyMaterialParameters()
      if (allocated(Ms)) deallocate (Ms)
      if (allocated(K1)) deallocate (K1)
      if (allocated(K2)) deallocate (K2)
      if (allocated(K1p)) deallocate (K1p)
      if (allocated(K2p)) deallocate (K2p)
      if (allocated(K3p)) deallocate (K3p)
      if (allocated(K4p)) deallocate (K4p)
      if (allocated(K5p)) deallocate (K5p)
      if (allocated(Ka)) deallocate (Ka)
      if (allocated(Kaa)) deallocate (Kaa)
      if (allocated(Ku)) deallocate (Ku)
      if (allocated(Kb)) deallocate (Kb)
      if (allocated(Kbb)) deallocate (Kbb)
      if (allocated(Kab)) deallocate (Kab)
      if (allocated(Aex)) deallocate (Aex)
      if (allocated(SigmaS)) deallocate (SigmaS)
      if (allocated(LamdaS)) deallocate (LamdaS)

      if (allocated(anisform)) deallocate (anisform)
      if (allocated(EasyAxis)) deallocate (EasyAxis)
      if (allocated(StressAxis)) deallocate (StressAxis)
      if (allocated(CubicAxes)) deallocate (CubicAxes)

      if (allocated(htot)) deallocate (htot)
      if (allocated(hanis)) deallocate (hanis)
      if (allocated(hstress)) deallocate (hstress)
      if (allocated(hdemag)) deallocate (hdemag)
      if (allocated(DemagH)) deallocate (DemagH)
      if (allocated(hext)) deallocate (hext)
      if (allocated(hexch)) deallocate (hexch)
      if (allocated(hexch2)) deallocate (hexch2)
      if (allocated(hexch3)) deallocate (hexch3)
      if (allocated(hexch4)) deallocate (hexch4)
      if (allocated(DExPhi)) deallocate (DExPhi)
      if (allocated(DExTheta)) deallocate (DExTheta)

      if (allocated(DemagEnergSD)) deallocate (DemagEnergSD)
      if (allocated(ExchangeESD)) deallocate (ExchangeESD)
      if (allocated(BEnergSD)) deallocate (BEnergSD)
      if (allocated(StressEnergSD)) deallocate (StressEnergSD)
      if (allocated(AnisEnergSD)) deallocate (AnisEnergSD)
      if (allocated(Mag_netSD)) deallocate (Mag_netSD)
   end subroutine DestroyMaterialParameters

   subroutine BuildMaterialParameters()
      use Tetrahedral_Mesh_Data, only: SubDomainIds
      implicit none
      ! Set NMaterials from number of SubDomainIds
      NMaterials = size(SubDomainIds)

      ! Saturation Magnetization, Anisotropy, Exchange constants.
      call ReAlloc1(Ms, NMaterials)
      call ReAlloc1(K1, NMaterials)
      call ReAlloc1(K2, NMaterials)
      call ReAlloc1(Aex, NMaterials)
      call ReAlloc1(K1p, NMaterials)
      call ReAlloc1(K2p, NMaterials)
      call ReAlloc1(K3p, NMaterials)
      call ReAlloc1(K4p, NMaterials)
      call ReAlloc1(K5p, NMaterials)
      call ReAlloc1(Ka, NMaterials)
      call ReAlloc1(Kaa, NMaterials)
      call ReAlloc1(Kb, NMaterials)
      call ReAlloc1(Kbb, NMaterials)
      call ReAlloc1(Kab, NMaterials)
      call ReAlloc1(Ku, NMaterials)
      call ReAlloc1(SigmaS, NMaterials)
      call ReAlloc1(LamdaS, NMaterials)

      call ReAlloci1(anisform, NMaterials)
      call ReAlloc2(EasyAxis, NMaterials)
      call ReAlloc2(StressAxis, NMaterials)
      call ReAlloc3(CubicAxes, NMaterials)

      call ReAlloc1(DemagEnergSD, NMaterials)
      call ReAlloc1(ExchangeESD, NMaterials)
      call ReAlloc1(BEnergSD, NMaterials)
      call ReAlloc1(StressEnergSD, NMaterials)
      call ReAlloc2(AnisEnergSD, NMaterials)
      call ReAlloc2(Mag_netSD, NMaterials)

      call FieldAllocate()
   contains
      subroutine ReAlloc1(target, new_size)
         real(KIND=DP), allocatable, intent(INOUT) :: target(:)
         integer, intent(IN) :: new_size

         real(KIND=DP), allocatable :: tmp(:)

         if (allocated(target)) then
            call move_alloc(target, tmp)
            allocate (target(new_size))
            target = tmp(1)
            target(1:min(new_size, size(tmp))) = tmp(1:min(new_size, size(tmp)))
         else
            write (*, *) "ERROR: ReAlloc called on unallocated variable!"
            write (*, *) "Called in BuildMaterialParameters"
            ERROR stop
         end if
      end subroutine ReAlloc1

      subroutine ReAlloci1(target, new_size)
         integer, allocatable, intent(INOUT) :: target(:)
         integer, intent(IN) :: new_size

         integer, allocatable :: tmp(:)

         if (allocated(target)) then
            call move_alloc(target, tmp)
            allocate (target(new_size))
            target = tmp(1)
            target(1:min(new_size, size(tmp))) = tmp(1:min(new_size, size(tmp)))
         else
            write (*, *) "ERROR: ReAlloc called on unallocated variable!"
            write (*, *) "Called in BuildMaterialParameters"
            ERROR stop
         end if
      end subroutine ReAlloci1

      subroutine ReAlloc2(target, new_size)
         real(KIND=DP), allocatable, intent(INOUT) :: target(:, :)
         integer, intent(IN) :: new_size

         real(KIND=DP), allocatable :: tmp(:, :)
         integer :: i

         if (allocated(target)) then
            call move_alloc(target, tmp)
            allocate (target(size(tmp, DIM=1), new_size))
            forall (i=1:size(target, DIM=1)) target(i, :) = tmp(i, 1)
            target(:, 1:min(new_size, size(tmp, DIM=2))) &
               = tmp(:, 1:min(new_size, size(tmp, DIM=2)))
         else
            write (*, *) "ERROR: ReAlloc called on unallocated variable!"
            write (*, *) "Called in BuildMaterialParameters"
            ERROR stop
         end if
      end subroutine ReAlloc2

      subroutine ReAlloc3(target, new_size)
         real(KIND=DP), allocatable, intent(INOUT) :: target(:, :, :)
         integer, intent(IN) :: new_size

         real(KIND=DP), allocatable :: tmp(:, :, :)
         integer :: i, j

         if (allocated(target)) then
            call move_alloc(target, tmp)
            allocate (target(size(tmp, DIM=1), size(tmp, DIM=2), new_size))
            forall (i=1:size(target, DIM=1), j=1:size(target, DIM=2))
               target(i, j, :) = tmp(i, j, 1)
            end forall
            target(:, :, 1:min(new_size, size(tmp, DIM=3))) &
               = tmp(:, :, 1:min(new_size, size(tmp, DIM=3)))
         else
            write (*, *) "ERROR: ReAlloc called on unallocated variable!"
            write (*, *) "Called in BuildMaterialParameters"
            ERROR stop
         end if
      end subroutine ReAlloc3
   end subroutine BuildMaterialParameters

   !---------------------------------------------------------------
   !            SetRotationMatrix
   !---------------------------------------------------------------

   subroutine SetRotationMatrix(phirot, thetarot, alpharot, sd)
      implicit none

      real(KIND=DP), intent(IN) :: phirot, thetarot, alpharot
      integer, optional, intent(IN) :: sd

      integer :: sd_min, sd_max
      integer :: i

      if (present(sd)) then
         sd_min = sd
         sd_max = sd
      else
         sd_min = 1
         sd_max = NMaterials
      end if

      do i = sd_min, sd_max
         CubicAxes(1, 1, i) = dcos(phirot)*dcos(thetarot)
         CubicAxes(2, 1, i) = dsin(phirot)*dcos(alpharot) &
                              + dcos(phirot)*dsin(thetarot)*dsin(alpharot)
         CubicAxes(3, 1, i) = dsin(phirot)*dsin(alpharot) &
                              - dsin(thetarot)*dcos(phirot)*dcos(alpharot)

         CubicAxes(1, 2, i) = -dsin(phirot)*dcos(thetarot)
         CubicAxes(2, 2, i) = dcos(phirot)*dcos(alpharot) &
                              - dsin(phirot)*dsin(thetarot)*dsin(alpharot)
         CubicAxes(3, 2, i) = dcos(phirot)*dsin(alpharot) &
                              + dsin(phirot)*dsin(thetarot)*dcos(alpharot)

         CubicAxes(1, 3, i) = dsin(thetarot)
         CubicAxes(2, 3, i) = -dcos(thetarot)*dsin(alpharot)
         CubicAxes(3, 3, i) = dcos(thetarot)*dcos(alpharot)
      end do

      write (*, *) "CubicAxes:"
      do i = sd_min, sd_max ! To be tested to prevent repeated output of all rotated cubic axes
!    DO i=1,NMaterials
         write (*, *) "SD: ", i
         write (*, *) "  ", CubicAxes(:, 1, i)
         write (*, *) "  ", CubicAxes(:, 2, i)
         write (*, *) "  ", CubicAxes(:, 3, i)
      end do
   end subroutine SetRotationMatrix

   !---------------------------------------------------------------
   !          FieldAllocate
   !---------------------------------------------------------------

   subroutine FieldAllocate()
      use Tetrahedral_Mesh_Data, only: NNODE
      real(KIND=DP) :: hznorm, eanorm, stressnorm
      integer :: i

      if (NNODE < 1) then
         write (*, *) ' ERROR ALLOCATING FIELDS: NNODE=', NNODE
         stop
      end if

      if (allocated(hdemag)) deallocate (hdemag)
      if (allocated(htot)) deallocate (htot)
      if (allocated(DemagH)) deallocate (DemagH)
      if (allocated(hanis)) deallocate (hanis)
      if (allocated(hstress)) deallocate (hstress)
      if (allocated(hext)) deallocate (hext)
      if (allocated(hexch)) deallocate (hexch)
      if (allocated(hexch2)) deallocate (hexch2)
      if (allocated(hexch3)) deallocate (hexch3)
      if (allocated(hexch4)) deallocate (hexch4)

      if (allocated(DExPhi)) deallocate (DExPhi)
      if (allocated(DExTheta)) deallocate (DExTheta)

      allocate ( &
         htot(NNODE, 3), &
         hdemag(NNODE, 3), DemagH(NNODE, 3), hanis(NNODE, 3), hext(NNODE, 3), &
         hexch(NNODE, 3), hexch2(NNODE, 3), hexch3(NNODE, 3), hexch4(NNODE, 3), &
         hstress(NNODE, 3) &
         )
      allocate (DExPhi(NNODE), DExTheta(NNODE))

      htot = 0.0
      hdemag = 0.0
      DemagH = 0.0
      hanis = 0.0
      hstress = 0.0
      hext = 0.0
      hexch = 0.0
      hexch2 = 0.0
      hexch3 = 0.0
      hexch4 = 0.0

      DExPhi = 0.0
      DExTheta = 0.0

      !-------------------------------------------------------------------
      ! Normalize external field, stress and anisotropy direction vectors.
      !-------------------------------------------------------------------

      hznorm = sqrt(hz(1)**2 + hz(2)**2 + hz(3)**2)
      if (hznorm > 0.0) then
         hz(1) = hz(1)/hznorm
         hz(2) = hz(2)/hznorm
         hz(3) = hz(3)/hznorm
      end if

      do i = 1, NMaterials
         eanorm = sqrt(EasyAxis(1, i)**2 + EasyAxis(2, i)**2 + EasyAxis(3, i)**2)
         stressnorm = sqrt(StressAxis(1, i)**2 + StressAxis(2, i)**2 + StressAxis(3, i)**2)
         if (eanorm > 0.0) then
            EasyAxis(1, i) = EasyAxis(1, i)/eanorm
            EasyAxis(2, i) = EasyAxis(2, i)/eanorm
            EasyAxis(3, i) = EasyAxis(3, i)/eanorm
            StressAxis(1, i) = StressAxis(1, i)/stressnorm
            StressAxis(2, i) = StressAxis(2, i)/stressnorm
            StressAxis(3, i) = StressAxis(3, i)/stressnorm
         end if
      end do

   end subroutine FieldAllocate

   !---------------------------------------------------------------
   ! SUBROUTINE Magnetite( TCelsius)
   !        defines temperature dependent material constants
   !        for magnetite above room temperature
   !---------------------------------------------------------------

   subroutine Magnetite_Legacy(TCelsius)
      implicit none
      real(KIND=DP) :: TCelsius

      T = TCelsius
      TC = 580 ! Curie temperature of magnetite in Celsius

      if (T < 0 .and. warnflag .eq. 1) write (*, *) 'Warning :: Magnetite material constants for &
          &T<0 deg Celsius are not reliable !! T=', TCelsius
      if (T .le. TC) then
         anisform = ANISFORM_CUBIC
         Aex = (sqrt(21622.526 + 816.476*(TC - T)) - 147.046)/408.238e11
         Ms = 737.384*51.876*(TC - T)**0.4
         K1 = -2.13074e-5*(TC - T)**3.2
         Kd = mu*maxval(Ms)*maxval(Ms)*0.5
         LambdaEx = sqrt(Aex(maxloc(Ms, 1))/Kd)
         QHardness = K1(maxloc(Ms, 1))/Kd
         !LamdaS from Moskowitz: Magneostriction in Magnetite and Titanomagnetite
         ! JGR 98, 359-371 , 1993
         LamdaS = (47.455 + 0.013836*T - 5.9357e-4*T**2 + 1.4976e-6*T**3 &
                   - 1.5043e-9*T**4 + 3.0251e-13*T**5)*1.0e-6
      else
         if (warnflag .eq. 1) write (*, *) 'Warning :: Magnetite material constants for &
           &T>580 deg Celsius are ZERO T=', TCelsius
         anisform = ANISFORM_CUBIC
         Aex = 0
         Ms = 0
         K1 = 0
         LamdaS = 0.
      end if
   end subroutine Magnetite_Legacy

   !---------------------------------------------------------------
   ! SUBROUTINE TM54 TCelsius)
   !        defines temperature dependent material constants
   !        for TM54 above room temperature
   !---------------------------------------------------------------

   subroutine TM54(TCelsius)
      implicit none
      real(KIND=DP) :: TCelsius
! Data for TM54 provided by Evgeniya Khakhalova, Institute of Rock Mag, Minnesota, 2018

      T = TCelsius
      TC = 216 ! Curie temperature of TM54 in Celsius

      if (T < 0 .and. warnflag .eq. 1) write (*, *) 'Warning :: TM54 material constants for &
          &T<0 deg Celsius are not reliable !! T=', TCelsius
      if (T .le. TC) then
         anisform = ANISFORM_CUBIC
         Aex = 1.004e-22*T**5 - 5.150e-20*T**4 + 1.013e-17*T**3 - 1.009e-15*T**2 &
               + 8.858e-15*T + 8.007e-12

         Ms = -1.0256e-05*T**4 - 1.4336e-02*T**3 + 2.3745e+00*T**2 &
              - 4.7881e+02*T + 1.6958e+05

         K1 = 8.656e-07*T**4 - 4.561e-04*T**3 + 4.585e-02*T**2 + 8.620e+00*T - 1.293e+03

         Kd = mu*maxval(Ms)*maxval(Ms)*0.5

         LambdaEx = sqrt(Aex(maxloc(Ms, 1))/Kd)
         QHardness = K1(maxloc(Ms, 1))/Kd
      else
         if (warnflag .eq. 1) write (*, *) 'Warning :: TM54 material constants for &
           &T>212 deg Celsius are ZERO T=', TCelsius
         anisform = ANISFORM_CUBIC
         Aex = 0
         Ms = 0
         K1 = 0
      end if
   end subroutine TM54

   !---------------------------------------------------------------
   ! SUBROUTINE TM(TMx TCelsius)
   !        defines temperature dependent material constants
   !        for titanomagnetite above room temperature
   !---------------------------------------------------------------

   subroutine TM(TMx, TCelsius)
      implicit none
      real(KIND=DP) :: TMx, TCelsius, Tnorm

      ! Convert Ti-concentration into decimal fraction
      TMx = TMx/100

      T = TCelsius

      ! Get Tc
      Tc = 3.7237e+02*TMx**3 - 6.9152e+02*TMx**2 - 4.1385e+02*TMx**1 + 5.8000e+02

      write (*, *) 'TMx, Tc, T = ', TMx, Tc, T

      ! Get normalized temperature
      Tnorm = T/Tc

      if (T .lt. 0 .and. warnflag .eq. 1) then
         write (*, *) 'Warning :: TM material constants for &
           &T<0 deg Celsius are not reliable !! T=', TCelsius
      end if

      ! Warn about high TM values being unreliable
      if (TMx .gt. 0.650 .and. warnflag .eq. 1) then
         write (*, *) 'Warning :: TM material constants for &
           &TMx>0.65 are not robust. Results may be unreliable'
      end if

      if (T .le. TC) then
         anisform = ANISFORM_CUBIC

         Ms = (-2.8106e+05*TMx**3 + 5.2850e+05*TMx**2 - 7.9381e+05*TMx**1 + 4.9537e+05)*(1 - Tnorm)**4.0025e-01

         K1 = 1e4*(-3.5725e+01*TMx**3 + 5.0920e+01*TMx**2 - 1.5257e+01*TMx**1 - 1.3579e+00) &
              *(1 - Tnorm)**(-6.3643e+00*TMx**2 + 2.3779e+00*TMx**1 + 3.0318e+00)

         K2 = 1e4*(1.5308e+02*TMx**4 - 2.2600e+01*TMx**3 - 4.9734e+01*TMx**2 + 1.5822e+01*TMx**1 - 5.5522e-01) &
              *(1 - Tnorm)**7.2652e+00

         Aex = 1e-11*1.3838e+00*((Tc + 273.15)/853.15)*(1 - Tnorm)**6.7448e-01

         Kd = mu*maxval(Ms)*maxval(Ms)*0.5

         LambdaEx = sqrt(Aex(maxloc(Ms, 1))/Kd)
         QHardness = K1(maxloc(Ms, 1))/Kd
         write (*, *) 'Ms, Aex, k1, k2 Lex(nm) = ', Ms, Aex, K1, K2, LambdaEx*1e9

      else
         if (warnflag .eq. 1) then
            write (*, *) 'Warning :: TM material constants for &
            &T>Tc deg Celsius are ZERO T=', TCelsius
         end if

         anisform = ANISFORM_CUBIC
         Aex = 0
         Ms = 0
         K1 = 0
         K2 = 0
      end if

   end subroutine TM

   !---------------------------------------------------------------
   ! SUBROUTINE Magnetite(TCelsius)
   !        defines temperature dependent material constants
   !        for magnetite between -200 and 580 C
   !---------------------------------------------------------------

   subroutine Magnetite(TCelsius)
      implicit none
      real(KIND=DP) :: TCelsius, Tnorm, Tlim

      T = TCelsius

      ! Get Tc and normalized temperature
      Tc = 580
      Tnorm = 1 - (T/Tc)

      ! The lower temperature limit for properties
      ! Currently limited by Ms (-200C)
      ! Anisotropy data go to -268C
      Tlim = -200

      ! Do Spontaneous Magnetization
      if (Tnorm .le. 1.0346) then
         ! -20.09C <= T <= Tc
         Ms = 1e5*(4.8944*Tnorm**4.0833e-1)
      elseif ((1.0346 .lt. Tnorm) .and. (Tnorm .le. 1.3296)) then
         ! -191.16C <= T < -20.09C
         Ms = 1e5*(-1.8774e1*Tnorm**2 + 4.1848e1*Tnorm - 1.8237e1)
      elseif ((1.3296 .lt. Tnorm) .and. (T .ge. Tlim)) then
         ! Tlim <= T < -191.16C
         Ms = 1e5*(6.1874e1*Tnorm - 7.8053e1)
      else
         Ms = 0; 
         !Print*, 'Ms = ', Ms
      end if

      ! Do Exchange
      if (T .ge. 19) then
         Aex = 1e-11*(1.3763*Tnorm**6.6809e-1)
      elseif ((T .lt. 19) .and. (T .ge. Tlim)) then
         Aex = 1e-11*(6.8003e-10*Ms**1.6356)
      else
         Aex = 0
         !Print*, 'Aex = ', Aex
      end if

      ! Do K1 anisotropy
      if ((T .le. TC) .and. (T .gt. -153)) then
         K1 = 1e4*(1.4951e1*Tnorm**6 - 3.2908e1*Tnorm**5 + 2.3098e1*Tnorm**4 - 6.5297*Tnorm**3)
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey - set explicitly to zero
         K1 = 0; 
      else
         K1 = 0
         !Print*, 'K1 = ', K1
      end if

      ! Do K2 anisotropy
      if ((T .le. TC) .and. (T .gt. -153)) then
         K2 = 1e4*(1.3976*Tnorm**6 - 1.7253*Tnorm**5)
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey - set explicitly to zero
         K2 = 0; 
      else
         K2 = 0
         !Print*, 'K2 = ', K2
      end if

      ! Do Ka anisotropy
      if ((T .le. TC) .and. (T .gt. -153)) then
         ! Above verwey - set explicitly to zero
         Ka = 0; 
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey
         Ka = 1e4*(3.1097e2*Tnorm**3 - 1.3722e3*Tnorm**2 + 2.0167e3*Tnorm - 9.6172e2)
      else
         Ka = 0
         !Print*, 'Ka = ', Ka
      end if

      ! Do Kaa anisotropy
      if ((T .le. TC) .and. (T .gt. -153)) then
         ! Above verwey - set explicitly to zero
         Kaa = 0; 
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey
         Kaa = 1.61e4
      else
         Kaa = 0
         !Print*, 'Kaa = ', Kaa
      end if

      ! Do Kb anisotropy
      if ((T .le. TC) .and. (T .gt. -153)) then
         ! Above verwey - set explicitly to zero
         Kb = 0; 
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey
         Kb = 1e4*(-3.1822e2*Tnorm**3 + 1.4205e3*Tnorm**2 - 2.1128e3*Tnorm + 1.0508e3)
      else
         Kb = 0
         !Print*, 'Kb = ', Kb
      end if

      ! Do Kbb anisotropy
      if ((T .le. TC) .and. (T .gt. -153)) then
         ! Above verwey - set explicitly to zero
         Kbb = 0; 
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey
         Kbb = 1e4*(1.9377e2*Tnorm**3 - 7.9607e2*Tnorm**2 + 1.0923e3*Tnorm - 4.9841e2)
      else
         Kbb = 0
         !Print*, 'Kbb = ', Kbb
      end if

      ! Do Kab anisotropy
      if ((T .le. TC) .and. (T .gt. -153)) then
         ! Above verwey - set explicitly to zero
         Kab = 0; 
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey
         Kab = 7.01e4
      else
         Kab = 0
         !Print*, 'Kab = ', Kab
      end if

      ! Do Ku anisotropy
      if ((T .le. TC) .and. (T .gt. -153)) then
         ! Above verwey - set explicitly to zero
         Ku = 0; 
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey
         Ku = 1e4*(-1.2183e2*Tnorm**2 + 3.5410e2*Tnorm - 2.5521e2)
      else
         Ku = 0
         !Print*, 'Ku = ', Ku
      end if

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Define Anisotropy form and print summary !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if ((T .le. TC) .and. (T .gt. -153)) then
         ! Above verwey
         anisform = ANISFORM_CUBIC
         Kd = mu*maxval(Ms)*maxval(Ms)*0.5
         LambdaEx = sqrt(Aex(maxloc(Ms, 1))/Kd)
         QHardness = K1(maxloc(Ms, 1))/Kd
         !LamdaS from Moskowitz: Magneostriction in Magnetite and Titanomagnetite
         ! JGR 98, 359-371 , 1993
         LamdaS = (47.455 + 0.013836*T - 5.9357e-4*T**2 + 1.4976e-6*T**3 &
                   - 1.5043e-9*T**4 + 3.0251e-13*T**5)*1.0e-6
         print *, 'Ms, Aex, K1, K2 Lex(nm), LamdaS = ', Ms, Aex, K1, K2, LambdaEx*1e9, LamdaS
      elseif ((T .le. -153) .and. (T .ge. Tlim)) then
         ! Below verwey
         anisform = ANISFORM_MONOCLINIC
         LambdaEx = sqrt(Aex(maxloc(Ms, 1))/Kd)
         QHardness = Ka(maxloc(Ms, 1))/Kd
         print *, 'Ms, Aex, Ka, Kaa, Kb, Kbb, Kab, Ku, Lex(nm) = ', Ms, Aex, Ka, Kaa, Kb, Kbb, Kab, Ku, LambdaEx*1e9
      else
         ! Out of temperature range
         anisform = ANISFORM_CUBIC
         if (warnflag .eq. 1) write (*, *) 'Warning :: Material constants undefined for &
           & T=', TCelsius, '. All constants set to ZERO!'
      end if

   end subroutine Magnetite

   !---------------------------------------------------------------
   ! SUBROUTINE Iron( TCelsius)
   !        defines temperature dependent material constants
   !        for iron above room temperature
   !---------------------------------------------------------------

   subroutine Iron(TCelsius)
      implicit none
      real(KIND=DP) :: TCelsius

      ! Temperatures T and TC in Kelvin here

      T = TCelsius + 273 ! polynomial expressions for iron use Temperature in Kelvin
      TC = 1044 ! Curie temperature of iron in Kelvin

     if(T<273.AND.warnflag.eq.1) write(*,*) 'Warning :: Iron material constants for T<0 deg Celsius are not reliable !! T=',TCelsius
      if (T .le. TC) then
         anisform = ANISFORM_CUBIC

         Aex = -1.8952e-12 + (3.0657e-13)*T - (1.599e-15)*(T**2) + (4.0151e-18)*(T**3) &
               - (5.3728e-21)*(T**4) + (3.6501e-24)*(T**5) - (9.9515e-28)*(T**6)

         Ms = 1.75221e6 - (1.21716e3)*T + (33.3368)*(T**2) - (0.363228)*(T**3) + (1.96713e-3)*(T**4) &
              - (5.98015e-6)*(T**5) + (1.06587e-8)*(T**6) - (1.1048e-11)*(T**7) + (6.16143e-15)*(T**8) &
              - (1.42904e-18)*(T**9)

         ! 4 Sept 2016: k1 is derived from fig 7.24 in Culity And Graham 'Intro to magnetic materials 2011.
         ! however the data is form Honda et al 1928. More recent measurements give room temp vales a bit higher (Graham 1958)
         ! so we scale the temperature dependent values below to give yield the more recent value 4.8e4 at room temp.

         k1 = 54967.1 + 44.2946*T - 0.426485*(T**2) + 0.000811152*(T**3) - (1.07579e-6)*(T**4) &
              + (8.83207e-10)*(T**5) - (2.90947e-13)*(T**6)
         k1 = k1*(480.0/456.0)
         ! older slightly less accurate value below
         !               k1=56645.8 + 10.655*T - 0.215214*(T**2) + 0.000212721*(T**3) - (2.24971e-7)*(T**4) &
         !                  + (2.90075e-10)*(T**5) - (1.29527e-13)*(T**6)
         !                k1=k1*(485.0/456.0)

         ! k2 for iron is included here agin form observations from fig7.24 in Culity and Graham. However more recent
         ! measurements by Graham 1958 show the value at least 100 times smaller than Honda et al 1928. Presently we
         ! recommend that K2=0 for iron at all temperatures
         K2 = 0.0
         if (T > 282 .and. T <= 379) K2 = 194200.+306.864*(-282.663 + T) - 0.00630088*(-282.663 + T)**3
         if (T > 379 .and. T <= 480) K2 = 218100.+132.229*(-378.781 + T) - 1.81688*(-378.781 + T)**2 + 0.0041246*(-378.781 + T)**3
         if (T > 480 .and. T <= 576) K2 = 217100.-109.287*(-480.43 + T) - 0.559099*(-480.43 + T)**2 - 0.0137673*(-480.43 + T)**3
         if (T > 576 .and. T <= 680) K2 = 189400.-595.383*(-576.221 + T) - 4.51544*(-576.221 + T)**2 + 0.0227715*(-576.221 + T)**3
         if (T > 680 .and. T <= 777) k2 = 104700.-798.569*(-679.664 + T) + 2.55121*(-679.664 + T)**2 - 0.00495542*(-679.664 + T)**3
         if (T > 777 .and. T <= 877) K2 = 46400.-441.912*(-777.388 + T) + 1.09842*(-777.388 + T)**2 - 0.00368236*(-777.388 + T)**3

         Kd = mu*maxval(Ms)*maxval(Ms)*0.5
         LambdaEx = sqrt(Aex(maxloc(Ms, 1))/Kd)
         QHardness = K1(maxloc(Ms, 1))/Kd
      else
         write (*, *) 'Warning :: Iron material constants for T>1044 deg Celsius are ZERO T=', TCelsius
         anisform = ANISFORM_CUBIC
         Aex = 0.
         Ms = 0.
         K1 = 0.
         K2 = 0.
      end if
   end subroutine Iron

end module Material_Parameters
