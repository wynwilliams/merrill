!> This small module holds fixed quantities which depend on
!> both MaterialParameters and geometry.  They are fixed throughout
!> a given minimization, but don't belong to the mesh or the material.
module System_Parameters
   use Utils, only: DP
   implicit none
   real(kind=DP):: TypicalEnergy, invTE, Enscale
contains
   subroutine InitializeTypicalEnergy()
      use Utils, only: NONZERO
      use Tetrahedral_Mesh_Data, only: total_volume
      use Material_Parameters, only: LS, Kd, Aex, Ms, K1

      EnScale = Kd*total_volume  ! Energy scale to transform into units of Kd V
      ! Note that a factor Ls**(-3/2) is intrinsic by using Aex*Ls
      if (.not. NONZERO(EnScale)) EnScale = 1
      ! Calculate typical energy scale
      ! (order of magnitude estimate of micromagnetic energy)
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = &
            (sqrt(Ls)**3) &
            *sqrt(Kd*sqrt(Aex(maxloc(Ms, 1))*abs(K1(maxloc(Ms, 1))))) &
            *(total_volume/(sqrt(Ls)**3))**(5./6.)
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = Kd*total_volume
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = maxval(Aex)*Ls*total_volume**(1./3.)
      end if
      ! If TypicalEnergy is still zero
      if (.not. (NONZERO(TypicalEnergy))) then
         TypicalEnergy = maxval(abs(K1))*total_volume
      end if
      !         print*,'opt_out = ', opt_out
      invTE = 100./TypicalEnergy

   end subroutine
end module
