!> This module contains routines and arrays for building FEM representations
!> of the micromagnetic equations.
!> Quantities and matrices which depend on mesh geomtry only are stored in this module.
module Finite_Element_Matrices
   use Utils, only: DP, SaveDPRow, SaveIntRow, SaveDPArray
   use iso_c_binding, only: c_ptr, c_bool, c_null_ptr
   implicit none

   real(KIND = DP) FEMTolerance

#if MERRILL_USE_H2LIB
   type(c_ptr), private:: gr  ! tet3d struct holding mesh geometry

   ! module data: holding matrix AB/M, 
   ! as in surfacephi2 = AB*surfacephi1
   ! a pointer to H2matrix C object
   ! construct once in FiniteElementMatrices, used many times in EnergyCalculator
   ! As H2 matrix struct, as _amatrix struct

   type(c_ptr):: root  ! These are part of BA_H2; memory management
   type(c_ptr):: broot  ! is easier if they are kept until the end

   type(c_ptr):: BA_H2 = c_null_ptr  ! Not protected-may get written to by file reading step in Command_Parser

   ! Sparse matrix objects for bulk FEM steps 1. and 3.
   type(c_ptr), protected:: PNM  ! sparsematrix-PoissonNeumannMatrix
   type(c_ptr), protected:: PNM_fixed  ! interaction sparsematrix-fixed element in PoissonNeumannMatrix
   type(c_ptr), protected:: PDM  ! sparsematrix-PoissonDirichletMatrix
   type(c_ptr), protected:: PDM_fixed  ! interaction sparsematrix-related to fixed (boundary) nodes of PoissonDirichletMatrix
   logical(c_bool), allocatable:: PDM_isdof(:)  !list "is degree of freedom" marking bulk nodes

   ! interface to my C functions
   interface
      !> The c function tet3d_to_surface3d in file
      !> sortmeshdata.c generates a (cptr to) H2lib _surface3d struct, 
      !> representing the surface mesh only, 
      !> from tet3d struct, representing the bulk mesh, 
      !> by appropriately arranging coordinates and indices of boundary nodes, edges, 
      !> and triangles into the internal _surface3d lists.
      type(c_ptr) function f_tet3d_to_surface3d(tet3d) bind(c, name="tet3d_to_surface3d")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: tet3d  ! ptr to tet3d mesh struct
      end function
   end interface

   interface
      !> The C function intervenes in a H2Lib tet3dbuilder object to allow
      !> setting of its list of vertex coordinates from my list
      subroutine f_modify_tet3dbuilder(tb, VCL, NNODE) bind(c, name="modify_tet3dbuilder")
         use iso_c_binding, only: c_int, c_char, c_ptr
         type(c_ptr), intent(in), value:: tb  ! cptr to tet3dbuilder
         type(c_ptr), intent(in), value:: VCL  ! cptr to array doubles NNODEx3
         integer(c_int), intent(in), value:: NNODE
      end subroutine
   end interface

   interface
      !> The C function modify_tet3dp1 intervenes in a H2Lib tet3dp1 to set number, 
      !> location of free vs fixed nodes
      subroutine f_modify_tet3dp1(dc, ndof, nfix, is_dof, idx_dof) bind(c, name="modify_tet3dp1")
         use iso_c_binding, only: c_int, c_ptr, c_bool
         type(c_ptr), value:: dc
         integer(c_int), intent(in), value:: ndof, nfix
         type(c_ptr), value:: is_dof  ! passes ptr to array of bool data
         type(c_ptr), value:: idx_dof  ! passes ptr to array of bool data
      end subroutine
   end interface

   interface

      subroutine f_free_idx(root) bind(c, name="free_idx")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: root
      end subroutine
   end interface

#else
! sparse matrices for old, native+slatec FEMBEM calculation

   !
   ! Sparse matrices are constructed in the following format:
   ! For, say, a NNODE x NNODE matrix named PoissonNeumannMatrix with
   ! abbreviation PNM
   !   NNODE   : number of columns
   !   nze_pnm : number of non-zero entries in matrix PNM
   !   PoissonNeumannMatrix(nze_pnm) : entries of the PNM matrix
   !   CNR_PNM(NNODE+1) : column indices of the PNM matrix
   !   RNR_PNM(nze_pnm) : row indices of the PNM matrix
   !
   ! The CNR and RNR indexing isn't obvious.
   ! The value of CNR_PNM(i) contains the starting index for the ith column
   ! of entries in PoissonNeumannMatrix. So the values
   !   PoissonNeumannMatrix(CNR_PNM(i) : CNR_PNM(i+1)-1)
   ! all belong to column i.
   ! The value of RNR_PNM(j) contains the row index of the jth data entry
   ! of PoissonNeumannMatrix.
   ! A matrix-vector multiplication of PNM with a vector X, with result
   ! stored in the vector V, can be written
   !   DO i = 1, NNODE
   !     DO j = CNR(i), CNR(i+1)-1
   !       V(i) = V(i) + PoissonNeumannMatrix(j)*X(RNR_PNM(j))
   !     END DO
   !   END DO
   !
   ! Matrices constructed from finite element formulation of problem
   ! stored in column major format
   !     PoissonNeumannMatrix(:)
   !     PoissonDirichletMatrix(:)
   !     YA4(:,:)
   !     FAX(:), FAY(:), FAZ(:)
   ! With column and row indices of the form CNRXX and RNRXX, and some
   ! with subdomain indices of the form SDNRXX.

   ! Sparse Matrices for the Poisson Bilinear Form using
   ! Neumann BCs and Dirichlet BCs. Abbreviated PNM and PDM.
   integer:: nze_pnm
   real(KIND = DP), allocatable:: PoissonNeumannMatrix(:)
   integer, allocatable:: RNR_PNM(:), CNR_PNM(:)

   integer, allocatable:: SaveNze_pnm(:)
   type(SaveDPRow), allocatable:: SavePoissonNeumannMatrix(:)
   type(SaveIntRow), allocatable:: SaveRNR_PNM(:), SaveCNR_PNM(:)

   integer:: nze_pdm
   real(KIND = DP), allocatable:: PoissonDirichletMatrix(:)
   integer, allocatable:: RNR_PDM(:), CNR_PDM(:)

   integer, allocatable:: SaveNze_pdm(:)
   type(SaveDPRow), allocatable:: SavePoissonDirichletMatrix(:)
   type(SaveIntRow), allocatable:: SaveRNR_PDM(:), SaveCNR_PDM(:)

#endif
   ! TODO Get these into new BLAS & H2Lib matrix formats
   ! The ExchangeMatrix is effectively the PoissonNeumannMatrix, but
   ! with subdomains included.
   integer:: nze_em
   real(KIND = DP), allocatable:: ExchangeMatrix(:)
   integer, allocatable:: RNR_EM(:), CNR_EM(:), SDNR_EM(:)

   integer, allocatable:: Savenze_em(:)
   type(SaveDPRow), allocatable:: SaveExchangeMatrix(:)
   type(SaveIntRow), allocatable:: SaveRNR_EM(:), SaveCNR_EM(:), SaveSDNR_EM(:)

   ! Sparse matrix for the \Int grad(m) . v dV RHS of the Poisson
   ! equation with Neumann BCs
   integer:: nze4
   real(KIND = DP), allocatable:: FAX(:), FAY(:), FAZ(:)
   integer, allocatable:: RNR4(:), CNR4(:), SDNR4(:)

   integer, allocatable:: SaveNze4(:)
   type(SaveDPRow), allocatable:: SaveFAX(:), SaveFAY(:), SaveFAZ(:)
   type(SaveIntRow), allocatable:: SaveRNR4(:), SaveCNR4(:), SaveSDNR4(:)

   ! The InterpolationMatrix, abbreviated IM.
   ! A multiphase version of vbox.
   ! Sparse matrix for interpolating node-wise expressions with element-wise
   ! material parameters to an energy gradient.
   integer:: nze_im
   real(KIND = DP), allocatable:: InterpolationMatrix(:)
   integer, allocatable:: RNR_IM(:), CNR_IM(:), SDNR_IM(:)
   integer, allocatable:: Savenze_im(:)
   type(SaveDPRow), allocatable:: SaveInterpolationMatrix(:)
   type(SaveIntRow), allocatable:: SaveRNR_IM(:), SaveCNR_IM(:), SaveSDNR_IM(:)

#if MERRILL_USE_H2LIB
! TODO add saving of H2matrix objects
#else
   ! PNM_RWORK, PNM_IWORK, PDM_RWORK, PDM_IWORK dependant on
   ! PoissonNeumannMatrix, PoissonDirichletMatrix.
   ! These hold the partial Cholesky decompositions for
   ! PoissonNeumannMatrix and PoissonDirichletMatrix as calculated in
   ! DSICCG for use in DCG, but cached for repeated use.
   real(KIND = DP), allocatable:: PNM_RWORK(:), PDM_RWORK(:)
   integer, allocatable:: PNM_IWORK(:), PDM_IWORK(:)

   type(SaveDPRow), allocatable:: SavePNM_RWORK(:), SavePDM_RWORK(:)
   type(SaveINTRow), allocatable:: SavePNM_IWORK(:), SavePDM_IWORK(:)
#endif
   !TODO temporarily used by both in sparse indexing scheme
   ! TODO Get these into new BLAS & H2Lib matrix formats
   real(KIND = DP), allocatable:: YA4(:, :)
   type(SaveDPArray), allocatable:: SaveYA4(:)

   ! The BEM matrix for finding the scalar potential at the surface of the
   ! material with physical boundary constraints (ie phi = 0 at infinity)
   ! from the scalar potential with neumann constraints
   ! (ie grad(phi) . n = 0 at the material surface.
   ! BA <=> The matrix B in Fredkin and Koehler Eq 15
   real(KIND = DP), allocatable, target:: BA(:, :)
   type(c_ptr):: BAcptr
   integer, allocatable:: BoundaryNodeIndex(:)

   type(SaveDPArray), allocatable:: SaveBA(:)
   type(SaveIntRow), allocatable:: SaveBoundaryNodeIndex(:)

contains

   !> Initialize the variables of the finite_element module.
   subroutine InitializeFiniteElement()

      call DestroyFiniteElement()

      FEMTolerance = 1.d-10
   end subroutine InitializeFiniteElement

   !> Destroy and clean up the variables in the finite_element module.
   subroutine DestroyFiniteElement()
#if MERRILL_USE_H2LIB
      use iso_c_binding, ONLY: c_null_ptr
#endif
      interface TryDeallocate
         procedure :: &
            TryDeallocateDP, TryDeallocateDPArr, &
            TryDeallocateInt
      end interface
#if MERRILL_USE_H2LIB
      call dealloc_H2Lib()
#else
      if (allocated(PoissonNeumannMatrix)) deallocate (PoissonNeumannMatrix)
      if (allocated(RNR_PNM)) deallocate (RNR_PNM)
      if (allocated(CNR_PNM)) deallocate (CNR_PNM)

      if (allocated(Savenze_pnm)) deallocate (Savenze_pnm)
      call TryDeallocate(SavePoissonNeumannMatrix)
      call TryDeallocate(SaveRNR_PNM)
      call TryDeallocate(SaveCNR_PNM)

      if (allocated(PoissonDirichletMatrix)) deallocate (PoissonDirichletMatrix)
      if (allocated(RNR_PDM)) deallocate (RNR_PDM)
      if (allocated(CNR_PDM)) deallocate (CNR_PDM)

      if (allocated(Savenze_pdm)) deallocate (Savenze_pdm)
      call TryDeallocate(SavePoissonDirichletMatrix)
      call TryDeallocate(SaveRNR_PDM)
      call TryDeallocate(SaveCNR_PDM)

      if (allocated(PNM_RWORK)) deallocate (PNM_RWORK)
      if (allocated(PDM_RWORK)) deallocate (PDM_RWORK)
      if (allocated(PNM_IWORK)) deallocate (PNM_IWORK)
      if (allocated(PDM_IWORK)) deallocate (PDM_IWORK)

      call TryDeallocate(SavePNM_RWORK)
      call TryDeallocate(SavePDM_RWORK)
      call TryDeallocate(SavePNM_IWORK)
      call TryDeallocate(SavePDM_IWORK)
#endif

      if (allocated(RNR_IM)) deallocate (RNR_IM)
      if (allocated(CNR_IM)) deallocate (CNR_IM)

      if (allocated(RNR_EM)) deallocate (RNR_EM)
      if (allocated(CNR_EM)) deallocate (CNR_EM)
      if (allocated(SDNR_EM)) deallocate (SDNR_EM)

      if (allocated(Savenze_em)) deallocate (Savenze_em)
      call TryDeallocate(SaveExchangeMatrix)
      call TryDeallocate(SaveRNR_EM)
      call TryDeallocate(SaveCNR_EM)
      call TryDeallocate(SaveSDNR_EM)

      if (allocated(FAX)) deallocate (FAX)
      if (allocated(FAY)) deallocate (FAY)
      if (allocated(FAZ)) deallocate (FAZ)
      if (allocated(RNR4)) deallocate (RNR4)
      if (allocated(CNR4)) deallocate (CNR4)
      if (allocated(SDNR4)) deallocate (SDNR4)

      if (allocated(Savenze4)) deallocate (Savenze4)
      call TryDeallocate(SaveFAX)
      call TryDeallocate(SaveFAY)
      call TryDeallocate(SaveFAZ)
      call TryDeallocate(SaveRNR4)
      call TryDeallocate(SaveCNR4)
      call TryDeallocate(SaveSDNR4)
      if (allocated(ExchangeMatrix)) deallocate (ExchangeMatrix)
      if (allocated(YA4)) deallocate (YA4)
      if (allocated(InterpolationMatrix)) deallocate (InterpolationMatrix)
      if (allocated(SDNR_IM)) deallocate (SDNR_IM)

      call TryDeallocate(SaveYA4)

      if (allocated(BA)) deallocate (BA)
      if (allocated(BoundaryNodeIndex)) deallocate (BoundaryNodeIndex)

      call TryDeallocate(SaveBA)
      call TryDeallocate(SaveBoundaryNodeIndex)

   contains
      subroutine TryDeallocateDP(s)
         type(SaveDPRow), allocatable, intent(INOUT):: s(:)
         integer:: i
         if (allocated(s)) then
            do i = 1, size(s)
               if (allocated(s(i)%DPSave)) deallocate (s(i)%DPSave)
            end do
            deallocate (s)
         end if
      end subroutine TryDeallocateDP

      subroutine TryDeallocateInt(s)
         type(SaveIntRow), allocatable, intent(INOUT):: s(:)
         integer:: i
         if (allocated(s)) then
            do i = 1, size(s)
               if (allocated(s(i)%IntSave)) deallocate (s(i)%IntSave)
            end do
            deallocate (s)
         end if
      end subroutine TryDeallocateInt

      subroutine TryDeallocateDPArr(s)
         type(SaveDPArray), allocatable, intent(INOUT):: s(:)
         integer:: i
         if (allocated(s)) then
            do i = 1, size(s)
               if (allocated(s(i)%DPArrSave)) deallocate (s(i)%DPArrSave)
            end do
            deallocate (s)
         end if
      end subroutine TryDeallocateDPArr
   end subroutine DestroyFiniteElement

   !> Build the FEM representations
   subroutine BuildFiniteElement()
      use Tetrahedral_Mesh_Data, only: TIL, TetSubDomains, b, c, d, vol, TetSolid, solid, NNODE, NodeOnBoundary, NTRI
      use iso_c_binding, only: c_associated
      implicit none

      ! kstiff, kstiff2 : Contributions to the Geometric stiffness
      !   matrices for finite elements on the mesh
      ! kinterpolatestiff : Contributions to the interpolation stiffness
      !   matrices on the mesh
      real(KIND = DP), allocatable:: kstiff(:), kstiff2(:, :)
      real(KIND = DP), allocatable:: kinterpolatestiff(:)
      ! these are defined in this scope to transfer the data between DemagStiff and
      ! NonZeroStiff (sparsifies DemagStiff) TODO clean up

      !krow, kcol  : Indices on sparse stiffness matrix contributions
      !kpid       : Tet Property ID on sparse stiffness matrix contributions
      !sparse     : Sorted indices for SLAP sparse matrix contributions
      integer, allocatable  :: krow(:), kcol(:), kpid(:), sparse(:)

      ! Build PNM and PDM, as H2 matrices or native sparse matrices
#if MERRILL_USE_H2LIB
      call demagStiffH2LibPNM()
      call demagStiffH2LibPDM()
#endif
      allocate (kstiff(16*NTRI), kstiff2(16*NTRI, 3), kinterpolatestiff(16*NTRI))
      allocate (krow(16*NTRI), kcol(16*NTRI), kpid(16*NTRI), sparse(16*NTRI))

      !Build FEM matrix, then convert to sparse representation
      call DemagStiff( &
         TIL, TetSubDomains, b, c, d, vol, TetSolid, solid, &
         kstiff, kstiff2, kinterpolatestiff, krow, kcol, kpid, sparse &
         )
      call NonZeroStiff( &
         NNODE, NodeOnBoundary, &
         kstiff, kstiff2, kinterpolatestiff, krow, kcol, kpid, sparse &
         )
      deallocate (kstiff, kstiff2, kinterpolatestiff)
      deallocate (krow, kcol, kpid, sparse)

      ! build Force Matrices
      call ForceMat()

#if MERRILL_USE_H2LIB

      ! Both ways Build BEM matrix BA (dense, on basis of boundary element indices), 
      ! TODO Ideally, in the future we can build from geometry to Hmatrix to H2matrix
      ! Big initial memory spike here because the whole dense BNODE x BNODE matrix is built
      ! Before being compressed to H2Matrix

      ! then sparsify it to H2 matrix
      ! only if we don't have one that was read from file
      if (.not. c_associated(BA_H2)) then
      call H2BoundMata()  ! TODO add all relevant initializers and destructors.  Test valgrind.
      else
            call FillBoundaryNodeIndex()
            write(*,*) "Skipping construction of new h2matrix"
      endif
#else
      call BoundMata()
#endif
   end subroutine BuildFiniteElement

#if MERRILL_USE_H2LIB
   !> this replaces setting up kstiff (dense stiffness matrix), kstiffnonzero (sparsified), and
   !> PoissonNeummanMatrix (same with a single fixed value) in FiniteElement:: DemagStiff and NonzeroStiff
   !> Let H2Lib set up sparse stiffness matrix from existing geometric quantities in TetrahedralMeshData.
   subroutine demagStiffH2LibPNM()
      use iso_c_binding, only: c_ptr, c_int, c_bool, c_f_pointer, c_funptr, c_loc
      use H2Lib_basic, only: f_init_h2lib_noargs
      use H2Lib_tet3d, only: f_tet3d, f_new_tet3dbuilder, f_addtetrahedron_tet3dbuilder, &
          & f_fixnormals_tet3d, f_check_tet3d, f_buildmesh_tet3dbuilder, f_del_tet3dbuilder
      use H2Lib_tet3dp1, only: f_tet3dp1, f_new_tet3dp1, &
          & f_build_tet3dp1_sparsematrix, f_build_tet3dp1_interaction_sparsematrix, &
          & f_assemble_tet3dp1_laplace_sparsematrix, f_del_tet3dp1
      use H2Lib_sparsematrix, only: f_sparsematrix
      use Tetrahedral_Mesh_Data, only: NNODE, NTRI, VCL, TIL

      integer(c_int):: n_vertices, n_faces, n_edges, n_tetrahedra
      type(c_ptr):: tb  ! tet3dbuilder
      type(c_ptr):: dc  ! tet3dp1 struct
      type(f_tet3d), pointer:: gr_fptr  ! fortran pointers for inspection of the structs
      type(f_tet3dp1), pointer:: dc_fptr
      type(f_sparsematrix), pointer:: fptr_A  ! To inspect same
      type(f_sparsematrix), pointer:: fptr_Af
      integer(c_int):: nfix, ndof
      ! The "save" attribute is a hack to deal with memory management between C and fortran:
      ! A pointer to the array becomes part of c struct tet3dp1 and the
      ! data is deallocated by C at del_tet3dp1 at the end of this subroutine.
      !   Fortran doesn't know about
      ! this.  To prevent attempted auto deallocation on subroutine exit and
      ! "double free" error, mark with save attribute.

      ! The save attribute ensures is_dof and idx_dof are not attempted
      ! to be deallocated on subroutine exit, which would lead to
      ! "double free or corruption" runtime error because they are
      ! already deallocated from C-side by del_tet3dp1 call
      logical(c_bool), dimension(:), allocatable, target, save:: is_dof  ! non-fixed node status: all except last
      integer(c_int), dimension(:), allocatable, target, save:: idx_dof  ! indices, degrees of freedom: just all indeces except last

      integer(c_int):: N
      integer(c_int):: faces_fixed  ! holding a return value, not used
      integer:: i

      call f_init_h2lib_noargs(0_c_int)
      !sw = new_stopwatch(); !TODO fortran stopwatches

      N = NNODE  ! len VCL as c_int

      tb = f_new_tet3dbuilder(N); !new tet3d mesh builder object
      !It's easiest to just iteratively add our tetrahdra
      call f_modify_tet3dbuilder(tb, c_loc(VCL), N)  ! my C fct copying data from VCL to the location in tb
      do i = 1, NTRI
         ! then set the connectivity of the points in tb from TCL
         ! but this is index in the fortran list VCL, for index in tb C data do-1
         call f_addtetrahedron_tet3dbuilder(tb, TIL(i, 1) - 1, TIL(i, 2) - 1, TIL(i, 3) - 1, TIL(i, 4) - 1)
      end do

      gr = f_buildmesh_tet3dbuilder(tb)  ! generate tet3d mesh object from builder obj

      faces_fixed = f_fixnormals_tet3d(gr)

      !check
      call f_check_tet3d(gr)  ! will cause printout to console from C-side "0 errors found"

      dc = f_new_tet3dp1(gr)  ! new tet3dp1, supposing p1 basis functions
      ! this also holds info on free vs fixed nodes

        !! make all nodes free except the last one
      allocate (is_dof(NNODE))  ! will be deallocated in del_tet3dp1
      allocate (idx_dof(NNODE))
      ndof = NNODE-1
      nfix = 1
      is_dof = .true.
      is_dof(NNODE) = .false.
      idx_dof(1:NNODE-1) = (/(i-1, i = 1, NNODE-1)/)
      idx_dof(NNODE) = 0
      !write (*,*) c_loc(is_dof)
      ! my c function to set new degrees of freedom in dc
      call f_modify_tet3dp1(dc, ndof, nfix, c_loc(is_dof), c_loc(idx_dof))
      !call c_f_pointer(dc, dc_fptr)
      !write (*,*) dc_fptr%nfix, dc_fptr%ndof, dc_fptr%is_dof

      PNM = f_build_tet3dp1_sparsematrix(dc)
      PNM_fixed = f_build_tet3dp1_interaction_sparsematrix(dc)
      call f_assemble_tet3dp1_laplace_sparsematrix(dc, PNM, PNM_fixed)  ! important stiffness matrices
      ! This should be the desired stiffness matrix corresponding to PoissonNeumannMatrix
      ! Here represented as two matrices, sperating the 1 fixed value from the rest.
      ! clean up local, intermediate objects used in this procedure
      call f_del_tet3dp1(dc)
      call f_del_tet3dbuilder(tb)
      ! while keeping sparse matrices PNM, PNM_fixed as module data

   end subroutine demagStiffH2LibPNM

   !> Set up Poisson Dirichlet matrix stiffness amtrix operator
   !> Using H2Lib, from mesh geometry
   !> in : module data built by demagStiffH2LibPNM call
   subroutine demagStiffH2LibPDM()
      !> this replaces setting up
      !> PoissonDirichletMatrix (same with a fixed value) in FiniteElement:: DemagStiff and NonzeroStiff
      !> Let H2Lib set up sparse stiffness matrix from existing geometrix quantities in TetrahedralMeshData.
      use iso_c_binding, only: c_ptr, c_int, c_bool, c_f_pointer, c_funptr
      use H2Lib_sparsematrix, only: f_sparsematrix
      use H2Lib_tet3d, only: f_tet3d
      use H2Lib_tet3dp1, only: f_tet3dp1, f_new_tet3dp1, &
          & f_build_tet3dp1_sparsematrix, f_build_tet3dp1_interaction_sparsematrix, &
          & f_assemble_tet3dp1_laplace_sparsematrix, f_del_tet3dp1
      use Tetrahedral_Mesh_Data, only: NNODE, NTRI, VCL, TIL

      integer(c_int):: n_vertices, n_faces, n_edges, n_tetrahedra
      type(c_ptr):: tb  ! tet3dbuilder
      type(c_ptr):: dc  ! tet3dp1
      type(f_tet3d), pointer:: gr_fptr  ! fortran pointers for inspection of the structs
      type(f_tet3dp1), pointer:: dc_fptr
      type(f_sparsematrix), pointer:: fptr_PDM  ! To inspect same
      type(f_sparsematrix), pointer:: fptr_PDM_fixed
      logical(c_bool), dimension(:), pointer:: PDM_isdof_fptr => null()  ! non-fixed node status: all except last
      integer(c_int), dimension(:), pointer:: idx_dof_fptr => null()  ! indices, degrees of freedom: just all indeces except last
      integer(c_int):: N
      integer(c_int):: faces_fixed  ! holding a return value, not used
      integer:: i


      dc = f_new_tet3dp1(gr)  ! new tet3dp1 from same mesh geometry
      ! TODO gr is not needed after this, dealloc sooner or pass
      ! differently than module data
      ! - and in BA_H"matrix methods

      ! Let this one stay on defaults: surface nodes considered fixed
      ! As this corresponds to our calculation of (rest of) phi2 from
      ! previously calculated values on boundary
      call c_f_pointer(dc, dc_fptr)
      ! save the array indicating whether node indices are on boundary as module data, for later
      call c_f_pointer(dc_fptr%is_dof, PDM_isdof_fptr, [NNODE])
      ! We want to copy the data, not just save a pointer to it -
      ! because the data at that address within the dc struct will
      ! dissapear on del_tet3dp1 dc, at the end of this function
      ! TODO = operator does copy ?
      allocate (PDM_isdof(NNODE))
      PDM_isdof = PDM_isdof_fptr

      ! Set up PDM an H2 matrix corresponding to Poisson Dirichlet Matrix
      PDM = f_build_tet3dp1_sparsematrix(dc); 
      PDM_fixed = f_build_tet3dp1_interaction_sparsematrix(dc); 
      call f_assemble_tet3dp1_laplace_sparsematrix(dc, PDM, PDM_fixed)

      ! Clean up intermediate local objects
      call f_del_tet3dp1(dc)

   end subroutine

   !> Compress existing dense matrix BA (module data)
   !> To H2matrix
   !> IN: using this module's data BA, gr
   !> Out: Sets this module's BA_H2 and BA_amatrix
   !> Deallocates original BA
   subroutine H2BoundMatA()
      use iso_c_binding, only: c_ptr, c_double, c_int, c_f_pointer, c_loc, c_bool, c_char, c_funloc
      use H2Lib_amatrix, only: f_amatrix, f_new_pointer_amatrix, f_del_amatrix
      use H2Lib_surface3d, only: f_surface3d, f_prepare_surface3d, f_check_surface3d, f_del_surface3d
      use H2Lib_bem3d, only: f_new_dlp_laplace_bem3d, f_build_bem3d_cluster
      use H2Lib_laplacebem3d, only: f_del_laplace_bem3d
      use H2Lib_cluster, only: f_del_cluster
      use H2Lib_block, only: f_build_strict_block, f_admissible_2_cluster, f_del_block
      use H2Lib_truncation, only: f_new_abseucl_truncmode, f_del_truncmode
      use H2Lib_h2compression, only: f_compress_amatrix_h2matrix

      use Tetrahedral_Mesh_Data, only: BNODE

      type(c_ptr):: BA_ptr
      type(c_ptr):: surface
      type(f_amatrix), pointer:: BA_amatrix_fptr
      real(kind = c_double), dimension(:, :), pointer:: BA_amatrix_data_fptr
      type(f_surface3d), pointer:: surface_fptr
      character(c_char):: basis  ! C enum maps to characters
      integer(c_int):: q
      real(c_double):: alpha, eta, eps
      integer(c_int):: m, clf
      integer(c_int), target:: z
      type(c_ptr):: bem_dlp
      type(c_ptr):: rbKM
      type(c_ptr):: cbKM
      type(c_ptr):: cptr
      type(c_ptr):: amatrix
      type(c_ptr):: truncmode
      integer(c_int):: nn, nd  ! both number of surface vertices, BNODE again
      type(c_ptr):: BA_amatrix


      ! For now-replace with construction from Hmatrix
      call Boundmata()

      ! use the previously built 3d tetrahedral mesh object gc
      ! Pick out the surface elements and build a surface mesh object

      !write(*,*) "BA in", BA(1, 1), BA(1, 2), BA(2, 1)

      ! call my c function to fill first 7 fields of surface3d
      surface = f_tet3d_to_surface3d(gr)

      !call f_check_surface3d(surface)
      call f_prepare_surface3d(surface)  ! this does the other 4 fields
      call f_check_surface3d(surface)  ! TODO optional debug, move to a test

      ! create bem_dlp object from surface geometry
      basis = 'l'  !BASIS_LINEAR_BEM3D; C enum choices are 0, 'c', 'l' or corresponding ints
      q = 3
      alpha = 0.5  !coefficient of mass matrix

      bem_dlp = f_new_dlp_laplace_bem3d(surface, q, q+2_c_int, basis, basis, alpha); 
      !cluster tree
      ! clf max size leaf clusters
      !m = 4  ! not sure if right for linear basis
      clf = 32
      eta = 2.0

      root = f_build_bem3d_cluster(bem_dlp, clf, basis); 
      !block tree
      ! admissible_2_cluster needs to be a function
      broot = f_build_strict_block(root, root, eta, c_funloc(f_admissible_2_cluster))

      !that wasnt it, try making the right h2matrix from my BA matrix data
      BA_ptr = c_loc(BA)
      BA_amatrix = f_new_pointer_amatrix(BA_ptr, BNODE, BNODE)
      !now amatrix to h2matrix
      truncmode = f_new_abseucl_truncmode()
      eps = 10e-7
      BA_H2 = f_compress_amatrix_h2matrix(BA_amatrix, broot, truncmode, eps)
      ! TODO for comparison, skip later
      !nn = surface_fptr%vertices
      !nd = surface_fptr%vertices
      !ABfull = f_new_amatrix(nn, nd)

      !bem_dlp->nearfield(NULL, NULL, bem_dlp, false, KMfull); 
      ! inspect amatrix
      call c_f_pointer(BA_amatrix, BA_amatrix_fptr)
      call c_f_pointer(BA_amatrix_fptr%a, BA_amatrix_data_fptr, [BNODE, BNODE])
      !write (*,*), "amatrix" , BA_amatrix_data_fptr(1, 1), BA_amatrix_data_fptr(1, 2), BA_amatrix_data_fptr(2, 1)
      !TODO delete surface and other auxillary objets, all except matrix AB
      ! clean up intermediate objects local to this methods
      ! TODO

      call f_del_laplace_bem3d(bem_dlp)
      call f_del_surface3d(surface)  ! in this order
      call f_del_truncmode(truncmode)
      call f_del_amatrix(BA_amatrix)
      if (allocated(BA)) deallocate (BA)

   end subroutine H2BoundMatA

   !> Clear storage associated with module-level H2lib objects, 
   !> if they have been set in demagStiffH2LibPDM, 
   !> demagStiffH2LibPDM, and H2BoundMatA .
   subroutine dealloc_H2Lib()
      use iso_c_binding, only: c_associated, c_null_ptr
      use H2Lib_tet3d, only: f_del_tet3d
      use H2Lib_h2matrix, only: f_del_h2matrix
      use H2Lib_cluster, only: f_del_cluster
      use H2Lib_block, only: f_del_block
      use H2Lib_sparsematrix, only: f_del_sparsematrix
      use H2Lib_basic, only: f_uninit_h2lib
      ! These are c_ptr fortran variables to h2lib objects
      ! call H2lib functions to free the storage at the pointer, 
      ! equivalent to deallocate
      ! c_ptr variables still hold the same addresses, where there is now
      ! no data, but are effectively c null pointers -
      ! querying c_associated again would show false
      if (c_associated(gr)) call f_del_tet3d(gr)
      if (c_associated(root)) then
         call f_free_idx(root)
         call f_del_cluster(root)
      end if
      if (c_associated(broot)) call f_del_block(broot)
      if (c_associated(BA_H2)) then
         call f_del_h2matrix(BA_H2)
         BA_H2 = c_null_ptr  ! null ptr to signal matrix object deleted
      endif
      if (c_associated(PNM)) call f_del_sparsematrix(PNM)
      if (c_associated(PNM_fixed)) call f_del_sparsematrix(PNM_fixed)
      if (c_associated(PDM)) call f_del_sparsematrix(PDM)
      if (c_associated(PDM_fixed)) call f_del_sparsematrix(PDM_fixed)
      ! and a (pointer to) fortran array.  Storage is freed and this pointer
      ! is dissassociated.
      if (allocated(PDM_isdof)) deallocate (PDM_isdof)
      if (allocated(BA)) deallocate (BA)
      call f_uninit_h2lib()
   end subroutine
#endif

   !---------------------------------------------------------------
   ! BEM  : FORCEMAT   geometric (boundary conditions)
   !---------------------------------------------------------------

   subroutine ForceMat()
      use Tetrahedral_Mesh_Data, only: NTRI, NEIGH, TetSubDomains, TIL, b, c, d
      implicit none

      integer:: j, k, l, el, opp, nj, nk
      integer:: idx(3)
      real(KIND = DP):: BAX, BAY, BAZ

      ! TODO do these printouts still mean anything to anyone?
      write (*, *)
      write (*, *) "Building components for:"
      write (*, *) "I grad(phi) . grad(v) dV = I m . grad(v) dV+I v*m . dS"
      write (*, *) "                                              ^^^^^^^^^^^^"

      ! Form force vector matrices, FAX, FAY, FAZ
      ! FAX(:)*m(:,1) = - integral m_x*v * n_x dS
      FAX(:) = -YA4(:, 1)
      FAY(:) = -YA4(:, 2)
      FAZ(:) = -YA4(:, 3)

      ! Add m.n Neumann condition to force vector
      ! Build up element contributions

      !DO i = 1, BFCE
      do el = 1, NTRI
         do opp = 1, 4

            ! Oriented area |A|*n(:) of triangle TIL(el, (/i, j, l/)) with
            ! n(:) the unit normal is given by the shape coefficients for
            ! the opposite node
            ! (1/2) (/ b(el, opp), c(el, opp), d(el, opp) /)

            ! add only if triangle is on the boundary, or on the boundary of
            ! a subdomain.
            ! MAX(NEIGH(el, opp), 1) here to avoid evaluation of TetSubDomains(0) when
            ! NEIGH(el, opp) is 0.

            if ( &
               NEIGH(el, opp) .eq. 0 &
               .or. &
               TetSubDomains(el) .ne. TetSubDomains(max(NEIGH(el, opp), 1)) &
               ) then

               ! Set j, k, l as properly oriented triangle
               select case (opp)
               case (1)
                  j = 2
                  k = 3
                  l = 4
               case (2)
                  j = 3
                  k = 1
                  l = 4
               case (3)
                  j = 4
                  k = 1
                  l = 2
                  !CASE(4)
               case DEFAULT
                  j = 2
                  k = 1
                  l = 3
               end select

               idx(1) = TIL(el, j)
               idx(2) = TIL(el, k)
               idx(3) = TIL(el, l)

               ! BAX = |A|*n(1) / 12 * (if brow != bcol: 1; else: 2)
               !     = (1/2) b(el, opp) / 12 * (...)
               do nj = 1, 3
                  do nk = 1, 3

                     ! Find the right point in FAX to add it
                     l = idx(nj)
                     k = idx(nk)
                     do j = CNR4(l), CNR4(l+1) - 1
                        if (RNR4(j) == k .and. SDNR4(j) .eq. TetSubDomains(el)) then
                           ! Add to FAX where idx(nk) refers to the v(idx(nk)) row

                           ! Find contribution from triangle el due to nodes idx(nj) and
                           ! idx(nk)
                           BAX = -b(el, opp)/24.d0
                           BAY = -c(el, opp)/24.d0
                           BAZ = -d(el, opp)/24.d0
                           if (nj .eq. nk) then
                              BAX = 2*BAX
                              BAY = 2*BAY
                              BAZ = 2*BAZ
                           end if

                           FAX(j) = FAX(j) + BAX
                           FAY(j) = FAY(j) + BAY
                           FAZ(j) = FAZ(j) + BAZ
                        end if
                     end do
                  end do
               end do
            end if

         end do
      end do

      write (*, *) "-- Done"
   end subroutine ForceMat

   !---------------------------------------------------------------
   !          StiffMatrixAllocate and SparseMatrixAllocate
   !---------------------------------------------------------------

   !> Allocate the FEM sparse matrices
   !>
   !> @param[in] nze_pnm  The number of non-zero entries for the Poisson matrix
   !>                     with Neumann boundary conditions (PNM).
   !> @param[in] ncol_pnm The number of columns in the PNM matrix.
   !> @param[in] nze_pdm  The number of non-zero entries for the Poisson matrix
   !>                     with Dirichlet bounary conditions (PDM).
   !> @param[in] ncol_pdn The number of columns in the PDM matrix.
   !> @param[in] nze4     The number of non-zero entries in the RHS vector of
   !>                     the micromagnetic equations, represented by YA4.
   !> @param[in] ncol4    The number of columns in the YA4 vector.
   !> @param[in] nze_im   The number of non-zero entries in the multiphase
   !>                     interpolation matrix (IM).
   !> @param[in] ncol_im  The number of columns in the IM matrix.
   !> @param[in] nbnode   The number of nodes on the boundary of the magnetic
   !>                     region.
   subroutine SparseMatrixAllocate( &
#if MERRILL_USE_H2LIB
#else
      nze_pnm, ncol_pnm, &
      nze_pdm, ncol_pdm, &
#endif
      nze_em, ncol_em, &
      nze4, ncol4, &
      nze_im, ncol_im, &
      nbnode &
      )

      implicit none
#if MERRILL_USE_H2LIB
#else
   ! TODO potential confusion with module-level variable of same name.
   ! and why is it intent(in) if it's to be modified
      integer, intent(IN):: nze_pnm, ncol_pnm
      integer, intent(IN):: nze_pdm, ncol_pdm
#endif
      integer, intent(IN):: nze_em, ncol_em
      integer, intent(IN):: nze4, ncol4
      integer, intent(IN):: nze_im, ncol_im
      integer, intent(IN):: nbnode
#if MERRILL_USE_H2LIB
#else
      if (allocated(PoissonNeumannMatrix)) deallocate (PoissonNeumannMatrix)
      if (allocated(RNR_PNM)) deallocate (RNR_PNM)
      if (allocated(CNR_PNM)) deallocate (CNR_PNM)
      if (allocated(PNM_RWORK)) deallocate (PNM_RWORK)
      if (allocated(PNM_IWORK)) deallocate (PNM_IWORK)

      if (allocated(PoissonDirichletMatrix)) deallocate (PoissonDirichletMatrix)
      if (allocated(RNR_PDM)) deallocate (RNR_PDM)
      if (allocated(CNR_PDM)) deallocate (CNR_PDM)
      if (allocated(PDM_RWORK)) deallocate (PDM_RWORK)
      if (allocated(PDM_IWORK)) deallocate (PDM_IWORK)
#endif
      if (allocated(RNR_EM)) deallocate (RNR_EM)
      if (allocated(CNR_EM)) deallocate (CNR_EM)
      if (allocated(SDNR_EM)) deallocate (SDNR_EM)

      if (allocated(YA4)) deallocate (YA4)
      if (allocated(FAX)) deallocate (FAX)
      if (allocated(FAY)) deallocate (FAY)
      if (allocated(FAZ)) deallocate (FAZ)
      if (allocated(RNR4)) deallocate (RNR4)
      if (allocated(CNR4)) deallocate (CNR4)
      if (allocated(SDNR4)) deallocate (SDNR4)

      if (allocated(ExchangeMatrix)) deallocate (ExchangeMatrix)

      if (allocated(InterpolationMatrix)) deallocate (InterpolationMatrix)
      if (allocated(RNR_IM)) deallocate (RNR_IM)
      if (allocated(CNR_IM)) deallocate (CNR_IM)
      if (allocated(SDNR_IM)) deallocate (SDNR_IM)

      if (allocated(BA)) deallocate (BA)
      if (allocated(BoundaryNodeIndex)) deallocate (BoundaryNodeIndex)
#if MERRILL_USE_H2LIB
#else
      allocate ( &
         PoissonNeumannMatrix(nze_pnm), &
         RNR_PNM(nze_pnm), CNR_PNM(ncol_pnm+1), &
         PNM_RWORK(nze_pnm+5*ncol_pnm), PNM_IWORK(nze_pnm+ncol_pnm+11), &
         )

      allocate ( &
         PoissonDirichletMatrix(nze_pdm), &
         RNR_PDM(nze_pdm), CNR_PDM(ncol_pdm+1), &
         PDM_RWORK(nze_pdm+5*ncol_pdm), PDM_IWORK(nze_pdm+ncol_pdm+11) &
         )
#endif
      allocate ( &
         ExchangeMatrix(nze_em), &
         RNR_EM(nze_em), CNR_EM(ncol_em+1), &
         SDNR_EM(nze_em) &
         )

      allocate ( &
         YA4(nze4, 3), FAX(nze4), FAY(nze4), FAZ(nze4), &
         RNR4(nze4), CNR4(ncol4+1), &
         SDNR4(nze4) &
         )

      allocate ( &
         InterpolationMatrix(nze_im), &
         RNR_IM(nze_im), CNR_IM(ncol_im+1), &
         SDNR_IM(nze_im) &
         )

      allocate (BA(nbnode, nbnode), BoundaryNodeIndex(nbnode))

   end subroutine SparseMatrixAllocate

   !---------------------------------------------------------------
   ! BEM  : DEMAGSTIFF  ===  Purely geometric
   !---------------------------------------------------------------

   !> Generate the entries for the FEM matrices
   subroutine DemagStiff( &
      TIL, TetSubDomains, b, c, d, vol, TetSolid, NodeSolid, &
      kstiff, kstiff2, kinterpolatestiff, krow, kcol, kpid, sparse &
      )
      use Utils, only: qsort, default_smaller
      implicit none

      integer, intent(IN):: TIL(:, :), TetSubDomains(:)
      real(KIND = DP), intent(IN):: b(:, :), c(:, :), d(:, :), vol(:)
      real(KIND = DP), intent(IN):: TetSolid(:, :), NodeSolid(:)

      real(KIND = DP), intent(OUT):: kstiff(:), kstiff2(:, :), kinterpolatestiff(:)
      integer, intent(OUT):: krow(:), kcol(:), kpid(:), sparse(:)

      integer:: knc, i, j, k
      integer, allocatable:: colrowind(:, :)

      knc = 0

      allocate (colrowind(16*size(TIL, 1), 4))

      write (*, *)
      write (*, *) "Building components for:"
      write (*, *) "I grad(phi) . grad(v) dV = I m . grad(v) dV+I v*m . dS"
      write (*, *) "^^^^^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^"
      ! (Galerkin form of the complete problem grad^2 phi = 4\pi grad M plus BC; 
      !  compare Fredkin and Koehler Eq 12
      ! phi magnetic scalar potential
      ! M (vector field) magnetic vector potential
      ! v basis functions, also psi in literature)
      kstiff = 0
      kstiff2 = 0
      kinterpolatestiff = 0

      do i = 1, size(TIL, 1)

         do j = 1, 4
            do k = 1, 4
               knc = 16*(i-1) + 4*(j-1) + k

               ! integral_{tet_i} grad(phi_j) . grad(v_k) dV
               ! where phi, v are linear elements
               kstiff(knc) = (b(i, j)*b(i, k) + c(i, j)*c(i, k) + d(i, j)*d(i, k)) &
                             /(36.d0*vol(i))

               ! m(j, :) * kstiff(knc, :) = integral_{tet_i} m(j, :) . grad(v_k)
               ! where m, v are linear elements
               kstiff2(knc, 1) = b(i, j)/(24.d0)
               kstiff2(knc, 2) = c(i, j)/(24.d0)
               kstiff2(knc, 3) = d(i, j)/(24.d0)

               ! Track column row and material id of entry knc
               krow(knc) = TIL(i, j)
               kcol(knc) = TIL(i, k)
               kpid(knc) = TetSubDomains(i)

               ! Store them in colrowind for later sorting, in order of sorting
               ! sorting importance. i.e. column-row, then material, then
               ! in order of knc. The values knc will then be stored in this order.
               colrowind(knc, 1) = TIL(i, k)
               colrowind(knc, 2) = TIL(i, j)
               colrowind(knc, 3) = TetSubDomains(i)
               colrowind(knc, 4) = knc

               ! Interpolation of subdomains given by splitting region up into
               ! vbox for each subdomain. Only has entry for nodes on diagonal
               ! which represent nodes appearing in different elements.
               ! so that vol(i) represents the box volume contribution from each element
               ! that the node appears in.
               if (j .eq. k) then
                  kinterpolatestiff(knc) = vol(i)/4
               end if
            end do
         end do

      end do

      write (*, *) "-- Done"

      write (*, *)
      write (*, *) "Sorting built components for Sparse Matrix"

      !
      ! Sparse matrices in SLAP column FORMAT (for GMRES)
      !
      ! We want to sort the entries in increasing order of
      !   (column, row, subdomain, tet_id)
      ! We also want diagonal values at the front of each new column.
      ! eg for a 3x3 matrix
      !   (1, 1, ...)
      !   (1, 2, ...)
      !   (1, 3, ...)
      !   (2, 2, ...)
      !   (2, 1, ...)
      !   (2, 3, ...)
      !   (3, 3, ...)
      !   (3, 1, ...)
      !   (3, 2, ...)
      ! we'll achieve this by temporary setting the row value for diagonal
      ! entries to 0.
      ! There's no need to set it back, since sparse(:) will be set from
      ! colrowind(:,4), and colrowind will never be used again.
      !

      ! Set diagonal row values to 0
      do i = 1, knc
         if (colrowind(i, 1) .eq. colrowind(i, 2)) then
            colrowind(i, 2) = 0
         end if
      end do

      !------------- temp bit
      !    print*,'THE SPARCE VECTOR BEFORE'
      !    do i = 1, SIZE(colrowind(:,4))
      !     print*,i, colrowind(i, 1), colrowind(i, 2), colrowind(i, 3), colrowind(i, 4)
      !    enddo
      !------------- temp b

      ! do the actual sorting
      call qsort(colrowind, default_smaller)

      ! Store the sorted values of colrowind in the array sparse(:)
      sparse(:) = colrowind(:, 4)

      !------------- temp bit
      !    print*,'THE SPARCE VECTOR AFTER', SIZE(colrowind(:,4))
      !    do i = 1, SIZE(colrowind(:,4))
      !     print*, i, colrowind(i, 1), colrowind(i, 2), colrowind(i, 3), colrowind(i, 4)
      !     enddo
      !------------- temp bit

      write (*, *) "-- Done"

   end subroutine DemagStiff

   !---------------------------------------------------------------
   ! BEM  : NONZEROSTIFF   purely geometric
   !---------------------------------------------------------------

   !> Assemble the FEM values generated by demagstiff into the sparse
   !> FEM matrices.
   !>
   !> @param[in] NNODE   The number of nodes, i.e. the number of
   !>                    columns.
   !> @param[in] is_boundary_node An array of values which are .TRUE. at index
   !>                             `i` if vertex `i` of `VCL` is on the boundary.
   !> @param[in] kstiff  An array containing the values for the Poisson FEM
   !>                    matrices.
   !> @param[in] kstiff2 An array containing the values for the Exchange FEM
   !>                    matrix.
   !> @param[in] kinterpolatestiff An array containing the values for the
   !>                              multiphase interpolation matrix.
   !> @param[in] krow An array indexing the FEM matrix row for the given sparse
   !>                 index.
   !> @param[in] kcol An array indexing the FEM matrix column for the given
   !>                 sparse index.
   !> @param[in] sparse The sorted indices of the sparce matrix entries.
   subroutine NonZeroStiff( &
      NNODE, is_boundary_node, &
      kstiff, kstiff2, kinterpolatestiff, krow, kcol, kpid, sparse &
      )
#if MERRILL_USE_H2LIB
#else
      use slatec_dsics, only: DSICS  ! sparse incomplete cholensky decomposition
      use slatec_xermsg, only: XERMSG
#endif
      implicit none

      integer, intent(IN):: NNODE
      logical, intent(IN):: is_boundary_node(:)

      real(KIND = DP), intent(IN):: kstiff(:), kstiff2(:, :), kinterpolatestiff(:)
      integer, intent(IN):: krow(:), kcol(:), kpid(:), sparse(:)

      integer:: i, j, j4, j_em, j_im, l
#if MERRILL_USE_H2LIB
#else

      integer, parameter:: LOCRB = 1, LOCIB = 11
      integer:: PNM_LOCJEL, PNM_LOCIEL, PNM_LOCIW
      integer:: PDM_LOCJEL, PDM_LOCIEL, PDM_LOCIW
      integer:: PNM_LOCEL, PNM_LOCDIN, PNM_LOCR, PNM_LOCZ, PNM_LOCP, PNM_LOCDZ, &
                PNM_LOCW
      integer:: PDM_LOCEL, PDM_LOCDIN, PDM_LOCR, PDM_LOCZ, PDM_LOCP, &
                PDM_LOCDZ, PDM_LOCW
      integer:: IERR
      character(len = 8):: XERN1
#endif
      logical:: new_row, new_column, new_subdomain, on_boundary, on_diagonal

      integer:: nbnode

      ! count # of actual non zero entries, nze_pnm for PoissonNeumannMatrix
      ! count # of actual non zero entries, nze_pdm for PoissonDirichletMatrix
      ! count # of actual non zero entries, nze_pdm for ExchangeMatrix
      ! count # of actual non zero entries, nze4 for YA4, FAX, FAY, FAZ
#if MERRILL_USE_H2LIB
#else
      nze_pnm = 0
      nze_pdm = 0
#endif
      nze_em = 0
      nze4 = 0
      nze_im = 0

      do i = 1, size(sparse)
         if (i .gt. 1) then
            new_column = kcol(sparse(i)) .ne. kcol(sparse(i-1))
            new_row = krow(sparse(i)) .ne. krow(sparse(i-1))
            new_subdomain = kpid(sparse(i)) .ne. kpid(sparse(i-1))

         else
            new_column = .true.
            new_row = .true.
            new_subdomain = .true.
         end if
         on_boundary = &
            is_boundary_node(kcol(sparse(i))) &
            .or. &
            is_boundary_node(krow(sparse(i)))
         on_diagonal = krow(sparse(i)) .eq. kcol(sparse(i))
#if MERRILL_USE_H2LIB
#else
         ! If we've moved on to a new matrix entry, add to nze_pnm
         if (new_column .or. new_row) then
            nze_pnm = nze_pnm+1
         end if

         ! If we've moved to a new matrix entry, and it's not on the boundary
         ! or it is a diagonal, add to nze_pdm
         if ( &
            (new_column .or. new_row) &
            .and. &
            ((.not. on_boundary) .or. on_diagonal) &
            ) then
            nze_pdm = nze_pdm+1
         end if
#endif
         ! If we've moved on to a new matrix entry, or a new subdomain, 
         ! add to nze_em and nze4
         if (new_column .or. new_row .or. new_subdomain) then
            nze_em = nze_em+1
            nze4 = nze4+1
         end if

         ! If we've moved to a new matrix entry, or a new subdomain, and
         ! we're on the diagonal, add to nze_im
         ! WW. 16.04.2021  since on diagonal row = column, so only triggered for new_subdomain

         if ( &
            (new_column .or. new_row .or. new_subdomain) &
            .and. &
            on_diagonal &
            ) then
            nze_im = nze_im+1
         end if

      end do

      nbnode = 0
      do i = 1, size(is_boundary_node)
         if (is_boundary_node(i)) nbnode = nbnode+1
      end do

      ! Allocate the sparse arrays
      call SparseMatrixAllocate( &
#if MERRILL_USE_H2LIB
#else
         nze_pnm, NNODE, &
         nze_pdm, NNODE, &
#endif
         nze_em, NNODE, &
         nze4, NNODE, &
         nze_im, NNODE, &
         nbnode &
         )

      !
      ! Build the sparse arrays
      !

      write (*, *)
      write (*, *) "Building Sparse Matrix for Poisson Eqn with Neumann BCs"
#if MERRILL_USE_H2LIB
#else
      PoissonNeumannMatrix = 0
      PoissonDirichletMatrix = 0
#endif
      ExchangeMatrix = 0
      YA4 = 0
      InterpolationMatrix = 0

      j = 0
      j4 = 0
      j_em = 0
      j_im = 0
      l = 0

      do i = 1, size(sparse)

         ! Check index changes
         if (i .gt. 1) then
            new_row = krow(sparse(i)) .ne. krow(sparse(i-1))
            new_column = kcol(sparse(i)) .ne. kcol(sparse(i-1))
            new_subdomain = kpid(sparse(i)) .ne. kpid(sparse(i-1))
         else
            new_row = .true.
            new_column = .true.
            new_subdomain = .true.
         end if
         on_diagonal = krow(sparse(i)) .eq. kcol(sparse(i))
         !      print*,'col, row, domain, diagonal ',i, new_column, new_row, new_subdomain, on_diagonal
         ! If we have any index changes...
         if (new_row .or. new_column .or. new_subdomain) then

            ! If we're on a new column, tick CNR_PNM along
            ! ww 16.04.2021  Only triggered for new column in column ordered data, so
            ! will only be triggered for each time a new node is found, thus l = node number
            !
            if (new_column) then
               l = l+1
               !         print*,'new column: l, (J_im+1)',l, j_im+1
#if MERRILL_USE_H2LIB
#else
               CNR_PNM(l) = j+1
#endif
               CNR_EM(l) = j_em+1
               CNR4(l) = j4+1
               CNR_IM(l) = j_im+1
            end if
#if MERRILL_USE_H2LIB
#else
            ! PNM tracks rows and cols, ignores material.
            !
            ! On row/column change, tick data pos along and track row number.
            if (new_row .or. new_column) then
               j = j+1

               RNR_PNM(j) = krow(sparse(i))
            end if
#endif
            ! EM and YA4 track rows, cols and subdomains.
            !
            ! On row/column/material change, tick data pos along and track row
            ! number and subdomain number.
            ! ww 16.04.2021. j4 counts a node each time it appears in different col-row pairing
            ! AND each time it appears in a new material. Note that col-row pair will occur
            ! more than once since they represent an element edge that may be shared by several
            ! elements. Each time the pair appears it must be in a different element.
            ! In the col-row ordered list we only increment j4 and J_em when the edge (col-row pair)
            !  are in a different subdomain
            if (new_row .or. new_column .or. new_subdomain) then
               j4 = j4+1
               j_em = j_em+1

               RNR_EM(j_em) = krow(sparse(i))
               SDNR_EM(j_em) = kpid(sparse(i))

               RNR4(j4) = krow(sparse(i))
               SDNR4(j4) = kpid(sparse(i))
            end if

            ! IM tracks diagonal rows and cols, and tracks subdomains.
            !
            ! On any change, if we're on a diagonal, tick data pos along, 
            ! track row number and subdomain number.
            ! WW. 16.04.2021  since on diagonal row = column, so only triggered for new_subdomain
            ! so.. for node l, CNR_IM(l) is a CUMULATIVE list will contain how many different materials
            ! that node is associated with.
            ! for example  a 3 subdomain model cannot have [CNR_IM(l) - CNR_IM(l+1) -1] greater than 3.
            !
            ! When we trigger a new subdomain, the subdomain number is stored in the array SDNR_IM with the
            ! same index value as CNR_IM
            ! RNR_IM(j_im) then points to the index number of the node in our original node list
            if ((new_row .or. new_column .or. new_subdomain) .and. on_diagonal) then
               j_im = j_im+1
               !   print*,'J_im updated to:  (row col domain)', j_im, new_row, new_column, new_subdomain
               RNR_IM(j_im) = kcol(sparse(i))
               SDNR_IM(j_im) = kpid(sparse(i))
            end if
         end if

#if MERRILL_USE_H2LIB
#else
         PoissonNeumannMatrix(j) = PoissonNeumannMatrix(j) + kstiff(sparse(i))
#endif
         ExchangeMatrix(j_em) = ExchangeMatrix(j_em) + kstiff(sparse(i))
         YA4(j4, :) = YA4(j4, :) + kstiff2(sparse(i), :)
         ! ww 18.04.2021 In the ordered list (indexed to by sparse(i)), the nodes appear in column order.
         ! Each node on the  diagonal has row value set to zero, so each node entry on the diagonal represents
         ! the fact that the node appears in a number of different elements.
         ! Now J_im incremented for each NEW node and each NEW subdomain, so will any  node that is attached
         ! to more than one subdomain will indexed by j_im for each new subdomain.
         ! The InterpolationMatrix(j_im) will contain the total total box volume for each node for each subdomain
         ! Thus if a node is part of 2 subdomain, it will have two InterpolationMatrix entries, one for each
         ! subdomain, and contains the total box vol for that subdomain.
         if (on_diagonal) then
            InterpolationMatrix(j_im) = InterpolationMatrix(j_im) &
                                        + kinterpolatestiff(sparse(i))
         end if
      end do
#if MERRILL_USE_H2LIB
#else
      CNR_PNM(NNODE+1) = nze_pnm+1
#endif
      CNR_EM(NNODE+1) = nze_em+1
      CNR4(NNODE+1) = nze4+1
      CNR_IM(NNODE+1) = nze_im+1
      !   do i = 1, size(CNR_IM(:))
      !      print*, 'i, CNR_IM', i, CNR_IM(i)
      !      enddo
      !   do i = 1, size(CNR4(:))
      !      print*, 'i, CNR4', i, CNR4(i)
      !      enddo

      write (*, *) "-- Done"
#if MERRILL_USE_H2LIB
#else
      ! form PoissonDirichletMatrix which is a duplicate of PoissonNeumannMatrix
      ! with boundary nodes set to 1 for phi2 set exterior boundary values to 0
      ! This corresponds to pure Dirichlet boundary conditions.

      write (*, *)
      write (*, *) "Building Sparse Matrix for Poisson Eqn with Dirichlet BCs"

      j = 1
      do i = 1, NNODE

         CNR_PDM(i) = j

         do l = CNR_PNM(i), CNR_PNM(i+1) - 1
            ! Set diagonal to 1 for boundary row and column
            if (is_boundary_node(RNR_PNM(l)) .and. (RNR_PNM(l) == i)) then
               PoissonDirichletMatrix(j) = 1.0d0
               RNR_PDM(j) = i
               j = j+1
            end if

            ! Only add non-boundary entries of PoissonNeumannMatrix
            if ( &
               (.not. is_boundary_node(RNR_PNM(l))) &
               .and. &
               (.not. is_boundary_node(i)) &
               ) then
               PoissonDirichletMatrix(j) = PoissonNeumannMatrix(l)
               RNR_PDM(j) = RNR_PNM(l)
               j = j+1
            end if
         end do

      end do

      CNR_PDM(NNODE+1) = nze_pdm+1
#endif
      write (*, *) "-- Done"

#if MERRILL_USE_H2LIB
#else
      ! Adding boundary condition to PoissonNeumannMatrix on the final node
      ! to break its singularity.
      ! During solving, this will node will be set to zero to avoid having
      ! to add the values to the RHS vector.

      do i = 1, NNODE
         do j = CNR_PNM(i), CNR_PNM(i+1) - 1
            if (i .eq. NNODE .or. RNR_PNM(j) .eq. NNODE) then
               if (i .eq. RNR_PNM(j)) then
                  ! 1 on the diagonal
                  PoissonNeumannMatrix(j) = 1
               else
                  ! 0 off the diagonal
                  PoissonNeumannMatrix(j) = 0
               end if
            end if
         end do
      end do

      write (*, *)
      write (*, *) nze_pnm, 'non-zero entries in sparse matrix for Phi1'
      write (*, *) nze_pdm, 'non-zero entries in sparse matrix for Phi2'
#endif
      write (*, *) nze_em, 'non-zero entries in sparse matrix for Exchange'
      write (*, *) nze4, 'non-zero entries in sparse matrix for RHS'
#if MERRILL_USE_H2LIB
#else

      !
      ! From DSICCG
      !

      ! Compute offsets for RWORK and IWORK for PoissonNeumannMatrix and
      ! PoissonDirichletMatrix for use with DSICS. This is lifted straight from
      ! DSICCS with values changed to suit the dummy arguments.

      PNM_LOCJEL = LOCIB
      PDM_LOCJEL = LOCIB
      PNM_LOCIEL = PNM_LOCJEL+nze_pnm
      PDM_LOCIEL = PDM_LOCJEL+nze_pdm
      PNM_LOCIW = PNM_LOCIEL+NNODE+1
      PDM_LOCIW = PDM_LOCIEL+NNODE+1

      PNM_LOCEL = LOCRB
      PDM_LOCEL = LOCRB
      PNM_LOCDIN = PNM_LOCEL+nze_pnm
      PDM_LOCDIN = PDM_LOCEL+nze_pdm
      PNM_LOCR = PNM_LOCDIN+NNODE
      PDM_LOCR = PDM_LOCDIN+NNODE
      PNM_LOCZ = PNM_LOCR+NNODE
      PDM_LOCZ = PDM_LOCR+NNODE
      PNM_LOCP = PNM_LOCZ+NNODE
      PDM_LOCP = PDM_LOCZ+NNODE
      PNM_LOCDZ = PNM_LOCP+NNODE
      PDM_LOCDZ = PDM_LOCP+NNODE
      PNM_LOCW = PNM_LOCDZ+NNODE
      PDM_LOCW = PDM_LOCDZ+NNODE

      ! IWORK(1) = NL
      PNM_IWORK(1) = nze_pnm
      PDM_IWORK(1) = nze_pdm
      ! IWORK(2) = LOCJEL
      PNM_IWORK(2) = PNM_LOCJEL
      PDM_IWORK(2) = PDM_LOCJEL
      ! IWORK(3) = LOCIEL
      PNM_IWORK(3) = PNM_LOCIEL
      PDM_IWORK(3) = PDM_LOCIEL
      ! IWORK(4) = LOCEL
      PNM_IWORK(4) = PNM_LOCEL
      PDM_IWORK(4) = PDM_LOCEL
      ! IWORK(5) = LOCDIN
      PNM_IWORK(5) = PNM_LOCDIN
      PDM_IWORK(5) = PDM_LOCDIN
      ! IWORK(9) = LOCIW
      PNM_IWORK(9) = PNM_LOCIW
      PDM_IWORK(9) = PDM_LOCIW
      ! IWORK(10) = LOCW
      PNM_IWORK(10) = PNM_LOCW
      PDM_IWORK(10) = PDM_LOCW

      !
      ! Call DSICS for PoissonNeumannMatrix and PDM.
      !

      write (*, *)
      write (*, *) "Building Preconditioning Matrix for Neumann Poisson"

      ! Generate PNM_RWORK, PNM_IWORK for PoissonNeumannMatrix
      call DSICS( &
         NNODE, nze_pnm, RNR_PNM, CNR_PNM, PoissonNeumannMatrix, 0, &
         (nze_pnm+NNODE)/2, PNM_IWORK(PNM_LOCIEL), PNM_IWORK(PNM_LOCJEL), &
         PNM_RWORK(PNM_LOCEL), PNM_RWORK(PNM_LOCDIN), PNM_RWORK(PNM_LOCR), IERR &
         )

      if (IERR .ne. 0) then
         write (XERN1, '(I8)') IERR
         call XERMSG('SLATEC', 'DSICCG', &
                     'IC factorization broke down on step '//XERN1// &
                     '.  Diagonal was set to unity and factorization proceeded.', &
                     1, 1)
         IERR = 7
      end if

      write (*, *) "-- Done"

      write (*, *)
      write (*, *) "Building Preconditioning Matrix for Dirichlet Poisson"

      ! Generate PDM_RWORK, PDM_IWORK for PoissonDirichletMatrix
      call DSICS( &
         NNODE, nze_pdm, RNR_PDM, CNR_PDM, PoissonDirichletMatrix, 0, &
         (nze_pdm+NNODE)/2, PDM_IWORK(PDM_LOCIEL), PDM_IWORK(PDM_LOCJEL), &
         PDM_RWORK(PDM_LOCEL), PDM_RWORK(PDM_LOCDIN), PDM_RWORK(PDM_LOCR), IERR &
         )

      if (IERR .ne. 0) then
         write (XERN1, '(I8)') IERR
         call XERMSG('SLATEC', 'DSICCG', &
                     'IC factorization broke down on step '//XERN1// &
                     '.  Diagonal was set to unity and factorization proceeded.', &
                     1, 1)
         IERR = 7
      end if

      write (*, *) "-- Done"
#endif

   end subroutine NonZeroStiff

   subroutine FillBoundaryNodeIndex()
      use Tetrahedral_Mesh_Data, only: NNODE, NodeOnBoundary, BNODE, BFCE, BDFACE, TIL, VCL, GET_ANGLE, solid, b, c, d
      use Utils, only: pi, NONZERO

      implicit none

      real(KIND = DP) rx, ry, rz &
          & , area, normb, normc, normd, normval, SAngle &
          & , neta1, neta2, neta3, zeta, xp, xq, xr &
          & , yp, yq, yr, zp, zq, zr, s1, s2, s3, rho1, rho2, rho3, gam11 &
          & , gam12, gam13, gam22, gam21, gam23, gam33, gam31, gam32, q1, q2, q3 &
          & , tempval(3)
      integer i, j, el, opp   &
          & , k, l, dum2, p1, p2, p3, cn, dum3, p, q, r, n(3), rl  &
          & , bk

      integer, allocatable:: NodeToBoundary(:)

      l = 0
      k = 0
      j = 0

      allocate (NodeToBoundary(NNODE))

      NodeToBoundary = huge(NodeToBoundary)

      rl = 1
      do i = 1, NNODE
         if (NodeOnBoundary(i)) then
            BoundaryNodeIndex(rl) = i
            NodeToBoundary(i) = rl

            rl = rl+1
         end if
      end do
   end subroutine

   !---------------------------------------------------------------
   ! BEM  : BOUNDMATA
   !
   ! We will later repeatedly calculate phi2 as phi2 = M phi1
   ! (in EnergyCalculator:: CalcDEmag), see for example
   ! Fredkin and Koehler Eq 14 and 15.
   !
   ! This is a discretized version of
   ! The matrix M _ij= \int  v_j(x) dG(i, x)/dn dx + (Omega_i/4pi-1)delta_ij
   ! with v basis fct, G green's fct, n unit normal, Omega solid angle, 
   ! depends on geometry (shape basis fcts and normal vectors) only.
   !
   ! This function generates the matrix M.
   ! Called once during BuildFiniteElement.
   !
   ! M_ij is a dense matrix on the basis i, j of boundary node IDs, dims BNODExBNODE.
   ! Large memory footprint !
   ! TODO we could check predicted size vs available memory and give warning/error before building.
   !
   ! In: USE quantities related to node indices, boundary node indices, shape fcts from Tetrahedral_Mesh_Data
   ! Out: Fills array Finite_Element:: BA
   !---------------------------------------------------------------

   subroutine boundmata()
      use Tetrahedral_Mesh_Data, only: NNODE, NodeOnBoundary, BNODE, BFCE, BDFACE, TIL, VCL, GET_ANGLE, solid, b, c, d
      use Utils, only: pi, NONZERO

      implicit none

      real(KIND = DP) rx, ry, rz &
          & , area, normb, normc, normd, normval, SAngle &
          & , neta1, neta2, neta3, zeta, xp, xq, xr &
          & , yp, yq, yr, zp, zq, zr, s1, s2, s3, rho1, rho2, rho3, gam11 &
          & , gam12, gam13, gam22, gam21, gam23, gam33, gam31, gam32, q1, q2, q3 &
          & , tempval(3)
      integer i, j, el, opp   &
          & , k, l, dum2, p1, p2, p3, cn, dum3, p, q, r, n(3), rl  &
          & , bk

      integer, allocatable:: NodeToBoundary(:)

      l = 0
      k = 0
      j = 0

      allocate (NodeToBoundary(NNODE))

      NodeToBoundary = huge(NodeToBoundary)

      rl = 1
      do i = 1, NNODE
         if (NodeOnBoundary(i)) then
            BoundaryNodeIndex(rl) = i
            NodeToBoundary(i) = rl

            rl = rl+1
         end if
      end do

      !write (*,*) rl, "Nodes on boundary when counting boundarynodeindex"

      write (*, *)
      write (*, *) 'Assembling multiplicative matrix for phi2'
      write (*, *) "Building components for:"
      write (*, *) "phi2 = (LU-1)(phi1) on the boundary"
      write (*, *) "       ^^^^^^"
      write (*, *)

      BA = 0

      ! Assemble entries for operator LU for G(r, VCL(k, :)) from
      ! D. Lindholm, “Three-dimensional magnetostatic fields from point-matched
      ! integral equations with linearly varying scalar sources, ” IEEE
      ! Transactions on Magnetics, vol. 20, no. 5, pp. 2025–2032, 1984.

      ! Iterate over boundary nodes
      do bk = 1, BNODE
         k = BoundaryNodeIndex(bk)

         !      node k is the observation point
         rx = VCL(k, 1)
         ry = VCL(k, 2)
         rz = VCL(k, 3)

         ! Calculate contributions to phi2
         ! Iterate over surface triangles
         do dum3 = 1, BFCE

            el = BDFACE(dum3, 1)
            opp = BDFACE(dum3, 2)
            select case (opp)
            case (1)
               p = 3
               q = 2
               r = 4

            case (2)
               p = 1
               q = 3
               r = 4

            case (3)
               p = 4
               q = 2
               r = 1

               !CASE (4)
            case DEFAULT
               p = 1
               q = 2
               r = 3
            end select

            cn = k
            p3 = TIL(el, p)
            p2 = TIL(el, q)
            p1 = TIL(el, r)

            xp = VCL(p1, 1)
            xq = VCL(p2, 1)
            xr = VCL(p3, 1)
            yp = VCL(p1, 2)
            yq = VCL(p2, 2)
            yr = VCL(p3, 2)
            zp = VCL(p1, 3)
            zq = VCL(p2, 3)
            zr = VCL(p3, 3)

            ! get the solid anglemage by the element surface dum3 at the
            ! obervation point
            ! Lindholm's variable S_t

            SAngle = GET_ANGLE(cn, p1, p2, p3)
            ! IF the solid angle is ZERO THEN the observation point and the
            ! element dum3 must be in the same place, and the linhdolm funtion
            ! will be zero

            ! -- the INWARD normal to surface face of a boundary element
            !   (ie Lindholms variable Zeta)

            normval = sqrt(b(el, opp)**2+c(el, opp)**2+d(el, opp)**2)
            normb = -1.*(b(el, opp)/normval)
            normc = -1.*(c(el, opp)/normval)
            normd = -1.*(d(el, opp)/normval)

            area = 0.5*normval

            s1 = sqrt((xq-xp)**2 + (yq-yp)**2 + (zq-zp)**2)
            s2 = sqrt((xr-xq)**2 + (yr-yq)**2 + (zr-zq)**2)
            s3 = sqrt((xp-xr)**2 + (yp-yr)**2 + (zp-zr)**2)

            rho1 = sqrt((xp-rx)**2 + (yp-ry)**2 + (zp-rz)**2)
            rho2 = sqrt((xq-rx)**2 + (yq-ry)**2 + (zq-rz)**2)
            rho3 = sqrt((xr-rx)**2 + (yr-ry)**2 + (zr-rz)**2)

            zeta = ((normb)*(xp-rx) + (normc)*(yp-ry) + normd*(zp-rz))

            gam11 = ((xr-xq)*(xq-xp) + (yr-yq)*(yq-yp) + (zr-zq)*(zq-zp))/(s2*s1)
            gam21 = ((xp-xr)*(xq-xp) + (yp-yr)*(yq-yp) + (zp-zr)*(zq-zp))/(s3*s1)
            gam31 = 1.0d0
            gam12 = 1.0d0
            gam22 = ((xp-xr)*(xr-xq) + (yp-yr)*(yr-yq) + (zp-zr)*(zr-zq))/(s3*s2)
            gam32 = gam11
            gam13 = gam22
            gam23 = 1.0d0
            gam33 = gam21

            neta1 = ((normc*(zq-zp) - normd*(yq-yp))*(xp-rx) + (normd*(xq-xp) - normb &
                                                       &   *(zq-zp))*(yp-ry) + (normb*(yq-yp) - normc*(xq-xp))*(zp-rz))/s1

            neta2 = ((normc*(zr-zq) - normd*(yr-yq))*(xq-rx) + (normd*(xr-xq) - normb &
                                                       &   *(zr-zq))*(yq-ry) + (normb*(yr-yq) - normc*(xr-xq))*(zq-rz))/s2

            neta3 = ((normc*(zp-zr) - normd*(yp-yr))*(xr-rx) + (normd*(xp-xr) - normb &
                                                       &   *(zp-zr))*(yr-ry) + (normb*(yp-yr) - normc*(xp-xr))*(zr-rz))/s3

            !       IF ((p1 /= k).AND.(p2 /= k).AND.(p3 /= k)) THEN
                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if ((p1 == k) .or. (p2 == k) .or. (p3 == k)) then
               if (NONZERO(SAngle)) then
                  print *, 'Solid = ', SAngle
               end if
            else
                    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               q1 = log((rho1+rho2+s1)/(rho1+rho2-s1))
               q2 = log((rho2+rho3+s2)/(rho2+rho3-s2))
               q3 = log((rho3+rho1+s3)/(rho3+rho1-s3))

               dum2 = int(DSIGN(1.0d0, zeta))

               n(1) = TIL(el, p)
               tempval(1) = &
                  s2*(dum2*neta2*SAngle-zeta*(gam11*q1+gam12*q2+gam13*q3)) &
                  /(8*pi*area)

               n(2) = TIL(el, q)
               tempval(2) = &
                  s3*(dum2*neta3*SAngle-zeta*(gam21*q1+gam22*q2+gam23*q3)) &
                  /(8*pi*area)

               n(3) = TIL(el, r)
               tempval(3) = &
                  s1*(dum2*neta1*SAngle-zeta*(gam31*q1+gam32*q2+gam33*q3)) &
                  /(8*pi*area)

               BA(NodeToBoundary(n(:)), bk) = BA(NodeToBoundary(n(:)), bk) &
                                              + tempval(:)
            end if

         end do  ! loop over all boundary elements

         ! add on solid angle part
         ! Fredkin and Koehler Eq 14, 15
         BA(bk, bk) = BA(bk, bk) + ((solid(BoundaryNodeIndex(bk))/(4.*pi)) - 1.d0)

      end do  ! bk

   end subroutine boundmata

!TODO also saving H2Lib matrices (for switching between in script)

   !---------------------------------------------------------------
   !  SaveFEM(MeshNo) saves all important global variables
   !                   of the FEM into arrays
   !                   The index of the FEMState  is MeshNo
   !---------------------------------------------------------------
   subroutine SaveFEM(MeshNo)
      use Utils, only: AllocSet
      use Tetrahedral_Mesh_Data, only: MaxMeshNumber
      implicit none

      integer, intent(IN):: MeshNo

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveBA) .eqv. .false.) then  ! TODO H2Lib route
#if MERRILL_USE_H2LIB
! TODO save h2lib route objects PNM, PNM_fixed, PDM, PDM_fixed
#else
         allocate (Savenze_pnm(MaxMeshNumber))
         allocate (SavePoissonNeumannMatrix(MaxMeshNumber))
         allocate (SaveRNR_PNM(MaxMeshNumber))
         allocate (SaveCNR_PNM(MaxMeshNumber))
         allocate (SavePNM_RWORK(MaxMeshNumber))
         allocate (SavePNM_IWORK(MaxMeshNumber))

         allocate (Savenze_pdm(MaxMeshNumber))
         allocate (SavePoissonDirichletMatrix(MaxMeshNumber))
         allocate (SaveRNR_PDM(MaxMeshNumber))
         allocate (SaveCNR_PDM(MaxMeshNumber))
         allocate (SavePDM_RWORK(MaxMeshNumber))
         allocate (SavePDM_IWORK(MaxMeshNumber))
#endif
         allocate (Savenze_em(MaxMeshNumber))
         allocate (SaveExchangeMatrix(MaxMeshNumber))
         allocate (SaveRNR_EM(MaxMeshNumber))
         allocate (SaveCNR_EM(MaxMeshNumber))
         allocate (SaveSDNR_EM(MaxMeshNumber))

         allocate (Savenze4(MaxMeshNumber))
         allocate (SaveYA4(MaxMeshNumber))
         allocate (SaveFAX(MaxMeshNumber))
         allocate (SaveFAY(MaxMeshNumber))
         allocate (SaveFAZ(MaxMeshNumber))
         allocate (SaveRNR4(MaxMeshNumber))
         allocate (SaveCNR4(MaxMeshNumber))
         allocate (SaveSDNR4(MaxMeshNumber))

         allocate (Savenze_im(MaxMeshNumber))
         allocate (SaveInterpolationMatrix(MaxMeshNumber))
         allocate (SaveRNR_IM(MaxMeshNumber))
         allocate (SaveCNR_IM(MaxMeshNumber))
         allocate (SaveSDNR_IM(MaxMeshNumber))

         allocate (SaveBA(MaxMeshNumber))
         allocate (SaveBoundaryNodeIndex(MaxMeshNumber))
      end if

#if MERRILL_USE_H2LIB
! TODO save h2lib route objects PNM, PNM_fixed, PDM, PDM_fixed
#else
      Savenze_pnm(MeshNo) = nze_pnm
      call AllocSet( &
         PoissonNeumannMatrix, SavePoissonNeumannMatrix(MeshNo)%DPSave &
         )
      call AllocSet(RNR_PNM, SaveRNR_PNM(MeshNo)%IntSave)
      call AllocSet(CNR_PNM, SaveCNR_PNM(MeshNo)%IntSave)
      call AllocSet(PNM_RWORK, SavePNM_RWORK(MeshNo)%DPSave)
      call AllocSet(PNM_IWORK, SavePNM_IWORK(MeshNo)%IntSave)

      Savenze_pdm(MeshNo) = nze_pdm
      call AllocSet( &
         PoissonDirichletMatrix, SavePoissonDirichletMatrix(MeshNo)%DPSave &
         )
      call AllocSet(RNR_PDM, SaveRNR_PDM(MeshNo)%IntSave)
      call AllocSet(CNR_PDM, SaveCNR_PDM(MeshNo)%IntSave)
      call AllocSet(PDM_RWORK, SavePDM_RWORK(MeshNo)%DPSave)
      call AllocSet(PDM_IWORK, SavePDM_IWORK(MeshNo)%IntSave)
      call AllocSet(BA, SaveBA(MeshNo)%DPArrSave)
#endif

      Savenze_em(MeshNo) = nze_em
      call AllocSet( &
         ExchangeMatrix, SaveExchangeMatrix(MeshNo)%DPSave &
         )
      call AllocSet(RNR_EM, SaveRNR_EM(MeshNo)%IntSave)
      call AllocSet(CNR_EM, SaveCNR_EM(MeshNo)%IntSave)
      call AllocSet(SDNR_EM, SaveSDNR_EM(MeshNo)%IntSave)

      Savenze4(MeshNo) = nze4
      call AllocSet(YA4, SaveYA4(MeshNo)%DPArrSave)
      call AllocSet(FAX, SaveFAX(MeshNo)%DPSave)
      call AllocSet(FAY, SaveFAY(MeshNo)%DPSave)
      call AllocSet(FAZ, SaveFAZ(MeshNo)%DPSave)
      call AllocSet(RNR4, SaveRNR4(MeshNo)%IntSave)
      call AllocSet(CNR4, SaveCNR4(MeshNo)%IntSave)
      call AllocSet(SDNR4, SaveSDNR4(MeshNo)%IntSave)
      Savenze_im(MeshNo) = nze_im
      call AllocSet( &
         InterpolationMatrix, SaveInterpolationMatrix(MeshNo)%DPSave &
         )
      call AllocSet(RNR_IM, SaveRNR_IM(MeshNo)%IntSave)
      call AllocSet(CNR_IM, SaveCNR_IM(MeshNo)%IntSave)
      call AllocSet(SDNR_IM, SaveSDNR_IM(MeshNo)%IntSave)
      call AllocSet(BoundaryNodeIndex, SaveBoundaryNodeIndex(MeshNo)%IntSave)
      ! TODO save BA_H2 object?
   end subroutine SaveFEM

   !---------------------------------------------------------------
   !   LoadFEM(MeshNo) loads all important global variables
   !                    of the FEM
   !                    The index of the FEMState  is MeshNo
   !---------------------------------------------------------------

   subroutine LoadFEM(MeshNo)
      use Utils, only: AllocSet
      use Tetrahedral_Mesh_Data, only: MaxMeshNumber
      implicit none

      integer, intent(IN):: MeshNo

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveBA) .eqv. .false.) then
         write (*, *) ' No FEM data saved ... '
         return
      end if

#if MERRILL_USE_H2LIB
#else
      nze_pnm = Savenze_pnm(MeshNo)
      call AllocSet( &
         SavePoissonNeumannMatrix(MeshNo)%DPSave, PoissonNeumannMatrix &
         )
      call AllocSet(SaveRNR_PNM(MeshNo)%IntSave, RNR_PNM)
      call AllocSet(SaveCNR_PNM(MeshNo)%IntSave, CNR_PNM)
      call AllocSet(SavePNM_RWORK(MeshNo)%DPSave, PNM_RWORK)
      call AllocSet(SavePNM_IWORK(MeshNo)%IntSave, PNM_IWORK)

      nze_pdm = Savenze_pdm(MeshNo)
      call AllocSet( &
         SavePoissonDirichletMatrix(MeshNo)%DPSave, PoissonDirichletMatrix &
         )
      call AllocSet(SaveRNR_PDM(MeshNo)%IntSave, RNR_PDM)
      call AllocSet(SaveCNR_PDM(MeshNo)%IntSave, CNR_PDM)
      call AllocSet(SavePDM_RWORK(MeshNo)%DPSave, PDM_RWORK)
      call AllocSet(SavePDM_IWORK(MeshNo)%IntSave, PDM_IWORK)
#endif
      nze_em = Savenze_em(MeshNo)
      call AllocSet( &
         SaveExchangeMatrix(MeshNo)%DPSave, ExchangeMatrix &
         )
      call AllocSet(SaveRNR_EM(MeshNo)%IntSave, RNR_EM)
      call AllocSet(SaveCNR_EM(MeshNo)%IntSave, CNR_EM)
      call AllocSet(SaveSDNR_EM(MeshNo)%IntSave, SDNR_EM)

      nze4 = Savenze4(MeshNo)
      call AllocSet(SaveYA4(MeshNo)%DPArrSave, YA4)
      call AllocSet(SaveFAX(MeshNo)%DPSave, FAX)
      call AllocSet(SaveFAY(MeshNo)%DPSave, FAY)
      call AllocSet(SaveFAZ(MeshNo)%DPSave, FAZ)
      call AllocSet(SaveRNR4(MeshNo)%IntSave, RNR4)
      call AllocSet(SaveCNR4(MeshNo)%IntSave, CNR4)
      call AllocSet(SaveSDNR4(MeshNo)%IntSave, SDNR4)

      nze_im = Savenze_im(MeshNo)
      call AllocSet( &
         SaveInterpolationMatrix(MeshNo)%DPSave, InterpolationMatrix &
         )
      call AllocSet(SaveRNR_IM(MeshNo)%IntSave, RNR_IM)
      call AllocSet(SaveCNR_IM(MeshNo)%IntSave, CNR_IM)
      call AllocSet(SaveSDNR_IM(MeshNo)%IntSave, SDNR_IM)

      call AllocSet(SaveBA(MeshNo)%DPArrSave, BA)
      call AllocSet(SaveBoundaryNodeIndex(MeshNo)%IntSave, BoundaryNodeIndex)
   end subroutine LoadFEM

end module Finite_Element_Matrices
