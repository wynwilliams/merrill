!> A module containing routines for building a tetrahedral mesh and related
!> data structures.
module Tetrahedral_Mesh_Data
   use Utils, only: DP, SaveDPArray, SaveDPRow, SaveIntRow, SaveIntArray, SaveLogicalRow
   implicit none
   save

   ! NNODE : Number of nodes in the mesh
   ! NTRI  : Number of tetrahedra in the mesh
   ! BFCE  : Number of boundary faces in the mesh
   ! BNODE : Number of boundary nodes in the mesh
   ! NFIX  : Number of nodes with fixed magnetization in the mesh
   ! QDMnodes : Number of points in the QDM scan data file

   integer MaxMeshNumber

   integer NNODE, NTRI, BFCE, BNODE, NFIX, QDMnodes
   integer, allocatable  ::  SaveNNODE(:), SaveNTRI(:), SaveBFCE(:), SaveBNODE(:)

   ! VCL(:,3) : 1-3 : List of node coordinates
   ! vol(:) :  List of tetrahedra volumes
   ! vbox(:) :  List of  volumes associated to nodes
   ! vbox_MagOnly(:) : List of magnetic volumes associated to nodes (non mag matrix excluded)
   ! solid(:) :  List of  solid angles associated to nodes (4 pi for interior)
   ! NodeBlockNumber(:) : List of block number to which each node is assigned.
   !     Nodes which are free during minimization have number 0
   ! NodeBodyIndex(:) : Index of body for the given node
   !     (1, 2.. for first, second, ... separate polyhedra ...)
   ! NodeOnBoundary(:) : Value .TRUE. if node is on the boundary of a polyhedron
   !     .FALSE. otherwise
   ! TetNodeBlockNumber : Used to characterize fixed or free nodes in the mesh.
   !     Free = 1

   real(KIND=DP), allocatable, target:: VCL(:, :)
   real(KIND=DP), allocatable:: vol(:), vbox(:), vbox_MagOnly(:), solid(:)
   integer, allocatable:: NodeBodyIndex(:)
   logical, allocatable:: NodeOnBoundary(:)
   integer, allocatable:: NodeBlockNumber(:)

   type(SaveDPArray), allocatable:: SaveVCL(:), SaveMAG(:)
   type(SaveDPRow), allocatable:: Savevbox(:), Savevol(:), Savevbox_MagOnly(:)
   type(SaveIntRow), allocatable:: SaveNodeBodyIndex(:), SaveTetSubDomains(:), SaveSubDomainIds(:)
   type(SaveLogicalRow), allocatable:: SaveNodeOnBoundary(:)

   ! TIL(:,5) : 1-4 : List of indices of tetrahedra vertices 5: index of body
   ! BDFACE(:,2) : 1-4 : List of (index of tetrahedron, face index) for boundary
   !     faces
   ! b, c, d(:4) :  Lists of  shape coefficients associated to tetrahedra
   ! TetSolid(:,4) : The solid angle inside the tetrahedron for each node of the
   !     tetrahedron.
   ! TetSubDomains(:) : The subdomain of each tetrahedron.
   ! SubDomainIds(:) : The ID of each subdomain as given by the input mesh file.

   integer, allocatable:: TIL(:, :), BDFACE(:, :), NEIGH(:, :)
   real(KIND=DP), allocatable:: b(:, :), c(:, :), d(:, :)
   real(KIND=DP), allocatable:: TetSolid(:, :)
   integer, allocatable:: TetSubDomains(:), SubDomainIds(:)

   type(SaveIntArray), allocatable:: SaveTIL(:), SaveBDFACE(:)
   type(SaveDPArray), allocatable  :: Saveb(:), Savec(:), Saved(:)

   ! TODO these are conceptually different, not geometric quantities
   ! m(:,3) : Magnetization vectors associated to nodes
   !B_vect(:,3) : the external magnetc induction field vector associated with nodes(absolute in Tesla), only used for non-uniform fields
   ! mold(:,3) : Previous magnetization vectors associated to nodes
   ! mdotn(:,3) : normal components ???

   real(KIND=DP), allocatable  :: gradc(:, :), heffc(:, :), &
     & mdotn(:, :), m(:, :), B_vect(:, :), totphi(:), phi1(:), phi2(:)

   real(KIND=DP):: total_volume, total_volume_MagOnly, MeanMag(3)
   real(KIND=DP):: zone, zoneinc, zoneflag  ! Zone variables for WriteTecplot
   integer:: saved_mesh_number

   ! Variables for MeshInterpolate: Values depend on m

   logical:: InterpolatableQ = .false., InterpolatableQSD = .false.

   integer:: IntNQ, IntNW, IntNR
   integer, allocatable:: IntLCELL(:, :, :), IntLNEXT(:)
   real(KIND=DP):: IntXYZMin(3), IntXYZDel(3), IntRMax(3)
   real(KIND=DP), allocatable:: IntRSQ(:, :), IntA(:, :, :)

   character(LEN=5):: packing

   ! variables for QDM arrays
   real(KIND=DP), allocatable:: QDM(:, :)

contains

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Subroutines for Mesh related operations
   !    ->  InitializeTetrahedralMeshData  : ensure variables and arrays are
   !                                            ready to be used with any
   !                                            routines in this module and
   !                                            set reasonable default values
   !    ->  DestroyTetrahedralMesh         :
   !    ->  BuildTetrahedralMeshData       : run any routines that calculate
   !                                            useful mesh relations after
   !                                            VCL and TIL have been set
   !    ->  MeshAllocate and FaceAllocate  : allocate right amount of memory
   !    ->  GETBOUNDARY                    : analyses the mesh for
   !                                            boundaries
   !    ->  SOLIDANGLE                     : calculate solid angles at
   !                                            boundary nodes
   !    ->  GETANGLE                       : calculate solid angle for
   !                                            tetrahedron
   !    ->  SHAPECOEFFICIENTS              : calculate volumes and shape
   !                                            coefficients for tetrahedra
   !    ->  WRITEmag                       : writes out the magnetization
   !                                            (.dat) and restart
   !                                            (.restart) files
   !    ->  WRITEhyst                      : writes out m.h_est against
   !                                            |h_ext|
   !    ->  WRITETecplot                   : writes out the  TecPlot format
   !                                            for visualization
   !    ->  MeshInterpolate                : Interpolate mesh functions at
   !                                            arbitrary points
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! Set some reasonable default values
   subroutine InitializeTetrahedralMeshData()

      implicit none

      NFIX = 0  ! No fixed mesh nodes as default

      ! Redefine if more than 5 meshes needed
      ! not memory critical because only pointers are allocated
      ! Large memory blocks are allocated only when meshes are loaded
      MaxMeshNumber = 5

      ! zone/zoneinc not used
      zone = 0.0
      zoneinc = 1.0
      zoneflag = 0.0

      MeanMag = 0

   end subroutine InitializeTetrahedralMeshData

   subroutine DestroyTetrahedralMeshData()
      interface TryDeallocate
         procedure :: &
            TryDeallocateDP, TryDeallocateDPArr, &
            TryDeallocateInt, TryDeallocateIntArr, &
            TryDeallocateLogical
      end interface

      if (allocated(SaveNNODE)) deallocate (SaveNNODE)
      if (allocated(SaveNTRI)) deallocate (SaveNTRI)
      if (allocated(SaveBFCE)) deallocate (SaveBFCE)
      if (allocated(SaveBNODE)) deallocate (SaveBNODE)

      if (allocated(VCL)) deallocate (VCL)
      if (allocated(vol)) deallocate (vol)
      if (allocated(vbox)) deallocate (vbox)
      if (allocated(vbox_MagOnly)) deallocate (vbox_MagOnly)
      if (allocated(solid)) deallocate (solid)
      if (allocated(NodeBodyIndex)) deallocate (NodeBodyIndex)
      if (allocated(NodeOnBoundary)) deallocate (NodeOnBoundary)
      if (allocated(NodeBlockNumber)) deallocate (NodeBlockNumber)

      call TryDeallocate(SaveVCL)
      call TryDeallocate(SaveMAG)
      call TryDeallocate(Savevbox)
      call TryDeallocate(Savevbox_MagOnly)
      call TryDeallocate(Savevol)
      call TryDeallocate(SaveNodeBodyIndex)
      call TryDeallocate(SaveNodeOnBoundary)
      if (allocated(TIL)) deallocate (TIL)
      if (allocated(BDFACE)) deallocate (BDFACE)
      if (allocated(NEIGH)) deallocate (NEIGH)
      if (allocated(b)) deallocate (b)
      if (allocated(c)) deallocate (c)
      if (allocated(d)) deallocate (d)
      if (allocated(TetSolid)) deallocate (TetSolid)
      if (allocated(TetSubDomains)) deallocate (TetSubDomains)
      if (allocated(SubDomainIds)) deallocate (SubDomainIds)

      call TryDeallocate(SaveTIL)
      call TryDeallocate(SaveTetSubDomains)
      call TryDeallocate(SaveSubDomainIds)
      call TryDeallocate(SaveBDFACE)
      call TryDeallocate(Saveb)
      call TryDeallocate(Savec)
      call TryDeallocate(Saved)
      if (allocated(gradc)) deallocate (gradc)
      if (allocated(heffc)) deallocate (heffc)
      if (allocated(mdotn)) deallocate (mdotn)
      if (allocated(m)) deallocate (m)
      if (allocated(totphi)) deallocate (totphi)
      if (allocated(phi1)) deallocate (phi1)
      if (allocated(phi2)) deallocate (phi2)
      if (allocated(IntLCELL)) deallocate (IntLCELL)
      if (allocated(IntLNEXT)) deallocate (IntLNEXT)
      if (allocated(IntRSQ)) deallocate (IntRSQ)
      if (allocated(IntA)) deallocate (IntA)
   contains
      subroutine TryDeallocateDP(s)
         type(SaveDPRow), allocatable, intent(INOUT):: s(:)
         integer:: i
         if (allocated(s)) then
            do i = 1, size(s)
               if (allocated(s(i)%DPSave)) deallocate (s(i)%DPSave)
            end do
            deallocate (s)
         end if
      end subroutine TryDeallocateDP

      subroutine TryDeallocateInt(s)
         type(SaveIntRow), allocatable, intent(INOUT):: s(:)
         integer:: i
         if (allocated(s)) then
            do i = 1, size(s)
               if (allocated(s(i)%IntSave)) deallocate (s(i)%IntSave)
            end do
            deallocate (s)
         end if
      end subroutine TryDeallocateInt

      subroutine TryDeallocateDPArr(s)
         type(SaveDPArray), allocatable, intent(INOUT):: s(:)
         integer:: i
         if (allocated(s)) then
            do i = 1, size(s)
               if (allocated(s(i)%DPArrSave)) deallocate (s(i)%DPArrSave)
            end do
            deallocate (s)
         end if
      end subroutine TryDeallocateDPArr

      subroutine TryDeallocateIntArr(s)
         type(SaveIntArray), allocatable, intent(INOUT):: s(:)
         integer:: i
         if (allocated(s)) then
            do i = 1, size(s)
               if (allocated(s(i)%IntArrSave)) deallocate (s(i)%IntArrSave)
            end do
            deallocate (s)
         end if
      end subroutine TryDeallocateIntArr

      subroutine TryDeallocateLogical(s)
         type(SaveLogicalRow), allocatable, intent(INOUT):: s(:)
         integer:: i
         if (allocated(s)) then
            do i = 1, size(s)
               if (allocated(s(i)%LogicalSave)) deallocate (s(i)%LogicalSave)
            end do
            deallocate (s)
         end if
      end subroutine TryDeallocateLogical
   end subroutine DestroyTetrahedralMeshData

   !---------------------------------------------------------------
   !        BuildTetrahedralMeshData
   !        Call the routines necessary to build any useful
   !        mesh relations defined here after VCL and TIL have
   !        been set.
   !---------------------------------------------------------------
   subroutine BuildTetrahedralMeshData()
      use utils, only: qsort, default_smaller
      integer:: i, j

      ! Compact arbitrarily numbered TetSubDomains into enumerated domains
      ! and store the original numbering in SubDomainIds
      if (allocated(SubDomainIds)) deallocate (SubDomainIds)
      allocate (SubDomainIds(1))
      SubDomainIds(1) = TetSubDomains(1)

      do i = 1, size(TetSubDomains)
         ! If TetSubDomains(i) not in SubDomainIds, add it.
         if (.not. any(SubDomainIds(:) .eq. TetSubDomains(i))) then
            block
               integer, allocatable:: tmp(:)
               allocate (tmp(size(SubDomainIds) + 1))
               tmp(1:size(SubDomainIds)) = SubDomainIds(:)
               call move_alloc(tmp, SubDomainIds)
            end block
            SubDomainIds(size(SubDomainIds)) = TetSubDomains(i)
         end if
      end do

      ! Sort SubDomainIds
      block
         integer, allocatable:: sdid2(:, :)
         allocate (sdid2(size(SubDomainIds), 1))
         sdid2(:, 1) = SubDomainIds
         call qsort(sdid2, default_smaller)
         SubDomainIds = sdid2(:, 1)
      end block

      ! Renumber TetSubDomains
      do i = 1, size(TetSubDomains)
         do j = 1, size(SubDomainIds)
            if (TetSubDomains(i) .eq. SubDomainIds(j)) then
               TetSubDomains(i) = j
               exit
            end if
         end do
      end do

      ! NEIGHfromFACELST
      ! sets neighborship relations for tetrahedra
      ! and determines boundary faces

      call NEIGHfromFACELST()

      write (*, *) ' Mesh Data'
      write (*, *) '-----------'
      write (*, *) NNODE, 'nodes'
      write (*, *) NTRI, 'elements'
      write (*, *) BFCE, 'boundary faces'

      call FaceAllocate()
      ! Mesh preparation for calculations  MODULE Tetrahedral_Mesh_Data
      call getboundary()
      call shapecoeffs()
      call solidangle()
   end subroutine BuildTetrahedralMeshData

   !---------------------------------------------------------------
   !          MeshAllocate and FaceAllocate
   !---------------------------------------------------------------

   subroutine MeshAllocate(nodes, tetrahedra)

      implicit none
      integer, intent(IN):: nodes, tetrahedra

      NNODE = nodes
      NTRI = tetrahedra

      if (allocated(VCL)) deallocate (VCL)
      if (allocated(vol)) deallocate (vol)
      if (allocated(vbox)) deallocate (vbox)
      if (allocated(vbox_MagOnly)) deallocate (vbox_MagOnly)
      if (allocated(solid)) deallocate (solid)
      if (allocated(NodeBodyIndex)) deallocate (NodeBodyIndex)
      if (allocated(NodeOnBoundary)) deallocate (NodeOnBoundary)
      if (allocated(NodeBlockNumber)) deallocate (NodeBlockNumber)

      if (allocated(TIL)) deallocate (TIL)
      if (allocated(NEIGH)) deallocate (NEIGH)
      if (allocated(b)) deallocate (b)
      if (allocated(c)) deallocate (c)
      if (allocated(d)) deallocate (d)
      if (allocated(TetSolid)) deallocate (TetSolid)
      if (allocated(TetSubDomains)) deallocate (TetSubDomains)
      if (allocated(SubDomainIds)) deallocate (SubDomainIds)

      allocate (VCL(NNODE, 3))
      allocate (vol(NTRI))
      allocate (vbox(NNODE))
      allocate (vbox_MagOnly(NNODE))
      allocate (solid(NNODE))
      allocate (NodeBodyIndex(NNODE))
      allocate (NodeOnBoundary(NNODE))
      allocate (NodeBlockNumber(NNODE))

      allocate (TIL(NTRI, 5))
      allocate (NEIGH(NTRI, 4))
      allocate (b(NTRI, 4))
      allocate (c(NTRI, 4))
      allocate (d(NTRI, 4))
      allocate (TetSolid(NTRI, 4))
      allocate (TetSubDomains(NTRI))
      allocate (SubDomainIds(1))

      NodeBodyIndex = 0
      NodeOnBoundary = .false.
      NodeBlockNumber = 0  ! Default : All nodes are free

      TetSubDomains = 1
      SubDomainIds = 1

      if (allocated(gradc)) deallocate (gradc)
      if (allocated(heffc)) deallocate (heffc)
      if (allocated(mdotn)) deallocate (mdotn)
      if (allocated(m)) deallocate (m)
      if (allocated(totphi)) deallocate (totphi)
      if (allocated(phi1)) deallocate (phi1)
      if (allocated(phi2)) deallocate (phi2)

      allocate (gradc(NNODE, 3))
      allocate (heffc(NNODE, 3))
      allocate (mdotn(NNODE, 3))
      allocate (m(NNODE, 3))
      allocate (totphi(NNODE))
      allocate (phi1(NNODE))
      allocate (phi2(NNODE))

      ! Initialize m to [111]
      ! Demag solver complains for m = 0.
      m = 1/sqrt(3.0d0)

      ! Initialize values so Valgrind stops complaining
      gradc = 0
      heffc = 0
      phi1 = 0
      phi2 = 0
      mdotn = 0
      totphi = 0

   end subroutine MeshAllocate

   !---------------------------------------------------------------
   !          QDM surface node array allocation
   !---------------------------------------------------------------

   subroutine QDMAllocate(QDMnodes)

      implicit none
      integer QDMnodes

      if (allocated(QDM)) deallocate (QDM)
      allocate (QDM(QDMnodes, 4))

   end subroutine QDMAllocate

   subroutine FaceAllocate()
      implicit none
      if (allocated(BDFACE)) deallocate (BDFACE)
      allocate (BDFACE(BFCE, 2))
   end subroutine FaceAllocate

   !---------------------------------------------------------------
   !        NEIGHfromFACELST
   !        sets neighborship relations for tetrahedra
   !        and determines boundary faces
   !---------------------------------------------------------------

   subroutine NEIGHfromFACELST()
      use Utils, only: default_smaller, qsort

      implicit none

      integer, allocatable:: FACELST(:, :)

      integer i, swap
      logical:: eq3

      ! FACELST contains all faces
      ! FACELST(i, :): i-th face
      !   1-3 : node indices
      !   4 : position of missing vertex from TIL(j, :)  (= tetrahedron face index)
      !   5 : Index j of tetrahedron in  TIL

      allocate (FACELST(4*NTRI, 5))

      do i = 1, NTRI
         FACELST(4*i - 3, 1) = TIL(i, 2)
         FACELST(4*i - 3, 2) = TIL(i, 4)
         FACELST(4*i - 3, 3) = TIL(i, 3)

         FACELST(4*i - 2, 1) = TIL(i, 1)
         FACELST(4*i - 2, 2) = TIL(i, 3)
         FACELST(4*i - 2, 3) = TIL(i, 4)

         FACELST(4*i - 1, 1) = TIL(i, 1)
         FACELST(4*i - 1, 2) = TIL(i, 4)
         FACELST(4*i - 1, 3) = TIL(i, 2)

         FACELST(4*i, 1) = TIL(i, 1)
         FACELST(4*i, 2) = TIL(i, 2)
         FACELST(4*i, 3) = TIL(i, 3)

         FACELST(4*i - 3, 4) = 1
         FACELST(4*i - 3, 5) = i
         FACELST(4*i - 2, 4) = 2
         FACELST(4*i - 2, 5) = i

         FACELST(4*i - 1, 4) = 3
         FACELST(4*i - 1, 5) = i
         FACELST(4*i, 4) = 4
         FACELST(4*i, 5) = i
      end do

      ! SORT vertices of each face in FACELST by index number,
      ! keeping record of orientation.
      ! This makes sure that a face shared by two elements has the same
      ! triple of vertices in the same order.
      do i = 1, 4*NTRI
         if (FACELST(i, 1) > FACELST(i, 2)) then
            swap = FACELST(i, 1); FACELST(i, 1) = FACELST(i, 2); FACELST(i, 2) = swap
            FACELST(i, 4) = -FACELST(i, 4)
         end if
         if (FACELST(i, 2) > FACELST(i, 3)) then
            swap = FACELST(i, 2); FACELST(i, 2) = FACELST(i, 3); FACELST(i, 3) = swap
            FACELST(i, 4) = -FACELST(i, 4)
         end if
         if (FACELST(i, 1) > FACELST(i, 2)) then
            swap = FACELST(i, 1); FACELST(i, 1) = FACELST(i, 2); FACELST(i, 2) = swap
            FACELST(i, 4) = -FACELST(i, 4)
         end if
      end do

      ! Sort the FACELST.
      ! Equivalent faces in neighbouring faces should be next to each other
      ! after sorting.
      call qsort(FACELST, default_smaller)

      ! Get the number of boundary faces
      Neigh(:, :) = 0
      BFCE = 4*NTRI
      do i = 1, 4*NTRI - 1
         eq3 = (FACELST(i, 1) == FACELST(i + 1, 1) .and. &
                FACELST(i, 2) == FACELST(i + 1, 2) .and. &
                FACELST(i, 3) == FACELST(i + 1, 3))
         if (eq3) then
            ! FACELST(i, 5) and FACELST(i+1, 5) are neighbors
            NEIGH(FACELST(i, 5), abs(FACELST(i, 4))) = FACELST(i + 1, 5)
            NEIGH(FACELST(i + 1, 5), abs(FACELST(i + 1, 4))) = FACELST(i, 5)
            BFCE = BFCE - 2
         end if
      end do

      return
   end subroutine NEIGHfromFACELST

   !---------------------------------------------------------------
   ! BEM  : GETBOUNDARY
   !---------------------------------------------------------------

   subroutine getboundary()
      use Utils, only: MachEps

      implicit none

      integer i, j, BF
      integer e2, n1, n2, n3, nn1, nn2, chk, cubdy, l

      ! create element neighbour list in NEIGH

      ! create list of boundary elements in BDFACE
      BF = 0
      do i = 1, NTRI
         do j = 1, 4
            if (NEIGH(i, j) == 0) then
               BF = BF + 1

               BDFACE(BF, 1) = i
               BDFACE(BF, 2) = j

               n1 = mod(j + 1, 4) + 1
               n2 = mod(j + 2, 4) + 1
               n3 = mod(j, 4) + 1

               NodeOnBoundary(TIL(i, n1)) = .true.
               NodeOnBoundary(TIL(i, n2)) = .true.
               NodeOnBoundary(TIL(i, n3)) = .true.
            end if
         end do
      end do

      ! Count number of boundary nodes
      BNODE = 0
      do j = 1, NNODE
         if (NodeOnBoundary(j)) BNODE = BNODE + 1
      end do
      write (*, *) BNODE, 'boundary nodes'

      ! assign body number to TIL(:,5)
      TIL(:, 5) = 0
      ! set starting element e2 for body 1
      e2 = 1
      chk = 0
      cubdy = 0
      do while (chk == 0)
         cubdy = cubdy + 1
         TIL(e2, 5) = cubdy

         if (NEIGH(e2, 1) /= 0) TIL(NEIGH(e2, 1), 5) = cubdy
         if (NEIGH(e2, 2) /= 0) TIL(NEIGH(e2, 2), 5) = cubdy
         if (NEIGH(e2, 3) /= 0) TIL(NEIGH(e2, 3), 5) = cubdy
         if (NEIGH(e2, 4) /= 0) TIL(NEIGH(e2, 4), 5) = cubdy

         chk = 0
         do while (chk == 0)
            chk = 1
            do i = 1, NTRI
               do j = 1, 4
                  if (NEIGH(i, j) /= 0) then
                     if (TIL(NEIGH(i, j), 5) == cubdy) then
                        if (TIL(i, 5) == 0) then
                           TIL(i, 5) = cubdy
                           chk = 0
                        end if
                        do l = 1, 4
                           if (NEIGH(i, l) /= 0) then
                              if (TIL(NEIGH(i, l), 5) == 0) then
                                 TIL(NEIGH(i, l), 5) = cubdy
                                 chk = 0
                              end if
                           end if
                        end do
                     end if
                  end if
               end do
            end do
         end do

         ! find starting element for next body
         chk = 1
         do i = 1, NTRI
            if (TIL(i, 5) == 0) then
               e2 = i
               chk = 0
            end if
         end do
      end do

      ! assign body number to NodeBodyIndex
      do i = 1, NTRI
         do j = 1, 4
            NodeBodyIndex(TIL(i, j)) = TIL(i, 5)
         end do
      end do

      ! count number of nodes and elements in each body
      write (*, *) cubdy, 'separate bodies'
      do i = 1, cubdy
         nn1 = 0
         nn2 = 0
         do j = 1, NNODE
            if (NodeBodyIndex(j) .eq. i) nn1 = nn1 + 1
         end do
         write (*, *) nn1, 'nodes in body ', i
         do j = 1, NTRI
            if (abs(TIL(j, 5) - i) < MachEps) nn2 = nn2 + 1
         end do
         write (*, *) nn2, 'elements in body', i
      end do

      return
   end subroutine getboundary

   !---------------------------------------------------------------
   ! BEM  : SOLIDANGLE
   !---------------------------------------------------------------

   subroutine solidangle()

      ! --- All we have to do in order to find the solid angle that
      ! --- each node subtends at the surface is to add up all the solid angles
      ! --- that each surface node makes with the opposite surafce of each
      ! --- tetahedra to which it belongs.

      use Utils, only: pi
      implicit none

      integer i, cn, p1, p2, p3
      ! REAL(KIND = DP), INTRINSIC:: GET_ANGLE

      solid(:) = 0.

      do i = 1, NTRI
         cn = TIL(i, 1)
         p1 = TIL(i, 3)
         p2 = TIL(i, 2)
         p3 = TIL(i, 4)
         ! using the GET_ANGLE function
         TetSolid(i, 1) = GET_ANGLE(cn, p1, p2, p3)
         solid(cn) = solid(cn) + TetSolid(i, 1)

         cn = TIL(i, 2)
         p1 = TIL(i, 1)
         p2 = TIL(i, 3)
         p3 = TIL(i, 4)
         ! using the GET_ANGLE function
         TetSolid(i, 2) = GET_ANGLE(cn, p1, p2, p3)
         solid(cn) = solid(cn) + TetSolid(i, 2)

         cn = TIL(i, 3)
         p1 = TIL(i, 4)
         p2 = TIL(i, 2)
         p3 = TIL(i, 1)
         ! using the GET_ANGLE function
         TetSolid(i, 3) = GET_ANGLE(cn, p1, p2, p3)
         solid(cn) = solid(cn) + TetSolid(i, 3)

         cn = TIL(i, 4)
         p1 = TIL(i, 1)
         p2 = TIL(i, 2)
         p3 = TIL(i, 3)
         ! using the GET_ANGLE function
         TetSolid(i, 4) = GET_ANGLE(cn, p1, p2, p3)
         solid(cn) = solid(cn) + TetSolid(i, 4)
      end do  ! the i loop

      do i = 1, NNODE
         ! Ensure that interior nodes are exactly 4*pi.
         if (.not. (NodeOnBoundary(i))) solid(i) = 4*pi
      end do
   end subroutine solidangle

   !---------------------------------------------------------------
   ! BEM  FUNCTION: GETANGLE
   !---------------------------------------------------------------
   ! from indices
   real(KIND=DP) function GET_ANGLE(cn, indexp1, indexp2, indexp3)
      use Utils, only: NONZERO

      implicit none
      integer, intent(in) :: indexp1, indexp2, indexp3, cn
      real(KIND=DP) x0(3), p1(3), p2(3), p3(3)
      real(KIND=DP) pp1(3), pp2(3), pp3(3)

      x0 = VCL(cn, 1:3)
      p1 = VCL(indexp1, 1:3)
      p2 = VCL(indexp2, 1:3)
      p3 = VCL(indexp3, 1:3)

      GET_ANGLE = get_angle_coords(x0, p1, p2, p3)

   end function GET_ANGLE

   ! from coordinate triples
   real(kind=dp) function get_angle_coords(x0, p1, p2, p3)
      use Utils, only: NONZERO, PI
      implicit none
      real(KIND=DP), intent(in) :: x0(3), p1(3), p2(3), p3(3)
      real(kind=dp), dimension(3) :: pp1, pp2, pp3 !edge lengths
      real(KIND=DP) top, bottom !

      ! -- p1 = r0-r1
      pp1 = x0 - p1

      ! -- p2 = r0-r2
      pp2 = x0 - p2

      ! -- p3 = r0-r3
      pp3 = x0 - p3

      top = sqrt(dot_product(pp1, pp1)*dot_product(pp2, pp2)*dot_product(pp3, pp3)) &
         & + sqrt(dot_product(pp1, pp1))*dot_product(pp2, pp3) &
         & + sqrt(dot_product(pp2, pp2))*dot_product(pp3, pp1) &
         & + sqrt(dot_product(pp3, pp3))*dot_product(pp1, pp2)

      bottom = sqrt( &
        &     2*( &
        &         sqrt(dot_product(pp2, pp2)*dot_product(pp3, pp3)) &
        &         + dot_product(pp2, pp3) &
        &     ) &
        &     *( &
        &         sqrt(dot_product(pp3, pp3)*dot_product(pp1, pp1)) &
        &         + dot_product(pp3, pp1) &
        &     ) &
        &     *( &
        &         sqrt(dot_product(pp1, pp1)*dot_product(pp2, pp2)) &
        &         + dot_product(pp1, pp2) &
        &     ) &
        & )

      ! -- the result

      if ((.not. (NONZERO(bottom))) .or. (abs(top) > abs(bottom))) then
         GET_ANGLE_coords = 0
      else
         GET_ANGLE_coords = (2.0d0*(dacos(top/bottom)))
      end if

   end function

   !---------------------------------------------------------------
   ! BEM  : SHAPECOEFFICIENTS
   !---------------------------------------------------------------

   subroutine shapecoeffs()
      implicit none

      integer i, j, ifix
      real(KIND=DP) :: signvolume, volume
      integer n1, n2, n3, n4 !indices of 4 points in Tetradron Index List
      real(kind=dp), dimension(3) :: p1, p2, p3, p4  ! coords 4 points

      do i = 1, NTRI

         do j = 0, 3

            n1 = TIL(i, mod((j + 1), 4) + 1)
            n2 = TIL(i, mod((j + 2), 4) + 1)
            n3 = TIL(i, mod((j + 3), 4) + 1)
            n4 = TIL(i, j + 1)

            p1 = VCL(n1, :)
            p2 = VCL(n2, :)
            p3 = VCL(n3, :)
            p4 = VCL(n4, :)

            volume = calc_signed_volume(p1, p2, p3, p4)

            signvolume = sign(1.0_dp, volume)

            b(i, j + 1) = calc_b(p1, p2, p3, signvolume)
            c(i, j + 1) = calc_c(p1, p2, p3, signvolume)
            d(i, j + 1) = calc_d(p1, p2, p3, signvolume)

            vol(i) = abs(volume)

         end do

      end do

      ! calculate volume vbox associated with node i

      vbox(:) = 0.
      do i = 1, NTRI
         do j = 1, 4
            vbox(TIL(i, j)) = vbox(TIL(i, j)) + (0.25*vol(i))
         end do
      end do

      ! calculate volume vbox associated with node i BUT
      ! excluding non-magnetic elements
      ! the non magnetic matrix must have BlockID of 999 to be marked at non-magnetic
      vbox_MagOnly(:) = 0.
      do i = 1, NTRI
         ifix = 1
         do j = 1, 4
            if (SubDomainIds(TetSubDomains(i)) .eq. 999) ifix = 0
            vbox_MagOnly(TIL(i, j)) = vbox_MagOnly(TIL(i, j)) + (0.25*vol(i))*ifix
         end do
      end do

      total_volume = 0.0d0
      do i = 1, NTRI
         total_volume = total_volume + vol(i)
      end do

      total_volume_MagOnly = 0.0d0
      do i = 1, NTRI
         ifix = 1.
         if (SubDomainIds(TetSubDomains(i)) .eq. 999) ifix = 0
         total_volume_MagOnly = total_volume_MagOnly + vol(i)*ifix
      end do

!    do i = 1, NNODE
!     print*,'box vols: All, Magonly', vbox(i), vbox_MagOnly(i)
!    end do

      write (*, *) 'Total volume = ', total_volume
      write (*, *) 'Total magnetic volume=', total_volume_MagOnly
      return
   end subroutine shapecoeffs

   function calc_signed_volume(p1, p2, p3, p4) result(vol)
      implicit none
      real(kind=dp), dimension(3), intent(in) :: p1, p2, p3, p4 ! coordinate tripes, 3 points in a tetrahedron
      real(kind=dp) :: volume, vol
      ! volume of a tetrahedron:
      !                 p1_1 p2_1 p3_1 p4_1
      ! 6 V = abs det ( p1_2 p2_2 p3_2 p4_2 )
      !                 p1_3 p2_3 p3_3 p4_3
      !                 1    1    1    1
      !
      ! here we return signed volume, det(... ) without abs()
      ! value not used, only sign.
      ! Easier way to get sign ?
      volume =  &
        &  p1(1)*p2(2)*p3(3) - p1(1)*p2(3)*p3(2) &
        & - p2(1)*p1(2)*p3(3) + p2(1)*p1(3)*p3(2) &
        & + p3(1)*p1(2)*p2(3) - p3(1)*p1(3)*p2(2) &
        & - p4(1)*p2(2)*p3(3) + p4(1)*p2(3)*p3(2) &
        & + p2(1)*p4(2)*p3(3) - p2(1)*p4(3)*p3(2) &
        & - p3(1)*p4(2)*p2(3) + p3(1)*p4(3)*p2(2) &
        & + p4(1)*p1(2)*p3(3) - p4(1)*p1(3)*p3(2) &
        & - p1(1)*p4(2)*p3(3) + p1(1)*p4(3)*p3(2) &
        & + p3(1)*p4(2)*p1(3) - p3(1)*p4(3)*p1(2) &
        & - p4(1)*p1(2)*p2(3) + p4(1)*p1(3)*p2(2) &
        & + p1(1)*p4(2)*p2(3) - p1(1)*p4(3)*p2(2) &
        & - p2(1)*p4(2)*p1(3) + p2(1)*p4(3)*p1(2)
      vol = volume/6.0_dp
   end function

   ! Shape function b from 4 points in a tetrahedron
   ! without the correct sign
   ! same math again, for calculating from coordinates in individual calls from
   ! H matrix consructor
   function calc_b(p1, p2, p3, signvolume) result(b)
      real(kind=dp) :: b ! shape function for a point & triangle
      real(kind=dp), intent(in) :: signvolume ! sign
      real(kind=dp), dimension(3), intent(in) :: p1, p2, p3 ! coordinate triples, 3 points in a tetrahedron

      b = ( &
        &     p2(2)*p1(3) - p3(2)*p1(3) &
        &     - p1(2)*p2(3) + p3(2)*p2(3) &
        &     + p1(2)*p3(3) - p2(2)*p3(3) &
        & )/signvolume

   end function

   function calc_c(p1, p2, p3, signvolume) result(c)
      real(kind=dp) :: c ! shape function for a point & triangle
      real(kind=dp), intent(in) :: signvolume
      real(kind=dp), dimension(3), intent(in) :: p1, p2, p3! coordinate triples, 3 points in a triangle

      c = ( &
        &     -p2(1)*p1(3) + p3(1)*p1(3) &
        &     + p1(1)*p2(3) - p3(1)*p2(3) &
        &     - p1(1)*p3(3) + p2(1)*p3(3) &
        & )/signvolume

   end function

   function calc_d(p1, p2, p3, signvolume) result(d)
      real(kind=dp) :: d ! shape function for a point & triangle
      real(kind=dp), intent(in) :: signvolume
      real(kind=dp), dimension(3), intent(in) :: p1, p2, p3! coordinate triples, 3 points in a triangle

      d = ( &
        &     p2(1)*p1(2) - p3(1)*p1(2) &
        &     - p1(1)*p2(2) + p3(1)*p2(2) &
        &     + p1(1)*p3(2) - p2(1)*p3(2) &
        & )/signvolume

   end function

   !---------------------------------------------------------------
   ! MeshInterpolate
   !---------------------------------------------------------------

   subroutine MeshInterpolate(vec, IntM)
      use qshep3d, only: qs3val, qshep3
      implicit none

      integer IntERROR
      real(KIND=DP):: vec(3), IntM(3), IntNorm
      ! 0, if no errors were encountered.
      ! 1, if n, nq, nw, or nr is out of range.
      ! 2, if duplicate nodes were encountered.
      ! 3, if all nodes are coplanar.

      IntNQ = 40  ! changed form pervious value of 10, recommended = 17
      IntNW = 40  ! changed from previous value of 15, recommended = 32
      IntNR = int(real(NNODE/2)**(1.0/3.0))

      if (InterpolatableQ .eqv. .false.) then
         if (allocated(IntLCELL)) deallocate (IntLCELL, IntLNEXT, IntRSQ, IntA)
         allocate (IntLCELL(IntNR, IntNR, IntNR), IntLNEXT(NNODE))
         allocate (IntRSQ(NNODE, 3), IntA(9, NNODE, 3))

         call qshep3(NNODE, VCL(:, 1), VCL(:, 2), VCL(:, 3), m(:, 1), &
           & IntNQ, IntNW, IntNR, IntLCELL, IntLNEXT, &
           & IntXYZMin, IntXYZDel, IntRMax(1), IntRSQ(:, 1), IntA(:, :, 1), IntERROR)
         if (IntERROR /= 0) write (*, *) 'IntERROR mx. ERR=', IntERROR

         call qshep3(NNODE, VCL(:, 1), VCL(:, 2), VCL(:, 3), m(:, 2), &
           & IntNQ, IntNW, IntNR, IntLCELL, IntLNEXT, &
           & IntXYZMin, IntXYZDel, IntRMax(2), IntRSQ(:, 2), IntA(:, :, 2), IntERROR)
         if (IntERROR /= 0) write (*, *) 'IntERROR my. ERR=', IntERROR

         call qshep3(NNODE, VCL(:, 1), VCL(:, 2), VCL(:, 3), m(:, 3), &
           & IntNQ, IntNW, IntNR, IntLCELL, IntLNEXT, &
           & IntXYZMin, IntXYZDel, IntRMax(3), IntRSQ(:, 3), IntA(:, :, 3), IntERROR)
         if (IntERROR /= 0) write (*, *) 'IntERROR mz. ERR=', IntERROR

         InterpolatableQ = .true.
      end if

      IntM(1) = qs3val(vec(1), vec(2), vec(3), &
        &  NNODE, VCL(:, 1), VCL(:, 2), VCL(:, 3), m(:, 1), &
        & IntNR, IntLCELL, IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(1), IntRSQ(:, 1), IntA(:, :, 1))

      IntM(2) = qs3val(vec(1), vec(2), vec(3), &
        &  NNODE, VCL(:, 1), VCL(:, 2), VCL(:, 3), m(:, 2), &
        & IntNR, IntLCELL, IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(2), IntRSQ(:, 2), IntA(:, :, 2))

      IntM(3) = qs3val(vec(1), vec(2), vec(3), &
        &  NNODE, VCL(:, 1), VCL(:, 2), VCL(:, 3), m(:, 3), &
        & IntNR, IntLCELL, IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(3), IntRSQ(:, 3), IntA(:, :, 3))

      IntNorm = sqrt(IntM(1)*IntM(1) + IntM(2)*IntM(2) + IntM(3)*IntM(3))
      IntM(1) = IntM(1)/IntNorm; IntM(2) = IntM(2)/IntNorm; IntM(3) = IntM(3)/IntNorm
   end subroutine MeshInterpolate

   !---------------------------------------------------------------
   ! MeshInterpolate a single SubDomain
   !---------------------------------------------------------------

   subroutine MeshInterpolateSD(vec, IntM, SDvcl, SDNNode, SDm)
      use qshep3d, only: qs3val, qshep3
      implicit none

      !REAL(KIND = DP), ALLOCATABLE, INTENT(INOUT):: SDvcl(:,:), SDm(:,:)
      integer IntERROR
      integer SDNNode
      real(KIND=DP):: vec(3), IntM(3), SDvcl(SDNNode, 3), Sdm(SDNNode, 3)
      real(KIND=DP):: Max_SD(3), Min_SD(3)
      ! 0, if no errors were encountered.
      ! 1, if n, nq, nw, or nr is out of range.
      ! 2, if duplicate nodes were encountered.
      ! 3, if all nodes are coplanar.

      IntNQ = 17  ! changed from previosu value of 10, recommended 17
      IntNW = 32  ! changed from pervious value of 15, recommended 32
      IntNR = int(real(SDNNODE/2)**(1.0/3.0))  ! needs to be number of nodes in extracted mesh

      Max_SD(:) = maxval(SDvcl, Dim=1)
      Min_SD(:) = minval(SDvcl, Dim=1)

      ! print*, "sdnnode", SDNNode
      ! print*, "max array", Max_SD
      ! print*, "min array", Min_SD

      if (InterpolatableQ .eqv. .false.) then
         ! print*, 'setting up interpolation'
         if (allocated(IntLCELL)) deallocate (IntLCELL, IntLNEXT, IntRSQ, IntA)
         allocate (IntLCELL(IntNR, IntNR, IntNR), IntLNEXT(SDNNODE))
         allocate (IntRSQ(SDNNODE, 3), IntA(9, SDNNODE, 3))

         call qshep3(SDNNODE, SDVCL(:, 1), SDVCL(:, 2), SDVCL(:, 3), SDm(:, 1), &
           & IntNQ, IntNW, IntNR, IntLCELL, IntLNEXT, &
           & IntXYZMin, IntXYZDel, IntRMax(1), IntRSQ(:, 1), IntA(:, :, 1), IntERROR)
         if (IntERROR /= 0) write (*, *) 'IntERROR mx. ERR=', IntERROR

         call qshep3(SDNNODE, SDVCL(:, 1), SDVCL(:, 2), SDVCL(:, 3), SDm(:, 2), &
           & IntNQ, IntNW, IntNR, IntLCELL, IntLNEXT, &
           & IntXYZMin, IntXYZDel, IntRMax(2), IntRSQ(:, 2), IntA(:, :, 2), IntERROR)
         if (IntERROR /= 0) write (*, *) 'IntERROR my. ERR=', IntERROR

         call qshep3(SDNNODE, SDVCL(:, 1), SDVCL(:, 2), SDVCL(:, 3), SDm(:, 3), &
           & IntNQ, IntNW, IntNR, IntLCELL, IntLNEXT, &
           & IntXYZMin, IntXYZDel, IntRMax(3), IntRSQ(:, 3), IntA(:, :, 3), IntERROR)
         if (IntERROR /= 0) write (*, *) 'IntERROR mz. ERR=', IntERROR

         InterpolatableQ = .true.
      end if

      IntM(1) = qs3val(vec(1), vec(2), vec(3), &
        &  SDNNODE, SDVCL(:, 1), SDVCL(:, 2), SDVCL(:, 3), SDm(:, 1), &
        & IntNR, IntLCELL, IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(1), IntRSQ(:, 1), IntA(:, :, 1))

      IntM(2) = qs3val(vec(1), vec(2), vec(3), &
        &  SDNNODE, SDVCL(:, 1), SDVCL(:, 2), SDVCL(:, 3), SDm(:, 2), &
        & IntNR, IntLCELL, IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(2), IntRSQ(:, 2), IntA(:, :, 2))

      IntM(3) = qs3val(vec(1), vec(2), vec(3), &
        &  SDNNODE, SDVCL(:, 1), SDVCL(:, 2), SDVCL(:, 3), SDm(:, 3), &
        & IntNR, IntLCELL, IntLNEXT, &
        & IntXYZMin, IntXYZDel, IntRMax(3), IntRSQ(:, 3), IntA(:, :, 3))

      !  IntNorm = sqrt(IntM(1)*IntM(1)+IntM(2)*IntM(2)+IntM(3)*IntM(3))
      !  if(IntNorm .eq. 0) then
      !   print*, 'Zero Norm-interpolation error at' , Vec(:)
      !   Stop
      ! end if
      !  IntM(1)=IntM(1)/IntNorm; IntM(2)=IntM(2)/IntNorm; IntM(3)=IntM(3)/IntNorm
   end subroutine MeshInterpolateSD

   !---------------------------------------------------------------
   !         Working with multiple meshes
   !---------------------------------------------------------------

   !---------------------------------------------------------------
   !           SaveMesh(MeshNo) saves all important global variables
   !                            of the mesh into arrays
   !                            The index of the mesh is MeshNo
   !---------------------------------------------------------------

   subroutine SaveMesh(MeshNo)
      use Utils, only: AllocSet
      implicit none

      integer MeshNo

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveNNODE) .eqv. .false.) then
         allocate (SaveNNODE(MaxMeshNumber))
         allocate (SaveNTRI(MaxMeshNumber))
         allocate (SaveBFCE(MaxMeshNumber))
         allocate (SaveBNODE(MaxMeshNumber))

         allocate (SaveTIL(MaxMeshNumber))
         allocate (SaveTetSubDomains(MaxMeshNumber))
         allocate (SaveSubDomainIds(MaxMeshNumber))
         allocate (SaveBDFACE(MaxMeshNumber))

         allocate (SaveVCL(MaxMeshNumber))
         allocate (SaveMAG(MaxMeshNumber))
         allocate (Savevol(MaxMeshNumber))
         allocate (Savevbox(MaxMeshNumber))
         allocate (Savevbox_MagOnly(MaxMeshNumber))
         allocate (Saveb(MaxMeshNumber))
         allocate (Savec(MaxMeshNumber))
         allocate (Saved(MaxMeshNumber))
         allocate (SaveNodeBodyIndex(MaxMeshNumber))
         allocate (SaveNodeOnBoundary(MaxMeshNumber))
      end if

      SaveNNODE(MeshNo) = NNODE
      SaveNTRI(MeshNo) = NTRI
      SaveBFCE(MeshNo) = BFCE
      SaveBNODE(MeshNo) = BNODE

      call AllocSet(TIL, SaveTIL(MeshNo)%IntArrSave)
      call AllocSet(TetSubDomains, SaveTetSubDomains(MeshNo)%IntSave)
      call AllocSet(SubDomainIds, SaveSubDomainIds(MeshNo)%IntSave)
      call AllocSet(BDFACE, SaveBDFACE(MeshNo)%IntArrSave)

      call AllocSet(vbox, Savevbox(MeshNo)%DPSave)
      call AllocSet(vbox_MagOnly, Savevbox_MagOnly(MeshNo)%DPSave)
      call AllocSet(vol, Savevol(MeshNo)%DPSave)
      call AllocSet(VCL, SaveVCL(MeshNo)%DPArrSave)
      call AllocSet(m, SaveMAG(MeshNo)%DPArrSave)
      call AllocSet(b, Saveb(MeshNo)%DPArrSave)
      call AllocSet(c, Savec(MeshNo)%DPArrSave)
      call AllocSet(d, Saved(MeshNo)%DPArrSave)
      call AllocSet(NodeBodyIndex, SaveNodeBodyIndex(MeshNo)%IntSave)
      call AllocSet(NodeOnBoundary, SaveNodeOnBoundary(MeshNo)%LogicalSave)

   end subroutine SaveMesh

   !-----------------------------------------------------------------
   !           SaveMAGstate(MeshNo) saves a magnetic state associated with
   !                            a current or previously loaded mesh
   !                            The index of the mesh is MeshNo
   !-----------------------------------------------------------------

   subroutine SaveMAGstate(MeshNo)
      use Utils, only: AllocSet
      implicit none

      integer MeshNo

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveNNODE) .eqv. .false.) then
         allocate (SaveNNODE(MaxMeshNumber))

         allocate (SaveMAG(MaxMeshNumber))

      end if

      SaveNNODE(MeshNo) = NNODE

      call AllocSet(m, SaveMAG(MeshNo)%DPArrSave)

   end subroutine SaveMAGstate

   !---------------------------------------------------------------
   !           LoadMagState(MeshNo) loads all important global variables
   !                            of the mesh
   !                            The index of the mesh is MeshNo
   !---------------------------------------------------------------

   subroutine LoadMagState(MeshNo)
      use Utils, only: AllocSet
      implicit none

      integer MeshNo

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveNNODE) .eqv. .false.) then
         write (*, *) ' No meshes saved ... '
         return
      end if

      NNODE = SaveNNODE(MeshNo)

      call AllocSet(SaveMAG(MeshNo)%DPArrSave, m)  ! changed form m to mremesh

   end subroutine LoadMagState

   !---------------------------------------------------------------
   !           LoadMesh(MeshNo) loads all important global variables
   !                            of the mesh
   !                            The index of the mesh is MeshNo
   !---------------------------------------------------------------

   subroutine LoadMesh(MeshNo)
      use Utils, only: AllocSet
      implicit none

      integer MeshNo

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveNNODE) .eqv. .false.) then
         write (*, *) ' No meshes saved ... '
         return
      end if

      NNODE = SaveNNODE(MeshNo)
      NTRI = SaveNTRI(MeshNo)
      BFCE = SaveBFCE(MeshNo)
      BNODE = SaveBNODE(MeshNo)

      call AllocSet(SaveTIL(MeshNo)%IntArrSave, TIL)
      call AllocSet(SaveTetSubDomains(MeshNo)%IntSave, TetSubDomains)
      call AllocSet(SaveSubDomainIds(MeshNo)%IntSave, SubDomainIds)
      call AllocSet(SaveBDFACE(MeshNo)%IntArrSave, BDFACE)

      call AllocSet(SaveVCL(MeshNo)%DPArrSave, VCL)
      call AllocSet(SaveMAG(MeshNo)%DPArrSave, m)
      call AllocSet(Savevbox(MeshNo)%DPSave, vbox)
      call AllocSet(Savevol(MeshNo)%DPSave, vol)
      call AllocSet(Saveb(MeshNo)%DPArrSave, b)
      call AllocSet(Savec(MeshNo)%DPArrSave, c)
      call AllocSet(Saved(MeshNo)%DPArrSave, d)
      call AllocSet(SaveNodeBodyIndex(MeshNo)%IntSave, NodeBodyIndex)
      call AllocSet(SaveNodeOnBoundary(MeshNo)%LogicalSave, NodeOnBoundary)

      if (allocated(gradc)) deallocate (gradc)
      if (allocated(heffc)) deallocate (heffc)
      if (allocated(mdotn)) deallocate (mdotn)
      if (allocated(m)) deallocate (m)
      if (allocated(totphi)) deallocate (totphi)
      if (allocated(phi1)) deallocate (phi1)
      if (allocated(phi2)) deallocate (phi2)

      allocate (gradc(NNODE, 3))
      allocate (mdotn(NNODE, 3))
      allocate (m(NNODE, 3))
      allocate (totphi(NNODE))
      allocate (phi1(NNODE))
      allocate (phi2(NNODE))

      gradc = 0
      mdotn = 0
      m = 1/sqrt(real(3, KIND=DP))
      totphi = 0
      phi1 = 0
      phi2 = 0

   end subroutine LoadMesh

   !---------------------------------------------------------------
   ! RemeshTo(MeshNo) takes the current magnetization m
   !                  of the mesh and remeshes it to the saved
   !                  mesh with index MeshNo
   !                  The resulting magnetization is in the array mremesh
   !                  This is accessible and of right size after LoadMesh(MeshNo)
   !---------------------------------------------------------------

   subroutine RemeshTo(MeshNo, mremesh)
      implicit none

      real(KIND=DP), allocatable, intent(INOUT):: mremesh(:, :)
      integer, intent(IN):: MeshNo
      integer:: SNNODE, i
      real(KIND=DP):: srvec(3)
      real(KIND=DP):: MLen2

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveNNODE) .eqv. .false.) then
         write (*, *) ' No meshes saved ... '
         return
      end if

      ! If(ALLOCATED(SaveVCL(MeshNo)%DPArrSave).EQV..FALSE.)  THEN
      !   Write(*,*) ' No XYZ  saved  for Meshnumber ',MeshNo
      !   RETURN
      ! ENDIF

      SNNODE = SaveNNODE(MeshNo)  ! number of nodes in saved mesh
      if (allocated(mremesh)) deallocate (mremesh)
      allocate (mremesh(SNNODE, 3))   ! allocated magnetization space for the saved mesh we will inertpolate onto

      InterpolatableQ = .false.
      do i = 1, SNNODE
         srvec(:) = SaveVCL(MeshNo)%DPArrSave(i, :)  ! set the node positions of saved mesh to srvec
         call MeshInterpolate(srvec, mremesh(i, :))  ! do the ineterpolation of current mag, m, onto new mesh and put into mremesh.
      end do

      do i = 1, SNNODE
         MLen2 = sum(mremesh(i, :)**2)
         mremesh(i, :) = mremesh(i, :)/sqrt(MLen2)
      end do

   end subroutine RemeshTo

   !---------------------------------------------------------------
   ! RemeshToSD(MeshNo) takes the current magnetization m
   !                  of the mesh in region with subdomain ID 'n'
   !                  and remeshes it to the saved mesh with index MeshNo,
   !                  and identical subdomin ID.
   !                  The resulting magnetization is in the array mremesh
   !                  This is accessible and of right size after LoadMesh(MeshNo)
   !---------------------------------------------------------------
   ! ADDED Sept 3 2021
   !---------------------------------------------------------------

   subroutine RemeshToSD(MeshNo, mremesh, SDNum)
      use Utils, only: RandUnitVec, AllocSet
      implicit none

      real(KIND=DP), allocatable, intent(INOUT):: mremesh(:, :)
      integer, intent(IN):: MeshNo
      integer:: SNNODE, SNTRI, i, j, SDnum, SDNNODE, undefinednodes
      integer:: vclcounter, SavedElementSD
      real(KIND=DP), allocatable:: SDvcl(:, :), SDm(:, :)
      real(KIND=DP):: srvec(3), dm(3)
      real(KIND=DP):: MLen2

      integer, allocatable:: nodepos(:)

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveNNODE) .eqv. .false.) then
         write (*, *) ' No meshes saved ... '
         return
      end if

      SNTRI = SaveNTRI(MeshNo)
      SNNODE = SaveNNODE(MeshNo)

!!!  added to set the original magnetizations of destination to mrmesh
      if (allocated(mremesh)) deallocate (mremesh)
      allocate (mremesh(SNNODE, 3))

! recall the  magnetiation associated with mesh number meshNo and set to mremesh array
      call AllocSet(SaveMAG(MeshNo)%DPArrSave, mremesh)

      if (allocated(nodepos)) deallocate (nodepos)
      allocate (nodepos(NNODE))

      ! determine the number of nodes in subdomain 'SDnum'
      ! of the CURRENT mesh
      nodepos(:) = 0
      do i = 1, NTRI
         if (SubDomainIds(TetSubDomains(i)) .eq. SDnum) then
            nodepos(TIL(i, 1)) = 1
            nodepos(TIL(i, 2)) = 1
            nodepos(TIL(i, 3)) = 1
            nodepos(TIL(i, 4)) = 1
         end if
      end do

      SDnnode = 0
      do i = 1, NNODE
         if (nodepos(i) .eq. 1) SDnnode = SDnnode + 1
      end do

! allocate space to the nodes vector vcl, and magnetization m, of the subdomain SD of current data.
      if (allocated(SDvcl)) deallocate (SDvcl)
      allocate (SDvcl(SDnnode, 3))

      if (allocated(SDm)) deallocate (SDm)
      allocate (SDm(SDnnode, 3))

! populate the node vector and magetization arrays of the subdomain
      vclcounter = 0
      do i = 1, NNODE
         if (nodepos(i) .eq. 1) then
            vclcounter = vclcounter + 1
            SDvcl(vclcounter, :) = VCL(i, :)
            SDm(vclcounter, :) = m(i, :)
         end if
      end do

      ! interpolate onto saved mesh only for those nodes
      ! that have the same SubDomain number as current mesh

      ! go through all elements in new mesh to find those in Subdomain = SDnum
      ! and get interpolated values for all nodes on those elements
      ! This will cover the same node more than once, and more correctly
      ! we should build an array of unique nodes, - but this is easier and
      ! probably fast eneough

      InterpolatableQ = .false.

      if (allocated(nodepos)) deallocate (nodepos)
      allocate (nodepos(SNNODE))

      nodepos(:) = 0

    !!!!!!!mremesh = SaveMAG(MeshNo)%DPArrSave(i, :)  ! populate the orignal magnetization associated with this mesh

      do i = 1, SNTRI
         SavedElementSD = SaveTetSubDomains(MeshNo)%IntSave(i)
         if (SaveSubDomainIds(MeshNo)%IntSave(SavedElementSD) .eq. SDnum) then
            do j = 1, 4
               nodepos(SaveTIL(MeshNo)%IntArrSave(i, j)) = 1
            end do
         end if
      end do

      undefinednodes = 0
      vclcounter = 0
      do i = 1, SNNODE
         if (nodepos(i) .eq. 1) then
            vclcounter = vclcounter + 1
            srvec(:) = SaveVCL(MeshNo)%DPArrSave(i, :)
            call MeshInterpolateSD(srvec, mremesh(i, :), SDvcl, SDNnode, SDm)
            MLen2 = sum(mremesh(i, :)**2)
            if (MLen2 .eq. 0) then  ! node is unefined
               undefinednodes = undefinednodes + 1
               call RandUnitVec(dm)
               MLen2 = sum(dm(:)**2)
               mremesh(i, :) = dm(:)
            end if
            mremesh(i, :) = mremesh(i, :)/sqrt(MLen2)
         end if
      end do

      write (*, '(A, I0, A, I0)') "Total number of Nodes ", NNODE, ", and number of nodes set to random = ", undefinednodes

   end subroutine RemeshToSD

   !---------------------------------------------------------------
   ! ReplaceSD(MeshNo) takes the current magnetization m
   !                  and maps it a prevously loaded mesh and magnetization
   !                  overwriting subdomain SD with the current magnetization
   !                  which is mapped by centering and scaling to dimensions of
   !                  subdomain SD of previously loaded mesh number 'meshno'
   !---------------------------------------------------------------
   ! ADDED Sept 10 2022
   !---------------------------------------------------------------

   subroutine ReplaceSD(MeshNo, mremesh, SDNum)
      use Utils, only: RandUnitVec, AllocSet
      implicit none

      real(KIND=DP), allocatable, intent(INOUT):: mremesh(:, :)
      integer, intent(IN):: MeshNo
      integer:: SNNODE, SNTRI, i, j, SDnum, currentSDnodes
      integer:: vclcounter, SavedElementSD, undefinednodes
      real(KIND=DP), allocatable:: SDvcl(:, :), SDm(:, :)
      real(KIND=DP):: srvec(3), Amap(3), Cmap(3), Max_SD(3), Min_SD(3), current_vec(3), dm(3)
      real(KIND=DP):: MLen2, savedxmin, savedymin, savedzmin, savedxmax, &
       & savedymax, savedzmax
      integer, allocatable:: nodepos(:)

      if (MeshNo > MaxMeshNumber) then  ! check that the new mesh exists
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveNNODE) .eqv. .false.) then  !savennode is an integer array containing the number of nodes in each mesh
         write (*, *) ' No previous meshes saved ... '
         return
      end if

      SNTRI = SaveNTRI(MeshNo)  ! elements in saved mesh we want to map onto
      SNNODE = SaveNNODE(MeshNo)  ! nodes in saved mesh we want to map onto

!!!  added to set the original magnetizations of destination to mrmesh
      if (allocated(mremesh)) deallocate (mremesh)
      allocate (mremesh(SNNODE, 3))

! recall the  magnetiation associated with mesh number meshNo and set to mremesh array
      call AllocSet(SaveMAG(MeshNo)%DPArrSave, mremesh)  ! note this may not exist.

! set array nodepos to store locations of subdomain nodes
      if (allocated(nodepos)) deallocate (nodepos)
      allocate (nodepos(NNODE))  ! nodepos array value will be set to 1 if its not belongs to a subdomain SDnum

      !SDnnode = NNODE  ! all nodes on current mesh to me remapped

! allocate temp space to the nodes vector vcl, and magnetization m, of current data.
!!    If(ALLOCATED(SDvcl) ) DEALLOCATE(SDvcl)
!!    ALLOCATE( SDvcl(nnode, 3))  ! assumed size is maximum possibel for all nodes in all subdomains

!!    If(ALLOCATED(SDm) ) DEALLOCATE(SDm)
!!    ALLOCATE( SDm(nnode, 3))

!  determine nodes of subdomain SDnum in CURRENT mesh
!  if SDnum = -1, the assume all subdomains are to be remeshed

      nodepos(:) = 0

      if (SDnum .ne. -1) then
         do i = 1, NTRI
            ! TetSubDomains are compressed subdomain ID's (.e. 1, 2, 3 etc )
            ! SubDominIds returns the original Sundomain Number (e.g. 101, 102, 103 etc)
            if (SubDomainIds(TetSubDomains(i)) .eq. SDnum) then
               do j = 1, 4
                  nodepos(TIL(i, j)) = 1
               end do
            end if
         end do
      end if

! populate the node vector and magetization arrays of the subdomain
! for  the moment comment out if statement enfoce mapping all current mesh
      vclcounter = 0
      do i = 1, NNODE
         if (nodepos(i) .eq. 1) vclcounter = vclcounter + 1
      end do
      currentSDnodes = vclcounter

! allocate temp space to the nodes vector vcl, and magnetization m, of current data.
      if (allocated(SDvcl)) deallocate (SDvcl)
      allocate (SDvcl(currentSDnodes, 3))  ! assumed size is maximum possibel for all nodes in all subdomains

      if (allocated(SDm)) deallocate (SDm)
      allocate (SDm(currentSDnodes, 3))

! populate the node vector and magetization arrays of the subdomain
! for  the moment comment out if statement enfoce mapping all current mesh
      vclcounter = 0
      do i = 1, NNODE
         if (nodepos(i) .eq. 1) then
            vclcounter = vclcounter + 1
            SDvcl(vclcounter, :) = VCL(i, :)
            SDm(vclcounter, :) = m(i, :)
         end if
      end do

!  find max and min x y z node location values of CURRENT mesh with SD value SDNUM
      Max_SD(:) = maxval(SDvcl, Dim=1)
      Min_SD(:) = minval(SDvcl, Dim=1)

      vclcounter = 0

      do i = 1, NNODE
         if (nodepos(i) .eq. 1) then
            vclcounter = vclcounter + 1
            current_vec(:) = VCL(i, :)
            if (vclcounter .eq. 1) then
               Min_SD(1) = current_vec(1)
               Min_SD(2) = current_vec(2)
               Min_SD(3) = current_vec(3)
               Max_SD(1) = current_vec(1)
               Max_SD(2) = current_vec(2)
               Max_SD(3) = current_vec(3)
            else
               if (current_vec(1) .lt. Min_SD(1)) Min_SD(1) = current_vec(1)
               if (current_vec(2) .lt. Min_SD(2)) Min_SD(2) = current_vec(2)
               if (current_vec(3) .lt. Min_SD(3)) Min_SD(3) = current_vec(3)
               if (current_vec(1) .gt. Max_SD(1)) Max_SD(1) = current_vec(1)
               if (current_vec(2) .gt. Max_SD(2)) Max_SD(2) = current_vec(2)
               if (current_vec(3) .gt. Max_SD(3)) Max_SD(3) = current_vec(3)

            end if
         end if
      end do

      ! interpolate onto saved mesh only for those nodes
      ! that have the same SubDomain number as current mesh

      ! go through all elements in new mesh to find those in Subdomain = SDnum
      ! and get interpolated values for all nodes on those elements
      ! This will cover the same node more than once, and more correctly
      ! we should build an array of unique nodes, - but this is easier and
      ! probably fast eneough

! reuse nodepos for subdomain node location on saved mesh we are remeshing onto
! reset nodepos and allcate new size which has a max number of SNNODe nodes.
      if (allocated(nodepos)) deallocate (nodepos)
      allocate (nodepos(SNNODE))  ! ie. a max of SNNODE (nnodes of saved mesh) in subdomain SDnum

      nodepos(:) = 0

!  determine nodes of subdomain SDnum in SAVED mesh Meshno
!  if SDnum = -1, the assume all subdomains are to be remeshed
      if (SDnum .ne. -1) then
         do i = 1, SNTRI
            SavedElementSD = SaveTetSubDomains(MeshNo)%IntSave(i)
            if (SaveSubDomainIds(MeshNo)%IntSave(SavedElementSD) .eq. SDnum) then
               do j = 1, 4
                  nodepos(SaveTIL(MeshNo)%IntArrSave(i, j)) = 1
               end do
            end if
         end do
      end if

! find max and min of nodel locatiosn for subdomain SDnum in SAVED mesh Meshno
      vclcounter = 0
      do i = 1, SNNODE
         if (nodepos(i) .eq. 1) then
            vclcounter = vclcounter + 1
            srvec(:) = SaveVCL(MeshNo)%DPArrSave(i, :)
            if (vclcounter .eq. 1) then
               savedxmin = srvec(1)
               savedymin = srvec(2)
               savedzmin = srvec(3)
               savedxmax = srvec(1)
               savedymax = srvec(2)
               savedzmax = srvec(3)
            else
               if (srvec(1) .lt. savedxmin) savedxmin = srvec(1)
               if (srvec(2) .lt. savedymin) savedymin = srvec(2)
               if (srvec(3) .lt. savedzmin) savedzmin = srvec(3)
               if (srvec(1) .gt. savedxmax) savedxmax = srvec(1)
               if (srvec(2) .gt. savedymax) savedymax = srvec(2)
               if (srvec(3) .gt. savedzmax) savedzmax = srvec(3)

            end if
         end if
      end do

      ! map current node positions onto locations of subdomain SD of saved mesh
      ! using linear mapping Currnet_new_node = A . Saved_SD_node+C
      Amap(1) = (savedxmax - savedxmin)/(Max_SD(1) - Min_SD(1))
      Amap(2) = (savedymax - savedymin)/(Max_SD(2) - Min_SD(2))
      Amap(3) = (savedzmax - savedzmin)/(Max_SD(3) - Min_SD(3))
      Cmap(1) = savedxmax - Amap(1)*Max_SD(1)
      Cmap(2) = savedymax - Amap(2)*Max_SD(2)
      Cmap(3) = savedzmax - Amap(3)*Max_SD(3)

      ! remap CURRENT node positions onto those of saved mesh
      ! this remapping makes current mesh geometry have the same position and
      ! extent as the SD subdomain of saved mesh
      ! this overwrites current node values SDVCL, but persist in VCL(:)

      do i = 1, currentSDnodes
         SDVCL(i, :) = Amap(:)*SDvcl(i, :) + Cmap(:)
      end do

      InterpolatableQ = .false.

! only replace those values in mremash that are part of Subdomain SDnum of
! saved mesh meshno
      vclcounter = 0
      undefinednodes = 0
      do i = 1, SNNODE  ! i.e. over the nodes of the saved mesh we are mapping onto
         if (nodepos(i) .eq. 1) then
            vclcounter = vclcounter + 1
            srvec(:) = SaveVCL(MeshNo)%DPArrSave(i, :)
            call MeshInterpolateSD(srvec, mremesh(i, :), SDvcl, currentSDnodes, SDm)
            MLen2 = sum(mremesh(i, :)**2)
            if (MLen2 .eq. 0) then  ! node is unefined
               undefinednodes = undefinednodes + 1
               call RandUnitVec(dm)
               MLen2 = sum(dm(:)**2)
               mremesh(i, :) = dm(:)
            end if
            mremesh(i, :) = mremesh(i, :)/sqrt(MLen2)
         end if
      end do

      write (*, '(A, I0, A, I0)') "Total number of Nodes ", NNODE, ", and number of nodes set to random = ", undefinednodes

   end subroutine ReplaceSD

   !---------------------------------------------------------------
   ! RotateMag(alhpha, beta, gamma) takes the current magnetization m
   !                  and rotates it alpha, beta, gamm about x, y, z
   !                  respectively. In reallity it rotates the node locations
   !                  of the magnetization and then remaps this back to the
   !                  original mesh node locations. In this way the geometry
   !                  of structure need not match the symmetry of the
   !                  requested rotation.
   !---------------------------------------------------------------
   ! ADDED Sept 11 2022
   !---------------------------------------------------------------

   subroutine RotateMag(MeshNo, mremesh, alpha, beta, gamma)
      use Utils, only: RandUnitVec, AllocSet
      implicit none

      real(KIND=DP), allocatable, intent(INOUT):: mremesh(:, :)
      integer, intent(IN):: MeshNo
      integer ::  i, undefinednodes
!    INTEGER ::  SavedElementSD, newnodenumber, tempcount
      real(KIND=DP), allocatable:: tempvcl(:, :), tempM(:, :)
      real(KIND=DP):: srvec(3), Max_SD(3), Min_SD(3), Max_new(3), Min_new(3), xold, yold, zold
      real(KIND=DP):: Min_orig(3), Max_orig(3), dm(3)
      real(KIND=DP):: MLen2, alpha, beta, gamma, xmid_old, ymid_old, zmid_old
      real(KIND=DP):: Mxold, Myold, Mzold

      double precision, parameter:: pi = 3.141592653589793238  ! TODO why not use from Utils

      if (MeshNo > MaxMeshNumber) then
         write (*, *) ' Meshnumber ', MeshNo, ' too large'
         return
      end if

      if (allocated(SaveNNODE) .eqv. .false.) then
         write (*, *) ' No previous meshes saved ... '
         return
      end if

      alpha = pi*alpha/180.
      beta = pi*beta/180.
      gamma = pi*gamma/180.
      ! SNTRI = SaveNTRI(MeshNo)
      ! SNNODE = SaveNNODE(MeshNo)

!!!  added to set the original magnetizations of destination to mrmesh
      if (allocated(mremesh)) deallocate (mremesh)
      allocate (mremesh(NNODE, 3))

! set the current magnetiation to the temporary array mrmesh
      call AllocSet(SaveMAG(MeshNo)%DPArrSave, mremesh)

! make a temporary copy of the current node locations
      if (allocated(tempvcl)) deallocate (tempvcl)
      allocate (tempvcl(nnode, 3))
      if (allocated(tempM)) deallocate (tempM)
      allocate (tempM(nnode, 3))

!  find max and min x y z node locatiob values of CURRENT mesh
      Max_Orig(:) = maxval(vcl, Dim=1)
      Min_Orig(:) = minval(vcl, Dim=1)

! find mid point of current mesh
      xmid_old = (Max_orig(1) + Min_orig(1))/2.0
      ymid_old = (Max_orig(2) + Min_orig(2))/2.0
      zmid_old = (Max_orig(3) + Min_orig(3))/2.0

      do i = 1, NNODE  ! centre the mesh so that the rotation works properly
         tempvcl(i, 1) = VCL(i, 1) - xmid_old
         tempvcl(i, 2) = VCL(i, 2) - ymid_old
         tempvcl(i, 3) = VCL(i, 3) - zmid_old
      end do
      Max_SD(:) = maxval(tempvcl, Dim=1)
      Min_SD(:) = minval(tempvcl, Dim=1)

      ! remap CURRENT node positions onto those of saved (original)mesh
      ! this overwrites current node values SDVCL, but persist in VCL(:)

      do i = 1, nnode
         xold = tempvcl(i, 1)
         yold = tempvcl(i, 2)
         zold = tempvcl(i, 3)
         Mxold = m(i, 1)
         Myold = m(i, 2)
         Mzold = m(i, 3)

! new x coord node
         tempVCL(i, 1) = cos(beta)*cos(gamma)*xold + &
             & (sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma))*yold + &
             & (cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma))*zold

         tempM(i, 1) = cos(beta)*cos(gamma)*Mxold + &
             & (sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma))*Myold + &
             & (cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma))*Mzold

! new y coord node
         tempVCL(i, 2) = cos(beta)*sin(gamma)*xold + &
           & (sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma))*yold + &
           & (cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma))*zold

         tempM(i, 2) = cos(beta)*sin(gamma)*Mxold + &
           & (sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma))*Myold + &
           & (cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma))*Mzold

! new z coord node
         tempVCL(i, 3) = -sin(beta)*xold + &
           & sin(alpha)*cos(beta)*yold + &
           & cos(alpha)*cos(beta)*zold

         tempM(i, 3) = -sin(beta)*Mxold + &
           & sin(alpha)*cos(beta)*Myold + &
           & cos(alpha)*cos(beta)*Mzold

      end do

! the max and min of the centred but rotated nodes
      Max_new(:) = maxval(tempvcl, Dim=1)
      Min_new(:) = minval(tempvcl, Dim=1)

! map the NODES to coords of the orginal unrotated, non-centred,  mesh.
      do i = 1, NNODE
         tempvcl(i, 1) = (tempVCL(i, 1)*((Max_new(1) - Min_new(1))/(Max_SD(1) - Min_SD(1))) + xmid_old)
         tempvcl(i, 2) = (tempVCL(i, 2)*((Max_new(2) - Min_new(2))/(Max_SD(2) - Min_SD(2))) + ymid_old)
         tempvcl(i, 3) = (tempVCL(i, 3)*((Max_new(3) - Min_new(3))/(Max_SD(3) - Min_SD(3))) + zmid_old)
      end do

      Max_SD(:) = maxval(tempvcl, Dim=1)
      Min_SD(:) = minval(tempvcl, Dim=1)

      ! print*, 'max rotated mesh mapped (must be greater than original mesh)', Max_SD
      ! print*, 'max original mesh (must be less that original)', Max_orig

      ! print*, 'min rotated mesh mapped (must be smaller (more neg) than original mesh)', Min_SD

      ! print*, 'min orig mesh (must be greater than original)', Min_orig

      ! print*, 'NNode', Nnode

!STOP

! Map the rotated magnetization onto the original (current) mesh locations
! srvec = points where ne magnetization values are needed
! mrmesh = the returned mapped magnetization values
! SDVLC = the current (rotated) array of node locations
! NNODE = number of nodes in current mesh geometry
! SDm = the magnetization array assocauted with nodes SDvlc (which have been rotated)

      InterpolatableQ = .false.
      undefinednodes = 0

      do i = 1, NNODE
         ! for each node in the current mesh find the apprpraite magnetization from the intepolation of the rotated magetization
         ! srvec(:)=SaveVCL(MeshNo)%DPArrSave(i, :)  ! the original node locations of currnet mesh (Meshno neets to be current)
         srvec(:) = vcl(i, :)  ! since vcl is the current mesh nodes
         call MeshInterpolateSD(srvec, mremesh(i, :), tempvcl, Nnode, tempM)
         MLen2 = sum(mremesh(i, :)**2)
         if (MLen2 .eq. 0) then
            undefinednodes = undefinednodes + 1
            call RandUnitVec(dm)
            MLen2 = sum(dm(:)**2)
            mremesh(i, :) = dm(:)
         end if

         mremesh(i, :) = mremesh(i, :)/sqrt(MLen2)

      end do

      write (*, '(A, I0, A, I0)') "Total number of Nodes ", NNODE, ", and number of nodes set to random = ", undefinednodes

   end subroutine RotateMag

   !---------------------------------------------------------------
   !     subroutine  InvertMag( strx, stry, strz  )
   !---------------------------------------------------------------

   subroutine InvertMag(strx, stry, strz)
      use strings, only: value
      implicit none

      character(LEN=*):: strx, stry, strz
      integer ::  ios, i
      real(KIND=DP):: inv(3)

      call value(strx, inv(1), ios)
      if (ios .eq. 0) then
         call value(stry, inv(2), ios)
         if (ios .eq. 0) call value(strz, inv(3), ios)
      end if
      if (ios /= 0) return

      inv(:) = inv(:)/abs(inv(:))
      do i = 1, NNODE
         m(i, :) = m(i, :)*inv(:)
      end do

      return
   end subroutine InvertMag

   !---------------------------------------------------------------
   !     subroutine  FixNodes( SD numbers  )
   !---------------------------------------------------------------

   subroutine FixNodes(args)
      use strings, only: value
      implicit none
      character(len=*), intent(IN):: args(:)

      integer ::  ierr, i, j, nargs, sdnumber
      nargs = size(args)

      nodeblocknumber(:) = 0

      if (NArgs .gt. 1) then

         do j = 2, Nargs
            call value(args(j), SDNumber, ierr)
            write (*, *) ' Fixing nodes in block', SDnumber

            do i = 1, NTRI
               if (SubDomainIds(TetSubDomains(i)) .eq. SDnumber) then
                  nodeblocknumber(TIL(i, 1)) = 1
                  nodeblocknumber(TIL(i, 2)) = 1
                  nodeblocknumber(TIL(i, 3)) = 1
                  nodeblocknumber(TIL(i, 4)) = 1
               end if
            end do
         end do
      else
         write (*, *) 'ERROR: No Subdomain specified-you must specify at least one Subdomain argument to the FixBlockSD command'

      end if

      nfix = 0
      do i = 1, NNode
         if (nodeblocknumber(i) .eq. 1) nfix = nfix + 1
      end do
      write (*, *) 'Total number of fixed nodes (that will not be optimized) =', nfix
      return
   end subroutine FixNodes

   !---------------------------------------------------------------
   !     subroutine  RenumberBlocks( strx, stry, strz  )
   !---------------------------------------------------------------

   subroutine RenumberBlocks(SDnumber1, SDnumber2, currentmesh, ios)

      use Utils, only: default_smaller, qsort

      implicit none

      integer  :: tetnumber, ios
      integer ::  i, j, SDnumber1, SDnumber2, currentmesh

      tetnumber = 0
      ios = 0
      do i = 1, NTRI
         TetSubDomains(i) = SubDomainIds(TetSubDomains(i))
         if (TetSubDomains(i) .eq. SDnumber1) then
            TetSubDomains(i) = SDnumber2
            tetnumber = tetnumber + 1
         end if
      end do

      if (tetnumber .eq. 0) then
         ios = 1
         return
      else
         write (*, 1001) tetnumber, SDnumber1, SDnumber2
1001     format(I0, ' elements renamed from ', I0, ' to ', I0)
      end if

      ! Compact arbitrarily numbered TetSubDomains into enumerated domains
      ! and store the original numbering in SubDomainIds
      if (allocated(SubDomainIds)) deallocate (SubDomainIds)
      allocate (SubDomainIds(1))
      SubDomainIds(1) = TetSubDomains(1)

      do i = 1, size(TetSubDomains)
         ! If TetSubDomains(i) not in SubDomainIds, add it.
         if (.not. any(SubDomainIds(:) .eq. TetSubDomains(i))) then
            block
               integer, allocatable:: tmp(:)
               allocate (tmp(size(SubDomainIds) + 1))
               tmp(1:size(SubDomainIds)) = SubDomainIds(:)
               call move_alloc(tmp, SubDomainIds)
            end block
            SubDomainIds(size(SubDomainIds)) = TetSubDomains(i)
         end if
      end do

      ! Sort SubDomainIds
      block
         integer, allocatable:: sdid2(:, :)
         allocate (sdid2(size(SubDomainIds), 1))
         sdid2(:, 1) = SubDomainIds
         call qsort(sdid2, default_smaller)
         SubDomainIds = sdid2(:, 1)
      end block

      ! Renumber TetSubDomains
      do i = 1, size(TetSubDomains)
         do j = 1, size(SubDomainIds)
            if (TetSubDomains(i) .eq. SubDomainIds(j)) then
               TetSubDomains(i) = j
               exit
            end if
         end do
      end do

      call SaveMesh(currentmesh)

      return
   end subroutine RenumberBlocks

   !---------------------------------------------------------------
   !     subroutine  ModifyMag( str, angle )
   !---------------------------------------------------------------

   subroutine ModifyMag(str, angle)
      use strings, only: lowercase
      use Utils, only: pi, RandUnif, RandUnitVec
      implicit none

      character(LEN=*), intent(IN):: str
      character(LEN=len(str)):: lstr
      integer ::  i, j
      real(KIND=DP):: angle, dm(3), pm(3), dmnorm, rangle = 0.

      lstr = lowercase(str)
      rangle = tan(angle/180.*pi)
      do j = 1, 3  ! calculate a random but uniform disturbation vector
         dm(j) = RandUnif() - 0.5
      end do

      do i = 1, NNODE
         ! in this case every spin is disturbed independently
         if (lstr .eq. 'random') then
            call RandUnitVec(dm)
         end if

         ! Cross product to get perpendicular vector to m and dm
         pm(1) = dm(2)*m(i, 3) - dm(3)*m(i, 2)
         pm(2) = dm(3)*m(i, 1) - dm(1)*m(i, 3)
         pm(3) = dm(1)*m(i, 2) - dm(2)*m(i, 1)
         dmnorm = rangle/sqrt(pm(1)**2 + pm(2)**2 + pm(3)**2)

         do j = 1, 3
            dm(j) = m(i, j) + pm(j)*dmnorm
         end do

         dmnorm = sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
         if (dmnorm > 0.0) then
            dm(1) = dm(1)/dmnorm
            dm(2) = dm(2)/dmnorm
            dm(3) = dm(3)/dmnorm
         end if

         m(i, :) = dm(:)
      end do

      return
   end subroutine ModifyMag

!---------------------------------------------------------------
!     subroutine randomchange(args(2), args(3), sd )
!---------------------------------------------------------------

   subroutine randomchange(str, sran, sd)
      use strings, only: lowercase, value
      use Utils, only: pi, RandUnitVec
      implicit none

      character(LEN=*), intent(IN):: str, sran
      character(LEN=len(str)):: lstr
      integer, optional, intent(IN):: sd
      integer ::  ios, i, j, k !!, sd_min, sd_max
      real(KIND=DP):: dm(3), pm(3), dmnorm, rangle = 0.

      if (present(sd)) then

         lstr = lowercase(str)
         if (lstr .eq. 'all') then

            do i = 1, NTRI

               if (TetSubDomains(i) .eq. sd) then  ! only randomise nodes of this subdomain
                  ! tempcount = tempcount+1
                  do k = 1, 4  ! nodes per tet
                     call RandUnitVec(dm)
                     m(TIL(i, k), :) = dm(:)
                  end do
               end if
            end do

         else
            call value(sran, rangle, ios)
            if (ios /= 0) write (*, *) 'ERROR setting random angle: ', sran
            rangle = tan(rangle/180.*pi)
            do i = 1, NTRI
               if (TetSubDomains(i) .eq. sd) then  ! only randomise nodes in this domain

                  do k = 1, 4
                     call RandUnitVec(dm)

                     ! Cross product to get perpendicular vector to m and dm
                     pm(1) = dm(2)*m(i, 3) - dm(3)*m(i, 2)
                     pm(2) = dm(3)*m(i, 1) - dm(1)*m(i, 3)
                     pm(3) = dm(1)*m(i, 2) - dm(2)*m(i, 1)
                     dmnorm = rangle/sqrt(pm(1)**2 + pm(2)**2 + pm(3)**2)

                     do j = 1, 3
                        dm(j) = m(i, j) + pm(j)*dmnorm
                     end do

                     dmnorm = sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)

                     if (dmnorm > 0.0) then
                        dm(1) = dm(1)/dmnorm
                        dm(2) = dm(2)/dmnorm
                        dm(3) = dm(3)/dmnorm
                     end if
                     m(TIL(i, k), :) = dm(:)

                  end do
               end if
            end do

         end if

      else

         lstr = lowercase(str)
         if (lstr .eq. 'all') then

            do i = 1, NNODE

               if (NodeBlockNumber(i) .eq. 0) then  ! only free nodes are randomized !!
                  call RandUnitVec(dm)
                  m(i, :) = dm(:)
               end if
            end do
         else
            call value(sran, rangle, ios)
            if (ios /= 0) write (*, *) 'ERROR setting random angle: ', sran
            rangle = tan(rangle/180.*pi)
            do i = 1, NNODE
               if (NodeBlockNumber(i) .eq. 0) then  ! only free nodes are randomized !!

                  call RandUnitVec(dm)

                  ! Cross product to get perpendicular vector to m and dm
                  pm(1) = dm(2)*m(i, 3) - dm(3)*m(i, 2)
                  pm(2) = dm(3)*m(i, 1) - dm(1)*m(i, 3)
                  pm(3) = dm(1)*m(i, 2) - dm(2)*m(i, 1)
                  dmnorm = rangle/sqrt(pm(1)**2 + pm(2)**2 + pm(3)**2)

                  do j = 1, 3
                     dm(j) = m(i, j) + pm(j)*dmnorm
                  end do

                  dmnorm = sqrt(dm(1)**2 + dm(2)**2 + dm(3)**2)
                  if (dmnorm > 0.0) then
                     dm(1) = dm(1)/dmnorm
                     dm(2) = dm(2)/dmnorm
                     dm(3) = dm(3)/dmnorm
                  end if
                  m(i, :) = dm(:)
               end if
            end do
         end if

      end if

      return
   end subroutine randomchange

   !
   ! SETNonUniformField(R1, R2)
   ! A and B and endponts that define a line represeting a wire
   ! and calulates the distance r from this line of any point P
   ! AP  = P-A; AB = B-A
   ! the points P are the node locations stored in VCL
   ! VCL(:,3) : 1-3 : List of node coordinates
   ! note need to convert VCL coords in microns to meters
   ! distance is the minimum distance from P to the line AB
   ! direction is the unit vector in the direction of line P to AB
   ! I_vect is the unit vector of current, i.e. in the direction of line AB
   ! r0 = thickness of wire
   ! mu_r relative per
   subroutine SETNonUniformField(A, B, current, mu_r, r0)
      implicit none
      integer:: i
      real(KIND=DP), intent(IN):: A(3), B(3)

      real(KIND=DP):: AB(3), Q(3), AP_cross_AB(3), AP(3), P(3)
      real(KIND=DP):: I_vect(3), direction(3)
      real(KIND=DP):: distance, r0, mu_r, AB_norm, current, AP_BP_norm
      real(KIND=DP):: magnitude_sq, AB_DOT_AB, AP_CROSS_AB_norm

      if (allocated(B_vect)) deallocate (B_vect)
      allocate (B_vect(NNODE, 3))
      write (*, *) "Bvect allocated with node size = ", NNODE

      do i = 1, NNODE

         P = VCL(i, 1:3)*1.e-6

         ! Vector from A to P
         AP = P - A

         ! Vector from A to B
         AB = B - A
         AB_norm = sqrt(DOT(AB, AB))

         AP_CROSS_AB = CROSS(AP, AB)

         AP_CROSS_AB_norm = sqrt(DOT(AP_CROSS_AB, AP_CROSS_AB))
         distance = AP_CROSS_AB_norm/AB_norm

         AP_BP_norm = DOT(AP, AB)/(AB_norm**2)

         ! Q are the coords of the nearest point to P on the line along A B
         Q = A + AB*AP_BP_norm

         ! U is the unit vector along the shortest distance i.e. (Q-P)

         I_vect = AB/AB_norm

         direction = 0.
         if (distance .ne. 0.0) direction = (P - Q)/distance

         ! calulate the Biot-Savart field
         ! 2E-7 = mu_o/2*pi

         if (distance .gt. r0) then
            B_vect(i, :) = 2.0e-7*current*CROSS(I_vect, direction)/distance

         end if

         if ((distance .le. r0) .and. (distance .gt. 0.0)) then
            B_vect(i, :) = 2.0e-7*current*mu_r*CROSS(I_vect, direction)*distance/(r0**2)
         end if

         if (distance .eq. 0.0) then
            B_vect(i, :) = 0.0
         end if
      end do

      write (*, *) 'Max Bx (in Tesla) from currrent =', maxval(B_vect(:, 1))
      write (*, *) 'Max By (in Tesla) from currrent =', maxval(B_vect(:, 2))
      write (*, *) 'Max Bz (in Tesla) from currrent =', maxval(B_vect(:, 3))

   contains
      function CROSS(a, b)
         real(KIND=DP):: CROSS(3)
         real(KIND=DP), intent(IN):: a(3), b(3)

         CROSS(1) = a(2)*b(3) - a(3)*b(2)
         CROSS(2) = a(3)*b(1) - a(1)*b(3)
         CROSS(3) = a(1)*b(2) - a(2)*b(1)
         return
      end function CROSS

      function DOT(u, v)
         real(KIND=DP):: DOT
         real(KIND=DP), intent(IN):: u(3), v(3)

         DOT = sum(u*v)
         return
      end function DOT

   end subroutine SETNonUniformField

   !
   ! VortexState(p, [p0], [v], [orientation])
   !   Seed a rough vortex state with core oriented along the line
   !   from point p0 to p, with component v in the direction of the core.
   !   Orientation can be specified using
   !     orientation = 'RH'
   !   or
   !     orientation = 'LH'
   !   Default orientation is RH.
   !
   subroutine VortexMag(p, p0, v, orientation)
      use Utils, only: NONZERO
      implicit none

      real(KIND=DP), intent(IN):: p(3)
      real(KIND=DP), optional, intent(IN):: p0(3), v
      character(len=*), optional, intent(IN):: orientation

      integer:: orientation_v
      integer, parameter:: RH = 1, LH = -1

      real(KIND=DP):: p0_v(3), v_v

      real(KIND=DP):: dirn(3)
      integer:: i

      ! Set p0
      if (present(p0)) then
         p0_v = p0
      else
         p0_v = 0
      end if

      ! Set v
      if (present(v)) then
         v_v = v
      else
         v_v = 0
      end if

      ! Set orientation
      if (present(orientation)) then
         select case (orientation)
         case ('RH')
            orientation_v = RH
         case ('LH')
            orientation_v = LH
         case DEFAULT
            write (*, *) "ERROR: VortexState: Unknown orientation ", trim(orientation)
            ERROR stop
         end select
      else
         orientation_v = RH
      end if

      dirn = p - p0

      do i = 1, NNODE
         m(i, :) = orientation_v*CROSS(dirn, VCL(i, 1:3) - p0_v) + v_v*dirn

         block
            real(KIND=DP):: normm

            normm = norm2(m(i, :))
            if (NONZERO(normm)) then
               m(i, :) = m(i, :)/normm
            end if
         end block
      end do

   contains
      function CROSS(a, b)
         real(KIND=DP):: CROSS(3)
         real(KIND=DP), intent(IN):: a(3), b(3)

         CROSS(1) = a(2)*b(3) - a(3)*b(2)
         CROSS(2) = a(3)*b(1) - a(1)*b(3)
         CROSS(3) = a(1)*b(2) - a(2)*b(1)
      end function CROSS
   end subroutine VortexMag

end module Tetrahedral_Mesh_Data
