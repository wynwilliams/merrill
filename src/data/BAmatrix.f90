! core calculation filling elements of the dense matrix
!
! formerly in subroutine FiniteElementMatrices::BoundmatA
module BAmatrix
contains

   logical function samepoint(p1, p2)
      use Utils, only: DP
      implicit none
      real(kind=dp), dimension(3), intent(in)::p1, p2
      real(kind=dp):: dist_sq
      real(kind=dp):: cutoff_sq = 0.000000001 ! TODO
      dist_sq = (p1(1) - p2(1))**2 + (p1(2) - p2(2))**2 + (p1(3) - p2(3))**2
      samepoint = (dist_sq < cutoff_sq)
      if (samepoint) then
!    write(*,*) p1, p2
      end if
   end function

   subroutine Lindholm(x0, p1, p2, p3, tempval) bind(c, name="c_Lindholm")
      use iso_c_binding, only: c_double
      use Utils, only: DP, pi, nonzero
      use Tetrahedral_Mesh_Data, only: GET_ANGLE_COORDS, solid, calc_b, calc_c, calc_d, calc_signed_volume
      implicit none
      real(KIND=c_double) rx, ry, rz &
           & , area, normb, normc, normd, normval, SAngle &
           & , neta1, neta2, neta3, zeta, xp, xq, xr &
           & , yp, yq, yr, zp, zq, zr, s1, s2, s3, rho1, rho2, rho3, gam11 &
           & , gam12, gam13, gam22, gam21, gam23, gam33, gam31, gam32, q1, q2, q3, &
           & b, c, d, signvolume
      real(kind=c_double), dimension(3), intent(in) :: x0, p1, p2, p3
      real(kind=c_double), dimension(3), intent(out) :: tempval
      integer i, j, el, opp   &
          & , k, l, dum2, indexp1, indexp2, indexp3, cn, dum3, p, q, r, n(3), rl  &
          & , bk

      rx = x0(1)
      ry = x0(2)
      rz = x0(3)

      xp = p1(1)
      xq = p2(1)
      xr = p3(1)
      yp = p1(2)
      yq = p2(2)
      yr = p3(2)
      zp = p1(3)
      zq = p2(3)
      zr = p3(3)

      ! get the solid anglemage by the element surface dum3 at the
      ! obervation point
      ! Lindholm's variable S_t

      SAngle = get_angle_coords(x0, p1, p2, p3)
      if (samepoint(x0, p1) .or. samepoint(x0, p2) .or. samepoint(x0, p3)) then
         if (NONZERO(SAngle)) then
            print *, 'Solid = ', SAngle
         end if
         tempval = (/0.0, 0.0, 0.0/)
      else

         ! need basis function b,c,d from values, not indices

         ! -- the INWARD normal to surface face of a boundary element
         !   (ie Lindholms variable Zeta)
         signvolume = sign(1.0_dp, calc_signed_volume(x0, p1, p2, p3))
         b = calc_b(p1, p2, p3, signvolume)
         c = calc_c(p1, p2, p3, signvolume)
         d = calc_d(p1, p2, p3, signvolume)

         normval = sqrt(b**2 + c**2 + d**2)
         normb = (b/normval)
         normc = (c/normval)
         normd = (d/normval)

         area = 0.5*normval

         s1 = sqrt((xq - xp)**2 + (yq - yp)**2 + (zq - zp)**2)
         s2 = sqrt((xr - xq)**2 + (yr - yq)**2 + (zr - zq)**2)
         s3 = sqrt((xp - xr)**2 + (yp - yr)**2 + (zp - zr)**2)

         rho1 = sqrt((xp - rx)**2 + (yp - ry)**2 + (zp - rz)**2)
         rho2 = sqrt((xq - rx)**2 + (yq - ry)**2 + (zq - rz)**2)
         rho3 = sqrt((xr - rx)**2 + (yr - ry)**2 + (zr - rz)**2)

         zeta = ((normb)*(xp - rx) + (normc)*(yp - ry) + normd*(zp - rz))

         gam11 = ((xr - xq)*(xq - xp) + (yr - yq)*(yq - yp) + (zr - zq)*(zq - zp))/(s2*s1)
         gam21 = ((xp - xr)*(xq - xp) + (yp - yr)*(yq - yp) + (zp - zr)*(zq - zp))/(s3*s1)
         gam31 = 1.0d0
         gam12 = 1.0d0
         gam22 = ((xp - xr)*(xr - xq) + (yp - yr)*(yr - yq) + (zp - zr)*(zr - zq))/(s3*s2)
         gam32 = gam11
         gam13 = gam22
         gam23 = 1.0d0
         gam33 = gam21

         neta1 = ((normc*(zq - zp) - normd*(yq - yp))*(xp - rx) + (normd*(xq - xp) - normb &
                                                       &   *(zq - zp))*(yp - ry) + (normb*(yq - yp) - normc*(xq - xp))*(zp - rz))/s1

         neta2 = ((normc*(zr - zq) - normd*(yr - yq))*(xq - rx) + (normd*(xr - xq) - normb &
                                                       &   *(zr - zq))*(yq - ry) + (normb*(yr - yq) - normc*(xr - xq))*(zq - rz))/s2

         neta3 = ((normc*(zp - zr) - normd*(yp - yr))*(xr - rx) + (normd*(xp - xr) - normb &
                                                       &   *(zp - zr))*(yr - ry) + (normb*(yp - yr) - normc*(xp - xr))*(zr - rz))/s3

         !       IF ((p1 /= k).AND.(p2 /= k).AND.(p3 /= k)) THEN
                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         q1 = log((rho1 + rho2 + s1)/(rho1 + rho2 - s1))
         q2 = log((rho2 + rho3 + s2)/(rho2 + rho3 - s2))
         q3 = log((rho3 + rho1 + s3)/(rho3 + rho1 - s3))

         dum2 = int(DSIGN(1.0d0, zeta))

         tempval(1) = &
            s2*(dum2*neta2*SAngle - zeta*(gam11*q1 + gam12*q2 + gam13*q3)) &
            /(8*pi*area)

         tempval(2) = &
            s3*(dum2*neta3*SAngle - zeta*(gam21*q1 + gam22*q2 + gam23*q3)) &
            /(8*pi*area)

         tempval(3) = &
            s1*(dum2*neta1*SAngle - zeta*(gam31*q1 + gam32*q2 + gam33*q3)) &
            /(8*pi*area)
      end if

      if ((isnan(tempval(1))) .or. (isnan(tempval(2))) .or. (isnan(tempval(3)))) then
         write (*, *) "nan", tempval
         !else
         !    write (*,*) "notnan" tempval
      end if

   end subroutine Lindholm

end module BAmatrix
