!> Convenience module which loads and initializes all the modules used in
!> a typical MERRILL program.
module InitMerrill
   implicit none

contains

   subroutine InitializeMerrill()
      use Utils, only: InitializeUtils, MachEps, tstamp
      use Material_Parameters, only: InitializeMaterialParameters
      use Tetrahedral_Mesh_Data, only: InitializeTetrahedralMeshData
      use Finite_Element_Matrices, only: InitializeFiniteElement
      use Magnetization_Path, only: InitializeMagnetizationPath
      use Hubert_Minimizer, only: InitializeHubertMinimizer
      use Energy_Calculator, only: InitializeEnergyCalculator
      use LLG_Solver, only: InitializeLLG_Solver

      call InitializeUtils()
      call InitializeTetrahedralMeshData()
      call InitializeFiniteElement()
      call InitializeMaterialParameters()
      call InitializeMagnetizationPath()
      call InitializeHubertMinimizer()
      call InitializeEnergyCalculator()
      call InitializeLLG_Solver()
      write (*, *) 'Machine epsilon =', MachEps
      write (*, *) 'MERRILL started on:'
      call tstamp()
   end subroutine InitializeMerrill

   subroutine DestroyMerrill()
      use Utils, only: tstamp, RunTime
      use Material_Parameters, only: DestroyMaterialParameters
      use Tetrahedral_Mesh_Data, only: DestroyTetrahedralMeshData
      use Finite_Element_Matrices, only: DestroyFiniteElement
      use Magnetization_Path, only: DestroyMagnetizationPath
      !use Energy_Calculator, only: DestroyEnergyCalculator
      use LLG_Solver, only: DestroyLLG_Solver

      !call DestroyEnergyCalculator()
      call DestroyMagnetizationPath()
      call DestroyMaterialParameters()
      call DestroyFiniteElement()
      call DestroyTetrahedralMeshData()
      call DestroyLLG_Solver()
      write (*, *) 'MERRILL completed on:'
      call tstamp()
      call RunTime()
   end subroutine DestroyMerrill

end module InitMerrill
