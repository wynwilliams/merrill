!> The Command_Parser module.
module Command_Parser
   use Utils, only: dp
   use iso_c_binding, only: c_null_char
   implicit none

   !> A type representing a MScript function parser, which should parse
   !> a MScript function and execute the equivalent Fortran functions.
   type:: CommandParser
      !> The name of the command.
      character(len = 200):: name

      !> The parser for the command.
      procedure(CommandParserInterface), pointer, nopass:: parser => null()
   end type CommandParser

   ! Values representing the success or failure of a CommandParser parser.

   !> The return value for a successful parse.
   integer, parameter:: PARSE_SUCCESS = 1

   !> The return value for when no parsing is done.
   integer, parameter:: NO_PARSE = 0

   !> The return value for when an error occurred during parsing.
   integer, parameter:: PARSE_ERROR = -1

   interface
      !> The interface for a `parser` referenced by a CommandParser.
      subroutine CommandParserInterface(args, ierr)
         !> The interface for a `parser` referenced by a CommandParser.
         !> @param[in]  args The arguments to parse
         !> @param[out] ierr The result of the parse
         implicit none

         character(len=*), intent(IN):: args(:)
         integer, intent(OUT):: ierr
      end subroutine CommandParserInterface
   end interface

   !> A list of the available command parsers.
   type(CommandParser), allocatable:: command_parsers(:)

contains

   !> Initialize command_parsers array with the default commands
   subroutine InitializeCommandParser()
      implicit none

      call DestroyCommandParser()
      allocate (command_parsers(0))

      call AddCommandParser("print", ParsePrint)
      call AddCommandParser("systemcommand", ParseSystemCommand)
      call AddCommandParser("keypause", ParseKeyPause)
      call AddCommandParser("reportenergy", ParseReportEnergy)
      call AddCommandParser("nowarnings", ParseNoWarnings)
      call AddCommandParser("timestamp", ParseTimeStamp)

      call AddCommandParser("cubic", ParseAnisotropy)
      call AddCommandParser("monoclinic", ParseAnisotropy)
      call AddCommandParser("monoclinicPyrrhotite", ParseAnisotropy)
      call AddCommandParser("HexPyrrhotite", ParseAnisotropy)
      call AddCommandParser("uniaxial", ParseAnisotropy)
      call AddCommandParser("uni", ParseAnisotropy)
      call AddCommandParser("cubicrotation", ParseCubicRotation)
      call AddCommandParser("cubicaxes", ParseCubicAxes)
      call AddCommandParser("easyaxis", ParseEasyAxis)
      call AddCommandParser("stressaxis", ParseStressAxis)
      call AddCommandParser("stressfibonacciaxis", ParseStressFibonacciAxis)

      call AddCommandParser("readcubicrotation", ParseReadCubicRotation)

      call AddCommandParser("magnetitelegacy", ParseMagnetiteLegacy)
      call AddCommandParser("iron", ParseIron)
      call AddCommandParser("TM54", ParseTM54)
      call AddCommandParser("TM", ParseTM)
      call AddCommandParser("Magnetite", ParseMagnetite)

      call AddCommandParser("HystLoop", ParseHystLoop)
      call AddCommandParser("IRM", ParseIRM)
      call AddCommandParser("DCD", ParseDCD)
      call AddCommandParser("IRMDCD", ParseIRMDCD)
      call AddCommandParser("SimpleFORC", ParseSimpleFORC)
      call AddCommandParser("SimpleFORCRestart", ParseSimpleFORCRestart)

      call AddCommandParser("external", ParseSetField)
      call AddCommandParser("magnetic", ParseSetField)
      call AddCommandParser("NonUniformField", ParseNonUniformField)

      call AddCommandParser("readmesh", ParseReadMesh)
      call AddCommandParser("loadmesh", ParseLoadMesh)
      call AddCommandParser("savemagstate", ParseSaveMagState)
      call AddCommandParser("loadmagstate", ParseLoadMagState)
      call AddCommandParser("savemesh", ParseSaveMesh)
      call AddCommandParser("remesh", ParseRemesh)
      call AddCommandParser("remeshSD", ParseRemeshsd)
      call AddCommandParser("replaceSD", ParseReplaceSD)
      call AddCommandParser("RotateMag", ParseRotateMag)
      call AddCommandParser("InvertMag", ParseInvertMag)

      ! H2Lib compressed matrix BA(BNODE x BNODE)
      ! Internal object save/load TODO
      ! call AddCommandParser("SaveBEMMatrix", ParsSaveBEMMatrix)
      ! call AddCommandParser("LoadBEMMatrix", PareLoadBEMMatrix)

      call AddCommandParser("readmag", ParseReadMag)
      call AddCommandParser("readmagnetization", ParseReadMag)
      call AddCommandParser("ReadQDM", ParseReadQDM)
      call AddCommandParser("writemag", ParseWriteMag)
      call AddCommandParser("writedemag", ParseWriteDemag)
      call AddCommandParser("appenddemagzone", ParseAppendDemagZone)
      call AddCommandParser("writemagnetization", ParseWriteMag)
      call AddCommandParser("writehyst", ParseWriteHyst)
      call AddCommandParser("writestresshyst", ParseWriteStressHyst)
      call AddCommandParser("writeloopdata", ParseWriteLoopData)
      call AddCommandParser("writeboxdata", ParseWriteBoxData)
      call AddCommandParser("energylog", ParseEnergyLog)
      call AddCommandParser("logfile", ParseEnergyLog)
      call AddCommandParser("pathlogfile", ParsePathLogFile)
      call AddCommandParser("endlog", ParseCloseLogFile)
      call AddCommandParser("closelog", ParseCloseLogFile)
      call AddCommandParser("closelogfile", ParseCloseLogFile)

      call AddCommandParser("conjugategradient", ParseConjugateGradient)
      call AddCommandParser("steepestdescent", ParseSteepestDescent)
      call AddCommandParser("minimize", ParseEnergyMin)  ! defaults to call energyminmult
      call AddCommandParser("LLG", ParseLLGODE)  ! defaults to call LLG_ODE_solver
      call AddCommandParser("minimizeCart", ParseEnergyMinCart)
      call AddCommandParser("minimizePolX", ParseEnergyMinPolX)
      call AddCommandParser("minimizePolY", ParseEnergyMinPolY)
      call AddCommandParser("minimizePolZ", ParseEnergyMinPolZ)
      call AddCommandParser("minimizeMult", ParseEnergyMinMult)
      call AddCommandParser("pathminimize", ParsePathMinimize)

      call AddCommandParser("resize", ParseResize)
      call AddCommandParser("gradienttest", ParseGradientTest)

      call AddCommandParser("uniform", ParseUniformMagnetization)
      call AddCommandParser("randomize", ParseRandomizeMagnetization)
      call AddCommandParser("invert", ParseInvertMagnetization)
      call AddCommandParser("FixBlockSD", ParseFixBlockSD)
      call AddCommandParser("RenumberBlockSD", ParseRenumberBlockSD)
      call AddCommandParser("vortex", ParseVortexMagnetization)
      call AddCommandParser("RandomSaturation", ParseRandomSaturation)

      call AddCommandParser("magtopath", ParseMagnetizationToPath)
      call AddCommandParser("magnetizationtopath", ParseMagnetizationToPath)
      call AddCommandParser("pathtomag", ParsePathToMagnetization)
      call AddCommandParser("pathtomagnetization", ParsePathToMagnetization)
      call AddCommandParser("pathrenew", ParsePathRenewDist)
      call AddCommandParser("renewpath", ParsePathRenewDist)
      call AddCommandParser("makepath", ParsePathRenewDist)
      call AddCommandParser("makeinitialpath", ParseMakeInitialPath)
      call AddCommandParser("makeinitialpathCart", ParseMakeInitialPathCart)
      call AddCommandParser("refinepathto", ParseRefinePathTo)
      call AddCommandParser("writetecplotpath", ParseWriteTecplotPath)
      call AddCommandParser("readtecplotpath", ParseReadTecplotPath)
      call AddCommandParser("readtecplotzone", ParseReadTecplotZone)
      call AddCommandParser("appendtecplotzone", ParseAppendTecplotZone)
      call AddCommandParser("ZoneName", ParseZoneName)
      call AddCommandParser("pathstructureenergies", ParsePathStructureEnergies)
      call AddCommandParser("pathstructureenergycomponents", ParsePathStructureEnergyComponents)
      call AddCommandParser("pathstructureallcomponents", ParsePathStructureAllComponents)
      call AddCommandParser("writepathstructures", ParseWritePathStructures)
      call AddCommandParser("energy", ParseEnergy)
      call AddCommandParser("sdenergysurface", ParseSDEnergySurface)

      call AddCommandParser("GenerateCubeMesh", ParseGenerateCubeMesh)
      call AddCommandParser("GenerateSphereMesh", ParseGenerateSphereMesh)

      call AddCommandParser("loadplugin", ParseLoadPlugin)
   end subroutine InitializeCommandParser

   !> Destroy the command_parsers array
   subroutine DestroyCommandParser()
      if (allocated(command_parsers)) deallocate (command_parsers)
   end subroutine DestroyCommandParser

   !>
   !> Add a command parser 'parser' with the name 'command_name' to
   !> the command_parsers array.
   !>
   subroutine AddCommandParser(command_name, parser)
      use strings, only: lowercase
      implicit none

      character(len=*), intent(IN):: command_name
      procedure(CommandParserInterface):: parser

      type(CommandParser):: command_parser

      type(CommandParser), allocatable:: tmp(:)
      integer:: n_commands

      ! make a command parser object containing the name and procedure
      command_parser%name = trim(lowercase(command_name))
      command_parser%parser => parser

      ! add it to the module's command_parsers array
      allocate (tmp(size(command_parsers) + 1))

      n_commands = size(command_parsers)
      if (n_commands .gt. 0) tmp(1:n_commands) = command_parsers
      call move_alloc(tmp, command_parsers)
      n_commands = n_commands+1

      command_parsers(n_commands) = command_parser
   end subroutine AddCommandParser

   !>
   !> Run the parser in command_parsers with the name command_name
   !>
   subroutine ParseCommand(command_name, args, ierr)
      implicit none

      character(len=*), intent(IN):: command_name
      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      type(CommandParser) command_parser
      integer:: i

      ierr = NO_PARSE

      ! search the module's list of command parsers for the one with the name
      do i = 1, size(command_parsers)
         if (trim(command_parsers(i)%name) .eq. trim(command_name)) then
            command_parser = command_parsers(i)
            ! run the correct function
            call command_parser%parser(args, ierr)
            exit
         end if
      end do
   end subroutine ParseCommand

   !> Assert the array \c args has \c nargs values.
   !> If not, print an error message.
   !>
   !> @param[in] args  The array to check
   !> @param[in] nargs The number of arguments to check for
   !> @returns .TRUE. if `array` has `nargs` elements.
   function AssertNArgs(args, nargs)
      character(len=*), intent(IN):: args(:)
      integer, intent(IN):: nargs
      logical:: AssertNArgs
      integer:: i

      if (size(args) .eq. nargs) then
         AssertNArgs = .true.
      else
         write (*, '(A, A, A, I4, A, A, I4, A)') &
            "Command '", trim(args(1)), "' expects ", nargs, " arguments.", &
            " Got ", size(args), " arguments."
         write (*, *) "ARGS: "
         do i = 1, size(args)
            write (*, '(I4, A, A)') i, ": ", trim(args(i))
         end do
         AssertNArgs = .false.
      end if
   end function AssertNArgs

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! CommandParser parsers                                                      !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !> Parse the `NoWarnings` command
   !>
   !> @param[in]  args The arguments to the `MakeInitialPath` command.
   !> @param[out] ierr The result of the parse
   !> @todo this forces Cartesian coords in EnergyMin
   subroutine ParseNowarnings(args, ierr)
      use Material_Parameters, only: warnflag
      implicit none

      character(len=*), intent(IN):: args(:)

      integer, intent(OUT):: ierr

      ! defines the Path variables assuming all magnetizations are filled
      !warnflag = 0 = off-i.e. warning messages supressed
      warnflag = 0

      ierr = PARSE_SUCCESS
   end subroutine ParseNoWarnings

   !> Parse the `Print` command
   !>
   !> @param[in]  args The arguments to the `Print` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParsePrint(args, ierr)
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs

      character(len = 200):: line
      integer:: i

      nargs = size(args)

      line = ''
      do i = 2, nargs
         line = line(:len_trim(line))//' '//args(i) (:len_trim(args(i)))
      end do
      write (*, *) line(:len_trim(line))

      ierr = PARSE_SUCCESS
   end subroutine ParsePrint

   !> Parse the `SystemCommand` command
   !>
   !> @param[in]  args The arguments to the `SystemCommand` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseSystemCommand(args, ierr)
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs
      character(len = 200):: sysline
      integer:: sys

      integer:: i

      nargs = size(args)

      if (nargs > 2) then
         sysline = ''
         do i = 2, nargs
            sysline = sysline(:len_trim(sysline))//' '//args(i) (:len_trim(args(i)))
         end do

         call EXECUTE_COMMAND_LINE(sysline, exitstat = sys)

         if (sys .eq. 0) then
            write (*, *) '          `-> OK'
            ierr = PARSE_SUCCESS
         else
            write (*, '(A20, I3)') '          `-> Error:', sys
            ierr = PARSE_ERROR
         end if
      else
         write (*, *) "Error in ", trim(args(1)), ": No command given."
         ierr = PARSE_ERROR
      end if
   end subroutine ParseSystemCommand

   !> Parse the `TimeStamp` command
   !>
   !> @todo DOCUMENT ME
   subroutine ParseTimeStamp(args, ierr)
      use Utils, only: tstamp, RunTime
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! Get a time stamp then the current runtime
      call tstamp()
      call runtime()

      ierr = PARSE_SUCCESS

   end subroutine ParseTimeStamp

   !> Parse the `Anisotropy` command
   !> MScript example:
   !> @code
   !>    Uniaxial Anisotropy
   !>    Cubic Anisotropy
   !>    Uniaxial Anisotropy sd = n
   !> @endcode
   !>
   !> @param[in]  args The arguments to the `Anisotropy` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseAnisotropy(args, ierr)
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: sd

      if (size(args) .eq. 5) then
         sd = ParseSubdomain(args(3:5), ierr)
         call setanis(args(1), ierr, sd = sd)
      else
         call setanis(args(1), ierr)
      end if

   end subroutine ParseAnisotropy

   !> Parse the `ReadCubicRotation` command
   !> MScript Example:
   !> @code
   !>
   !>    CubicRotation filename
   !> @endcode
   !> filename is the name of the file which contains:
   !> alpha is angle of rotation around x-axis
   !> theta is angle of rotation around y-axis
   !> phi is angle of rotation around z-axis
   !> sd = n is the subdomain number to which the rotation is applied.
   !>
   !> @param[in]  args The arguments to the `ReadCubicRotation` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseReadCubicRotation(args, ierr)
      use Material_Parameters, only: SetRotationMatrix
      use Tetrahedral_Mesh_Data, only: SubDomainIds
      implicit none

      character(len=*), intent(IN):: args(:)
      character(len = 1024):: token
      integer, intent(OUT):: ierr

      real(KIND = DP):: phirot, thetarot, alpharot
      real(KIND = DP):: dbltmp

      integer:: sd, i
      integer:: ios
      integer:: rotateUNIT

      logical:: l

      ierr = PARSE_ERROR

      open ( &
         NEWUNIT = rotateUNIT, FILE = args(2), &
         STATUS='OLD', ACTION='READ', IOSTAT = ierr &
         )
      if (ierr .ne. 0) then
         write (*, *) "Error opening file: ", trim(args(2))
         stop
      end if

      ! skip first line of header
      read (rotateUNIT, *)

      do
         read (rotateUNIT, *, iostat = ierr, iomsg = token) SD, alpharot, thetarot, phirot
         if (ierr .ne. 0) then
            if (IS_IOSTAT_END(ierr)) then
               ierr = PARSE_SUCCESS
               exit
            else
               write (*, *) 'Some other error', token
               ERROR stop
            end if
         end if
         write (*, *) 'Setting Rotation in subdomain', SD, ' to ', alpharot, thetarot, phirot

         if (sd .gt. size(SubDomainIds)) then
            write (*, *) 'Requested SD', SD, ' Is outside maximum allowed SD of', size(SubDomainIds)
            ierr = PARSE_ERROR
            exit
         else

            call SetRotationMatrix(phirot, thetarot, alpharot, sd = sd)
            ierr = PARSE_SUCCESS
         end if

      end do

      if (SD .lt. size(SubDomainIds)) then
         ierr = PARSE_SUCCESS
         write (*, *) 'WARNING: Only', SD, 'out of', size(SubDomainIds), ' subdomain rotations have been set'
      end if

      close (rotateUNIT)

   end subroutine ParseReadCubicRotation

   !> Parse the `CubicRotation` command
   !> MScript Example:
   !> @code
   !>    CubicRotation alpah theta phi
   !>    CubicRotation phi theta alpha sd = n
   !> @endcode
   !>
   !> alpha is angle of rotation around x-axis
   !> theta is angle of rotation around y-axis
   !> phi is angle of rotation around z-axis
   !>
   !> @param[in]  args The arguments to the `CubicRotation` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseCubicRotation(args, ierr)
      use strings, only: value
      use Material_Parameters, only: SetRotationMatrix
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: phirot, thetarot, alpharot
      real(KIND = DP):: dbltmp

      integer:: sd
      integer:: ios

      logical:: l

      ierr = PARSE_ERROR

      ! define phi, theta, alpha of cubic axes rotation matrix
!    print*,'args = ', SIZE(args)
      if (size(args) .ge. 4) then
         call value(args(2), dbltmp, ios)
         if (ios .eq. 0) then
            alpharot = dbltmp
         else
            write (*, *) "Error parsing value ", trim(args(2))
            ierr = PARSE_ERROR
            return
         end if

         call value(args(3), dbltmp, ios)
         if (ios .eq. 0) then
            thetarot = dbltmp
         else
            write (*, *) "Error parsing value ", trim(args(3))
            ierr = PARSE_ERROR
            return
         end if

         call value(args(4), dbltmp, ios)
         if (ios .eq. 0) then
            phirot = dbltmp
         else
            write (*, *) "Error parsing value ", trim(args(4))
            ierr = PARSE_ERROR
            return
         end if

         if (size(args) .eq. 7) then
            sd = ParseSubdomain(args(5:7), ierr)
            if (ierr .ne. PARSE_ERROR) then
               call SetRotationMatrix(phirot, thetarot, alpharot, sd = sd)
               ierr = PARSE_SUCCESS
            end if
         else if (AssertNArgs(args, 4)) then
            call SetRotationMatrix(phirot, thetarot, alpharot)
            ierr = PARSE_SUCCESS
         else
            ierr = PARSE_ERROR
         end if

      else
         l = AssertNArgs(args, 4)
         ierr = PARSE_ERROR
      end if
   end subroutine ParseCubicRotation

   !> Parse the `ConjugateGradient` command
   !>
   !> @param[in]  args The argumenn_commandsts to the `ConjugateGradient` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseConjugateGradient(args, ierr)
      use Hubert_Minimizer, only: ConGradQ
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ConGradQ = .true.

      ierr = PARSE_SUCCESS
   end subroutine ParseConjugateGradient

   !> Parse the `SteepestDescent` command
   !>
   !> @param[in]  args The arguments to the `SteepestDescent` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseSteepestDescent(args, ierr)
      use Hubert_Minimizer, only: ConGradQ
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ConGradQ = .false.

      ierr = PARSE_SUCCESS
   end subroutine ParseSteepestDescent

   !> Parse the `CubicAxes` command
   !> Users should take care axes are mutually orthogonal!
   !>
   !> MScript example:
   !> CubicAxes x1 y1 z1 x2 y2 z2 x3 y3 z3
   !> CubicAxes x1 y1 z1 x2 y2 z2 x3 y3 z3 sd = n
   !>
   !> @param[in]  args The arguments to the `CubicAxes` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseCubicAxes(args, ierr)
      use Material_Parameters, only: NMaterials, CubicAxes
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! subdomains to set
      integer:: sd
      integer:: i

      ! default all subdomains
      ierr = PARSE_ERROR

      ! Look for sd = #subdomain
      if (size(args) .eq. 13) then
         sd = ParseSubdomain(args(11:13), ierr)

         if (ierr .ne. PARSE_ERROR) then
            call RunParse(args(2:10), sd, sd, ierr)
         end if
      else if (AssertNArgs(args, 10)) then
         call RunParse(args(2:10), 1, NMaterials, ierr)
      else
         ierr = PARSE_ERROR
      end if

      write (*, *) "CubicAxes:"
      block
         integer:: sd, i
         do sd = 1, NMaterials
            write (*, *) "SD: ", sd
            do i = 1, 3
               write (*, *) CubicAxes(:, i, sd)
            end do
         end do
      end block

   contains
      subroutine RunParse(args, sd_min, sd_max, ierr)
         use utils, only: NONZERO
         use strings, only: value
         use Material_Parameters, only: CubicAxes
         implicit none

         character(LEN=*), intent(IN):: args(9)
         integer, intent(IN):: sd_min, sd_max
         integer, intent(OUT):: ierr

         ! temporary flattened axes
         real(KIND = DP):: axes(9)

         real(KIND = DP):: norm
         integer:: i, j

         ! Convert args to axes
         do i = 1, 9
            call value(args(i), axes(i), ierr)
            if (ierr .ne. 0) then
               write (*, *) "Error parsing floating value: ", trim(args(i))
               ierr = PARSE_ERROR
               return
            end if
         end do

         ! Set CubicAxes
         do i = sd_min, sd_max
            CubicAxes(:, :, i) = reshape(axes, (/3, 3/))
         end do

         ! Renormalize
         do i = sd_min, sd_max
            do j = 1, 3
               norm = norm2(CubicAxes(:, j, i))
               if (NONZERO(norm)) then
                  CubicAxes(:, j, i) = CubicAxes(:, j, i)/norm
               end if
            end do
         end do

         ierr = PARSE_SUCCESS
      end subroutine RunParse
   end subroutine ParseCubicAxes

   !> Parse the `EasyAxis` command
   !>
   !> MScript Example:
   !> @code
   !>    EasyAxis x y z
   !>    EasyAxis x y z sd = n
   !> @endcode
   !>
   !> @param[in]  args The arguments to the `EasyAxis` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEasyAxis(args, ierr)
      use strings, only: lowercase, value
      use Tetrahedral_Mesh_Data, only: SubDomainIds
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: sd
      integer:: i

      ! Look for sd = #subdomain
      if (size(args) .eq. 7) then
         ! Ensure format ... sd = n
         if ( &
            lowercase(trim(args(5))) .eq. "sd" &
            .and. &
            trim(args(6)) .eq. "=" &
            ) then

            ! Set sd
            call value(args(7), sd, ierr)
            if (ierr .ne. 0) then
               write (*, *) "Error parsing subdomain id: ", trim(args(7))
               ierr = PARSE_ERROR
               return
            end if

            ! Change subdomain to subdomain index
            do i = 1, size(SubDomainIds)
               if (sd .eq. SubDomainIds(i)) then
                  sd = i
                  exit
               end if

               if (i .eq. size(SubDomainIds)) then
                  write (*, *) "Error: Unknown subdomain: ", sd
                  ierr = PARSE_ERROR
                  return
               end if
            end do

            call seteasyaxis(args(2), args(3), args(4), ierr, sd = sd)
         end if
      else if (AssertNArgs(args, 4)) then
         call seteasyaxis(args(2), args(3), args(4), ierr)
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseEasyAxis

   !> Parse the `StressAxis` command
   !>
   !> MScript Example:
   !> @code
   !>    StressAxis x y z
   !>    StressAxis x y z sd = n
   !> @endcode
   !>
   !> @param[in]  args The arguments to the `StressAxis` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseStressAxis(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: SubDomainIds
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: sd
      integer:: i

      ! Look for sd = #subdomain
      if (size(args) .eq. 7) then
         ! Ensure format ... sd = n
         if ( &
            lowercase(trim(args(5))) .eq. "sd" &
            .and. &
            trim(args(6)) .eq. "=" &
            ) then

            ! Set sd
            call value(args(7), sd, ierr)
            if (ierr .ne. 0) then
               write (*, *) "Error parsing subdomain id: ", trim(args(7))
               ierr = PARSE_ERROR
               return
            end if

            ! Change subdomain to subdomain index
            do i = 1, size(SubDomainIds)
               if (sd .eq. SubDomainIds(i)) then
                  sd = i
                  exit
               end if

               if (i .eq. size(SubDomainIds)) then
                  write (*, *) "Error: Unknown subdomain: ", sd
                  ierr = PARSE_ERROR
                  return
               end if
            end do

            call setstressaxis(args(2), args(3), args(4), ierr, sd = sd)
         end if
      else if (AssertNArgs(args, 4)) then
         call setstressaxis(args(2), args(3), args(4), ierr)
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParsestressAxis

   !> Parse the `StressFibonacciAxis` command
   !>
   !> MScript Example:
   !> @code
   !>    StressFibonacciAxis i_th of N directions from a Fibonacci sphere
   !>    StressAxis i N sd = n
   !> @endcode
   !>
   !> @param[in]  args The arguments to the `StressFibonacciAxis` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   SUBROUTINE ParseStressFibonacciAxis(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: SubDomainIds
      implicit none

      CHARACTER(len=*), INTENT(IN):: args(:)
      INTEGER, INTENT(OUT):: ierr

      INTEGER:: sd
      INTEGER:: i

      ! Look for sd = #subdomain
      IF (SIZE(args) .EQ. 6) THEN
         ! Ensure format ... sd = n
         IF ( &
            lowercase(TRIM(args(4))) .EQ. "sd" &
            .AND. &
            TRIM(args(5)) .EQ. "=" &
            ) THEN

            ! Set sd
            CALL value(args(6), sd, ierr)
            IF (ierr .NE. 0) THEN
               WRITE (*, *) "Error parsing subdomain id: ", TRIM(args(6))
               ierr = PARSE_ERROR
               RETURN
            END IF

            ! Change subdomain to subdomain index
            DO i = 1, SIZE(SubDomainIds)
               IF (sd .EQ. SubDomainIds(i)) THEN
                  sd = i
                  EXIT
               END IF

               IF (i .EQ. SIZE(SubDomainIds)) THEN
                  WRITE (*, *) "Error: Unknown subdomain: ", sd
                  ierr = PARSE_ERROR
                  RETURN
               END IF
            END DO

            CALL setfibonaccistressaxis(args(2), args(3), ierr, sd = sd)
         END IF
      ELSE IF (AssertNArgs(args, 3)) THEN
         CALL setfibonaccistressaxis(args(2), args(3), ierr)
      ELSE
         ierr = PARSE_ERROR
      END IF
   END SUBROUTINE ParseStressFibonacciAxis

   !> Parse the `Magnetite` command
   !>
   !> @param[in]  args The arguments to the `MagnetiteLegacy` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseMagnetiteLegacy(args, ierr)
      use strings
      use Material_Parameters, only: Magnetite_Legacy
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: dbltmp
      integer:: ios

      ierr = PARSE_ERROR

      !  "Magnetite  400 C"  sets material constants for magnetite at 400 deg C
      if (AssertNArgs(args, 3)) then
         if (args(3) .eq. 'C') then
            call value(args(2), dbltmp, ios)
            if (ios .eq. 0) then
               call Magnetite_Legacy(dbltmp)

               ierr = PARSE_SUCCESS
            end if
         end if
      end if
   end subroutine ParseMagnetiteLegacy

   !> Parse the `Iron` command
   !>
   !> @param[in]  args The arguments to the `Iron` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseIron(args, ierr)
      use strings
      use Material_Parameters, only: Iron
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: dbltmp
      integer:: ios

      ierr = PARSE_ERROR

      !  "Iron  400 C"  sets material constants for Iron at 400 deg C
      if (AssertNArgs(args, 3)) then
         if (args(3) .eq. 'C') then
            call value(args(2), dbltmp, ios)
            if (ios .eq. 0) then
               call iron(dbltmp)

               ierr = PARSE_SUCCESS
            end if
         end if
      end if
   end subroutine ParseIron

   !> Parse the `TM54` command
   !>
   !> @param[in]  args The arguments to the `TM54` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseTM54(args, ierr)
      use strings
      use Material_Parameters, only: TM54
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: dbltmp
      integer:: ios

      ierr = PARSE_ERROR

      !  "TM54  400 C"  sets material constants for TM54 at 400 deg C
      if (AssertNArgs(args, 3)) then
         if (args(3) .eq. 'C') then
            call value(args(2), dbltmp, ios)
            if (ios .eq. 0) then
               call TM54(dbltmp)

               ierr = PARSE_SUCCESS
            end if
         end if
      end if
   end subroutine ParseTM54

   !> Parse the `TM` command
   !>
   !> @param[in]  args The arguments to the `TM` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseTM(args, ierr)
      use strings
      use Material_Parameters, only: tm
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: dblTi, dbltmp
      integer:: ios

      ierr = PARSE_ERROR

      !  "TM 40 120 C"  sets material constants for titanomagnetite-40 at 120 deg C
      if (AssertNArgs(args, 4)) then
         if (args(4) .eq. 'C') then
            call value(args(2), dblTi, ios)  ! Get Ti value
            call value(args(3), dbltmp, ios)  ! Get temperature
            if (ios .eq. 0) then
               call TM(dblTi, dbltmp)

               ierr = PARSE_SUCCESS
            end if
         end if
      end if
   end subroutine ParseTM

   !> Parse the `LowTempMagnetite` command
   !>
   !> @param[in]  args The arguments to the `LowTempMagnetite` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseMagnetite(args, ierr)
      use strings
      use Material_Parameters, only: Magnetite
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: dbltmp
      integer:: ios

      ierr = PARSE_ERROR

      if (AssertNArgs(args, 3)) then
         if (args(3) .eq. 'C') then
            call value(args(2), dbltmp, ios)
            if (ios .eq. 0) then
               call Magnetite(dbltmp)
               ierr = PARSE_SUCCESS
            end if
         end if
      end if
   end subroutine ParseMagnetite

   !> Parse the `HystLoop' command
   !>
   !> @param[in]  args The arguments to the `HystLoop` command.
   !> @param[out] ierr The result of the parse
   !> @todo Tidy up the code
   subroutine ParseHystLoop(args, ierr)
      use strings, only: value, compact
      use Mesh_IO, only: stem, hystfile, loopfile
      use Prebuilt_Measurements, only: HystLoop
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: Bmax, Bmin, Bstep, Bdir
      integer:: ios, nargs, StartFlag

      nargs = size(args)
      StartFlag = 1  ! default value for saturated initial guess, -ve values for no initial state
      if (nargs < 5) then
         ierr = PARSE_ERROR
      else
         call value(args(2), Bmax, ios)
         call value(args(3), Bmin, ios)
         call value(args(4), Bstep, ios)

         ! Check the direction of the field HystLoop
         ! This should be < 0
         Bdir = (Bmax-Bmin)/Bstep

         if (Bdir >= 0) then
            write (*, *) "Field step doesn't match max/min fields"
            ierr = PARSE_ERROR
            return
         end if

      !! DEFINE THE FILE NAMES TO SAVE TO
         stem = args(5)  ! used later in the tecplot file
         call compact(stem)
         loopfile = stem(:len_trim(stem))//'.loop'
         hystfile = stem(:len_trim(stem))//'.hyst'
         if (nargs .eq. 6) then
            call value(args(6), StartFlag, ios)
         end if

         ! Do the measurement loop
         call HystLoop(Bmax, Bmin, Bstep, StartFlag)

         ierr = PARSE_SUCCESS

      end if

   end subroutine ParseHystLoop

   !> Parse the `IRM' command
   !>
   !> @param[in]  args The arguments to the `IRM` command.
   !> @param[out] ierr The result of the parse
   !> @todo Tidy up the code
   subroutine ParseIRM(args, ierr)
      use strings
      use Mesh_IO, only: stem
      use Prebuilt_Measurements, only: IRM
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: Bmax, Bstep
      integer:: ios, nargs

      nargs = size(args)

      if (nargs < 4) then
         ierr = PARSE_ERROR
      else
         call value(args(2), Bmax, ios)
         call value(args(3), Bstep, ios)

         Bstep = abs(Bstep)

      !! DEFINE THE FILE NAMES TO SAVE TO
         stem = args(4)  ! used later in the tecplot file
         call compact(stem)
         !loopfile = stem(:LEN_TRIM(stem))//'.loop'

         ! Do the measurement loop
         call IRM(Bmax, Bstep)

         ierr = PARSE_SUCCESS

      end if

   end subroutine ParseIRM

   !> Parse the `DCD' command
   !>
   !> @param[in]  args The arguments to the `IRM` command.
   !> @param[out] ierr The result of the parse
   !> @todo Tidy up the code
   subroutine ParseDCD(args, ierr)
      use strings
      use Mesh_IO, only: stem
      use Prebuilt_Measurements, only: DCD
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: Bmax, Bstep
      integer:: ios, nargs

      nargs = size(args)

      if (nargs < 4) then
         ierr = PARSE_ERROR
      else
         call value(args(2), Bmax, ios)
         call value(args(3), Bstep, ios)

         Bstep = abs(Bstep)

      !! DEFINE THE FILE NAMES TO SAVE TO
         stem = args(4)  ! used later in the tecplot file
         call compact(stem)
         !loopfile = stem(:LEN_TRIM(stem))//'.loop'

         ! Do the measurement loop
         call DCD(Bmax, Bstep)

         ierr = PARSE_SUCCESS

      end if

   end subroutine ParseDCD

   !> Parse the `IRMDCD' command
   !>
   !> @param[in]  args The arguments to the `IRMDCD` command.
   !> @param[out] ierr The result of the parse
   !> @todo Tidy up the code
   subroutine ParseIRMDCD(args, ierr)
      use strings
      use Mesh_IO, only: stem
      use Prebuilt_Measurements, only: IRMDCD
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: Bmax, Bstep
      integer:: ios, nargs, infileflag

      infileflag = 0
      nargs = size(args)

      if (nargs < 4) then
         ierr = PARSE_ERROR
      else
         call value(args(2), Bmax, ios)
         call value(args(3), Bstep, ios)
         if (nargs .eq. 5) then
            call value(args(5), infileflag, ios)
         end if
         Bstep = abs(Bstep)

      !! DEFINE THE FILE NAMES TO SAVE TO
         stem = args(4)  ! used later in the tecplot file
         call compact(stem)
         !loopfile = stem(:LEN_TRIM(stem))//'.loop'

         ! Do the measurement loop
         call IRMDCD(Bmax, Bstep, infileflag)

         ierr = PARSE_SUCCESS

      end if

   end subroutine ParseIRMDCD

   !> Parse the `SimpleFORC' command
   !>
   !> @param[in]  args The arguments to the `SimpleFORC` command.
   !> @param[out] ierr The result of the parse
   !> @todo Tidy up the code
   !> Input args:
   !> 1-SimpleFORC call
   !> 2-Saturation field in mT
   !> 3-number of FORCs
   !> 4-Filename to save to
   !> 5-Save Tecplot file? (default = no)
   !> 6-Save last complete state? (default = yes)
   subroutine ParseSimpleFORC(args, ierr)
      use Mesh_IO, only: stem, datafile, loopfile
      use Prebuilt_Measurements, only: RestartSimpleFORC, HystLoop, SimpleFORC
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      character(len = 1024):: old_stem, flag_save_tec, flag_save_last
      real(KIND = DP):: Bsat, Bmin, Bstep
      integer:: ios, nargs, save_tec, save_last, nFORC, restart_forc, iStart, jStart
      logical:: exist1, existA, existB, existL

      !REAL(KIND = DP):: Bmax, Bmin, Bstep, Bdir

      nargs = size(args)

      if (nargs < 4) then
         ierr = PARSE_ERROR
      else
         if (nargs < 5) then
            flag_save_tec = 'no'
            flag_save_last = 'yes'
         else
            flag_save_tec = args(5)
            flag_save_last = 'yes'
         end if

         if (nargs < 6) then
            flag_save_last = 'yes'
         else
            flag_save_last = args(6)
         end if

         !CALL compact(flag_save_tec)

         if (lowercase(trim(flag_save_tec)) .eq. 'yes') then
            save_tec = 1
         else
            save_tec = 0
         end if

         if (lowercase(trim(flag_save_last)) .eq. 'yes') then
            save_last = 1
         else
            save_last = 0
         end if

         call value(args(2), Bsat, ios)
         call value(args(3), nFORC, ios)

         Bmin = -Bsat
         Bstep = (-2*Bsat)/nFORC

         restart_forc = 0  ! no restart
         iStart = 1  ! start Br loop from first index
         jStart = 1  ! start Bb loop from first index

         !> todo ADD ERROR CHECK ON INPUT
         !      IF (Bdir >= 0) THEN
         !        WRITE(*,*) "Field step doesn't match max/min fields"
         !        ierr = PARSE_ERROR
         !        RETURN
         !      END IF

      !! DEFINE THE FILE NAMES TO SAVE TO
         stem = args(4)  ! used later in the tecplot file
         call compact(stem)

         ! If any of these saved states exist in the current directory then
         ! this must be a restart, so hand over to RestartSimpleFOr restart and so we hand over to the restart subroutine

         datafile = stem(:len_trim(stem))//'_FORC_LastState.dat'
         inquire (FILE = datafile, EXIST = exist1)
         datafile = stem(:len_trim(stem))//'_FORC_LastStateA.dat'
         inquire (FILE = datafile, EXIST = existA)
         datafile = stem(:len_trim(stem))//'_FORC_LastStateB.dat'
         inquire (FILE = datafile, EXIST = existB)

         ! also check if there is a FORC loop file exisiting
         ! this may happen if the restart files have been deteled but the FORC loop file still exists
         datafile = stem(:len_trim(stem))//'_FORC.loop'
         inquire (FILE = datafile, EXIST = existL)

         if ((exist1 .eqv. .true.) .or. (existA .eqv. .true.) .or. (existB .eqv. .true.) .or. (existL .eqv. .true.)) then
            write (*, *) '*********************************************************************'
            write (*, *) '* Restart files found-attempting to continue from last saved file *'
            write (*, *) '*********************************************************************'
            call RestartSimpleFORC()

         else

            old_stem = stem  ! Save the original input name
            loopfile = stem(:len_trim(stem))//'.loop' ! file to save loop data
            stem = stem(:len_trim(stem))//'_Hysteresis'

            !> todo add support for existing hysteresis file???
            !IF (nargs == 5) THEN  ! We have an intial hyst loop
            !ELSE
            !END IF

            ! Do the Hysteresis loop measurement first
            write (*, *) 'Starting major branch loop...'
            call HystLoop(Bsat, Bmin, Bstep, 1)  ! 1 indicates saturted initial guess

            ! Redefine the loop file for the FORC
            stem = old_stem
            stem = stem(:len_trim(stem))//'_FORC' ! Reinstate the original name for the FORC
            datafile = stem(:len_trim(stem))//'_LastState.dat' ! file to save last complete state
            loopfile = stem(:len_trim(stem))//'.loop'

            write (*, *) 'Starting minor branch loops...'
            call SimpleFORC(Bsat, Bmin, Bstep, save_tec, save_last, restart_forc, iStart, jStart)

            ierr = PARSE_SUCCESS
         end if  ! for restart test

      end if  ! correct number or args test

   end subroutine ParseSimpleFORC

   !> Parse the `SimpleFORCRestart' command
   !>
   !> @param[in]  args The arguments to the `SimpleFORCRestart` command.
   !> @param[out] ierr The result of the parse
   !> @todo Tidy up the code
   !> Input args:
   !> 1-SimpleFORCRestart call
   !> 2-File name pattern to recover
   subroutine ParseSimpleFORCRestart(args, ierr)
      use strings
      use Mesh_IO, only: stem
      use Prebuilt_Measurements, only: RestartSimpleFORC
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      integer:: nargs

      nargs = size(args)

      if (nargs < 2) then
         ierr = PARSE_ERROR
      else

         stem = args(2)  ! used later in the tecplot file
         call compact(stem)

         call RestartSimpleFORC()

         ierr = PARSE_SUCCESS

      end if

   end subroutine ParseSimpleFORCRestart

   !> Parse the `Field` command
   !>
   !> @param[in]  args The arguments to the `Field` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseSetField(args, ierr)
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      character(len = 200):: tmp(4)

      integer:: nargs

      nargs = size(args)

      if (nargs .le. 6) then
         tmp = '0'
         tmp(1:nargs-2) = args(3:nargs)
         ! 'external field direction 1 0 0 '  or 'magnetic field strength 1e-3', or 'external field fibonacci 1 30'
         call setfield(tmp(1), tmp(2), tmp(3), tmp(4), ierr)
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseSetField

   !> Parse the `NonUniformField` command
   !>
   !> @param[in]  args The arguments to the `Field` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseNonUniformField(args, ierr)
      ! must must be called after a mesh has been read in otherwise NNODE is not set
      use strings
      use Tetrahedral_mesh_data, only: B_vect, NNODE, SetNonuniformField
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      character(len = 20):: key_val, FMT1, FMT2
      character(len = 55):: opt_args(15), opt_key(15), opt_val(15)
      real(KIND = DP):: A(3), B(3), AB(3)
      real(KIND = DP):: mu_r, r0, current, AB_norm
      integer:: ios, nargs, nargs2, i
      FMT1 = "(A55, ES15.5)"
      FMT2 = "(A35, ES15.5)"

      nargs = size(args)

      if (nargs .lt. 7) then
         write (*, *) ' You need to specify two points'
         write (*, *) ' Consult manual for allowed options'
         write (*, *) ' Stopping Programme'
         ierr = PARSE_ERROR
         stop
      end if

! **** SET DEFAULT values for parameters. Set negative to force user values
      mu_r = 1  ! the relative magnetic permeability of wire, set to zero for hollow wire
      r0 = 10.e-9  ! the wire diameter, if this is too small the field might get very large
      current = 1.e-8
      ! split options into key-value pairs
      !print*, 'number of args= ',size(args)

      do i = 2, nargs  ! skip i = 1 since that is the NonUniformField command
         key_val = trim(lowercase(args(i)))
         call parse(args(i), '=', opt_args, nargs2)
         opt_key(i-1) = trim(lowercase(opt_args(1)))
         opt_val(i-1) = trim(lowercase(opt_args(2)))

      end do

      ! set the options to the apprproate variables
      if (nargs .ge. 3) write (*, *) 'Setting parameters for Biot-Savart field'
      do i = 2, nargs

         select case (opt_key(i-1))

         case ("ax")
            call value(opt_val(i-1), A(1), ios)

         case ("ay")
            call value(opt_val(i-1), A(2), ios)

         case ("az")
            call value(opt_val(i-1), A(3), ios)

         case ("bx")
            call value(opt_val(i-1), B(1), ios)

         case ("by")
            call value(opt_val(i-1), B(2), ios)

         case ("bz")
            call value(opt_val(i-1), B(3), ios)

         case ("current")
            call value(opt_val(i-1), current, ios)

         case ("mu_r")
            call value(opt_val(i-1), mu_r, ios)

         case ("r0")
            call value(opt_val(i-1), r0, ios)

         case DEFAULT
            write (*, *) '**********************'
            write (*, *) ' Unknown NonUniformField Option Set : ', opt_key(i-1)
            write (*, *) ' Consult manual for allowed options'
            write (*, *) ' Stopping Programme'
            ierr = PARSE_ERROR
            stop

         end select

      end do
      write (*, *) '****************************************'
      write (*, *) 'Electric current defined along infinite wire though points'
      write (*, *) A
      write (*, *) B
      write (*, FMT2) 'Current = ', current
      write (*, FMT2) 'Relative magnetic permeability of wire = ', mu_r
      write (*, FMT2) 'Outer radius of wire = ', r0

      AB = B-A
      AB_norm = sqrt(sum(AB**2))

      if (AB_norm .eq. 0.0) then
         write (*, *) "ERROR: Points A and B are coincident, or only one point sepecified!"
         write (*, *) "modify your MERRILL SCRIPT"
         ierr = PARSE_ERROR

      elseif (NNODE .eq. 0) then
         write (*, *) "ERROR: Cannot set NonUniformField before mesh has been read. "
         write (*, *) "modify your MERRILL SCRIPT to read mesh first."
         ierr = PARSE_ERROR
      else

         call SETNonUniformField(A, B, current, mu_r, r0)
         ierr = PARSE_SUCCESS
      end if

   end subroutine ParseNonUniformField


   !> Parse the `ReadMesh` command
   !>
   !> @code
   !>    ReadMesh N MeshFile [filetype = PATRAN | TECPLOT]
   !> @endcode
   !>
   !> @param[in]  args The arguments to the `ReadMesh` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseReadMesh(args, ierr)
      use Mesh_IO
      use strings, only: removesp, lowercase, value, compact
      !use Energy_Calculator, only: ExtraEnergyCalculators
      use Tetrahedral_Mesh_Data, only: MaxMeshNumber, saved_mesh_number, SaveMesh
#if MERRILL_USE_H2LIB
#if H2LIB_USE_NETCDF
      USE H2Lib_h2matrix, ONLY: f_read_cdfcomplete_h2matrix, f_write_cdfcomplete_h2matrix
#endif
      use Finite_Element_Matrices, only: SaveFEM, BA_H2
      use iso_c_binding, only: c_char
#else
      use Finite_Element_Matrices, only: SaveFEM
#endif
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt
      integer:: MeshFileType
      integer, parameter:: FILETYPE_PATRAN = 1, FILETYPE_TECPLOT = 2
      character(LEN=:), allocatable:: filename
#if MERRILL_USE_H2LIB
      character(kind = c_char, LEN=:), allocatable:: meshname_filetype, meshname_stem, filename_h2matrix
      integer:: split_at, length
      logical:: h2matrix_file_found
#endif

      integer:: ios
      integer:: i

      logical:: l

      if (size(args) .lt. 3) then
         l = AssertNArgs(args, 3)
         ierr = PARSE_ERROR
         return
      end if

      !  e.g.:  ' ReadMesh 4 sphere-7.pat'
      call value(args(2), cnt, ios)
      if (ios .ne. 0) then
         write (*, *) "ERROR: Unable to parse INTEGER from ", trim(args(2))
         ierr = PARSE_ERROR
         return
      end if
      currentmesh = cnt
      if (cnt .gt. MaxMeshNumber) then
         write (*, *) "ERROR: Index ", cnt, " larger than MaxMeshNumber ", MaxMeshNumber
         ierr = PARSE_ERROR
         return
      end if

      MeshFileType = FILETYPE_PATRAN
      if (size(args) .gt. 3) then
         if (size(args) .ge. 6) then
            if (lowercase(trim(args(4))) .ne. "filetype") then
               write (*, *) "ERROR: Unexpected parameter ", trim(args(4))
               write (*, *) 'ERROR: Expected "filetype"'
               ierr = PARSE_ERROR
               return
            end if

            if (lowercase(trim(args(5))) .ne. "=") then
               write (*, *) "ERROR: Expected = after parameter ", trim(args(4))
               ierr = PARSE_ERROR
               return
            end if

            select case (lowercase(trim(args(6))))
            case ("patran")
               MeshFileType = FILETYPE_PATRAN
            case ("tecplot")
               MeshFileType = FILETYPE_TECPLOT
            end select
         end if

         if (size(args) .gt. 6) then
            write (*, *) "ERROR: Too many arguments to ", trim(args(1))
         end if
      end if

      filename = args(3)
      call removesp(filename)

#if MERRILL_USE_H2LIB
      ! detect stem of mesh file name
      ! split at last '.'
      split_at = index(filename, ".", back=.true.)
      if (split_at > 0) then
         meshname_filetype = filename(split_at:)
         meshname_stem = filename(:split_at-1)
      else
         write (*, *) "Could not interpret mesh filename as stem+extension, it does not contain '.' Assuming &
         & ", filename, " is stem and corresponding matrix file is ", filename, ".nc ."
         meshname_stem = filename
      end if

      ! check if file exists
      call compact(meshname_stem)

      filename_h2matrix = trim(meshname_stem(:len_trim(meshname_stem))//'.nc')
      ! set file found flag
      INQUIRE (FILE = filename_h2matrix, EXIST = h2matrix_file_found)

      if (h2matrix_file_found) then
#if H2LIB_USE_NETCDF
         ! read from file, set Finite_Element_Data BA_H2.  Existence/pointer allocated of this
         ! variable is also the signal that the matrix will not be built again within later ReadPatranMesh call
         write (*, *) "attempting read ", filename_h2matrix
         BA_H2 = f_read_cdfcomplete_h2matrix(filename_h2matrix//c_null_char)
         write (*, *) "Set H2matrix from file"
#else
         ! TODO display warning, file found but no netcdf capability
         write (*, *) "WARNING: A h2matrix netcdf file was detected at ", filename_h2matrix, ", but it could not be read. &
         & This version of merrill was compiled with H2LIB_USE_NETCDF = OFF and cannot read &
         & the netcdf file.  A new BEM H2 matrix will be generated from mesh data. &
         & Install netcdf and compile merrill with &
         & \n cmake .. -DH2LIB_USE_NETCDF = ON &
         & \n to use the file.  "
#endif
      else
         write (*, *) "No corresponding H2matrix netcdf file ", filename_h2matrix, " found, will create and compress &
         & new BEM H2matrix from mesh geometry."
      end if
#endif

      select case (MeshFileType)
      case (FILETYPE_PATRAN)
         call ReadPatranMesh(filename)
      case (FILETYPE_TECPLOT)
         call ReadTecplotMesh(filename)
      end select

#if MERRILL_USE_H2LIB

      ! write artefacts of Read_Mesh, namely h2 matrix, to file < stem>.nc
      if (.not. (h2matrix_file_found)) then  ! never overwrite  ! TODO no way to force overwrite
#if H2LIB_USE_NETCDF

         write (*, *) "Writing BEM H2matrix object to file ", filename_h2matrix
         write (*, *) len(filename_h2matrix)
         call f_write_cdfcomplete_h2matrix(BA_H2, filename_h2matrix//c_null_char)
#else
         ! TODO display warning, file found but no netcdf capability
         write (*, *) "WARNING: A h2matrix object was created,  but could not be saved to file. &
         & This version of merrill was compiled with H2LIB_USE_NETCDF = OFF and cannot write to &
         & netcdf file. &
         & Install netcdf and compile merrill with &
         & \n cmake .. -DH2LIB_USE_NETCDF = ON &
         & \n to save the h2matrix object. "
#endif
      end if
#endif

      ! Save mesh data for later saving/loading
      call SaveMesh(cnt)
      call SaveFEM(cnt)
      saved_mesh_number = cnt
      ! Initialize extra energy calculators, typically loaded by
      ! LoadPlugin
      !do i = 1, size(ExtraEnergyCalculators)
      !   call ExtraEnergyCalculators(i)%Initialize()
      !end do
      ierr = PARSE_SUCCESS
   end subroutine ParseReadMesh

   !> Parse the `LoadMesh` command
   !>
   !> @param[in]  args The arguments to the `LoadMesh` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseLoadMesh(args, ierr)
      use strings
      use Mesh_IO
      use Tetrahedral_Mesh_Data, only: MaxMeshNumber, saved_mesh_number, LoadMesh
      use Finite_Element_Matrices, only: LoadFEM
      use Material_Parameters, only: BuildMaterialParameters
      !use Energy_Calculator, only: BuildEnergyCalculator  ! TODO concept of initialize these should be somewhere else
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt
      integer:: ios

      !  e.g.:  ' LoadMesh 2'
      call value(args(2), cnt, ios)
      if ((ios .eq. 0) .and. (cnt .le. maxmeshnumber)) then
         saved_mesh_number = cnt
         currentmesh = cnt
         ! BEM Calculator
         call LoadMesh(cnt)
         call LoadFEM(cnt)
         call BuildMaterialParameters()
         !call BuildEnergyCalculator()

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseLoadMesh

   !> Parse the `LoadMagState` command
   !>
   !> @param[in]  args The arguments to the `LoadMagState` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseLoadMagState(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: MaxMeshNumber, LoadMagState
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt
      integer:: ios

      !  e.g.:  ' LoadMagState 2'
      call value(args(2), cnt, ios)
      if ((ios .eq. 0) .and. (cnt .le. maxmeshnumber)) then

         ! BEM Calculator
         call LoadMagState(cnt)

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseLoadMagState

   !> Parse the `SaveMagState` command
   !>
   !> @param[in]  args The arguments to the `LoadMagState` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseSaveMagState(args, ierr)
      use strings
      use Mesh_IO
      use Tetrahedral_Mesh_Data, only: SaveMAGstate
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: ios

      ! this only saves the current magnetiztion, commonly called after a solution is found
      ! since its a parser for teh current solution no argumenst are needed.

      write (*, *) 'Saving current magnetization to mesh ID', currentmesh
      call SaveMagState(currentmesh)
      ierr = PARSE_SUCCESS

   end subroutine ParseSaveMagState

   !> Parse the `SaveMesh` command
   !>
   !> @param[in]  args The arguments to the `SaveMesh` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseSaveMesh(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: MaxMeshNumber, saved_mesh_number, SaveMesh
      use Finite_Element_Matrices, only: SaveFEM
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt
      integer:: ios

      !  e.g.:  ' SaveMesh 2'
      call value(args(2), cnt, ios)
      if ((ios .eq. 0) .and. (cnt .le. maxmeshnumber)) then

         ! BEM Calculator
         call SaveMesh(cnt)
         call SaveFEM(cnt)
         saved_mesh_number = cnt
         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseSaveMesh

   !> Parse the `ReadMag` command
   !>
   !> @param[in]  args The arguments to the `ReadMag` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseReadMag(args, ierr)
      use strings, only: value
      use Tetrahedral_Mesh_Data, only: SaveMAGstate
      use Mesh_IO

      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt, ios

      logical:: l

      if (size(args) .lt. 2) then
         l = AssertNArgs(args, 2)
         ierr = PARSE_ERROR
         return
      end if

      ! For compatability with previous merrill versions
      ! if no mesh number is given then set cnt = 1
      ! but issue warning that this is what had been done
      if (size(args) .eq. 2) then
         write (*, *) 'No mesh number specified, assuming only one mesh loaded'
         cnt = 1
         call ReadMag(args(2), ios)
      end if

      if (size(args) .eq. 3) then !

         call value(args(2), cnt, ios)
         if (ios .ne. 0) then
            write (*, *) "ERROR: Unable to parse INTEGER from ", trim(args(2))
            ierr = PARSE_ERROR
            return
         end if
         !  e.g.:  ' ReadMag 4 trapez.dat'  read magnetization that fits onto saved mesh number 4
         !           Make sure it fits to the current mesh!!
         call ReadMag(args(3), ios)
      end if

      ! todo check at least the that the number of nodes matches that in mesh it is associated with
      ! Save magnetiation  data for later saving/loading
      call SaveMagState(cnt)
      ierr = PARSE_SUCCESS
   end subroutine ParseReadMag

   !> Parse the `ReadQDM` command
   !>
   !> @param[in]  args The arguments to the `ReadQDM` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseReadQDM(args, ierr)
      use Mesh_IO
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt, ios

      logical:: l

      if (size(args) .ne. 2) then
         l = AssertNArgs(args, 2)
         ierr = PARSE_ERROR
         return
      end if

      ! For compatability with previous merrill versions
      ! if no mesh number is given then set cnt = 1
      ! but issue warning that this is what had been done
      if (size(args) .eq. 2) then
         write (*, *) 'Reading QDM Hz field values'
         cnt = 1
         call ReadQDM(args(2), ios)
      end if

      ierr = PARSE_SUCCESS
   end subroutine ParseReadQDM

   !> Parse the `LLGODE` command
   !>
   !> This peforms one of three tyeps of time integration of the LLG
   !> equatiosn, Euler, Runge-Kutta 4th order, 
   !> Runge-Kutta with adaptive stepsize control.
   !> @param[in]  args The arguments to the `LLG` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseLLGODE(args, ierr)
      use strings, only: compact, lowercase, parse, value
      use Mesh_IO, only: stem
      use Tetrahedral_Mesh_Data, only: packing
      use LLG_Solver, only: ag, LLG_ODE_solver
      implicit none
      character(len = 5):: LLGfileformat
      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      character(len = 20):: LLGtype, key_val, FMT1, FMT2
      character(len = 55):: opt_args(15), opt_key(15), opt_val(15)
      integer:: ios, nargs, nargs2, i
      double precision:: DELTAT, MaxDelta, Start_t, DeltaT_min, Abs_tol, &
      & FirstStep, converge, LLGoutInterval

      FMT1 = "(A55, ES15.5)"
      FMT2 = "(A35, ES15.5)"
      nargs = size(args)
      stem = ""

! **** SET DEFAULT values for parameters. Set negative to force user values
      Start_t = -1
      DeltaT = -1.0
      ag = -1
      MaxDelta = -1.0
      DELTAT_min = -1.0
      Abs_tol = -1.0
      FirstStep = -1.0
      Converge = -1.0
      LLGoutInterval = 4.0
      LLGfileformat = 'block'

      ! split options into key-value pairs
      !print*, 'number of args= ',size(args)

      do i = 3, nargs  ! skip i = 1 and i = 2 arg since that is the LLG command and LLG type
         key_val = trim(lowercase(args(i)))
         call parse(args(i), '=', opt_args, nargs2)
         opt_key(i-2) = trim(lowercase(opt_args(1)))
         opt_val(i-2) = trim(lowercase(opt_args(2)))

      end do

      ! set the options to the apprproate variables
      if (nargs .ge. 3) write (*, *) 'Setting LLG integration parameters:'
      do i = 3, nargs

         select case (opt_key(i-2))

         case ("converge")
            call value(opt_val(i-2), Converge, ios)
            write (*, FMT2) 'Convergence Criterion= ', Converge

         case ("maxsteplength")
            call value(opt_val(i-2), MaxDelta, ios)
            write (*, FMT2) 'Max Step Length = ', MaxDelta

         case ("minsteplength")
            call value(opt_val(i-2), MaxDelta, ios)
            write (*, FMT2) 'Min Step Length = ', DeltaT_min

         case ("deltat")
            call value(opt_val(i-2), DeltaT, ios)
            write (*, FMT2) 'DeltaT = ', DeltaT

         case ("firststep")
            call value(opt_val(i-2), FirstStep, ios)
            write (*, FMT2) 'FirstStep = ', FirstSTep

         case ("starttime")
            call value(opt_val(i-2), Start_t, ios)
            write (*, FMT2) 'Start Time = ', Start_t

         case ("accuracy")
            call value(opt_val(i-2), Abs_tol, ios)
            write (*, FMT2) 'Accuracy = ', Abs_tol

         case ("damp")
            call value(opt_val(i-2), ag, ios)
            write (*, FMT2) 'Damping parameter = ', ag

         case ("interval")
            call value(opt_val(i-2), LLGoutInterval, ios)
            write (*, FMT2) 'Data output interval = ', LLGoutInterval

         case ("filename")
            stem = opt_val(i-2)
            call compact(stem)
            stem = stem(:len_trim(stem))
            !write(*,*) 'Output datafile = ', stem

         case ("format")
            packing = opt_val(i-2)
            call compact(packing)
            packing = packing(:len_trim(stem))
            !write(*,*) 'Datafile format = ', packing

         case DEFAULT
            write (*, *) '**********************'
            write (*, *) ' Unknown LLG Option Set : ', opt_key(i-2)
            write (*, *) ' Consult manual for allowed options'
            write (*, *) ' Stopping Programme'
            ierr = PARSE_ERROR
            stop

         end select

      end do

      ! write(*,*) 'number of regs = ', size(args)
      ! stop
      !  e.g.:  ' Minimize'
      ! Calls the appropriate inetgrator for the LLG
      LLGtype = trim(lowercase(args(2)))

      select case (LLGtype)
      case ("euler")
         write (*, *) 'LLG Intergrator Selected  :  ', LLGtype

         if (DeltaT .le. 0.) then
            Deltat = 8.0d-4
            write (*, FMT1) 'DeltaT not set, using default value : ', DeltaT
         end if

         if (ag .le. 0.) then
            ag = 1.0
            write (*, FMT1) 'Damping Parameter damp not set, using default value : ', ag
         end if

         if (converge .le. 0.) then
            converge = 1.0d-4
            write (*, FMT1) 'Convergence criterion not set, using default value : ', converge
         end if

         call LLG_ODE_solver(LLGtype, DeltaT, start_t, MaxDelta, &
           &  DELTAT_min, Abs_tol, ag, converge, LLGoutInterval)
         !=============================

      case ("rk4")
         write (*, *) 'LLG Intergrator Selected  :  ', LLGtype

         if (DeltaT .le. 0.) then
            Deltat = 8.0d-3
            write (*, FMT1) 'DeltaT not set, using default value : ', DeltaT
         end if
         if (ag .le. 0.) then
            ag = 1.0
            write (*, FMT1) 'Damping Parameter not set, using default value : ', ag
         end if

         if (converge .le. 0.) then
            converge = 1.0d-4
            write (*, FMT1) 'Convergence criterion not set, using default value : ', converge
         end if

         call LLG_ODE_solver(LLGtype, DeltaT, start_t, MaxDelta, &
           &  DELTAT_min, Abs_tol, ag, converge, LLGoutInterval)
         !============================

      case ("rkadapt")
         write (*, *) 'LLG Intergrator Selected  :  ', LLGtype

         if (converge .le. 0.) then
            converge = 1.0d-4
            write (*, FMT1) 'Convergence criterion not set, using default value : ', converge
         end if

         if (FirstStep .le. 0.) then
            FirstStep = 8.0d-3
            write (*, FMT1) 'FirstStep not set, using default value : ', FirstStep
         end if

         if (ag .le. 0.) then
            ag = 1.0
            write (*, FMT1) 'Damping Parameter not set, using default value : ', ag
         end if

         if (Start_t .le. 0.) then
            Start_t = 0.
            write (*, FMT1) 'StartTime not set, using default value : ', Start_t
         end if

         if (MaxDelta .le. 0.) then
            MaxDelta = 8.0d-2
            write (*, FMT1) 'Maximum steplength not set, using default value : ', MaxDelta
         end if

         if (DeltaT_min .le. 0.) then
            DeltaT_min = 0.0
            write (*, FMT1) 'Minimum steplength not set, using default value : ', DeltaT_min
         end if

         if (Abs_tol .le. 0.) then
            Abs_tol = 1.d-8
            write (*, FMT1) 'Inetgration accuracy not set, using default value : ', Abs_tol
         end if

         Start_t = 0.0

         ! DeltaT = 8.e-3
         ! MaxDelta = DeltaT*10
         ! DELTAT_min = 0.
         ! Abs_tol = 1.e-8
         call LLG_ODE_solver(LLGtype, FirstStep, start_t, MaxDelta, &
          &  DELTAT_min, Abs_tol, ag, converge, LLGoutInterval)
         !=======================================

      case DEFAULT
         write (*, *) 'Error in LLG type speificiation: Needs to be Euler, RK4 or RKadapt'
         write (*, *) 'You asked for: ', LLGtype
         ierr = PARSE_ERROR
         stop
      end select

      ierr = PARSE_SUCCESS
   end subroutine ParseLLGODE

   !> Parse the `EnergyMinCart` command
   !>
   !> @param[in]  args The arguments to the `EnergyMin` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEnergyMinCart(args, ierr)
      use Minimizing_Loops, only: EnergyMinCart
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      !  e.g.:  ' Minimize'
      ! Calls the minimization routine for the current mesh and magnetization
      call EnergyMinCart()

      ierr = PARSE_SUCCESS
   end subroutine ParseEnergyMinCart

   !> Parse the `EnergyMinMult` command
   !>
   !> @param[in]  args The arguments to the `EnergyMin` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEnergyMinMult(args, ierr)
      use Minimizing_Loops, only: EnergyMinMult
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      !  e.g.:  ' Minimize'
      ! Calls the minimization routine for the current mesh and magnetization
      call EnergyMinMult()

      ierr = PARSE_SUCCESS
   end subroutine ParseEnergyMinMult

   !> Parse the `EnergyMinPolX` command
   !>
   !> @param[in]  args The arguments to the `EnergyMin` command. where the polar axis is X
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEnergyMinPolX(args, ierr)
      use Minimizing_Loops, only: EnergyMinPol
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      !  e.g.:  ' Minimize'
      ! Calls the minimization routine for the current mesh and magnetization
      call EnergyMinPol('X')

      ierr = PARSE_SUCCESS
   end subroutine ParseEnergyMinPolX

   !> Parse the `EnergyMinPolY` command
   !>
   !> @param[in]  args The arguments to the `EnergyMin` command. where the polar axis is X
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEnergyMinPolY(args, ierr)
      use Minimizing_Loops, only: EnergyMinPol
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      !  e.g.:  ' Minimize'
      ! Calls the minimization routine for the current mesh and magnetization
      call EnergyMinPol('Y')

      ierr = PARSE_SUCCESS
   end subroutine ParseEnergyMinPolY

   !> Parse the `EnergyMinPolZ` command-depreciation of EnergyMin
   !>
   !> @param[in]  args The arguments to the `EnergyMin` command. where the polar axis is X
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEnergyMinPolZ(args, ierr)
      use Minimizing_Loops, only: EnergyMinPol
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      !  e.g.:  ' Minimize'
      ! Calls the minimization routine for the current mesh and magnetization
      call EnergyMinPol('Z')

      ierr = PARSE_SUCCESS
   end subroutine ParseEnergyMinPolZ

   !> Parse the `EnergyMin` command
   !>
   !> @param[in]  args The arguments to the `EnergyMin` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEnergyMin(args, ierr)
      use Minimizing_Loops, only: EnergyMinMult
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      !  e.g.:  ' Minimize'
      ! Calls the minimization routine for the current mesh and magnetization
      call EnergyMinMult()

      ierr = PARSE_SUCCESS
   end subroutine ParseEnergyMin

   !> Parse the `PathMinimize` command
   !>
   !> @param[in]  args The arguments to the `PathMinimize` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParsePathMinimize(args, ierr)
      use Magnetization_Path, only: PathMinimize
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      !  e.g.:  ' PathMinimize'
      ! Calls the path minimization routine for the current path
      call PathMinimize()

      ierr = PARSE_SUCCESS
   end subroutine ParsePathMinimize

   !> Parse the `Remesh` command
   !>
   !> @param[in]  args The arguments to the `Remesh` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseRemesh(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: m, MaxMeshNumber, SaveNNODE, SaveNTRI, RemeshTo, LoadMesh, SaveMAGstate, ReplaceSD
      use Finite_Element_Matrices, only: LoadFEM
      use Material_Parameters, only: BuildMaterialParameters
      !use Energy_Calculator, only: BuildEnergyCalculator
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP), allocatable:: mremesh(:, :)

      character(len = 25):: opt_args(15), opt_key(15), opt_val(15)
      character(len = 20):: key_val, rescale, FMT
      character(len = 1):: scaling
      integer:: cnt, nargs, nargs2, mesh_number, i, blockno, SNNODE, SNTRI
      integer:: ios

      ! defaults
      scaling = 'y'
      mesh_number = -1

      FMT = "(A45, I3)"

      nargs = size(args)
      do i = 2, nargs  ! skip i = 1  arg since that is the ReMesh command
         key_val = trim(lowercase(args(i)))
         call parse(args(i), '=', opt_args, nargs2)
         opt_key(i-1) = trim(lowercase(opt_args(1)))
         opt_val(i-1) = trim(lowercase(opt_args(2)))

      end do

      ! set the options to the apprproate variables
      do i = 2, nargs

         select case (opt_key(i-1))

         case ("mesh")
            call value(opt_val(i-1), mesh_number, ios)

         case ("rescale")
            scaling = trim(lowercase(opt_val(i-1)))
            !CALL value(opt_val(i-1), scaling, ios)

         case DEFAULT
            write (*, *) '**********************'
            write (*, *) ' Unknown remesh Option Set : ', opt_key(i-1)
            write (*, *) ' Consult manual for allowed options'
            write (*, *) ' Stopping Programme'
            stop

         end select
         ierr = PARSE_ERROR
      end do

      if (mesh_number .eq. -1) then
         write (*, *) 'ERROR: You must set a mesh number to remesh to!'
         write (*, *) ' Stopping Programme'
         stop
      end if

      if (scaling .ne. 'y') then
         !no rescaling so implement original re-meshing
         !  e.g.:  'ReMesh 3'
         ! Remeshes current magnetization to the saved mesh with index 3
         ! and loads this
         ! TODO warning cnt uninitialized
         if ((ios .eq. 0) .and. (cnt .le. maxmeshnumber)) then
            call RemeshTo(mesh_number, mremesh)
            call LoadMesh(mesh_number)
            call LoadFEM(mesh_number)
            call BuildMaterialParameters()
            !call BuildEnergyCalculator()
            m(:, :) = mremesh(:, :)

            ierr = PARSE_SUCCESS
         else
            ierr = PARSE_ERROR
         end if
      end if  ! for no resclaing

      if (scaling .eq. 'y') then
         !no rescaling so implement original re-meshing
         !  e.g.:  'ReMesh 3'
         ! Remeshes current magnetization to the saved mesh with index 3
         ! and loads this
         write (*, FMT) "Mapping current magnetization state to Mesh", mesh_number

         SNTRI = SaveNTRI(mesh_number)
         SNNODE = SaveNNODE(mesh_number)

         if (allocated(mremesh)) deallocate (mremesh)
         allocate (mremesh(SNNODE, 3))
         mremesh(:, :) = 0

         if ((ios .eq. 0) .and. (mesh_number .le. maxmeshnumber)) then
            blockno = -1  ! this implies that no subdomain matching is to be done in ReplaceSD
            call ReplaceSD(mesh_number, mremesh, blockno)
            call LoadMesh(mesh_number)
            call LoadFEM(mesh_number)
            call BuildMaterialParameters()
            !call BuildEnergyCalculator()

            m(:, :) = mremesh(:, :)

            call SaveMagState(mesh_number)

            ierr = PARSE_SUCCESS
         else
            ierr = PARSE_ERROR
         end if
      end if

   end subroutine ParseRemesh

   !> Parse the `RemeshSD` command
   !> Interpolates magnetization of all subomains of current mesh onto identically numbered
   !> subdomains of saved mesh
   !>
   !> @param[in]  args The arguments to the `RemeshSD` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseRemeshSD(args, ierr)
      use Tetrahedral_Mesh_Data, only: SubDomainIds, m, MaxMeshNumber, SaveNTRI, SaveNNODE, RemeshToSD, LoadMesh
      use Finite_Element_Matrices, only: LoadFEM
      use Material_Parameters, only: BuildMaterialParameters
      !use Energy_Calculator, only: BuildEnergyCalculator
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP), allocatable:: mremesh(:, :)

      integer:: cnt, i, SNNODE, SNTRI
      integer:: ios

      !  e.g.:  'ReMeshSD 3'
      ! Remeshes eperately and iteratively each subdomain in current
      ! magnetization to the saved mesh with index 3
      ! and loads the new mesh

      call value(args(2), cnt, ios)

      SNTRI = SaveNTRI(cnt)
      SNNODE = SaveNNODE(cnt)

      if (allocated(mremesh)) deallocate (mremesh)
      allocate (mremesh(SNNODE, 3))
      mremesh(:, :) = 0

      if ((ios .eq. 0) .and. (cnt .le. maxmeshnumber)) then
         do i = 1, size(SubDomainIds(:))
            write (*, *) 'Remeshing Subdomain = ', SubDomainIds(i)
            call RemeshToSD(cnt, mremesh, SubDomainIds(i))
         end do
         call LoadMesh(cnt)
         call LoadFEM(cnt)
         call BuildMaterialParameters()
         !call BuildEnergyCalculator()

         m(:, :) = mremesh(:, :)

         ! print out the interpolated domains to check
         ! Do i = 1, NTRI
         ! print*, 'm =',SubDomainIds(TetSubDomains(i)), m(TIL(i, 1), 1), m(TIL(i, 1), 2), m(TIL(i, 1), 2)
         ! END do

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if

   end subroutine ParseRemeshSD

   !> Parse the `RotateMag` command
   !>  This takes the current mesh and magetization and rotates it about the
   !> its center point (i.e. [max-min]/2) and the remaps this strcutre onto the
   !> original mesh. maps it onto a
   !> subdomain number of a saved mesh. It first resizes teh current mesh
   !> to the dimensions and location of the  SD part of the saved mesh
   !> and then maps the current magetization onto it, leaveing the magnetization
   !> in other SD of teh saved mesh untouched.
   !> this is useful for taking some saved domains state and an initial
   !> state for a
   !>
   !> @param[in]  args The arguments to the `Remap` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseRotateMag(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: m, saved_mesh_number, MaxMeshNumber, SaveNTRI, SaveNNODE, RotateMag, LoadMesh, SaveMAGstate
      use Finite_Element_Matrices, only: LoadFEM
      use Material_Parameters, only: BuildMaterialParameters
      !use Energy_Calculator, only: BuildEnergyCalculator
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      character(len = 25):: opt_args(15), opt_key(15), opt_val(15)
      character(len = 20):: key_val, FMT

      real(KIND = DP), allocatable:: mremesh(:, :)

      integer:: cnt, i, SNNODE, SNTRI, blockno
      integer:: ios, itest, nargs, nargs2
      double precision:: rot_alpha_X, rot_beta_Y, rot_gamma_Z

      FMT = "(A45, I3, A7, I4)"

      nargs = size(args)
      ! defaults
      rot_alpha_X = 0.0
      rot_beta_Y = 0.0
      rot_gamma_Z = 0.0

      do i = 2, nargs  ! skip i = 1  arg since that is the RotateMag command
         key_val = trim(lowercase(args(i)))
         call parse(args(i), '=', opt_args, nargs2)
         opt_key(i-1) = trim(lowercase(opt_args(1)))
         opt_val(i-1) = trim(lowercase(opt_args(2)))

      end do

      ! set the options to the apprproate variables
      do i = 2, nargs

         select case (opt_key(i-1))

         case ("alpha")
            call value(opt_val(i-1), rot_alpha_X, ios)

         case ("beta")
            call value(opt_val(i-1), rot_beta_Y, ios)

         case ("gamma")
            call value(opt_val(i-1), rot_gamma_Z, ios)

            ! case("block")
            !   CALL value(opt_val(i-1), blockno, ios)

         case DEFAULT
            write (*, *) '**********************'
            write (*, *) ' Unknown RotateMag Option Set : ', opt_key(i-1)
            write (*, *) ' Consult manual for allowed options'
            write (*, *) ' Stopping Programme'
            stop

         end select
         ierr = PARSE_ERROR

      end do
    !! here the mesh nuymber shoudl simply be the cirrent mesh   need ot check
      cnt = saved_mesh_number

      write (*, FMT) "Mapping current magnetization state to Mesh", cnt

      SNTRI = SaveNTRI(cnt)
      SNNODE = SaveNNODE(cnt)

      ! the rotated magnetization willinitially be put into mremesh
      if (allocated(mremesh)) deallocate (mremesh)
      allocate (mremesh(SNNODE, 3))

      mremesh(:, :) = 0

      if ((ios .eq. 0) .and. (cnt .le. maxmeshnumber)) then
         print *, ' call rotate for state ', cnt

         call RotateMag(cnt, mremesh, rot_alpha_X, rot_beta_Y, rot_gamma_Z)
         call LoadMesh(cnt)
         call LoadFEM(cnt)
         call BuildMaterialParameters()
         !call BuildEnergyCalculator()

         ! replace current magnetization with rotated one, and then save it.
         m(:, :) = mremesh(:, :)

         call SaveMagState(cnt)

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if

   end subroutine ParseRotateMag

   !> Parse the `InvertMag` command
   !>  This is a simple untiity to take the magetization at each node and
   !>  multiply its magnetization components by-1
   !>
   !> @param[in]  args The arguments to the `Remap` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseInvertMag(args, ierr)
      use Tetrahedral_Mesh_Data, only: m
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      character(len = 25):: opt_args(15), opt_key(15), opt_val(15)
      character(len = 20):: key_val, FMT

      real(KIND = DP), allocatable:: mremesh(:, :)

      integer:: cnt, i, SNNODE, SNTRI, blockno
      integer:: ios, itest, nargs, nargs2
      double precision:: rot_alpha_X, rot_beta_Y, rot_gamma_Z

      FMT = "(A45, I3, A7, I4)"

      nargs = size(args)

      if (nargs .ge. 2) then
         write (*, *) 'InvertMag does not allow optional parameters'
         write (*, *) 'Ingnoring options and continuing'
      end if

      ! replace current magnetization with rotated one, and then save it.
      m(:, :) = -1*m(:, :)

      ierr = PARSE_SUCCESS

   end subroutine ParseInvertMag

   !> Parse the `ReplaceSD` command
   !>  This takes the current mesh and magetization and maps it onto a
   !> subdomain number of a saved mesh. It first resizes the current mesh
   !> to the dimensions and location of the  SD part of the saved mesh
   !> and then maps the current magetization onto it, leaveing the magnetization
   !> in other SD of teh saved mesh untouched.
   !> this is useful for taking some saved domains state as an initial
   !> state for particular mesh of given block number.
   !>
   !> @param[in]  args The arguments to the `Remesh` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseReplaceSD(args, ierr)
      use strings
      use Mesh_IO
      use Tetrahedral_Mesh_Data, only: m, MaxMeshNumber, SaveNTRI, SaveNNODE, ReplaceSD, LoadMesh, SaveMAGstate
      use Finite_Element_Matrices, only: LoadFEM
      use Material_Parameters, only: BuildMaterialParameters
      !use Energy_Calculator, only: BuildEnergyCalculator
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      character(len = 25):: opt_args(15), opt_key(15), opt_val(15)
      character(len = 20):: key_val, FMT

      real(KIND = DP), allocatable:: mremesh(:, :)

      integer:: cnt, i, SNNODE, SNTRI, blockno
      integer:: ios, itest, nargs, nargs2

      FMT = "(A45, I3, A7, I4)"

      nargs = size(args)

      do i = 2, nargs  ! skip i = 1  arg since that is the ReplaceSD command
         key_val = trim(lowercase(args(i)))
         call parse(args(i), '=', opt_args, nargs2)
         opt_key(i-1) = trim(lowercase(opt_args(1)))
         opt_val(i-1) = trim(lowercase(opt_args(2)))

      end do

      ! set the options to the apprproate variables
      do i = 2, nargs

         select case (opt_key(i-1))

         case ("savedmesh")
            call value(opt_val(i-1), cnt, ios)

         case ("block")
            call value(opt_val(i-1), blockno, ios)

         case DEFAULT
            write (*, *) '**********************'
            write (*, *) ' Unknown ReplaceSD Option Set : ', opt_key(i-1)
            write (*, *) ' Consult manual for allowed options'
            write (*, *) ' Stopping Programme'
            stop

         end select
         ierr = PARSE_ERROR

      end do

      write (*, FMT) "Mapping current magnetization state to Mesh", cnt, ", block ", blockno

      SNTRI = SaveNTRI(cnt)
      SNNODE = SaveNNODE(cnt)

      if (allocated(mremesh)) deallocate (mremesh)
      allocate (mremesh(SNNODE, 3))
      mremesh(:, :) = 0

      if ((ios .eq. 0) .and. (cnt .le. maxmeshnumber)) then

         call ReplaceSD(cnt, mremesh, blockno)
         call LoadMesh(cnt)
         call LoadFEM(cnt)
         call BuildMaterialParameters()
         !call BuildEnergyCalculator()

         m(:, :) = mremesh(:, :)

         call SaveMagState(cnt)
         currentmesh = cnt

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if

   end subroutine ParseReplaceSD

   !> Parse the `Resize` command
   !>
   !> MScript example:
   !> Resizes mesh lengths from 500 nm to 100 nm, len = len/500*100
   !> @code
   !>    Resize 500 100
   !> @endcode
   !>
   !> @param[in]  args The arguments to the `Print` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseResize(args, ierr)
      use strings
      use Material_Parameters, only: Ls
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: dbltmp
      integer:: ios

      if (.not. AssertNArgs(args, 3)) then
         return
      end if

      call value(args(2), dbltmp, ios)
      if (ios .eq. 0) then
         Ls = sqrt(Ls)*dbltmp  ! Note that lengthes are  Len = 1/SQRT(Ls)
      else
         write (*, *) "ERROR: Unable to parse REAL from ", trim(args(2))
         ierr = PARSE_ERROR
         return
      end if

      call value(args(3), dbltmp, ios)
      if (ios .eq. 0) then
         Ls = (Ls/dbltmp)
      else
         write (*, *) "ERROR: Unable to parse REAL from ", trim(args(3))
         ierr = PARSE_ERROR
      end if
      ls = ls*ls

      ierr = PARSE_SUCCESS
   end subroutine ParseResize

   !> Parse the `WriteMag` command
   !>
   !> @param[in]  args The arguments to the `WriteMag` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseWriteMag(args, ierr)
      use Mesh_IO
      use strings, only: lowercase, compact
      use Tetrahedral_Mesh_Data, only: packing, zoneflag
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
!    CHARACTER(len = 5):: packing
      integer:: nargs

      nargs = size(args)

      !  e.g.:  ' WriteMag trapez'  uses stem 'trapez'
      if (nargs > 1) then
         stem = args(2)
         call compact(stem)
         datafile = stem(:len_trim(stem))//'.dat'
      end if

      packing = 'block' ! default to block format
      if (nargs .eq. 3) packing = lowercase(trim(args(3)))

      !  Writes mag ->  MODULE Mesh_IO
      call WriteMag()
!    print*, 'packing in parseMag = ', packing
      ! zoneflag = -1.0 implies use of zone/zoneinc
      call WriteTecplot(zoneflag, stem, 1)

      ierr = PARSE_SUCCESS
   end subroutine ParseWriteMag

   !> Parse the `ZoneName` command
   !>
   !> @param[in]  args  - a string of 80 characters max
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseZoneName(args, ierr)
      use Mesh_IO
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      character*1, SPACE
      integer, intent(OUT):: ierr
      integer:: nargs, i

      SPACE = ' '
      nargs = size(args)
      ZStr = ""

      if (nargs > 1) then
         do i = 2, nargs
            ZStr = trim(ZStr)//SPACE//trim(args(i))
         end do

         call compact(ZStr)

      end if

      ierr = PARSE_SUCCESS
   end subroutine ParseZoneName

   !> Parse the `AppendTecplotZone` command
   !>
   !> @param[in]  args The arguments to the `WriteMag` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseAppendTecplotZone(args, ierr)
      use Mesh_IO
      use strings
      use Tetrahedral_Mesh_Data, only: packing, zoneflag
      implicit none

      character(len=*), intent(IN):: args(:)
      character(LEN = 1024), save:: old_datafile = char(0)  ! set to NULL
      integer, intent(OUT):: ierr
      integer:: nargs
      integer, save:: zone_val = 0

      nargs = size(args)
      zone_val = zone_val+1

      old_datafile = (trim(old_datafile))

      !  e.g.:  ' WriteMag trapez'  uses stem 'trapez'
      if (nargs > 1) then
         stem = args(2)
         call compact(stem)
         datafile = stem(:len_trim(stem))//'.dat'
      end if

      if (datafile /= old_datafile) zone_val = 1  ! new filename thus set zone = 1 to write mesh

      old_datafile = datafile

      packing = 'block' ! default to block format
      if (nargs .eq. 3) packing = lowercase(trim(args(3)))

!    zoneflag = -1.0 implies use of zone/zoneinc
      call WriteTecplot(zoneflag, stem, zone_val)

      ierr = PARSE_SUCCESS
   end subroutine ParseAppendTecplotZone

   !> Parse the `WriteDemag` command
   !>
   !> @param[in]  args The arguments to the `WriteDemag` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseWriteDemag(args, ierr)
      use Mesh_IO
      use strings
      use Tetrahedral_Mesh_Data, only: zoneflag
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      character(len = 5):: packing

      integer:: nargs

      nargs = size(args)

      !  e.g.:  ' WriteMag trapez'  uses stem 'trapez'
      if (nargs > 1) then
         stem = args(2)
         call compact(stem)
      end if

      packing = 'block' ! default to block format
      if (nargs .eq. 3) packing = lowercase(trim(args(3)))

      !  Writes demag field ->  MODULE Mesh_IO
      call WriteDemag(zoneflag, stem, 1)

      ierr = PARSE_SUCCESS
   end subroutine ParseWriteDemag

   !> Parse the `AppendDemagZone` command
   !>
   !> @param[in]  args The arguments to the `WriteMag` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseAppendDemagZone(args, ierr)
      use Mesh_IO
      use strings
      use Tetrahedral_Mesh_Data, only: packing, zoneflag
      implicit none

      character(len=*), intent(IN):: args(:)
      character(LEN = 1024), save:: old_datafile = char(0)  ! set to NULL
      integer, intent(OUT):: ierr
      integer:: nargs
      integer, save:: zone_val = 0

      nargs = size(args)
      zone_val = zone_val+1

      old_datafile = (trim(old_datafile))

      !  e.g.:  ' WriteMag trapez'  uses stem 'trapez'
      if (nargs > 1) then
         stem = args(2)
         call compact(stem)
         datafile = stem(:len_trim(stem))//'.demag'
      end if

      if (datafile /= old_datafile) zone_val = 1  ! new filename thus set zone = 1 to write mesh

      old_datafile = datafile

      packing = 'block' ! default to block format
      if (nargs .eq. 3) packing = lowercase(trim(args(3)))

!    zoneflag = -1.0 implies use of zone/zoneinc
      call WriteDemag(zoneflag, stem, zone_val)

      ierr = PARSE_SUCCESS
   end subroutine ParseAppendDemagZone

   !> Parse the `WriteHyst` command
   !>
   !> @param[in]  args The arguments to the `WriteHyst` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseWriteHyst(args, ierr)
      use Mesh_IO
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs

      nargs = size(args)

      ! e.g.: ' WriteHyst trapez' uses stem 'trapez' to output file 'trapez.hyst'
      if (nargs > 1) then
         stem = args(2)
         call compact(stem)
         hystfile = stem(:len_trim(stem))//'.hyst'
      end if
      call WriteHyst()

      ierr = PARSE_SUCCESS
   end subroutine ParseWriteHyst

   !> Parse the `WriteStressHyst` command
   !>
   !> @param[in]  args The arguments to the `WriteHyst` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseWriteStressHyst(args, ierr)
      use Mesh_IO
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs

      nargs = size(args)

      ! e.g.: ' WriteStressHyst trapez' uses stem 'trapez' to output file 'trapez.stresshyst'
      if (nargs > 1) then
         stem = args(2)
         call compact(stem)
         stresshystfile = stem(:len_trim(stem))//'.stresshyst'
      end if
      call WriteStressHyst()

      ierr = PARSE_SUCCESS
   end subroutine ParseWriteStressHyst

   !> Parse the `WriteLoopData` command
   !>
   !> @param[in]  args The arguments to the `WriteLoopData` command.
   !> @param[out] ierr The result of the parse
   subroutine ParseWriteLoopData(args, ierr)
      use Mesh_IO
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs, nargs_left

      nargs = size(args)

      ! e.g.: ' WriteLoopData FileName Name1 Value1 Name2 Value2 Name3 Value3'
      if (nargs > 1) then
         stem = args(2)
         call compact(stem)
         loopfile = stem(:len_trim(stem))//'.loop'
      end if

      if (nargs > 2) then
         ! Need to parse the arguments for names and values
         nargs_left = size(args(3:nargs))
         if (mod(nargs_left, 2) .eq. 0) then  ! check for even number (redundant if only defined varaibles are used)
            call WriteLoopData(args(3:nargs-1:2), args(4:nargs:2))
            ierr = PARSE_SUCCESS
         else
            ierr = PARSE_ERROR
         end if
      else
         call WriteLoopData()
         ierr = PARSE_SUCCESS
      end if

   end subroutine ParseWriteLoopData

   !> Parse the `WriteBoxData` command
   !>
   !> @param[in]  args The arguments of the `WriteBoxData` (output file name)
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseWriteBoxData(args, ierr)
      use Mesh_IO
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs

      nargs = size(args)

      ! e.g.: ' WriteBoxData trapez' uses stem 'trapez' to output file 'trapez.vbox'
      if (nargs > 1) then
         stem = args(2)
         call compact(stem)
         vboxfile = stem(:len_trim(stem))//'.vbox'
      end if
      call WRITEvbox()

      ierr = PARSE_SUCCESS
   end subroutine ParseWriteBoxData

   !> Parse the `GradientTest` command
   !>
   !> @param[in]  args The arguments to the `GradientTest` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseGradientTest(args, ierr)
      use Energy_Calculator, only: GradientTest
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! tests the energy gradient
      if (AssertNArgs(args, 1)) then
         call GradientTest()

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseGradientTest

   !> Parse the `ReportEnergy` command
   !>
   !> @param[in]  args The arguments to the `ReportEnergy` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseReportEnergy(args, ierr)
      use Energy_Calculator, only: ReportEnergy
      use Mesh_IO, only: stem
      use strings, only: compact
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      integer:: nargs
      character(len = 1024):: energy_filename

      nargs = size(args)
      ! reports the micromagnetic energies, e.g. for test purposes
      if (nargs == 1) then
         call ReportEnergy()
         ierr = PARSE_SUCCESS
      else if (nargs == 2) then
         stem = args(2)
         call compact(stem)
         energy_filename = stem(:len_trim(stem))//'.energy'
         !write(*,*) 'file = ', energy_filename
         call ReportEnergy(energy_filename)
         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseReportEnergy

   !> Parse the `KeyPause` command
   !>
   !> @param[in]  args The arguments to the `KeyPause` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseKeyPause(args, ierr)
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      character(len = 1024):: tempstr

      !  Wait for [enter]
      write (*, *) ' Pause: Press any key+ENTER to continue'
      read (*, *) tempstr

      ierr = PARSE_SUCCESS
   end subroutine ParseKeyPause

   !> Parse the `UniformMagnetization` command
   !>
   !> @param[in]  args The arguments to the `UniformMagnetization` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseUniformMagnetization(args, ierr)
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs, sd
      character(len = 100), parameter:: zero = '0'

      nargs = size(args)

      ierr = PARSE_ERROR

      !  "uniform magnetization 0 1 1"
      ! Create uniform   magnetization  with defined direction

      ! "randomize magnetization 20"
      ! Changes current magnetization by random angles < 20 deg

      if (nargs == 8) then
         sd = ParseSubdomain(args(6:8), ierr)
         if (ierr .ne. PARSE_ERROR) then
            call setuniform(args(3), args(4), args(5), ierr, sd = sd)

         end if
      else if (nargs == 5) then
         call setuniform(args(3), args(4), args(5), ierr)
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseUniformMagnetization

   !> Parse the `RandomizeMagnetization` command
   !>
   !> @param[in]  args The arguments to the `RandomizeMagnetization` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseRandomizeMagnetization(args, ierr)
      use Tetrahedral_Mesh_Data, only: randomchange
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: sd
      integer:: ios

      ! "randomize magnetization 20"
      ! Changes current magnetization by random angles < 20 deg
      if (size(args) .eq. 6) then
         sd = ParseSubdomain(args(4:6), ierr)
         if (ierr .ne. PARSE_ERROR) then
            call randomchange(args(2), args(3), sd = sd)  ! ->Module_Mesh
            ierr = PARSE_SUCCESS
         end if
      else if (AssertNArgs(args, 3)) then
         call randomchange(args(2), args(3))  ! ->Module_Mesh
         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if

   end subroutine ParseRandomizeMagnetization

   !> Parse the `ParseInvertMagnetization` command
   !>
   !> @param[in]  args The arguments to the `ParseInvertMagnetization` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseInvertMagnetization(args, ierr)
      use Tetrahedral_Mesh_Data, only: InvertMag
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! "invert magnetization 1-1 1"
      ! Changes current magnetization mx, my, mz -> mx, -my, mz
      if (AssertNArgs(args, 5)) then
         call InvertMag(args(3), args(4), args(5))  ! ->Module_Mesh

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseInvertMagnetization

   !> Parse the `ParseFixBlockSD` command
   !>
   !> @param[in]  args The arguments to the `ParseFixBlockSD' command
   !> @todo DOCUMENT ME
   subroutine ParseFixBlockSD(args, ierr)
      use Strings
      use Tetrahedral_Mesh_Data, only: FixNodes
      implicit none

      integer, allocatable:: mremesh(:, :)

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      integer:: i, j, nargs, nfix, SDnumber
      ! IF(ALLOCATED(NodeBlockNumber))  DEALLOCATE(NodeBlockNumber)
      ! ALLOCATE(NodeBlockNumber(NNODE))
      ! print*,'nnode = ', nnode

      nargs = size(args)

      if (NArgs .ge. 1) then

         call FixNodes(args)

         ierr = PARSE_SUCCESS
      else
         write (*, *) 'ERROR: No Subdomain specified-you must specify at least one Subdomain argument to the FixBlockSD command'
         ierr = PARSE_ERROR
      end if
   end subroutine ParseFixBlockSD

   !> Parse the `ParseRenumberBlockSD` command
   !>
   !> @param[in]  args The arguments to the `ParseFixBlockSD' command
   !> @todo DOCUMENT ME
   subroutine ParseRenumberBlockSD(args, ierr)
      use Mesh_IO
      use Strings
      use Tetrahedral_Mesh_Data, only: RenumberBlocks

      implicit none

      integer, allocatable:: mremesh(:, :)

      character(len=*), intent(IN):: args(:)
      character(len = 2):: to
      integer, intent(OUT):: ierr
      integer ::  ios
      integer:: i, j, nargs, nfix, SDnumber1, SDnumber2
      ! IF(ALLOCATED(NodeBlockNumber))  DEALLOCATE(NodeBlockNumber)
      ! ALLOCATE(NodeBlockNumber(NNODE))
      ! print*,'nnode = ', nnode
      ios = 0
      to = trim(lowercase(args(3)))
      !print*, 'currrent mesh number', currentmesh
      if (AssertNArgs(args, 4) .and. to .eq. 'to') then
         call value(args(2), SDNumber1, ios)
         call value(args(4), SDNumber2, ios)
         call RenumberBlocks(SDNumber1, SDNumber2, currentmesh, ios)
         ierr = PARSE_SUCCESS
      else if (ios .eq. 1) then
         write (*, 1001) SDNumber1, currentmesh
1001     format('SubDomain number ', I0, ' does not appear in mesh ', I0)
         ierr = PARSE_ERROR
      else

         write (*, *) 'ERROR: RenumberBlockSD syntax incorrect'
         ierr = PARSE_ERROR
      end if
   end subroutine ParseRenumberBlockSD

   !> Parse the `Vortex Magnetization` command
   !>
   !> MScript Example:
   !> @code
   !>    Vortex Magnetization px py pz [p0x p0y p0z] [v] [RH/LH]
   !> @endcode
   !>
   !> @param[in]  args The arguments to the `VortexMagnetization` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseVortexMagnetization(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: VortexMag
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: p(3), p0(3), v
      character(len = 3):: orientation

      p = 0
      p0 = 0
      v = 0
      orientation = 'RH'
      if (size(args) .ge. 5) then

         ! First 3 args are p
         call TryParseP(args(3:5), ierr)
         if (ierr .ne. PARSE_SUCCESS) return

         ! Try find p0
         if (size(args) .ge. 8) then
            call TryParseP0(args(6:8), ierr)
            if (ierr .ne. PARSE_SUCCESS) return

            if (size(args) .gt. 8) then
               call TryParseVAndRH(args(9:), ierr)
            end if
         else if (size(args) .gt. 5) then
            call TryParseVAndRH(args(6:), ierr)
         end if

      else
         write (*, *) "ERROR: Vortex Magnetization expects at least 3 arguments"
         ierr = PARSE_ERROR
      end if

      if (ierr .eq. PARSE_SUCCESS) then
         call VortexMag(p, p0 = p0, v = v, orientation = orientation)
      end if

   contains
      subroutine TryParseReal(arg, dest, varname, ierr)
         character(len=*), intent(IN):: arg
         real(KIND = DP), intent(OUT):: dest
         character(len=*), intent(IN):: varname
         integer, intent(OUT):: ierr

         call value(arg, dest, ierr)
         if (ierr .ne. 0) then
            write (*, *) "ERROR: Unable to parse REAL for argument p1"
            ierr = PARSE_ERROR
         else
            ierr = PARSE_SUCCESS
         end if
      end subroutine

      subroutine TryParseP(args, ierr)
         character(len=*), intent(IN):: args(3)
         integer, intent(OUT):: ierr

         call TryParseReal(args(1), p(1), "px", ierr)
         if (ierr .eq. PARSE_ERROR) return

         call TryParseReal(args(2), p(2), "py", ierr)
         if (ierr .eq. PARSE_ERROR) return

         call TryParseReal(args(3), p(3), "pz", ierr)
         if (ierr .eq. PARSE_ERROR) return
      end subroutine TryParseP

      subroutine TryParseP0(args, ierr)
         character(len=*), intent(IN):: args(3)
         integer, intent(OUT):: ierr

         call TryParseReal(args(1), p0(1), "p0x", ierr)
         if (ierr .eq. PARSE_ERROR) return

         call TryParseReal(args(2), p0(2), "p0y", ierr)
         if (ierr .eq. PARSE_ERROR) return

         call TryParseReal(args(3), p0(3), "p0z", ierr)
         if (ierr .eq. PARSE_ERROR) return
      end subroutine TryParseP0

      subroutine TryParseVAndRH(args, ierr)
         character(len=*), intent(IN):: args(:)
         integer, intent(OUT):: ierr

         logical:: FoundV, FoundRHLH
         real(KIND = DP):: tmp_v
         integer:: i

         FoundV = .false.
         FoundRHLH = .false.
         do i = 1, size(args)
            ! Expect every iteration to parse something
            ierr = PARSE_ERROR

            ! Try parse v
            if (.not. FoundV) then
               call value(args(i), tmp_v, ierr)
               if (ierr .eq. 0) then
                  v = tmp_v
                  FoundV = .true.
                  ierr = PARSE_SUCCESS
                  cycle
               end if
            end if

            ! Try parse RH/LH
            if (.not. FoundRHLH) then
               select case (trim(lowercase(args(i))))
               case ('rh')
                  orientation = 'RH'
                  FoundRHLH = .true.
                  ierr = PARSE_SUCCESS
               case ('lh')
                  orientation = 'LH'
                  FoundRHLH = .true.
                  ierr = PARSE_SUCCESS
               end select
            end if

            if (ierr .eq. PARSE_ERROR) then
               write (*, *) "ERROR: Unable to parse argument ", trim(args(i))
               write (*, *) "ERROR: Expecting either REAL for 'v', or RH/LH for 'orientation'"
            end if
         end do

      end subroutine TryParseVAndRH

   end subroutine ParseVortexMagnetization

   !> Parse the `RandomSaturation` command
   !>
   !> @param[in]  args The arguments to the `RandomSaturation` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseRandomSaturation(args, ierr)
      use Utils, only: RandUnitVec
      use Material_Parameters, only: LambdaEx, hz
      implicit none
      !USE Utils

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      real(KIND = DP):: Vec(3)
      integer:: nargs, sd
      character(len = 100):: V1, V2, V3

      nargs = size(args)

      ierr = PARSE_ERROR

      ! Get a random unit vector and set the field
      call RandUnitVec(Vec)
      hz = Vec

      write (V1, '(F10.8)') Vec(1)
      write (V2, '(F10.8)') Vec(2)
      write (V3, '(F10.8)') Vec(3)

      print *, 'External field diretion = ', Vec

      if (nargs .gt. 1) then
         sd = ParseSubdomain(args(2:4), ierr)

         if (ierr .ne. PARSE_ERROR) then
            call setuniform(V1, V2, V3, ierr, sd = sd)
         end if

      else

         call setuniform(V1, V2, V3, ierr)

      end if

   end subroutine ParseRandomSaturation

   !> Parse the `EnergyLog` command
   !>
   !> @param[in]  args The arguments to the `EnergyLog` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEnergyLog(args, ierr)
      use strings
      use Utils, only: tstamp
      use Energy_Calculator, only: EnergyLogging, logfile, logunit
      use Tetrahedral_Mesh_Data, only: BFCE, BNODE, NNODE, NTRI, total_volume
      use Material_Parameters, only: extapp, kd, Ls, QHardness, LambdaEx, T, Ms, K1, LamdaS, SigmaS, Aex, hz
      use Mesh_IO, only: stem

      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      integer i

      ! Start logging the energy
      if (AssertNArgs(args, 2)) then
         stem = args(2)
         call compact(stem)
         logfile = stem(:len_trim(stem))//'.log'
         if (EnergyLogging) close (logunit)
         EnergyLogging = .true.
         open (NEWUNIT = logunit, FILE = logfile, STATUS='unknown')
         write (logunit, '(A)') &
            "Micromagnetic energy calculation : BEMScript 2014"
         write (logunit, '(A)') "Mesh data:"
         call tstamp(logunit)
         write (logunit, '(5A14)') &
            "Nodes", "Tetrahedra", "Bndry nodes", "Bndry faces", "Volume"
         write (logunit, '(4I14, E14.6)') &
            NNODE, NTRI, BNODE, BFCE, total_volume
         write (logunit, '(A)') "Material data:"
         do i = 1, size(Ms)
            if (size(Ms) .gt. 1) write (logunit, '(A15, I2)') "Sub Domain", i
            write (logunit, '(10A14)') &
               "Temp (c)", "Ms", "K1", "LamdaS", "SigmaS", "A_ex", "Vol^(1/3) (m)", &
               "QHardness", "Lam_Ex (nm)", &
               "Kd V (J)"
            write (logunit, '(i14, 9E14.6)') &
               int(T), Ms(i), K1(i), LamdaS(i), SigmaS(i), Aex(i), (total_volume**(1./3.))/sqrt(Ls), &
               QHardness, LambdaEx*1.d9, &
               Kd*total_volume/(sqrt(Ls)**3)
         end do
         write (logunit, '(A)') "External field direction (x, y, z) &
           &and strength B (T):"
         write (logunit, '(4A14)') "hx", "hy", "hz", "B (T)"
         write (logunit, '(4E14.6)') hz(1), hz(2), hz(3), extapp
         write (logunit, '(A)') "Energies in units of Kd V:"
         write (logunit, '(2A12, 1A20, 12A28)') &
            "Global-N-Eval", "N-Eval", "E-Anis", "E-Stress", "E-Exch1", "E-Exch2", &
            "E-Exch3", "E-Exch4", "E-ext", "E-Demag", &
            "E-Tot", "Mx", "My", "Mz"

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseEnergyLog

   !> Parse the `PathLogFile` command
   !>
   !> @param[in]  args The arguments to the `PathLogFile` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParsePathLogFile(args, ierr)
      use strings, only: compact
      use Utils, only: MERRILL_VERSION_STRING, tstamp
      use Mesh_IO, only: stem
      use Tetrahedral_Mesh_Data, only: BFCE, NTRI, BNODE, NNODE, total_volume
      use Material_Parameters, only: extapp, aex, ms, K1, kd, QHardness, SigmaS, LambdaEx, LamdaS, Ls, hz, ms, extapp
      use Magnetization_Path, only: PathLoggingQ, PathN, PLogEnFile, PLogGradFile, PLogDistFile, &
                                    PLogFile, PLogEnUnit, PLogGradUnit, PLogDistUnit
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! Start logging the energy
      if (AssertNArgs(args, 2)) then
         stem = args(2)
         call compact(stem)
         PLogEnFile = stem(:len_trim(stem))//'.enlog'
         PLogGradFile = stem(:len_trim(stem))//'.grlog'
         PLogDistFile = stem(:len_trim(stem))//'.dlog'
         PLogFile = stem(:len_trim(stem))//'-last.dat'
         if (PathLoggingQ) then
            close (PLogEnUnit)
            close (PLogGradUnit)
            close (PLogDistUnit)
            close (PLogDistUnit)
         end if
         PathLoggingQ = .true.
         open (NEWUNIT = PLogEnUnit, FILE = PLogEnFile, STATUS='unknown')
         open (NEWUNIT = PLogGradUnit, FILE = PLogGradFile, STATUS='unknown')
         open (NEWUNIT = PLogDistUnit, FILE = PLogDistFile, STATUS='unknown')
         write (PLogEnUnit, '(A, A)') "Micromagnetic path energies: ", &
            MERRILL_VERSION_STRING
         write (PLogEnUnit, '(A)') "Mesh data:"
         call tstamp(PLogEnUnit)
         write (PLogEnUnit, '(5A14)') "Nodes", "Tetrahedra", "Bndry nodes", &
            "Bndry faces", "Volume"
         write (PLogEnUnit, '(4I14, E14.6)') NNODE, NTRI, BNODE, BFCE, total_volume
         write (PLogEnUnit, '(A)') "Material data:"
         write (PLogEnUnit, '(9A14)') "Ms", "K1", "LamdaS", "SigmaS", "A_ex", "V^(1/3) (m)", &
            "QHardness", "Lam_Ex (nm)", "Kd V (J)"
         write (PLogEnUnit, '(7E14.6)') &
            Ms, K1, LamdaS, SigmaS, Aex, &
            (total_volume**(1./3.))/sqrt(Ls), QHardness, &
            LambdaEx*1.d9, Kd*total_volume/(sqrt(Ls)**3)
         write (PLogEnUnit, '(A)') "External field direction (x, y, z) &
           &and strength B (T):"
         write (PLogEnUnit, '(4A14)') "hx", "hy", "hz", "B (T)"
         write (PLogEnUnit, '(4E14.6)') hz(1), hz(2), hz(3), extapp
         write (PLogEnUnit, '(A13, I5)') "Path Length: ", PathN

         write (PLogEnUnit, *) "Path Energies in Kd V: "
         write (PLogGradUnit, '(A, A)') "Micromagnetic path energy gradient norms: ", &
            MERRILL_VERSION_STRING
         write (PLogDistUnit, '(A, A)') "Micromagnetic path cumulative distances: ", &
            MERRILL_VERSION_STRING

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParsePathLogFile

   !> Parse the `CloseLogFile` command
   !>
   !> @param[in]  args The arguments to the `CloseLogFile` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseCloseLogFile(args, ierr)
      use Energy_Calculator, only: EnergyLogging, logunit
      use Magnetization_Path, only: PathLoggingQ, PLogGradUnit, PLogDistUnit, PLogEnUnit
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! Stop logging the energy
      if (EnergyLogging) then
         call CloseIfOpen(logunit)
         EnergyLogging = .false.
      end if
      if (PathLoggingQ) then
         call CloseIfOpen(PLogEnUnit)
         call CloseIfOpen(PLogGradUnit)
         call CloseIfOpen(PLogDistUnit)
      end if
      PathLoggingQ = .false.

      ierr = PARSE_SUCCESS
   contains
      subroutine CloseIfOpen(unit)
         integer, intent(INOUT):: unit
         logical:: is_open
         is_open = .false.
         if (unit .ne. 0) inquire (UNIT = unit, OPENED = is_open)
         if (is_open) close (unit)

         unit = 0
      end subroutine CloseIfOpen
   end subroutine ParseCloseLogFile

   !> Parse the `MagnetizationToPath` command
   !>
   !> @param[in]  args The arguments to the `MagnetizationToPath` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseMagnetizationToPath(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: m
      use Magnetization_Path, only: PathN, PMag
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt
      integer:: ios

      ! Saves current magnetization to path index
      call value(args(2), cnt, ios)
      if (ios .eq. 0) then
         if (cnt < 0) cnt = PathN+1 - cnt
         if (cnt > 0 .and. cnt < (PathN+1)) PMag(cnt, :, :) = m(:, :)

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseMagnetizationToPath

   !> Parse the `PathToMagnetization` command
   !>
   !> @param[in]  args The arguments to the `PathToMagnetization` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParsePathToMagnetization(args, ierr)
      use strings
      use Magnetization_Path, only: PathN, PMag
      use Tetrahedral_Mesh_Data, only: m
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt
      integer:: ios

      ! Saves current magnetization to path index
      call value(args(2), cnt, ios)
      if (ios .eq. 0) then
         if (cnt < 0) cnt = PathN+1 - cnt
         if (cnt > 0 .and. cnt < (PathN+1)) m(:, :) = PMag(cnt, :, :)

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParsePathToMagnetization

   !> Parse the `RenewDist` command
   !>
   !> @param[in]  args The arguments to the `RenewDist` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParsePathRenewDist(args, ierr)
      use Magnetization_Path, only: PathRenewDist
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! defines the Path distances assuming all magnetizations are filled
      call PathRenewDist()

      ierr = PARSE_SUCCESS
   end subroutine ParsePathRenewDist

   !> Parse the `MakeInitialPath` command
   !>
   !> @param[in]  args The arguments to the `MakeInitialPath` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseMakeInitialPath(args, ierr)
      use Magnetization_Path, only: MakeInitialPath
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! defines the Path variables assuming all magnetizations are filled
      call MakeInitialPath()

      ierr = PARSE_SUCCESS
   end subroutine ParseMakeInitialPath

   !> Parse the `MakeInitialPathCart` command
   !>
   !> @param[in]  args The arguments to the `MakeInitialPath` command.
   !> @param[out] ierr The result of the parse
   !> @todo this forces Cartesian coords in EnergyMin
   subroutine ParseMakeInitialPathCart(args, ierr)
      use Material_Parameters, only: PathCoord
      use Magnetization_Path, only: MakeInitialPath
      implicit none

      character(len=*), intent(IN):: args(:)

      integer, intent(OUT):: ierr

      ! defines the Path variables assuming all magnetizations are filled
      PathCoord = "Conly"
      call MakeInitialPath()

      ierr = PARSE_SUCCESS
   end subroutine ParseMakeInitialPathCart

   !> Parse the `RefinePathTo` command
   !>
   !> @param[in]  args The arguments to the `RefinePathTo` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseRefinePathTo(args, ierr)
      use strings
      use Magnetization_Path, only: RefinePathTo
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: cnt
      integer:: ios

      ! Refines current path to new number of states
      call value(args(2), cnt, ios)
      if (ios .eq. 0) then
         if (cnt > 1) call RefinePathTo(cnt)

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseRefinePathTo

   !> Parse the `WriteTecplotPath` command
   !>
   !> @param[in]  args The arguments to the `WriteTecplotPath` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseWriteTecplotPath(args, ierr)

      use Strings, only: lowercase
      use Tetrahedral_mesh_data, only: packing
      use Magnetization_Path, only: PathOutFile, WriteTecplotPath
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer nargs
      packing = 'block' ! default to block format

      nargs = size(args)

!    print*, 'Nargs',nargs

      if (Nargs == 3) then  ! a packing format is specified
         PathOutFile = args(2)
         packing = lowercase(trim(args(3)))
!      Print*, 'SETTING WRITE PATH FORMAT To', packing

         call WriteTecplotPath()
         ierr = PARSE_SUCCESS
      end if

      if (Nargs == 2) then
         PathOutFile = args(2)
         call WriteTecplotPath()
!            Print*, 'PATH FORMAT assumed ', packing
         ierr = PARSE_SUCCESS
      end if

      if (Nargs .gt. 3) then
         ierr = PARSE_ERROR
      end if

!   print*,' packing in path parser= ', packing

   end subroutine ParseWriteTecplotPath

   !> Parse the `ReadTecplotPath` command
   !>
   !> @param[in]  args The arguments to the `ReadTecplotPath` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseReadTecplotPath(args, ierr)
      use Magnetization_Path, only: PathInFile
      use Mesh_IO, only: ReadTecplotPath
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! Imports path from a TecPlot file
      if (AssertNArgs(args, 2)) then
         PathInFile = args(2)
         call ReadTecplotPath()

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseReadTecplotPath

   !> Parse the `ReadTecplotZone` command
   !>
   !> @param[in]  args The arguments to the `ReadTecplotZone` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseReadTecplotZone(args, ierr)
      use strings
      use Material_Parameters, only: LambdaEx, LamdaS, Ls, izone
      use Magnetization_Path, only: ZoneInFile, ReadTecplotZone, WriteTecplotPath
      use Tetrahedral_Mesh_Data, only: saved_mesh_number, SaveMAGstate
      use Mesh_IO, only: ReadTecplotPath
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      integer:: ios, cnt, Zone_num, nargs, nargs2, i
      logical:: l
      character(len = 256):: opt_args(15), opt_key(15), opt_val(15)
      character(len = 20):: key_val, FMT

      ! Imports a single zone Izone from a TecPlot file and
      ! saves the state to associated mesh number cnt
      ierr = PARSE_SUCCESS
      FMT = "(A5, I3, A23, I3)"

      nargs = size(args)
!***** set default fro some options
      izone = 1  ! assume we are reading the first zone of file
      cnt = -1  !  Negative number means data will not be saved for later recal
      ZoneInFile = ""

      do i = 2, nargs  ! skip i = 1  arg since that is the readTecplotZone command
         key_val = trim(lowercase(args(i)))
         call parse(args(i), '=', opt_args, nargs2)
         opt_key(i-1) = trim(lowercase(opt_args(1)))
         opt_val(i-1) = trim(opt_args(2))

      end do

      ! set the options to the apprproate variables
      do i = 2, nargs

         select case (opt_key(i-1))

         case ("zone")
            call value(opt_val(i-1), izone, ios)

         case ("mesh")
            call value(opt_val(i-1), cnt, ios)

         case ("filename")
            ZoneInFile = trim(opt_val(i-1))

         case DEFAULT
            write (*, *) '**********************'
            write (*, *) ' Unknown ReadTecplotZone Option Set : ', opt_key(i-1)
            write (*, *) ' Consult manual for allowed options'
            write (*, *) ' Stopping Programme'
            stop

         end select
         ierr = PARSE_ERROR

      end do

      write (*, *) 'Reading file ', ZoneInFile
      if (cnt .eq. -1) then
         write (*, FMT) 'Zone ', izone, ' linked to saved mesh for later use ', cnt
      end if

      if (ZoneInFile .eq. "") then
         write (*, *) ' No tecplot filename given'
         write (*, *) ' Stopping'
      else
         call ReadTecplotZone()
      end if

      ! save the magnetiation state
      if (cnt .ge. 1) then
         call SaveMagState(cnt)
         saved_mesh_number = cnt
         write (*, '(A42, I3)') 'Data loaded, and assocaited with mesh :', cnt
      end if

      ierr = PARSE_SUCCESS
   end subroutine ParseReadTecplotZone

   !> Parse the `PathStructureEnergyComponents` command
   !>
   !> @param[in]  args The arguments to the `PathStructureEnergies` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParsePathStructureEnergycomponents(args, ierr)
      use Tetrahedral_Mesh_Data, only: SubDomainIds
      use Magnetization_Path, only: PathN, structureEnergy
      use Material_Parameters, only: NMaterials, Ls
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs

      real(KIND = DP):: dbltmp(NMaterials+1)
      integer:: i
      integer:: pseunit

      nargs = size(args)

      ! Output the energies for the path
      if (nargs == 2) then
         ! Write the path energies to a file.
         open (NEWUNIT = pseunit, FILE = args(2), STATUS='unknown')
         write (pseunit, '(A20, A30, *(I30))') 'Image_Number', 'Total', SubDomainIds(:)
         do i = 1, pathn
            dbltmp = structureEnergy(i)
            ! Normalize energy by length scale cubed to give Joule.
            dbltmp = dbltmp/sqrt(Ls)**3
            write (pseunit, '(I20, *(E30.15E5))') i, dbltmp(:)
         end do
         close (pseunit)

         ierr = PARSE_SUCCESS
      else if (nargs == 1) then
         ! Write the path energies to standard output.
         write (*, *) 'writing energies to screen', pathn
         ! do i = 1, NMaterials+1
         ! dbltmp = structureEnergy(i)
         ! enddo

         do i = 1, pathn
            dbltmp = structureEnergy(i)
            ! Normalize energy by length scale cubed to give Joule.
            dbltmp(:) = dbltmp(:)/sqrt(Ls)**3
            write (*, '(I20, *(E30.15E5))') i, dbltmp(:)
         end do

         ierr = PARSE_SUCCESS
      else
         write (*, *) "Command '", trim(args(1)), "' expected 1 or 2 arguments. &
           &Got ", nargs
         ierr = PARSE_ERROR
      end if
   end subroutine ParsePathStructureEnergyComponents

   function my_string(inum) result(mystring)
      implicit none
      integer, intent(in)  :: inum

      character*6 mystring

      if (inum .gt. 999) write (*, *) ' Number of subdomains exceeded. function my_string, command_paser.f90'
      if (inum .lt. 10) then
         write (mystring, '(I1)') inum
      else if (inum .lt. 100) then
         write (mystring, '(I2)') inum
      else if (inum .lt. 1000) then
         write (mystring, '(I3)') inum
      end if

!     print*, 'mystring', mystring
   end function my_string

   !> Parse the `PathStructureAllComponents` command
   !>
   !> @param[in]  args The arguments to the `PathStructureEnergies` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParsePathStructureAllComponents(args, ierr)
      use Material_Parameters, only: NMaterials, Ls
      use Magnetization_Path, only: PathN, structureEnergy, structureEnergyALL
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs
      character*6, HEADER(6)
      character*6, mynewstring
      real(KIND = DP):: dbltmp(6, NMaterials+1)
      integer:: i, j, k
      integer:: pseunit

      nargs = size(args)

      Header(1) = 'Total_'
      Header(2) = 'Demag_'
      Header(3) = 'Exchg_'
      Header(4) = 'Anist_'
      Header(5) = 'Bextn_'
      Header(6) = 'Stres_'

      ! Output the energies for the path

      if (nargs == 2) then
         ! Write the path energies to a file.

         open (NEWUNIT = pseunit, FILE = args(2), STATUS='unknown')

         write (pseunit, '(A20, *(A30))') 'Image_number',&
            & ((header(mod((k), 7))//my_string(j-1), k = 1, 6), j = 1, NMaterials+1)
         do i = 1, pathn
            !dbltmp = structureEnergy(i)
            dbltmp = structureEnergyALL(i)
            ! Normalize energy by length scale cubed to give Joule.
            dbltmp = dbltmp/sqrt(Ls)**3
            write (pseunit, '(I20, *(E30.15E5))') i, ((dbltmp(k, j), k = 1, 6), j = 1, NMaterials+1)
         end do
         close (pseunit)

         ierr = PARSE_SUCCESS
      else if (nargs == 1) then
         ! Write the path energies to standard output.
         write (*, *) 'writing energies to screen', pathn
         do i = 1, NMaterials+1
            dbltmp = structureEnergyALL(i)
         end do

         do i = 1, pathn
            ! Normalize energy by length scale cubed to give Joule.
            dbltmp = dbltmp/sqrt(Ls)**3
            write (*, '(I20, *(E30.15E5))') i, ((dbltmp(k, j), k = 1, 6), j = 1, NMaterials+1)
         end do

         ierr = PARSE_SUCCESS
      else
         write (*, *) "Command '", trim(args(1)), "' expected 1 or 2 arguments. &
           &Got ", nargs
         ierr = PARSE_ERROR
      end if
   end subroutine ParsePathStructureAllComponents

   !> Parse the `PathStructureEnergies` command
   !>
   !> @param[in]  args The arguments to the `PathStructureEnergies` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParsePathStructureEnergies(args, ierr)
      use Material_Parameters, only: NMaterials, Ls
      use Magnetization_Path, only: PathN, structureEnergy
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs

      real(KIND = DP):: dbltmp(NMaterials+1)
      integer:: i
      integer:: pseunit

      nargs = size(args)

      ! Output the energies for the path
      if (nargs == 2) then
         ! Write the path energies to a file.
         open (NEWUNIT = pseunit, FILE = args(2), STATUS='unknown')

         do i = 1, pathn
            dbltmp = structureEnergy(i)
            ! Normalize energy by length scale cubed to give Joule.
            dbltmp = dbltmp/sqrt(Ls)**3
            write (pseunit, '(I20, *(E30.15E5))') i, dbltmp(1)
         end do
         close (pseunit)

         ierr = PARSE_SUCCESS
      else if (nargs == 1) then
         ! Write the path energies to standard output.
         write (*, *) 'writing energies to screen', pathn

         do i = 1, pathn
            dbltmp = structureEnergy(i)
            ! Normalize energy by length scale cubed to give Joule.
            dbltmp(:) = dbltmp(:)/sqrt(Ls)**3
            write (*, '(I20, *(E30.15E5))') i, dbltmp(1)
         end do

         ierr = PARSE_SUCCESS
      else
         write (*, *) "Command '", trim(args(1)), "' expected 1 or 2 arguments. &
           &Got ", nargs
         ierr = PARSE_ERROR
      end if
   end subroutine ParsePathStructureEnergies

   !> Parse the `WritePathStructures` command
   !>
   !> @param[in]  args The arguments to the `WritePathStructures` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseWritePathStructures(args, ierr)
      use Magnetization_Path, only: PathN
      use Mesh_IO, only: stem
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: i

      ! Writes each structure as a separate tecplot file.
      do i = 1, pathn
         write (*, *) "it's the stem: ", stem
      end do

      ierr = PARSE_SUCCESS
   end subroutine ParseWritePathStructures

   !> Parse the `Energy` command
   !>
   !> @param[in]  args The arguments to the `Energy` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseEnergy(args, ierr)
      use Utils, only: NONZERO
      use Material_Parameters, only: Ls
      use Magnetization_Path, only: currentMagEnergy
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: nargs

      real(KIND = DP):: dbltmp
      integer:: i

      nargs = size(args)

      dbltmp = currentMagEnergy()
      dbltmp = dbltmp/sqrt(Ls)**3
      write (*, '(E30.15E5)') dbltmp

      ierr = PARSE_SUCCESS
   end subroutine ParseEnergy

   !> Parse the `SDEnergySurface' command
   !>
   !> @param[in]  args The arguments to the `SDEnergySurface` command.
   !> @param[out] ierr The result of the parse
   !> @todo Tidy up the code
   subroutine ParseSDEnergySurface(args, ierr)
      use strings
      use Mesh_IO, only: stem
      use Energy_Calculator, only: SDEnergySurface
      implicit none
      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr
      character(len = 20):: key_val, FMT1, FMT2
      character(len = 55):: opt_args(15), opt_key(15), opt_val(15), Surf_filename
      integer:: ios, nargs, nargs2, i
      double precision:: theta_min, theta_max, theta_delta, phi_min, &
      & phi_max, phi_delta

      FMT1 = "(A55, ES15.5)"
      FMT2 = "(A35, ES15.5)"
      nargs = size(args)
      stem = ""

! **** SET DEFAULT values for parameters. Set negative to force user values
! **** theta is the polar angle, phi is the azimuthal angle (input is in degrees)
      theta_min = 0
      theta_max = 180
      theta_delta = 1
      phi_min = 0
      phi_max = 359
      phi_delta = 1
      stem = ""

      ! split options into key-value pairs
      !print*, 'number of args= ',size(args)

      do i = 2, nargs  ! skip i = 1 since that is the SDEnergySurface command
         key_val = trim(lowercase(args(i)))
         call parse(args(i), '=', opt_args, nargs2)
         opt_key(i-1) = trim(lowercase(opt_args(1)))
         opt_val(i-1) = trim(lowercase(opt_args(2)))

      end do

      ! set the options to the apprproate variables
      !if (nargs .ge. 2)  write(*,*) 'Setting SD Energy Surafce parameters:'
      do i = 2, nargs

         select case (opt_key(i-1))

         case ("thetamin")
            call value(opt_val(i-1), theta_min, ios)
            write (*, FMT2) 'Theta minimum = ', theta_min

         case ("thetamax")
            call value(opt_val(i-1), theta_max, ios)
            write (*, FMT2) 'Theta maximum = ', theta_max

         case ("deltatheta")
            call value(opt_val(i-1), theta_delta, ios)
            write (*, FMT2) 'Theta Step Length = ', theta_delta

         case ("phimin")
            call value(opt_val(i-1), phi_min, ios)
            write (*, FMT2) 'Phi minimum = ', phi_min

         case ("phimax")
            call value(opt_val(i-1), phi_max, ios)
            write (*, FMT2) 'Phi maximum = ', phi_max

         case ("deltaphi")
            call value(opt_val(i-1), phi_delta, ios)
            write (*, FMT2) 'Theta Step Length = ', phi_delta

         case ("filename")
            stem = opt_val(i-1)
            call compact(stem)
            stem = stem(:len_trim(stem))
            !write(*,*) 'Output datafile = ', stem

         case DEFAULT
            write (*, *) '**********************'
            write (*, *) ' Unknown ParseSDEnergySurface Option Set : ', opt_key(i-1)
            write (*, *) ' Consult manual for allowed options'
            write (*, *) ' Stopping Programme'
            ierr = PARSE_ERROR
            stop

         end select

         ierr = PARSE_SUCCESS

      end do

      !  check filename is set
      if (stem .eq. "") then
         write (*, *) '**********************'
         write (*, *) ' No Filename set for command SDEnergySurface'
         write (*, *) ' Stopping Programme'
         stop
      end if
      Surf_filename = stem(:len_trim(stem))//'_surf.dat'

      call SDEnergySurface(theta_min, theta_max, theta_delta, phi_min, &
      & phi_max, phi_delta, Surf_filename)

   end subroutine ParseSDEnergySurface

   !> Parse the `LoadPlugin` command
   !>
   !> @param[in]  args The arguments to the `LoadPlugin` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseLoadPlugin(args, ierr)
      use Plugins
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      ! Load plugin
      if (AssertNArgs(args, 2)) then
         call LoadPlugin(args(2))

         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseLoadPlugin

   !> Parse the `GenerateCubeMesh` command
   !>
   !> @param[in]  args The arguments to the `GenerateCubeMesh` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseGenerateCubeMesh(args, ierr)
      use Mesh_IO
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: width, edge_length
      integer:: ios

      ierr = PARSE_SUCCESS

      ! Ensure usage: GenerateCubeMesh width edge_length
      if (AssertNArgs(args, 3)) then
         if (ierr .ne. PARSE_ERROR) then
            call value(args(2), width, ios)
            if (ios .ne. 0) ierr = PARSE_ERROR
         end if

         if (ierr .ne. PARSE_ERROR) then
            call value(args(3), edge_length, ios)
            if (ios .ne. 0) ierr = PARSE_ERROR
         end if

         if (ierr .ne. PARSE_ERROR) then
            call GenerateCubeMesh(width, edge_length)
         end if
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseGenerateCubeMesh

   !> Parse the `GenerateSphereMesh` command
   !>
   !> @param[in]  args The arguments to the `GenerateSphereMesh` command.
   !> @param[out] ierr The result of the parse
   !> @todo DOCUMENT ME
   subroutine ParseGenerateSphereMesh(args, ierr)
      use Mesh_IO
      use strings
      implicit none

      character(len=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      real(KIND = DP):: width, edge_length
      integer:: ios

      ierr = PARSE_SUCCESS

      ! Ensure usage: GenerateCubeMesh width edge_length
      if (AssertNArgs(args, 3)) then
         if (ierr .ne. PARSE_ERROR) then
            call value(args(2), width, ios)
            if (ios .ne. 0) ierr = PARSE_ERROR
         end if

         if (ierr .ne. PARSE_ERROR) then
            call value(args(3), edge_length, ios)
            if (ios .ne. 0) ierr = PARSE_ERROR
         end if

         if (ierr .ne. PARSE_ERROR) then
            call GenerateSphereMesh(width, edge_length)
         end if
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseGenerateSphereMesh

   !---------------------------------------------------------------
   !     subroutine setanis(anis)
   !---------------------------------------------------------------

   !> Set the anisotropy of subdomain \c sd based on the name of the
   !> anisotropy type in \c anis.
   !>
   !> @param[in]  anis The type of anisotropy form to set
   !> @param[out] ierr The result of parsing and setting
   !> @param[in]  sd   The subdomain to set the anisotropy for.
   !>                  If omitted, every subdomain is set to the given
   !>                  anisotropy.
   subroutine setanis(anis, ierr, sd)
      use strings
      use Material_Parameters, only: anisform, ANISFORM_CUBIC, ANISFORM_HEX_PYRR, &
                                     ANISFORM_MONOCLINIC, ANISFORM_MONOCLINIC_PYRR, ANISFORM_UNIAXIAL
      implicit none

      character(LEN=*), intent(IN):: anis
      integer, intent(OUT):: ierr
      integer, optional, intent(IN):: sd

      select case (lowercase(trim(anis)))
      case ('monoclinic', 'mono')
         write (*, *) "Anisotropy Form: Monoclinic Magnetite"
         if (present(sd)) then
            anisform(sd) = ANISFORM_MONOCLINIC
         else
            anisform(:) = ANISFORM_MONOCLINIC
         end if
         ierr = PARSE_SUCCESS

      case ('monoclinicpyrrhotite', 'monopyrr')
         write (*, *) "Anisotropy Form: Uniaxial Monoclinic Pyrrhotite"
         if (present(sd)) then
            anisform(sd) = ANISFORM_MONOCLINIC_PYRR
         else
            anisform(:) = ANISFORM_MONOCLINIC_PYRR
         end if
         ierr = PARSE_SUCCESS

      case ('hexpyrrhotite', 'hexpyrr')
         write (*, *) "Anisotropy Form: Hex Monoclinic Pyrrhotite"
         if (present(sd)) then
            anisform(sd) = ANISFORM_HEX_PYRR
         else
            anisform(:) = ANISFORM_HEX_PYRR
         end if
         ierr = PARSE_SUCCESS

      case ('cubic', 'cubicanisotropy')
         write (*, *) "Anisotropy Form: Cubic"
         if (present(sd)) then
            anisform(sd) = ANISFORM_CUBIC
         else
            anisform(:) = ANISFORM_CUBIC
         end if
         ierr = PARSE_SUCCESS

      case ('uniaxial', 'uni')
         write (*, *) "Anisotropy Form: Uniaxial"
         if (present(sd)) then
            anisform(sd) = ANISFORM_UNIAXIAL
         else
            anisform(:) = ANISFORM_UNIAXIAL
         end if
         ierr = PARSE_SUCCESS

      case DEFAULT
         write (*, *) "Unknown Anisotropy :", trim(anis)
         ierr = PARSE_ERROR
      end select
   end subroutine setanis

   !---------------------------------------------------------------
   !     subroutine seteasyaxis(args(3), args(4), args(5))
   !---------------------------------------------------------------

   !> Set the easy axis of the material for the subdomain \c sd.
   !>
   !> @param[in]  ax   The x-component of the easy axis.
   !> @param[in]  ay   The y-component of the easy axis.
   !> @param[in]  az   The z-component of the easz axis.
   !> @param[out] ierr The result of the parsing and setting.
   !> @param[in]  sd   The subdomain to set the easy axis for. If omitted, 
   !>                  the easy axis is set for all subdomains.
   subroutine seteasyaxis(ax, ay, az, ierr, sd)
      use Utils, only: NONZERO
      use strings
      use Material_Parameters, only: EasyAxis, NMaterials
      implicit none

      character(LEN=*), intent(IN):: ax, ay, az
      integer, intent(OUT):: ierr
      integer, optional, intent(IN):: sd

      integer:: ios
      real(KIND = DP) anix, aniy, aniz
      real(KIND = DP) eanorm

      integer:: i

      ierr = PARSE_SUCCESS

      call value(ax, anix, ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting anisotropy x: ', ax
         ierr = PARSE_ERROR
      end if
      call value(ay, aniy, ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting anisotropy y: ', ay
         ierr = PARSE_ERROR
      end if
      call value(az, aniz, ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting anisotropy z: ', az
         ierr = PARSE_ERROR
      end if

      if (present(sd)) then
         EasyAxis(1, sd) = anix
         EasyAxis(2, sd) = aniy
         EasyAxis(3, sd) = aniz
      else
         EasyAxis(1, :) = anix
         EasyAxis(2, :) = aniy
         EasyAxis(3, :) = aniz
      end if

      do i = 1, NMaterials
         eanorm = sqrt(EasyAxis(1, i)**2+EasyAxis(2, i)**2+EasyAxis(3, i)**2)
         if (NONZERO(eanorm)) then
            EasyAxis(1, i) = EasyAxis(1, i)/eanorm
            EasyAxis(2, i) = EasyAxis(2, i)/eanorm
            EasyAxis(3, i) = EasyAxis(3, i)/eanorm
         else
            write (*, *) "Easy Axis is Zero!"
            ierr = PARSE_ERROR
         end if
      end do

   end subroutine seteasyaxis

   !---------------------------------------------------------------
   !     subroutine setstressaxis(args(3), args(4), args(5))
   !---------------------------------------------------------------

   !> Set the stress axis of the material for the subdomain \c sd.
   !>
   !> @param[in]  ax   The x-component of the stress axis.
   !> @param[in]  ay   The y-component of the stress axis.
   !> @param[in]  az   The z-component of the stress axis.
   !> @param[out] ierr The result of the parsing and setting.
   !> @param[in]  sd   The subdomain to set the stress axis for. If omitted, 
   !>                  the stress axis is set for all subdomains.
   subroutine setstressaxis(sx, sy, sz, ierr, sd)
      use Utils, only: NONZERO
      use strings
      use Material_Parameters, only: StressAxis, NMaterials
      implicit none

      character(LEN=*), intent(IN):: sx, sy, sz
      integer, intent(OUT):: ierr
      integer, optional, intent(IN):: sd

      integer:: ios
      real(KIND = DP) stressx, stressy, stressz
      real(KIND = DP) stressnorm

      integer:: i

      ierr = PARSE_SUCCESS

      call value(sx, stressx, ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting stress x: ', sx
         ierr = PARSE_ERROR
      end if
      call value(sy, stressy, ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting stress y: ', sy
         ierr = PARSE_ERROR
      end if
      call value(sz, stressz, ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting stress z: ', sz
         ierr = PARSE_ERROR
      end if

      if (present(sd)) then
         StressAxis(1, sd) = stressx
         StressAxis(2, sd) = stressy
         StressAxis(3, sd) = stressz
      else
         StressAxis(1, :) = stressx
         StressAxis(2, :) = stressy
         StressAxis(3, :) = stressz
      end if

      do i = 1, NMaterials
         stressnorm = sqrt(StressAxis(1, i)**2+StressAxis(2, i)**2+StressAxis(3, i)**2)
         if (NONZERO(stressnorm)) then
            StressAxis(1, i) = StressAxis(1, i)/stressnorm
            StressAxis(2, i) = StressAxis(2, i)/stressnorm
            StressAxis(3, i) = StressAxis(3, i)/stressnorm
         else
            write (*, *) "Stress Axis is Zero!"
            ierr = PARSE_ERROR
         end if
      end do

   end subroutine setstressaxis

   !---------------------------------------------------------------
   !     subroutine setfibonaccistressaxis(args(2), args(3), args(4))
   !---------------------------------------------------------------

   !> Set the stress axis of the material for the subdomain \c sd.
   !>
   !> @param[in]  si   the i_th Finonacci direction
   !> @param[in]  sn   The total number of Fibonacci directions
   !> @param[out] ierr The result of the parsing and setting.
   !> @param[in]  sd   The subdomain to set the stress axis for. If omitted, 
   !>                  the stress axis is set for all subdomains.
   SUBROUTINE setfibonaccistressaxis(si, sn, ierr, sd)
      use Utils, only: NONZERO, Fibonacci_Direction
      use strings
      use Material_Parameters, only: StressAxis, NMaterials

      CHARACTER(LEN=*), INTENT(IN):: si, sn
      INTEGER, INTENT(OUT):: ierr
      INTEGER, OPTIONAL, INTENT(IN):: sd

      INTEGER:: ios
      REAL(KIND = DP) stressx, stressy, stressz
      REAL(KIND = DP) stressnorm

      INTEGER:: i, Fib_N, Fib_i

      ierr = PARSE_SUCCESS

      CALL value(si, Fib_i, ios)
      IF (ios /= 0) THEN
         WRITE (*, *) 'ERROR setting current Fibonacci directions: ', si
         ierr = PARSE_ERROR
      END IF
      CALL value(sn, Fib_N, ios)
      IF (ios /= 0) THEN
         WRITE (*, *) 'ERROR setting Max number of Fibonacci directions: ', sN
         ierr = PARSE_ERROR
      END IF

      CALL Fibonacci_Direction(Fib_i, Fib_N, stressx, stressy, stressz)
      write (*, *) 'Setting stress axis to ', stressx, stressy, stressz

      IF (PRESENT(sd)) THEN
         StressAxis(1, sd) = stressx
         StressAxis(2, sd) = stressy
         StressAxis(3, sd) = stressz
      ELSE
         StressAxis(1, :) = stressx
         StressAxis(2, :) = stressy
         StressAxis(3, :) = stressz
      END IF

      DO i = 1, NMaterials
         stressnorm = SQRT(StressAxis(1, i)**2+StressAxis(2, i)**2+StressAxis(3, i)**2)
         IF (NONZERO(stressnorm)) THEN
            StressAxis(1, i) = StressAxis(1, i)/stressnorm
            StressAxis(2, i) = StressAxis(2, i)/stressnorm
            StressAxis(3, i) = StressAxis(3, i)/stressnorm
         ELSE
            WRITE (*, *) "Stress Axis is Zero!"
            ierr = PARSE_ERROR
         END IF
      END DO

   END SUBROUTINE setfibonaccistressaxis

   !---------------------------------------------------------------
   !     subroutine setfield(args(3), args(4), args(5), args(6))
   !---------------------------------------------------------------

   !> Set the external/Zeeman field.
   !>
   !> @param[in]  str The mode of this function.
   !>                 If the value is 'direction', the `shx`, `shy`, and `shz`
   !>                 parameters will be used to set the unit direction of the
   !>                 Zeeman field.
   !>                 If the value is 'strength', the `shx` parameter will be
   !>                 used as the value, and `shy` will be used as the units, 
   !>                 effectively a scaling of the strength.
   !> @param[in]  shx When `str == 'direction'`, this is the x-component of the
   !>                 unit direction of the field.
   !>                 When `str == 'strength'`, this is the strength of the
   !>                 field.
   !> @param[in]  shy When `str == 'direction'`, this is the y-component of the
   !>                 unit direction of the field.
   !>                 When `str == 'strength'`, this is the units used to define
   !>                 the field strength.
   !> @param[in]  shz Only used when `str == 'direction'`. This is the
   !>                 z-component of the unit direction of the field.
   !> @param[out] ierr The result of the parse and the set.
   subroutine setfield(str, shx, shy, shz, ierr)
      use strings, only: lowercase, value
      use Utils, only: NONZERO, Fibonacci_Direction
      use Material_Parameters, only: hz, extapp, NMaterials
      implicit none

      character(LEN = 100):: str, shx, shy, shz, lcshy
      integer:: ios
      real(KIND = DP):: dh(3)
      real(KIND = DP):: hznorm

      integer:: ierr, Fib_i, Fib_N

      ierr = NO_PARSE

      select case (lowercase(str))
      case ('direction')
         call value(shx, dh(1), ios)
         if (ios /= 0) then
            write (*, *) 'ERROR setting external field x: ', shx
            ierr = PARSE_ERROR
         end if
         call value(shy, dh(2), ios)
         if (ios /= 0) then
            write (*, *) 'ERROR setting external field y: ', shy
            ierr = PARSE_ERROR
         end if
         call value(shz, dh(3), ios)
         if (ios /= 0) then
            write (*, *) 'ERROR setting external field z: ', shz
            ierr = PARSE_ERROR
         end if

         if (ierr .ne. PARSE_ERROR) then
            hz(:) = dh(:)
            hznorm = sqrt(hz(1)**2+hz(2)**2+hz(3)**2)
            if (NONZERO(hznorm)) then
               hz(1) = hz(1)/hznorm
               hz(2) = hz(2)/hznorm
               hz(3) = hz(3)/hznorm

               ierr = PARSE_SUCCESS
            else
               write (*, *) "Error: |H| = 0!"
               ierr = PARSE_ERROR
            end if
         end if

      case ('strength')
         call value(shx, dh(1), ios)
         if (ios == 0) then
            lcshy = lowercase(shy)

            ! force check on field strength units
            if (lcshy .eq. 't' .or. lcshy .eq. 'mt' .or. lcshy .eq. 'mut') then
               if (lcshy .eq. 'mt') dh(1) = dh(1)/1000.
               if (lcshy .eq. 'mut') dh(1) = dh(1)/1.e6

               extapp = dh(1)

               ierr = PARSE_SUCCESS
            else
               if (dh(1) .ne. 0) write (*, *) "Error: External field units not set!"
               if (dh(1) .ne. 0) ierr = PARSE_ERROR
               if (dh(1) .eq. 0) ierr = PARSE_SUCCESS
            end if

         else
            write (*, *) "Error: Invalid field strength!"
            ierr = PARSE_ERROR
         end if

      case ('fibonacci')
         call value(shx, Fib_i, ios)  ! the ith direction
         if (ios /= 0) then
            write (*, *) 'ERROR setting ith direction i: ', shx
            ierr = PARSE_ERROR
         end if

         call value(shy, Fib_N, ios)  ! the total number
         if (ios /= 0) then
            write (*, *) 'ERROR setting total directions N: ', shy
            ierr = PARSE_ERROR
         end if

         ! Check i >= 1, N >= 1, i <= N
         if (Fib_i .lt. 1) then
            write (*, *) 'ERROR ith direction should be >= 1; i: ', shx
            ierr = PARSE_ERROR
         end if
         if (Fib_i .lt. 1) then
            write (*, *) 'ERROR N direction should be >= 1; N: ', shy
            ierr = PARSE_ERROR
         end if
         if (Fib_i .gt. Fib_N) then
            write (*, *) 'ERROR N direction should be large than ith; i, N: ', shx, shy
            ierr = PARSE_ERROR
         end if

         if (ierr .ne. PARSE_ERROR) then
            call Fibonacci_Direction(Fib_i, Fib_N, hz(1), hz(2), hz(3))
            write (*, *) 'Field set to: ', hz
            ierr = PARSE_SUCCESS

         end if

      case DEFAULT
         write (*, *) "Error: unknown command"
         ierr = PARSE_ERROR
      end select
   end subroutine setfield

   !---------------------------------------------------------------
   !     subroutine setuniform(args(3), args(4), sd, args(6))
   !---------------------------------------------------------------

   !> Set a uniform magnetization
   !>
   !> @param[in]  smx  The x-component of the magnetization.
   !> @param[in]  smy  The y-component of the magnetization.
   !> @param[in]  smz  The z-component of the magnetization.
   !> @param[in]  bstr The block to set the magnetization for. If 0, all
   !>                  blocks are set.
   !> @param[out] ierr The result of the parse and set.
   subroutine setuniform(smx, smy, smz, ierr, sd)
      use strings, only: value
      use Utils, only: NONZERO
      use Tetrahedral_Mesh_Data, only: NTRI, TetSubDomains, TIL, NNODE, m
      implicit none

      character(LEN = 100):: smx, smy, smz
      integer, optional, intent(IN):: sd
      integer:: ios, i, k, block
      real(KIND = DP):: dm(3), dmnorm

      integer:: ierr

      ierr = NO_PARSE

      call value(smx, dm(1), ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting magnetization x: ', smx
         ierr = PARSE_ERROR
      end if
      call value(smy, dm(2), ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting magnetization y: ', smy
         ierr = PARSE_ERROR
      end if
      call value(smz, dm(3), ios)
      if (ios /= 0) then
         write (*, *) 'ERROR setting magnetization z: ', smz
         ierr = PARSE_ERROR
      end if

      if (present(sd)) then

         if (ierr .ne. PARSE_ERROR) then
            dmnorm = sqrt(dm(1)**2+dm(2)**2+dm(3)**2)
            if (NONZERO(dmnorm)) then
               dm(1) = dm(1)/dmnorm
               dm(2) = dm(2)/dmnorm
               dm(3) = dm(3)/dmnorm
            end if
            do i = 1, NTRI
               ! set magnetization if in the subdomain
               if (TetSubDomains(i) .eq. sd) then
               do k = 1, 4  ! nodes per tet
                  m(Til(i, k), :) = dm(:)
               end do
               end if
            end do

            ierr = PARSE_SUCCESS
         else
            ierr = PARSE_ERROR
         end if

      else

         if (ierr .ne. PARSE_ERROR) then
            dmnorm = sqrt(dm(1)**2+dm(2)**2+dm(3)**2)
            if (NONZERO(dmnorm)) then
               dm(1) = dm(1)/dmnorm
               dm(2) = dm(2)/dmnorm
               dm(3) = dm(3)/dmnorm

               do i = 1, NNODE
                  ! set magnetization if in the right block
                  ! block = 0 means all magnetizations
                  m(i, :) = dm(:)
               end do

               ierr = PARSE_SUCCESS
            else
               ierr = PARSE_ERROR
            end if
         end if

      end if

   end subroutine setuniform

   !> Parse 'sd = XXX' from the list of `args` passed in.
   !> @param[in]  args A list of arguments.
   !> @param[out] ierr The result of the parse.
   !> @returns The value `XXX` of the parsed subdomain, if a subdomain was
   !>          found.
   integer function ParseSubdomain(args, ierr)
      use strings
      use Tetrahedral_Mesh_Data, only: SubDomainIds
      implicit none

      character(LEN=*), intent(IN):: args(:)
      integer, intent(OUT):: ierr

      integer:: i

      ParseSubdomain = huge(ParseSubdomain)

      if (size(args) .ne. 3) then
         ierr = PARSE_ERROR
         return
      end if

      ! Ensure format ... sd = n
      if ( &
         lowercase(trim(args(1))) .eq. "sd" &
         .and. &
         trim(args(2)) .eq. "=" &
         ) then

         ! Set sd
         call value(args(3), ParseSubdomain, ierr)
         if (ierr .ne. 0) then
            write (*, *) "Error parsing subdomain id: ", trim(args(3))
            ierr = PARSE_ERROR
            return
         end if

         ! Change subdomain to subdomain index
         do i = 1, size(SubDomainIds)
            if (ParseSubdomain .eq. SubDomainIds(i)) then
               ParseSubdomain = i
               ierr = PARSE_SUCCESS
               return
            end if

            if (i .eq. size(SubDomainIds)) then
               write (*, *) "Error: Unknown subdomain: ", ParseSubdomain
               ierr = PARSE_ERROR
               return
            end if
         end do
      else
         write (*, *) "Expected 'sd = ...' specifier."
         ierr = PARSE_ERROR
      end if

      ierr = PARSE_ERROR
   end function ParseSubdomain

end module Command_Parser
