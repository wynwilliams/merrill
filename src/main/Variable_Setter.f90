!> This module provides interfaces for exposing to and setting variables from
!> the MScript language.
module Variable_Setter
   use Utils, only: DP
   implicit none

   !> A type representing a Variable in MScript and functions for setting
   !> and updating the equivalent object in Fortran.
   type :: VariableSetter
      !> The name of the variable
      character(len=200) :: name

      !> The type of the variable
      integer :: variable_type

      !> A pointer to an Integer variable
      integer, pointer :: integer_ptr

      !> A pointer to a Real variable
      real(KIND=DP), pointer :: real_ptr

      !> A pointer to a function that sets a Real value
      procedure(RealFunction), pointer, nopass :: real_function => null()

      !> A pointer to a Character variable
      character(len=1024), pointer :: character_ptr

      !> A pointer to a Logical variable
      logical, pointer :: logical_ptr

      !> The callback to call when the variable is set.
      procedure(SetVariableCallback), pointer, pass :: callback
   end type VariableSetter

   ! A list of values representing the type of variable held by the
   ! VariableSetter

   !> A variablesetter.variable_type value: No variable type set
   integer, parameter :: UNSET_VARIABLE = 0

   !> A variablesetter.variable_type value: An Integer variable
   integer, parameter :: INTEGER_VARIABLE = 1

   !> A variablesetter.variable_type value: A Real variable
   integer, parameter :: REAL_VARIABLE = 2

   !> A variablesetter.variable_type value: A Real Function variable
   integer, parameter :: REAL_FUNCTION = 3

   !> A variablesetter.variable_type value: A Character variable
   integer, parameter :: CHARACTER_VARIABLE = 4

   !> A variablesetter.variable_type value: A Logical variable
   integer, parameter :: LOGICAL_VARIABLE = 5

   abstract interface
      !> An interface for a callback routine to run after a variable has been
      !> set with the VariableSetter.
      subroutine SetVariableCallback(set_variable_parser, ierr)
         !> An interface for a callback routine to run after a variable has been
         !> set with the VariableSetter.
         !>
         !> @param[inout] set_variable_parser The VariablSetter used to set the
         !>                                   variable.
         !> @param[out]   ierr                The result of the parsing and
         !>                                   setting.
         import VariableSetter
         implicit none

         class(VariableSetter), intent(INOUT) :: set_variable_parser
         integer, intent(OUT) :: ierr
      end subroutine SetVariableCallback

      !> An interface for getting and setting values via a Real function.
      function realfunction(index, value, ierr)
         !> @brief An interface for getting and setting values via a Real function.
         !>
         !> @returns The value of the function at the given point.
         !> @param[in]  index The index to update the array at.
         !> @param[in]  value The value to update at.
         !> @param[out] ierr  The result of the parse and set.
         !>
         !> This is intended for getters and setters on Real arrays which may
         !> be allocated after the command parser has been initialized, meaning
         !> Pointers to the array can't be used.
         !>
         use Utils, only: DP
         real(KIND=DP) :: RealFunction
         integer, optional, intent(IN) :: index
         real(KIND=DP), optional, intent(IN) :: value
         integer, intent(OUT) :: ierr
      end function realfunction

   end interface

   !> The list of variable setters.
   type(VariableSetter), allocatable :: variable_setters(:)

   !> Interface for adding variables and variable setters to the MScript
   !> language.
   interface AddVariable
      module procedure :: &
         AddIntegerVariableSetter, &
         AddRealVariableSetter, AddRealFunctionVariableSetter, &
         AddCharacterVariableSetter, &
         AddLogicalVariableSetter
   end interface

contains

   !>
   !> Initialize variable_setters array with the default variable setters
   !> Requires InitializeCommandParser already initialized
   !>
   subroutine InitializeVariableSetter()
      use Mesh_IO, only: stem
      use Command_Parser, only: AddCommandParser
      use Hubert_Minimizer, only: AlphaScale, DAlpha, FTolerance, GTolerance, MinAlpha
      use System_Parameters, only: TypicalEnergy
      use Energy_Calculator, only: CalcAllExchQ
      use Magnetization_Path, only: CurveWeight, PathN, PHooke
      use Material_Parameters, only: Ls, MaxEnergyEval, MaxRestarts, mu, switch_count, switch_per, WhichExchange, MaxPathEval
      use Tetrahedral_Mesh_Data, only: MaxMeshNumber, zone, zoneinc
      implicit none

      call DestroyVariableSetter()

      ! Add command 'set' which sets the variables given here
      call AddCommandParser("set", ParseSet)
      ! Add command 'SetSubDomain' which sets the variables given here
      ! for a given subdomain
      call AddCommandParser("setsubdomain", ParseSetSubDomain)

      allocate (variable_setters(0))

      call AddVariable("pathn", PathN, SetPathNCallback)
      call AddVariable("numberofpathstates", PathN, SetPathNCallback)
      call AddVariable( &
         "exchangecalculator", WhichExchange, SetExchangeCalculatorCallback &
         )
      call AddVariable("zone", zone, SetZoneCallback)
      call AddVariable("zonevalue", zone, SetZoneCallback)
      call AddVariable("zoneinc", zoneinc)
      call AddVariable("zoneincrement", zoneinc)
      call AddVariable("mu", mu)
      call AddVariable("permeability", mu)
      call AddVariable("lengthscale", Ls, SetLengthScaleCallback)
      call AddVariable("ls", Ls, SetLengthScaleCallback)
      call AddVariable("meshscale", Ls, SetMeshScaleCallback)

      ! Usual magnetic parameters
      call AddVariable("ms", SetMs)
      call AddVariable("satmagnetization", SetMs)
      call AddVariable("exchange", SetAex)
      call AddVariable("aex", SetAex)
      call AddVariable("anisotropy", SetK1)
      call AddVariable("k1", SetK1)
      call AddVariable("k2", SetK2)
      call AddVariable("ka", SetKa)
      call AddVariable("kb", SetKb)
      call AddVariable("ku", SetKu)
      call AddVariable("kaa", SetKaa)
      call AddVariable("kbb", SetKbb)
      call AddVariable("kab", SetKab)
      call AddVariable("k1p", SetK1p)
      call AddVariable("k2p", SetK2p)
      call AddVariable("k3p", SetK3p)
      call AddVariable("k4p", SetK4p)
      call AddVariable("k5p", SetK5p)
      call AddVariable("sigmas", SetSigmaS)
      call AddVariable("lamdas", SetLamdaS)

      ! HubertMinimizer parameters
      call AddVariable("ftolerance", FTolerance)
      call AddVariable("gtolerance", GTolerance)
      call AddVariable("alphascale", AlphaScale)
      call AddVariable("minalpha", MinAlpha)
      call AddVariable("dalpha", DAlpha)
      call AddVariable("typicalenergy", typicalenergy)

      call AddVariable("maxenergyevaluations", MaxEnergyEval)
      call AddVariable("maxenergyeval", MaxEnergyEval)

      call AddVariable("switchcount", Switch_Count)
      call AddVariable("switchper", Switch_Per)
      call AddVariable("switchpercentage", Switch_Per)

      ! Magnetization Path parameters
      call AddVariable("nebspring", PHooke)
      call AddVariable("springconstant", PHooke)
      call AddVariable("curvatureweight", CurveWeight)
      call AddVariable("maxrestarts", MaxRestarts)
      call AddVariable("maxpathevaluations", MaxPathEval)
      call AddVariable("maxpatheval", MaxPathEval)

      call AddVariable("meshnumber", MaxMeshNumber)
      call AddVariable("maxmeshnumber", MaxMeshNumber)
      call AddVariable("stem", stem, SetStemCallback)
      call AddVariable("filetemplate", stem, SetStemCallback)
      call AddVariable("filestem", stem, SetStemCallback)

      call AddVariable("allexchange", CalcAllExchQ)
   end subroutine InitializeVariableSetter

   !> Destroy and clean up the variable_setters array.
   subroutine DestroyVariableSetter()
      if (allocated(variable_setters)) deallocate (variable_setters)
   end subroutine DestroyVariableSetter

   !> Initialize an empty instance of a VariableSetter
   !>
   !> @param[inout] variable_setter The VariableSetter to initialize
   subroutine InitializeDefaultVariableSetter(variable_setter)
      implicit none

      type(VariableSetter), intent(OUT) :: variable_setter

      variable_setter%name = "NULL"
      variable_setter%integer_ptr => null()
      variable_setter%real_ptr => null()
      variable_setter%real_function => null()
      variable_setter%character_ptr => null()
      variable_setter%logical_ptr => null()
      variable_setter%variable_type = UNSET_VARIABLE
      variable_setter%callback => DefaultSetCallback
   end subroutine InitializeDefaultVariableSetter

   !> Add an INTEGER variable
   !>
   !> @param[in] variable_name The name of the variable.
   !> @param[in] integer_ptr   The variable to point to.
   !> @param[in] callback      A callback to be called when the variable
   !>                          has been set (optional).
   subroutine AddIntegerVariableSetter(variable_name, integer_ptr, callback)
      implicit none

      character(len=*), intent(IN) :: variable_name
      integer, target, intent(INOUT) :: integer_ptr
      procedure(SetVariableCallback), optional :: callback

      type(VariableSetter) :: variable_setter

      call InitializeDefaultVariableSetter(variable_setter)

      variable_setter%name = variable_name
      variable_setter%integer_ptr => integer_ptr
      variable_setter%variable_type = INTEGER_VARIABLE
      if (present(callback)) variable_setter%callback => callback

      call AddVariableSetterInstance(variable_setter)
   end subroutine AddIntegerVariableSetter

   !> Add a REAL variable
   !>
   !> @param[in] variable_name The name of the variable.
   !> @param[in] real_ptr      The variable to point to.
   !> @param[in] callback      A callback to be called when the variable
   !>                          has been set (optional).
   subroutine AddRealVariableSetter(variable_name, real_ptr, callback)
      implicit none

      character(len=*), intent(IN) :: variable_name
      real(KIND=DP), target, intent(INOUT) :: real_ptr
      procedure(SetVariableCallback), optional :: callback

      type(VariableSetter) :: variable_setter

      call InitializeDefaultVariableSetter(variable_setter)

      variable_setter%name = variable_name
      variable_setter%real_ptr => real_ptr
      variable_setter%variable_type = REAL_VARIABLE
      if (present(callback)) variable_setter%callback => callback

      call AddVariableSetterInstance(variable_setter)
   end subroutine AddRealVariableSetter

   !> Add a REAL FUNCTION variable
   !>
   !> @param[in] variable_name    The name of the variable.
   !> @param[in] real_function_cb The function to call to set the variable.
   !> @param[in] callback         A callback to be called when the variable
   !>                             has been set (optional).
   subroutine AddRealFunctionVariableSetter( &
      variable_name, real_function_cb, callback &
      )
      implicit none

      character(len=*), intent(IN) :: variable_name
      procedure(RealFunction) :: real_function_cb
      procedure(SetVariableCallback), optional :: callback

      type(VariableSetter) :: variable_setter

      call InitializeDefaultVariableSetter(variable_setter)

      variable_setter%name = variable_name
      variable_setter%real_function => real_function_cb
      variable_setter%variable_type = REAL_FUNCTION
      if (present(callback)) variable_setter%callback => callback

      call AddVariableSetterInstance(variable_setter)
   end subroutine AddRealFunctionVariableSetter

   !> Add a CHARACTER variable
   !>
   !> @param[in] variable_name The name of the variable.
   !> @param[in] character_ptr The variable to point to.
   !> @param[in] callback      A callback to be called when the variable
   !>                          has been set (optional).
   subroutine AddCharacterVariableSetter(variable_name, character_ptr, callback)
      implicit none

      character(len=*), intent(IN) :: variable_name
      character(len=1024), target, intent(INOUT) :: character_ptr
      procedure(SetVariableCallback), optional :: callback

      type(VariableSetter) :: variable_setter

      call InitializeDefaultVariableSetter(variable_setter)

      variable_setter%name = variable_name
      variable_setter%character_ptr => character_ptr
      variable_setter%variable_type = CHARACTER_VARIABLE
      if (present(callback)) variable_setter%callback => callback

      call AddVariableSetterInstance(variable_setter)
   end subroutine AddCharacterVariableSetter

   !> Add a LOGICAL variable
   !>
   !> @param[in] variable_name The name of the variable.
   !> @param[in] logical_ptr   The variable to point to.
   !> @param[in] callback      A callback to be called when the variable
   !>                          has been set (optional).
   subroutine AddLogicalVariableSetter(variable_name, logical_ptr, callback)
      implicit none

      character(len=*), intent(IN) :: variable_name
      logical, target, intent(INOUT) :: logical_ptr
      procedure(SetVariableCallback), optional :: callback

      type(VariableSetter) :: variable_setter

      call InitializeDefaultVariableSetter(variable_setter)

      variable_setter%name = variable_name
      variable_setter%logical_ptr => logical_ptr
      variable_setter%variable_type = LOGICAL_VARIABLE
      if (present(callback)) variable_setter%callback => callback

      call AddVariableSetterInstance(variable_setter)
   end subroutine AddLogicalVariableSetter

   !> Add an instance of VariableSetterType to the variable_setters array.
   !>
   !> @param[in] variable_setter The instance to add to the variable_setters
   !>                            array.
   subroutine AddVariableSetterInstance(variable_setter)
      implicit none

      type(VariableSetter), intent(IN) :: variable_setter

      type(VariableSetter), allocatable :: tmp(:)
      integer :: n_setters

      allocate (tmp(size(variable_setters) + 1))

      n_setters = size(variable_setters)
      if (n_setters .gt. 0) tmp(1:n_setters) = variable_setters
      call move_alloc(tmp, variable_setters)
      n_setters = n_setters + 1

      variable_setters(n_setters) = variable_setter
   end subroutine AddVariableSetterInstance

   !> Run the setter in variable_setters with the name variable_name
   !>
   !> @param[in]  variable_name  The name of the variable to set.
   !> @param[in]  variable_index The index on the variable to set.
   !> @param[in]  value_string   The value to parse and set.
   !> @param[out] ierr           The result of the parse and set.
   subroutine SetVariable(variable_name, variable_index, value_string, ierr)
      use strings, only: value, lowercase
      use Command_Parser, only: NO_PARSE, PARSE_SUCCESS, PARSE_ERROR
      implicit none

      character(len=*), intent(IN) :: variable_name
      character(len=*), intent(IN) :: variable_index
      character(len=*), intent(IN) :: value_string
      integer, intent(OUT) :: ierr

      type(VariableSetter) :: variable_setter
      integer :: i

      integer       :: iv
      real(KIND=DP) :: dv

      integer :: ios

      ierr = NO_PARSE

      do i = 1, size(variable_setters)
         if (trim(variable_setters(i)%name) .eq. trim(lowercase(variable_name))) then
            variable_setter = variable_setters(i)

            select case (variable_setter%variable_type)

            case (UNSET_VARIABLE)
               write (*, *) "ERROR: Variable ", trim(variable_setter%name), "is of unknown type."
               ierr = PARSE_ERROR

            case (INTEGER_VARIABLE)
               call value(value_string, iv, ios)
               if (ios == 0) then
                  variable_setter%integer_ptr = iv
                  ierr = PARSE_SUCCESS
               else
                  write (*, *) "Error: Unable to parse integer from ", value_string
                  ierr = PARSE_ERROR
               end if

            case (REAL_VARIABLE)
               call value(value_string, dv, ios)
               if (ios == 0) then
                  variable_setter%real_ptr = dv
                  ierr = PARSE_SUCCESS
               else
                  write (*, *) "Error: Unable to parse real from ", value_string
                  ierr = PARSE_ERROR
               end if

            case (REAL_FUNCTION)
               call value(value_string, dv, ios)

               if (ios == 0) then
                  if (trim(variable_index) .eq. ':') then
                     dv = variable_setter%real_function(value=dv, ierr=ierr)
                  else
                     call value(variable_index, iv, ios)
                     if (ios == 0) then
                        dv = variable_setter%real_function(index=iv, value=dv, ierr=ierr)
                     end if
                  end if
               else
                  write (*, *) "Error: Unable to parse real from ", value_string
                  ierr = PARSE_ERROR
               end if

            case (CHARACTER_VARIABLE)
               variable_setter%character_ptr = value_string
               ierr = PARSE_SUCCESS

            case (LOGICAL_VARIABLE)
               call value(value_string, iv, ios)
               if (ios == 0) then
                  if (iv > 0) then
                     variable_setter%logical_ptr = .true.
                  else
                     variable_setter%logical_ptr = .false.
                  end if
                  ierr = PARSE_SUCCESS
               else
                  write (*, *) "Error: Unable to parse integer/logical from ", value_string
                  ierr = PARSE_ERROR
               end if
            end select

            if (associated(variable_setter%callback)) then
               call variable_setter%callback(ierr)
            end if

            exit
         end if
      end do

      if (ierr .eq. NO_PARSE) then
         write (*, *) "Unable to find variable ", trim(variable_name), " to set"
         ierr = PARSE_ERROR
      else if (ierr .eq. PARSE_ERROR) then
         write (*, *) "Error setting variable ", trim(variable_name)
         ierr = PARSE_ERROR
      end if
   end subroutine SetVariable

   !> Parser to hook into Command_Parser to add a `Set` command to MScript.
   !>
   !> MScript example to set a variable called `MyVariable`.
   !> @code
   !>    Set MyVariable %value
   !>    Set MyVariable %value sd = n
   !> @endcode
   !>
   !> @param[in]  args The arguments to parse
   !> @param[out] ierr The result of the parse and set.
   subroutine ParseSet(args, ierr)
      use Command_Parser, only: PARSE_ERROR, ParseSubdomain, AssertNArgs
      implicit none

      character(len=*), intent(IN) :: args(:)
      integer, intent(OUT) :: ierr

      character(len=len(args)) :: idx_str
      integer :: sd
      logical :: l

      if (size(args) .ge. 3) then
         if (size(args) .eq. 6) then
            sd = ParseSubdomain(args(4:6), ierr)

            if (ierr .ne. PARSE_ERROR) then
               idx_str = ""
               write (idx_str, *) sd
               call SetVariable(args(2), idx_str, args(3), ierr)
            else
               return
            end if
         else if (AssertNArgs(args, 3)) then
            call SetVariable(args(2), ':', args(3), ierr)
         else
            ierr = PARSE_ERROR
         end if
      else
         l = AssertNArgs(args, 3)
         ierr = PARSE_ERROR
      end if
   end subroutine ParseSet

   !> A parser for the `SetSubdomain` command
   !>
   !> MScript example to set the variable `VarName` to the value `1.5e6`
   !> on the subdomain `2`
   !> @code
   !>    SetSubDomain 2 VarName 1.5e6
   !> @endcode
   !>
   !> @param[in]  args The arguments to parse.
   !> @param[out] ierr The result of the parse and set.
   subroutine ParseSetSubDomain(args, ierr)
      use strings, only: value
      use Command_Parser, only: PARSE_ERROR, AssertNArgs
      use Tetrahedral_Mesh_Data, only: SubDomainIds
      implicit none

      character(len=*), intent(IN) :: args(:)
      integer, intent(OUT) :: ierr

      character(len=(len(args))) :: idx_str
      integer :: sd_id
      integer :: i

      if (AssertNArgs(args, 4)) then
         ! Find Subdomain for given SubDomainId
         ! Replace sd_id with the subdomain index
         call value(args(2), sd_id, ierr)
         do i = 1, size(SubDomainIds)
            if (sd_id .eq. SubDomainIds(i)) then
               sd_id = i
               exit
            end if
         end do

         ! Convert sd_id to string
         idx_str = ""
         write (idx_str, *) sd_id

         call SetVariable(args(3), idx_str, args(4), ierr)
      else
         ierr = PARSE_ERROR
      end if
   end subroutine ParseSetSubDomain

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! VariableSetter callbacks                                                   !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !> A default callback for a VariableSetter, which does nothing.
   !>
   !> @param[in]  variable_setter The variable setter which set the variable.
   !> @param[out] ierr            The result of the callback.
   subroutine DefaultSetCallback(variable_setter, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      implicit none

      class(VariableSetter), intent(INOUT) :: variable_setter
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
   end subroutine DefaultSetCallback

   !> The callback for setting PathN which calls PathAllocate().
   !>
   !> @param[in]  variable_setter The variable setter which set the variable.
   !> @param[out] ierr            The result of the callback.
   subroutine SetPathNCallback(variable_setter, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Magnetization_Path, only: PathAllocate
      implicit none

      class(VariableSetter), intent(INOUT) :: variable_setter
      integer, intent(OUT) :: ierr

      call PathAllocate()

      ierr = PARSE_SUCCESS
   end subroutine

   !> The callback for setting the exchange calculator.
   !> Tests whether a valid exchange calculator was chosen.
   !> Currently broken and does nothing.
   !>
   !> @param[in]  variable_setter The variable setter which set the variable.
   !> @param[out] ierr            The result of the callback.
   subroutine SetExchangeCalculatorCallback(variable_setter, ierr)
      use Command_Parser, only: PARSE_SUCCESS, PARSE_ERROR
      implicit none

      class(VariableSetter), intent(INOUT) :: variable_setter
      integer, intent(OUT) :: ierr

      integer :: i

      !> @todo FIXME
      !i = variable_setters%integer_ptr
      i = 1

      if ((i > 0) .and. (i < 5)) then
         ierr = PARSE_SUCCESS
      else
         ierr = PARSE_ERROR
      end if
   end subroutine SetExchangeCalculatorCallback

   !> The callback for setting the Zone variable.
   !> Sets `zoneflag` to -1.
   !>
   !> @param[in]  variable_setter The variable setter which set the variable.
   !> @param[out] ierr            The result of the callback.
   subroutine SetZoneCallback(variable_setter, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Tetrahedral_Mesh_Data, only: zoneflag
      implicit none

      class(VariableSetter), intent(INOUT) :: variable_setter
      integer, intent(OUT) :: ierr

      zoneflag = -1.0 ! Flag for using zone in WriteTecPlot()
!    zoneflag=-1.0 ! Flag for using zone in WriteTecPlot()

      ierr = PARSE_SUCCESS
   end subroutine

   !> Callback for setting the length scale.
   !> Takes the value that was set and changes it to the format actually
   !> used by MERRILL. Also updates the `EnergyUnit` variable.
   !>
   !> @param[in]  variable_setter The variable setter which set the variable.
   !> @param[out] ierr            The result of the callback.
   subroutine SetLengthScaleCallback(variable_setter, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: EnergyUnit, Kd
      use Tetrahedral_Mesh_Data, only: total_volume
      implicit none

      class(VariableSetter), intent(INOUT) :: variable_setter
      integer, intent(OUT) :: ierr

      real(KIND=DP) :: Ls

      Ls = variable_setter%real_ptr
      EnergyUnit = Kd*total_volume/(Ls**(3.0/2.0))

      ierr = PARSE_SUCCESS
   end subroutine SetLengthScaleCallback

   !> Callback for setting the length scale after setting the `MeshScale`
   !> variable.
   !> Updates `Ls` and `EnergyUnit` to the appropriate values.
   !>
   !> @param[in]  variable_setter The variable setter which set the variable.
   !> @param[out] ierr            The result of the callback.
   subroutine SetMeshScaleCallback(variable_setter, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: EnergyUnit, Kd
      use Tetrahedral_Mesh_Data, only: total_volume
      implicit none

      class(VariableSetter), intent(INOUT) :: variable_setter
      integer, intent(OUT) :: ierr

      real(KIND=DP) :: LInv
      real(KIND=DP) :: Ls

      LInv = variable_setter%real_ptr
      variable_setter%real_ptr = 1./(LInv*LInv)  !!  Square !
      Ls = variable_setter%real_ptr
      EnergyUnit = Kd*total_volume/(Ls**(3.0/2.0))

      ierr = PARSE_SUCCESS
   end subroutine SetMeshScaleCallback

   !> Asserts the given index is within the given array indices.
   !> Prints an error and returns a `PARSE_ERROR` if it isn't.
   !>
   !> @param[in]  idx   The index to test.
   !> @param[in]  array The array whose bounds we want to check
   !> @param[out] ierr  The result of the check
   !> @returns Whether `idx` is a valid index for `array`.
   logical function AssertIndexInRange(idx, array, ierr)
      use Command_Parser, only: PARSE_SUCCESS, PARSE_ERROR
      integer, intent(IN) :: idx
      real(KIND=DP), intent(IN) :: array(:)
      integer, intent(OUT) :: ierr

      if (idx .ge. lbound(array, 1) .and. idx .le. ubound(array, 1)) then
         AssertIndexInRange = .true.
         ierr = PARSE_SUCCESS
      else
         write (*, *) "index out of bound:"
         write (*, *) "  requested: ", idx
         write (*, *) "  bounds: ", lbound(array), ubound(array)
         AssertIndexInRange = .false.
         ierr = PARSE_ERROR
      end if
   end function AssertIndexInRange

   !> Get/Set an array at the given index with the given value.
   !> If no index is given, set all the entries of the array to the given value.
   !> If no value is given, return the value at the given index.
   !>
   !> @param[in]  array The array to set.
   !> @param[in]  index The index for the array where we want to set. (optional)
   !> @param[in]  value The value we want to set. (optional)
   !> @param[out] ierr  The result of the indexing and setting.
   !> @returns The value of `array` at index `index`. If `index` has been
   !>          omitted, return the first value in `array`.
   real(KIND=DP) function SetArray(array, index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS, PARSE_ERROR
      real(KIND=DP), intent(INOUT) :: array(:)
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS

      ! Handle setting the array
      if (present(value)) then
         if (present(index)) then
            if (AssertIndexInRange(index, Array, ierr)) then
               Array(index) = value
            else
               ierr = PARSE_ERROR
               return
            end if
         else
            Array = value
         end if
      end if

      ! Handle the return value
      if (present(index)) then
         if (AssertIndexInRange(index, Array, ierr)) then
            SetArray = Array(index)
         else
            ierr = PARSE_ERROR
            return
         end if
      else
         SetArray = Array(lbound(Array, 1))
      end if
   end function SetArray

   !> A RealFunction for setting `Ms`.
   !>
   !> @param[in]  index The subdomain to set `Ms` for.
   !> @param[in]  value The value to set `Ms` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `Ms` at `index`.
   real(KIND=DP) function SetMs(index, value, ierr)
      use Utils, only: NONZERO
      use Material_Parameters, only: EnergyUnit, Kd, LambdaEx, Ls, Ms, mu, Aex
      use Tetrahedral_Mesh_Data, only: total_volume
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      SetMs = SetArray(Ms, index, value, ierr)

      if (present(value)) then
         Kd = mu*maxval(Ms)**2*0.5
         EnergyUnit = Kd*total_volume/(Ls**(3.0/2.0))
         if (NONZERO(Aex(maxloc(Ms, 1)))) LambdaEx = sqrt(Aex(maxloc(Ms, 1))/Kd)
      end if
   end function SetMs

   !> A RealFunction for setting `Aex`.
   !>
   !> @param[in]  index The subdomain to set `Aex` for.
   !> @param[in]  value The value to set `Aex` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `Aex` at `index`.
   real(KIND=DP) function SetAex(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Utils, only: NONZERO
      use Material_Parameters, only: Kd, LambdaEx, Ms, Aex
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetAex = SetArray(Aex, index, value, ierr)

      if (present(value)) then
         if (NONZERO(Kd)) LambdaEx = sqrt(Aex(maxloc(Ms, 1))/Kd)
      end if
   end function SetAex

   !> A RealFunction for setting `K1`.
   !>
   !> @param[in]  index The subdomain to set `K1` for.
   !> @param[in]  value The value to set `K1` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `K1` at `index`.
   real(KIND=DP) function SetK1(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Utils, only: NONZERO
      use Material_Parameters, only: kd, Ms, QHardness, k1
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetK1 = SetArray(K1, index, value, ierr)

      if (present(value)) then
         if (NONZERO(Kd)) QHardness = K1(maxloc(Ms, 1))/Kd
      end if
   end function SetK1

   !> A RealFunction for setting `K2`.
   !>
   !> @param[in]  index The subdomain to set `K2` for.
   !> @param[in]  value The value to set `K2` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `K2` at `index`.
   real(KIND=DP) function SetK2(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: K2
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetK2 = SetArray(K2, index, value, ierr)
   end function SetK2

   !> A RealFunction for setting `K1p`.
   !>
   !> @param[in]  index The subdomain to set `K1p` for.
   !> @param[in]  value The value to set `K1p` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `K1p` at `index`.
   real(KIND=DP) function SetK1p(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: K1p
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetK1p = SetArray(K1p, index, value, ierr)
   end function SetK1p

   !> A RealFunction for setting `K2p`.
   !>
   !> @param[in]  index The subdomain to set `K2p` for.
   !> @param[in]  value The value to set `K2p` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `K2p` at `index`.
   real(KIND=DP) function SetK2p(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: K2p
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetK2p = SetArray(K2p, index, value, ierr)
   end function SetK2p

   !> A RealFunction for setting `K3p`.
   !>
   !> @param[in]  index The subdomain to set `K3p` for.
   !> @param[in]  value The value to set `K3p` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `K3p` at `index`.
   real(KIND=DP) function SetK3p(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: K3p
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetK3p = SetArray(K3p, index, value, ierr)
   end function SetK3p

   !> A RealFunction for setting `K4p`.
   !>
   !> @param[in]  index The subdomain to set `K4p` for.
   !> @param[in]  value The value to set `K4p` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `K4p` at `index`.
   real(KIND=DP) function SetK4p(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: K4p
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetK4p = SetArray(K4p, index, value, ierr)
   end function SetK4p

   !> A RealFunction for setting `K5p`.
   !>
   !> @param[in]  index The subdomain to set `K5p` for.
   !> @param[in]  value The value to set `K5p` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `K5p` at `index`.
   real(KIND=DP) function SetK5p(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: K5p
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetK5p = SetArray(K5p, index, value, ierr)
   end function SetK5p

   !> A RealFunction for setting `Ka`.
   !>
   !> @param[in]  index The subdomain to set `Ka` for.
   !> @param[in]  value The value to set `Ka` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `Ka` at `index`.
   real(KIND=DP) function SetKa(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: Ka
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetKa = SetArray(Ka, index, value, ierr)
   end function SetKa

   !> A RealFunction for setting `Kaa`.
   !>
   !> @param[in]  index The subdomain to set `Kaa` for.
   !> @param[in]  value The value to set `Kaa` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `Kaa` at `index`.
   real(KIND=DP) function SetKaa(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: Kaa
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetKaa = SetArray(Kaa, index, value, ierr)
   end function SetKaa

   !> A RealFunction for setting `Kb`.
   !>
   !> @param[in]  index The subdomain to set `Kb` for.
   !> @param[in]  value The value to set `Kb` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `Kb` at `index`.
   real(KIND=DP) function SetKb(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: kb
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetKb = SetArray(Kb, index, value, ierr)
   end function SetKb

   !> A RealFunction for setting `Kbb`.
   !>
   !> @param[in]  index The subdomain to set `Kbb` for.
   !> @param[in]  value The value to set `Kbb` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `Kbb` at `index`.
   real(KIND=DP) function SetKbb(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: Kbb
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetKbb = SetArray(Kbb, index, value, ierr)
   end function SetKbb

   !> A RealFunction for setting `Kab`.
   !>
   !> @param[in]  index The subdomain to set `Kbb` for.
   !> @param[in]  value The value to set `Kbb` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `Kbb` at `index`.
   real(KIND=DP) function SetKab(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: Kab
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetKab = SetArray(Kab, index, value, ierr)
   end function SetKab

   !> A RealFunction for setting `Ku`.
   !>
   !> @param[in]  index The subdomain to set `Ku` for.
   !> @param[in]  value The value to set `Ku` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `Ku` at `index`.
   real(KIND=DP) function SetKu(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: Ku
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetKu = SetArray(Ku, index, value, ierr)
   end function SetKu

   !> A RealFunction for setting `SigmaS`.
   !>
   !> @param[in]  index The subdomain to set `SigmaS` for.
   !> @param[in]  value The value to set `SimgaS` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `SigmaS` at `index`.
   real(KIND=DP) function SetSigmaS(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: SigmaS
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetSigmaS = SetArray(SigmaS, index, value, ierr)
   end function SetSigmaS

   !> A RealFunction for setting `LamdaS`.
   !>
   !> @param[in]  index The subdomain to set `LamdaS` for.
   !> @param[in]  value The value to set `LamdaS` to.
   !> @param[out] ierr The result of setting.
   !> @returns The value for `LamdaS` at `index`.
   real(KIND=DP) function SetLamdaS(index, value, ierr)
      use Command_Parser, only: PARSE_SUCCESS
      use Material_Parameters, only: LamdaS
      integer, optional, intent(IN) :: index
      real(KIND=DP), optional, intent(IN) :: value
      integer, intent(OUT) :: ierr

      ierr = PARSE_SUCCESS
      SetLamdaS = SetArray(LamdaS, index, value, ierr)
   end function SetLamdaS

   !> The callback for the `stem` variable.
   !> This sets the `datafile` variable to the appropriate `stem` value.
   !>
   !> @param[in]  variable_setter The variable setter which set the variable.
   !> @param[out] ierr            The result of the callback.
   subroutine SetStemCallback(variable_setter, ierr)
      use Mesh_IO, only: stem, datafile
      use Command_Parser, only: PARSE_SUCCESS
      implicit none

      class(VariableSetter), intent(INOUT) :: variable_setter
      integer, intent(OUT) :: ierr

      datafile = stem(:len_trim(variable_setter%character_ptr))//'.dat'

      ierr = PARSE_SUCCESS
   end subroutine SetStemCallback

end module Variable_Setter
