!> The Plugins module contains routines for loading libraries which can extend
!> MERRILL's functionality at run time.
module Plugins
   use :: iso_c_binding
   implicit none

   integer(c_int), parameter :: RTLD_LAZY = 1, RTLD_NOW = 2, RTLD_GLOBAL = 256

   ! Interfaces for Linux dynamic library routines
   interface
      !> Interface for void *dlopen(const char* filename, int mode);
      function dlopen(filename, mode) bind(C, NAME="dlopen")
         !>
         !> Interface for void *dlopen(const char* filename, int mode);
         !>
         !> @param[in] filename The filename to open.
         !> @param[in] mode     The mode to open it in.
         !> @returns A handle to the library.
         !>

         use iso_c_binding
         implicit none

         type(c_ptr) :: dlopen
         character(c_char), intent(IN) :: filename(*)
         integer(c_int), value :: mode
      end function dlopen

      !> Interface for void *dlsym(void *handle, const char *name);
      function dlsym(handle, name) bind(C, NAME="dlsym")
         !>
         !> Interface for void *dlsym(void *handle, const char *name);
         !>
         !> @param[in] handle The handle to the library returned by dlopen.
         !> @param[in] name   The name of the function to get.
         !> @returns A pointer to the function.
         !>

         use iso_c_binding
         implicit none

         type(c_funptr) :: dlsym
         type(c_ptr), value :: handle
         character(c_char), intent(IN) :: name(*)
      end function

      !> Interface for char *dlerror(void);
      function c_dlerror() bind(C, NAME="dlerror")
         !> Interface for char *dlerror(void);
         !>
         !> Defining slightly different name because of char* return type
         !> and dealing with it later.
         use iso_c_binding
         implicit none

         type(c_ptr) :: c_dlerror
      end function c_dlerror
   end interface

   interface
      !> Interface for void InitializeMerrillPlugin();
      subroutine InitializeMerrillPlugin() bind(C)
         !> Interface for void InitializeMerrillPlugin();
         !>
         !> This is the entry function for a library. When MERRILL loads a
         !> plugin it will call this routine defined in the library.
         !> The plugin is then expected to inject itself into whatever parts
         !> of MERRILL it needs, e.g. by calling
         !> energy_calculator.addenergycalulator() and
         !> command_parser.addcommandparser()
      end subroutine InitializeMerrillPlugin
   end interface

contains

#ifdef MERRILL_ENABLE_PLUGINS
   function dlerror()
      use iso_fortran_env

      character(KIND=c_char, len=1024) :: dlerror

      type(c_ptr) :: c_char_ptr
      character(KIND=c_char), pointer :: c_char_arr(:)
      character :: ch

      integer :: i

      c_char_ptr = c_dlerror()

      dlerror = ""
      if (c_associated(c_char_ptr)) then
         call c_f_pointer(c_char_ptr, c_char_arr, (/1024/))
         do i = 1, 1024
            ch = c_char_arr(i)
            if (ch .ne. c_null_char) then
               dlerror(i:i) = ch
            else
               exit
            end if
         end do
      end if
   end function dlerror

   subroutine LoadPlugin(library_filename)
      character(len=*), intent(IN) :: library_filename

      type(c_ptr) :: handle
      type(c_funptr) :: c_initialize_plugin_ptr
      procedure(InitializeMerrillPlugin), bind(C), pointer :: &
         InitializeMerrillPlugin_ptr

      handle = dlopen(trim(library_filename)//c_null_char, RTLD_LAZY)
      if (.not. c_associated(handle)) then
         write (*, *) "dlerror: ", trim(dlerror())
         write (*, *) "Error: Unable to open ", trim(library_filename)
         stop
      end if

      c_initialize_plugin_ptr = dlsym( &
                                handle, "InitializeMerrillPlugin"//c_null_char &
                                )
      if (.not. c_associated(c_initialize_plugin_ptr)) then
         write (*, *) "ERROR: Unable to find procedure InitializeMerrillPlugin"
         stop
      end if

      call c_f_procpointer( &
         c_initialize_plugin_ptr, InitializeMerrillPlugin_ptr &
         )

      write (*, *) "Loading Plugin ", trim(library_filename)
      call InitializeMerrillPlugin_ptr()
   end subroutine LoadPlugin
#else
   subroutine LoadPlugin(library_filename)
      character(len=*), intent(IN) :: library_filename

      write (*, *) "Error: LoadPlugin not enabled"
      stop
   end subroutine
#endif
end module Plugins
