!> The Loop_parser module contains the routines used to parse an MScript
!> program.
module Loop_Parser
   use Utils, only: DP

   implicit none
   save

   character(LEN=200), allocatable :: LineIn(:, :), LineOutStack(:, :), DefVar(:)

   integer, allocatable :: NArgsIn(:), NArgsOut(:)
   real(KIND=DP), allocatable :: DefVal(:)
   integer :: FileLength, LoopCnt, AllLineCnt, NDefVar

contains

   subroutine InitializeLoopParser()
      call DestroyLoopParser()
   end subroutine InitializeLoopParser

   subroutine DestroyLoopParser()
      if (allocated(LineIn)) deallocate (LineIn)
      if (allocated(LineOutStack)) deallocate (LineOutStack)
      if (allocated(DefVar)) deallocate (DefVar)
      if (allocated(NArgsIn)) deallocate (NArgsIn)
      if (allocated(NArgsOut)) deallocate (NArgsOut)
      if (allocated(DefVal)) deallocate (DefVal)
   end subroutine DestroyLoopParser

   !---------------------------------------------------------------
   !   Subroutine ReadLoopFile
   !---------------------------------------------------------------

   subroutine ReadLoopFile(filename)
      use strings, only: lowercase, readline, parse, value
      implicit none

      character(LEN=200) ::  filename, args(20), lstr, varstr
      character(LEN=200) :: line

      integer ios, nargs, i, j, k
      integer OutCnt, LineNo, lp, MaxDef

      real(KIND=DP)  :: a, b, s, dbltmp, varval

      integer, allocatable  ::   LoopStart(:), LoopEnd(:)

      integer :: scriptunit

      open (NEWUNIT=scriptunit, file=filename, status='unknown')

      !      Read original scriptfile and count number of script lines ->FileLength
      LineNo = 0
      ios = 1
      do while (ios .ge. 0)
         call Readline(scriptunit, line, ios)
         LineNo = LineNo + 1
      end do
      FileLength = LineNo

      rewind (scriptunit)
      !  Allocate more than enough space to parse all loops
      if (allocated(LineIn)) deallocate (LineIn)
      if (allocated(NArgsIn)) deallocate (NArgsIn)
      if (allocated(LoopStart)) deallocate (LoopStart)
      if (allocated(LoopEnd)) deallocate (LoopEnd)

      allocate (LineIn(FileLength + 10, 20))
      allocate (NArgsIn(FileLength + 10))
      allocate (LoopStart(FileLength))
      allocate (LoopEnd(FileLength))

      MaxDef = 0
      LineNo = 0
      LoopCnt = 0
      AllLineCnt = 0 ! Number of lines after unravelling all loops
      lp = 0         ! lp is the number of current inner loop lines
      ios = 1
      do while (ios .ge. 0)
         call Readline(scriptunit, line, ios)
         if (ios .lt. 0) line = "end" ! ios=-1 at end of file and 'line' is garbage
         LineNo = LineNo + 1
         if (lp > 0) then
            AllLineCnt = AllLineCnt + lp
         else
            AllLineCnt = AllLineCnt + 1
         end if
         call Parse(line, ' ,:', args, nargs)
         NArgsIn(LineNo) = nargs   ! Number of all input arguments in LineNo
         do i = 1, nargs
            LineIn(LineNo, i) = args(i) ! LineIn =  Array of all input arguments in LineNo
         end do
         lstr = lowercase(args(1))
         select case (lstr)
         case ('define')
            MaxDef = MaxDef + 1  !contains maximal number of defined variables
         case ('loop')
            if (lp > 0) then
               write (*, *) 'ERROR in scriptfile :', filename
               write (*, *) 'Nested loop command @:', LineNo
               write (*, *) 'Line :', line
               stop
            end if
            call value(args(3), a, ios)
            call value(args(4), b, ios)
            if (nargs > 4) then
               call value(args(5), s, ios)
            else
               s = 1.0
            end if
            lp = int(abs((b - a)/s) + 1)  ! number of loop repetitions
            LoopCnt = LoopCnt + 1
            LoopStart(LoopCnt) = LineNo
         case ('endloop')
            if (lp .eq. 0) then
               write (*, *) 'ERROR in scriptfile :', filename
               write (*, *) 'No loop to end  @:', LineNo
               write (*, *) 'Line :', line
               stop
            end if
            lp = 0
            LoopEnd(LoopCnt) = LineNo
         end select
      end do

      ! Allocate more than enough space for the unravelled loops
      if (allocated(LineOutStack)) deallocate (LineOutStack)
      if (allocated(NArgsOut)) deallocate (NArgsOut)
      allocate (LineOutStack(AllLineCnt + 10, 20))
      allocate (NArgsOut(AllLineCnt + 10))

      LineNo = 1
      OutCnt = 1
      do i = 1, LoopCnt
         ! If not in a loop just copy commands from In -> Out
         do j = LineNo, LoopStart(i) - 1
            LineOutStack(OutCnt, :) = LineIn(j, :)
            NArgsOut(OutCnt) = NArgsIn(j)
            OutCnt = OutCnt + 1
         end do
         call value(LineIn(LoopStart(i), 3), a, ios)
         call value(LineIn(LoopStart(i), 4), b, ios)
         if (NArgsIn(LoopStart(i)) > 4) then
            call value(LineIn(LoopStart(i), 5), s, ios)
         else
            s = 1.0
         end if
         lp = int(abs((b - a)/s) + 1)    ! number of i-th loop repetitions
         varstr = LineIn(LoopStart(i), 2)  ! String with the loop variable
         do j = 1, lp   ! Unravel  loop i from In -> Out
            varval = a + (j - 1)*s
            do k = LoopStart(i) + 1, LoopEnd(i) - 1
               ! Replace varstr by right value
               call ReplaceVar(k, OutCnt, varstr, varval)
               ! and   LineIn(k,:)  --->  LineOutStack(OutCnt,:)
               NArgsOut(OutCnt) = NArgsIn(k)
               OutCnt = OutCnt + 1
            end do
         end do
         LineNo = LoopEnd(i) + 1
      end do
      do j = LineNo, FileLength
         LineOutStack(OutCnt, :) = LineIn(j, :)
         NArgsOut(OutCnt) = NArgsIn(j)
         OutCnt = OutCnt + 1
      end do
      !       Now replace defined variables    Define xx 23
      !                                        AddTo  xx 5
      !                                        Undefine xx
      !
      ! Redefine LineIn for easy second parsing
      deallocate (LineIn, NArgsIn)
      allocate (LineIn(AllLineCnt + 10, 20), NArgsIn(AllLineCnt + 10))
      allocate (DefVar(MaxDef + 10), DefVal(MaxDef + 10))
      LineIn(:, :) = LineOutStack(:, :)
      NArgsIn(:) = NArgsOut(:)
      AllLineCnt = OutCnt - 1
      NDefVar = 0
      do i = 1, AllLineCnt
         lstr = lowercase(LineIn(i, 1))
         select case (lstr)
         case ('define')
            if (NArgsIn(i) > 2) then
               k = 0
               do j = 1, NDefVar
                  ! check if variable is already defined
                  if (LineIn(i, 2) .eq. DefVar(j)) k = j
               end do
               if (k .eq. 0) then
                  NDefVar = NDefVar + 1 ! new variable
                  DefVar(NDefVar) = LineIn(i, 2)
                  call value(LineIn(i, 3), DefVal(NDefVar), ios)
               else
                  call value(LineIn(i, 3), DefVal(k), ios) ! reassign variable
               end if
            end if
         case ('addto')
            if (NArgsIn(i) > 2) then
               call value(LineIn(i, 3), dbltmp, ios)
               do j = 1, NDefVar
                  if (LineIn(i, 2) .eq. DefVar(j)) DefVal(j) = DefVal(j) + dbltmp
               end do
            end if
         case ('undefine')
            if (NArgsIn(i) > 1) then
               k = 0
               do j = 1, NDefVar
                  if (LineIn(i, 2) .eq. DefVar(j)) k = j
               end do
               if (k > 0) then
                  NDefVar = NDefVar - 1
                  if (k < NDefVar) then
                     DefVar(k) = DefVar(NDefVar)
                     DefVal(k) = DefVal(NDefVar)
                  end if
               end if
            end if
         end select
         do j = 1, NDefVar
            call ReplaceVar(i, i, DefVar(j), DefVal(j))
            LineIn(i, :) = LineOutStack(i, :)
            NArgsIn(i) = NArgsOut(i)
         end do
      end do

      close (scriptunit)
   end subroutine ReadLoopFile

   !---------------------------------------------------------------
   !   Subroutine ReplaceVar
   !
   !---------------------------------------------------------------

   subroutine ReplaceVar(k, OutCnt, varstr, varval)
      use Utils, only: MachEps
      use strings, only: writenum
      implicit none

      integer k, OutCnt, varint, i
      character(LEN=200) ::  varstr
      character(LEN=20) ::   vi, vd, vs, vsi, vsd
      real(KIND=DP) ::   varval
      logical :: IntQ
      varint = int(varval)
      if ((varval - varint) < MachEps) then
         intQ = .true.
      else
         intQ = .false.
      end if

      call writenum(varint, vsi, 'I8')
      call writenum(varval, vsd, 'E12.6')

      vi = '#'//varstr(:len_trim(varstr))
      vd = '%'//varstr(:len_trim(varstr))
      vs = '$'//varstr(:len_trim(varstr))//'$'
      LineOutStack(OutCnt, :) = LineIn(k, :)

      do i = 2, NArgsIn(k)
         call repsubstr(LineOutStack(OutCnt, i), vi, vsi)
         call repsubstr(LineOutStack(OutCnt, i), vd, vsd)
         if (intQ) then
            call repsubstr(LineOutStack(OutCnt, i), vs, vsi)
         else
            call repsubstr(LineOutStack(OutCnt, i), vd, vsd)
         end if
      end do

   end subroutine ReplaceVar

   !**********************************************************************

   subroutine repsubstr(str, substr, repstr)
      implicit none
      ! Deletes first occurrence of substring 'substr' from string 'str' and
      ! replaces it by repstr. Trailing spaces or blanks are
      ! not considered part of 'substr'.

      character(len=*):: str, substr, repstr
      integer :: ipos, lensubstr

      lensubstr = len_trim(substr)
      ipos = index(str, substr(:lensubstr))

      do while (ipos > 0)
         if (ipos == 1) then
            str = repstr(:len_trim(repstr))//str(lensubstr + 1:)
         else
            str = str(:ipos - 1)//repstr(:len_trim(repstr))//str(ipos + lensubstr:)
         end if
         ipos = index(str, substr(:lensubstr))
      end do
      return

   end subroutine repsubstr

end module Loop_Parser
