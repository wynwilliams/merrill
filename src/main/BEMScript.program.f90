!     ***********************************************************************
!     *                   MERRILL 2.0.0                                     *
!     *                                                                     *
!     * (or whatever is reported by the MERRILL_VERSION_STRING in Utils.F90)*
!     * Micromagnetic Earth Related Robust Interpreter Language Laboratory  *
!     *                   Finite element solver                             *
!     *                   by                                                *
!     *                   Wyn Williams 2022                                 *
!     *                   with contributions                                *
!     *                   Hubert-Minimization/Paths/Scripting            *
!     *                   by Karl Fabian  2014                              *
!     *                   recent contributors:                              *
!     *                   Les Nagy, Greig Paterson, Paddy O Conbhui,        *
!     *                   David Cortes, Phil Ridley                         *
!     *                   Jason Klebes                                 *
!     *                                                                     *
!     *                  Open source routines  by                           *
!     *                  Press et al. :   sorting                           *
!     *                  G. Benthien  :   string module                     *
!     *                  NAG library  :   sparse matrix solver              *
!     *                  H2Lib        :   matrix compression                *
!     ***********************************************************************

!> The MScript parser program
program BEM_Script

   use Loop_Parser, only: InitializeLoopParser, ReadLoopFile, AllLineCnt, NArgsOut, LineOutStack, DestroyLoopParser
   use strings, only: removesp, lowercase
   use Utils, only: MERRILL_VERSION_STRING, InitializeUtils
   use InitMerrill, only: InitializeMerrill, DestroyMerrill
   use Command_Parser, only: InitializeCommandParser, ParseCommand, DestroyCommandParser, NO_PARSE, PARSE_ERROR, PARSE_SUCCESS
   use Variable_Setter, only: InitializeVariableSetter, DestroyVariableSetter
   use iso_c_binding, only: c_double

   implicit none

   integer:: i, nargs, linecnt, ransalt
   character(LEN = 200) :: &
      arg1, scriptfile, line, args(20), &
      command, ransaltstr
   logical:: exist2
   integer:: ierr

   !
   ! Process command line arguments
   !
   call get_command_argument(1, arg1) 
   write (*, *) MERRILL_VERSION_STRING
   
   if (arg1 == "--version" .or. arg1 == "-v") then
      ! detect request for version number ... having printed version number, exit
      STOP
   else 
   !>cccccccccccccccccc  Argument is the name of the script file  ccccccccccccccc
   scriptfile = arg1
   endif 


   call RemoveSp(scriptfile)
   write (*, *) 'Script file is : <'//scriptfile(:len_trim(scriptfile))//'>'

   !cc Check that to see if there is a random 'salt' (useful for parallel) ccccc
   if (command_argument_count() > 1) then
      call get_command_argument(2, ransaltstr)
      call removeSp(ransaltstr)
      read (ransaltstr, '(I10)') ransalt
      write (*, *) 'Using salt value:', ransalt
   else
      ransalt = 0
   end if

   !---------------------------------------------------------------
   !            Initialize all modules used.
   !---------------------------------------------------------------

   call InitializeMerrill()

   ! Reinitialize Utils (ie sdrand) with ransalt (BEFORE DRAND is called!)
   call InitializeUtils(sdrand_salt = ransalt)

   call InitializeCommandParser()
   call InitializeVariableSetter()
   call InitializeLoopParser()

   !  CHECK IF INPUT FILES EXIST
   inquire (FILE = scriptfile, EXIST = exist2)
   do while (exist2 .eqv. .false.)
      if (exist2) then
         continue
      else
         write (*, *) 'Cannot OPEN file :', scriptfile
         write (*, *) 'Input new script file name:'
         read (*, *) scriptfile
         scriptfile = scriptfile(:len_trim(scriptfile))
         inquire (FILE = scriptfile, EXIST = exist2)
         if (exist2) write (*, *) 'Found scriptfile :', scriptfile
      end if
   end do

   !---------------------------------------------------------------
   !            SCRIPT LANGUAGE INTERPRETER
   !---------------------------------------------------------------
   !            Start reading and executing scriptfile.
   !            Relies heavily on the parsing functions of Module strings
   !            by  G. Benthien (http://gbenthien.net/strings/)
   !---------------------------------------------------------------
   !
   call ReadLoopFile(scriptfile)  ! Performs loop parser

   do linecnt = 1, AllLineCnt

      nargs = NArgsOut(linecnt)
      args(1:nargs) = LineOutStack(linecnt, 1:nargs)
      args(nargs+1:) = ""

      command = lowercase(args(1))
      line = ''
      do i = 1, nargs
         line = line(:len_trim(line))//' '//args(i) (:len_trim(args(i)))
      end do
      write (*, *) " Parsing_commands : "//line(:len_trim(line))

      ierr = NO_PARSE

      select case (command)
      case ('stop', 'end')
         write (*, *) "Done"
         ierr = PARSE_SUCCESS
         exit
      case ('define', 'undefine', 'addto')
         ierr = PARSE_SUCCESS
         continue
      case DEFAULT
         call ParseCommand(trim(command), args(1:nargs), ierr)
      end select

      if (ierr .eq. NO_PARSE) then
         write (*, *) "Unknown command '", trim(command), "' on line", linecnt
         write (*, *) "Line:", trim(line)
         write (*, *) "Exiting!"
         exit
      else if (ierr .eq. PARSE_ERROR) then
         write (*, *) "Error in '", trim(command), "' on line", linecnt
         write (*, *) "Line: ", trim(line)
         write (*, *) "Exiting!"
         exit
      end if

   end do

   call DestroyLoopParser()
   call DestroyVariableSetter()
   call DestroyCommandParser()

   call DestroyMerrill()

   stop 'Scripting finished'

end program BEM_Script
