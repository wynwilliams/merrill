!> Prebuilt_Measurements contains a set of standard measurement routines
!> that can be called by sinlge line script commands
!> @todo Tidy up the code
module Prebuilt_Measurements
   use strings
   use Utils
   use Mesh_IO
   use Tetrahedral_Mesh_Data
   use Energy_Calculator
   use Material_Parameters
   use Magnetization_Path
   use Finite_Element_Matrices

   implicit none

   character(LEN=1024) :: forcfile, forcparamfile

contains

!
! Function to simulate a changing field loop(i.e., hysteresis loop)
! All fields are in mT
! HystLoop(Bmax, Bmin, Bstep)
! Bmax the peak field
! Bmin to minimum field
! Bstep the field step size
!
   subroutine HystLoop(Bmax, Bmin, Bstep, StartFlag)
      use Minimizing_Loops, only: EnergyMinMult
      implicit none

      real(KIND=DP) :: Bmax, Bmin, Bstep, Btmp

      integer :: Nstep, i, StartFlag

      packing = 'block' ! default to block format ONLY
      !> @todo Add option to change packing ?

      Nstep = floor(abs((Bmax - Bmin)/Bstep)) + 1

      if (Bstep == 0) then
         Nstep = 1
      end if

    !! Set a saturated magnetization in the field direction if StartFlag >=0
      !CALL setuniform(hz(1), hz(2), hz(3)) ! Doesn't work as in command parser
      if (StartFlag .ge. 0) then
         write (*, *) 'HYSTLOOP Setting saturated initial guess'
         do i = 1, NNODE
            m(i, :) = hz(:)
         end do
      else
         write (*, *) 'HYSTLOOP not setting saturated initial guess'
         write (*, *) 'Please ensure you have set an appriate initial state explictly'
      end if

      ! Loop through the fields
      do i = 1, Nstep

         ! Set the external field
         Btmp = Bmax + (i - 1)*Bstep
         extapp = Btmp/1000. ! Default assume mT

         !IF (i==1) THEN
         ! Set an initial state or make user do it???
         !END IF

         ! Randomize the moments
         call randomchange('Magnetization', '5')

         ! Do the minimization
         call EnergyMinMult()

         ! Write out the results
         call WriteLoopData()

         ! Tecplot file
         write (ZStr, '(F14.4 A)') Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

         !> @todo add flexibility for data output types?

      end do

   end subroutine HystLoop

!
! Function to simulate an IRM acquisiton curve
! All fields are in mT
! IRM(Bmax, Bstep)
! Bmax the peak field
! Bstep the field step size (linear spacing)
!> @todo Add status output to STDOUT
   subroutine IRM(Bmax, Bstep)
      use Minimizing_Loops, only: EnergyMinMult
      implicit none

      real(KIND=DP) :: Bmax, Bmin, Bstep, Btmp, Binterp, MAX_MdotB
      real(KIND=DP), dimension(:), allocatable :: MdotB_ZF, MdotB_IF, Bvals, Dot_Diff
      character(len=10) :: B_str
      character(len=1024) :: orig_stem

      integer :: Nstep, i, j

      orig_stem = stem

      packing = 'block' ! default to block format ONLY
      !> @todo Add option to change packing ?

      Nstep = floor(abs((Bmax)/Bstep)) + 1

      if (Bstep == 0) then
         Nstep = 1
      end if

    !! Allocate the size of MdotB to record the magnetization
      allocate (MdotB_ZF(Nstep))
      allocate (MdotB_IF(Nstep))
      allocate (Bvals(Nstep))
      allocate (Dot_Diff(Nstep))

    !! Set a saturated magnetization in the field direction
      !CALL setuniform(hz(1), hz(2), hz(3)) ! Doesn't work as in command parser
      do i = 1, NNODE
         m(i, :) = hz(:)
      end do

    !! Get the Ms max
      call Get_MdotB(MAX_MdotB)
      write (*, *) 'Maximum saturation is ', MAX_MdotB, 'Am2'

      ! Start with a randomized guess
      ! This the zero field magnetization
      ! will minimize to a random direction
      call randomchange('all', 'moments')

      ! IRM Steps
      do i = 1, Nstep

         ! Set the external field
         Btmp = (i - 1)*Bstep
         write (B_str, '(F10.4)') Btmp/1000
         extapp = Btmp/1000. ! Default assume mT
         Bvals(i) = Btmp

         write (*, *) 'Setting in-field step @ ', Btmp, 'mT'

         ! Randomize the moments
         call randomchange('Magnetization', '5')
         ! Do the minimization
         call EnergyMinMult()

         ! Write out the results
         stem = orig_stem(:len_trim(orig_stem))//'_IRM_InField'
         loopfile = stem(:len_trim(stem))//'.irm'
         call WriteLoopData(['B'], [B_str])

         ! Tecplot file
         write (ZStr, '(A, F14.4, A)') 'IRM ', Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

         ! Get the in-field momenet and normalize it to [0,1]
         call Get_MdotB(MdotB_IF(i))
         Dot_Diff(i) = abs((MdotB_IF(i) - MdotB_IF(1))/(MAX_MdotB - MdotB_IF(1)))

         write (*, *) 'Normalized in-field momement = ', 100.*Dot_Diff(i), '%'

         ! Check if we need to ramp the field down slowly
         if (Dot_Diff(i) .ge. 0.75) then
            ! Stop at 75%, 50% and 25% of saturation before zero field
            do j = 1, 3

               if (j == 1) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 75.d-2, Binterp)
               else if (j == 2) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 50.d-2, Binterp)
               else
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
               end if

               extapp = Binterp/1000.
               write (*, *) 'Ramping field down @ ', Binterp, 'mT'
               ! Randomize the moments
               call randomchange('Magnetization', '5')
               ! Do the minimization
               call EnergyMinMult()

            end do

         else if (Dot_Diff(i) .ge. 0.5) then
            ! Stop at 50% and 25% of saturation before zero field

            do j = 1, 2
               if (j == 1) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 50.d-2, Binterp)
               else
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
               end if

               extapp = Binterp/1000.
               write (*, *) 'Ramping field down @ ', Binterp, 'mT'
               ! Randomize the moments
               call randomchange('Magnetization', '5')
               ! Do the minimization
               call EnergyMinMult()

            end do

         else if (Dot_Diff(i) .ge. 0.25) then
            ! Stop at 25% of saturation before zero field
            call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)

            extapp = Binterp/1000.
            write (*, *) 'Ramping field down @ ', Binterp, 'mT'
            ! Randomize the moments
            call randomchange('Magnetization', '5')
            ! Do the minimization
            call EnergyMinMult()

         end if

         ! Set for the zero field step
         write (*, *) 'Zero field step after ', Btmp, 'mT in field step'
         extapp = 0

         ! Randomize the moments
         call randomchange('Magnetization', '5')
         ! Do the minimization
         call EnergyMinMult()
         call Get_MdotB(MdotB_ZF(i))

         ! Write out the results
         stem = orig_stem(:len_trim(orig_stem))//'_IRM_ZeroField'
         loopfile = stem(:len_trim(stem))//'.irm'
         call WriteLoopData(['B'], [B_str])

         ! Tecplot file
         write (ZStr, '(A, F14.4, A)') 'IRM ', Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

         !> @todo add flexibility for data output types?

      end do

   end subroutine IRM

!
! Function to simulate a DC demagnetization curve
! DCD(Bmax, Bstep)
! Bmax the peak negative field
! Bstep the field step size (linear spacing)
!> @todo Add status output to STDOUT
   subroutine DCD(Bmax, Bstep)
      use Minimizing_Loops, only: EnergyMinMult
      implicit none

      real(KIND=DP) :: Bmax, Bmin, Bstep, Btmp, Binterp, MAX_MdotB
      character(len=10) :: B_str
      character(len=1024) :: orig_stem
      real(KIND=DP), dimension(:), allocatable :: MdotB_ZF, MdotB_IF, Bvals, Dot_Diff
      !REAL(KIND=DP) :: mhdot, mag_av(3), InterpolationSum

      integer :: Nstep, i, j

      orig_stem = stem

      packing = 'block' ! default to block format ONLY
      !> @todo Add option to change packing ?

      Nstep = floor(abs((Bmax)/Bstep)) + 1

      if (Bstep == 0) then
         Nstep = 1
      end if

    !! Allocate the size of MdotB to record the magnetization
      allocate (MdotB_ZF(Nstep))
      allocate (MdotB_IF(Nstep))
      allocate (Bvals(Nstep))
      allocate (Dot_Diff(Nstep))

    !! Set a saturated magnetization in the field direction
      !CALL setuniform(hz(1), hz(2), hz(3)) ! Doesn't work as in command parser
      do i = 1, NNODE
         m(i, :) = hz(:)
      end do

    !! Get the Ms max
      call Get_MdotB(MAX_MdotB)
      write (*, *) 'Maximum saturation is ', MAX_MdotB, 'Am2'

    !! Reduce "field" slowly from saturation
    !! NOTE: This is mainly set for magnetite
      do i = 1, 3
         if (i == 1) then
            extapp = 80/1000.
         else if (i == 2) then
            extapp = 20/1000.
         else
            extapp = 5/1000.
         end if
         ! Do the minimization
         write (*, *) 'Decreasing from saturation, field @ ', extapp*1000., ' mT'
         call EnergyMinMult()
      end do

    !! DCD Steps
      do i = 1, Nstep
         ! Set the external field
         Btmp = -(i - 1)*Bstep
         write (B_str, '(F10.4)') Btmp/1000
         extapp = Btmp/1000. ! Default assume mT
         Bvals(i) = Btmp

         write (*, *) 'Setting in-field step @ ', Btmp, 'mT'

         ! Randomize the moments
         call randomchange('Magnetization', '5')
         ! Do the minimization
         call EnergyMinMult()

         ! Write out the results
         stem = orig_stem(:len_trim(orig_stem))//'_DCD_InField'
         loopfile = stem(:len_trim(stem))//'.dcd'
         call WriteLoopData(['B'], [B_str])

         ! Tecplot file
         write (ZStr, '(A, F14.4, A)') 'DCD ', Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

         ! Get the in-field momenet and normalize it to [0,1]
         call Get_MdotB(MdotB_IF(i))
         Dot_Diff(i) = abs((MdotB_IF(i) - MdotB_IF(1))/(-MAX_MdotB - MdotB_IF(1)))

         write (*, *) 'Normalized in-field momement = ', 100.*Dot_Diff(i), '%'

         ! Check if we need to ramp the field down slowly
         if (Dot_Diff(i) .ge. 0.75) then
            ! Stop at 75%, 50% and 25% of saturation before zero field
            do j = 1, 3

               if (j == 1) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 75.d-2, Binterp)
               else if (j == 2) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 50.d-2, Binterp)
               else
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
               end if

               extapp = Binterp/1000.
               write (*, *) 'Ramping field down @ ', Binterp, 'mT'
               ! Randomize the moments
               call randomchange('Magnetization', '5')
               ! Do the minimization
               call EnergyMinMult()

            end do

         else if (Dot_Diff(i) .ge. 0.5) then
            ! Stop at 50% and 25% of saturation before zero field

            do j = 1, 2
               if (j == 1) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 50.d-2, Binterp)
               else
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
               end if

               extapp = Binterp/1000.
               write (*, *) 'Ramping field down @ ', Binterp, 'mT'
               ! Randomize the moments
               call randomchange('Magnetization', '5')
               ! Do the minimization
               call EnergyMinMult()

            end do

         else if (Dot_Diff(i) .ge. 0.25) then
            ! Stop at 25% of saturation before zero field
            call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)

            extapp = Binterp/1000.
            write (*, *) 'Ramping field down @ ', Binterp, 'mT'
            ! Randomize the moments
            call randomchange('Magnetization', '5')
            ! Do the minimization
            call EnergyMinMult()

         end if

         ! Set for the zero field step
         write (*, *) 'Zero field step after ', Btmp, 'mT in field step'
         extapp = 0

         ! Randomize the moments
         call randomchange('Magnetization', '5')
         ! Do the minimization
         call EnergyMinMult()
         call Get_MdotB(MdotB_ZF(i))

         ! Write out the results
         stem = orig_stem(:len_trim(orig_stem))//'_DCD_ZeroField'
         loopfile = stem(:len_trim(stem))//'.dcd'
         call WriteLoopData(['B'], [B_str])

         ! Tecplot file
         write (ZStr, '(A, F14.4, A)') 'DCD ', Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

         !> @todo add flexibility for data output types?

      end do

   end subroutine DCD

!
! Function to simulate an IRM acquisition and DC demagnetization curve
! IRMDCD(Bmax, Bstep)
! Bmax the peak field (both positive nd negative)
! Bstep the field step size (linear spacing)
!> @todo Add status output to STDOUT
   subroutine IRMDCD(Bmax, Bstep, infileflag)
      use Minimizing_Loops, only: EnergyMinMult
      implicit none

      real(KIND=DP) :: Bmax, Bmin, Bstep, Btmp, Binterp, MAX_MdotB
      real(KIND=DP), dimension(:), allocatable :: MdotB_ZF, MdotB_IF, Bvals, Dot_Diff
      character(len=10) :: B_str
      character(len=1024) :: orig_stem

      integer :: Nstep, i, j, infileflag

      orig_stem = stem

      packing = 'block' ! default to block format ONLY
      !> @todo Add option to change packing ?

      Nstep = floor(abs((Bmax)/Bstep)) + 1

      if (Bstep == 0) then
         Nstep = 1
      end if

    !! Allocate the size of MdotB to record the magnetization
      allocate (MdotB_ZF(Nstep))
      allocate (MdotB_IF(Nstep))
      allocate (Bvals(Nstep))
      allocate (Dot_Diff(Nstep))

    !! Set a saturated magnetization in the field direction
      !CALL setuniform(hz(1), hz(2), hz(3)) ! Doesn't work as in command parser
      do i = 1, NNODE
         m(i, :) = hz(:)
      end do

    !! Get the Ms max
      call Get_MdotB(MAX_MdotB)
      write (*, *) 'Maximum saturation is ', MAX_MdotB, 'Am2'

      ! Start with a randomized guess
      ! This the zero field magnetization
      ! will minimize to a random direction
      if (infileflag .eq. 0) then
         call randomchange('all', 'moments')
      end if
      if (infileflag .ne. 0) then
         write (*, *) 'Using preloaded state for DCDIRM'
      end if
      ! IRM Steps
      write (*, *) 'Commencing IRM acquisition'
      do i = 1, Nstep

         ! Set the external field
         Btmp = (i - 1)*Bstep
         write (B_str, '(F10.4)') Btmp/1000
         extapp = Btmp/1000. ! Default assume mT
         Bvals(i) = Btmp

         write (*, *) 'Setting in-field step @ ', Btmp, 'mT'

         ! Randomize the moments
         call randomchange('Magnetization', '5')
         ! Do the minimization
         call EnergyMinMult()

         ! Write out the results
         stem = orig_stem(:len_trim(orig_stem))//'_IRM_InField'
         loopfile = stem(:len_trim(stem))//'.irm'
         call WriteLoopData(['B'], [B_str])

         ! Tecplot file
         write (ZStr, '(A, F14.4, A)') 'IRM ', Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

         ! Get the in-field momenet and normalize it to [0,1]
         call Get_MdotB(MdotB_IF(i))
         Dot_Diff(i) = abs((MdotB_IF(i) - MdotB_IF(1))/(MAX_MdotB - MdotB_IF(1)))

         write (*, *) 'Normalized in-field momement = ', 100.*Dot_Diff(i), '%'

         ! Check if we need to ramp the field down slowly
         if (Dot_Diff(i) .ge. 0.75) then
            ! Stop at 75%, 50% and 25% of saturation before zero field
            do j = 1, 3

               if (j == 1) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 75.d-2, Binterp)
               else if (j == 2) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 50.d-2, Binterp)
               else
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
               end if

               extapp = Binterp/1000.
               write (*, *) 'Ramping field down @ ', Binterp, 'mT'
               ! Randomize the moments
               call randomchange('Magnetization', '5')
               ! Do the minimization
               call EnergyMinMult()

            end do

         else if (Dot_Diff(i) .ge. 0.5) then
            ! Stop at 50% and 25% of saturation before zero field

            do j = 1, 2
               if (j == 1) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 50.d-2, Binterp)
               else
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
               end if

               extapp = Binterp/1000.
               write (*, *) 'Ramping field down @ ', Binterp, 'mT'
               ! Randomize the moments
               call randomchange('Magnetization', '5')
               ! Do the minimization
               call EnergyMinMult()

            end do

         else if (Dot_Diff(i) .ge. 0.25) then
            ! Stop at 25% of saturation before zero field
            call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
            extapp = Binterp/1000.
            write (*, *) 'Ramping field down @ ', Binterp, 'mT'
            ! Randomize the moments
            call randomchange('Magnetization', '5')
            ! Do the minimization
            call EnergyMinMult()

         end if

         ! Set for the zero field step
         write (*, *) 'Zero field step after ', Btmp, 'mT in field step'
         extapp = 0

         ! Randomize the moments
         call randomchange('Magnetization', '5')
         ! Do the minimization
         call EnergyMinMult()
         call Get_MdotB(MdotB_ZF(i))

         ! Write out the results
         stem = orig_stem(:len_trim(orig_stem))//'_IRM_ZeroField'
         loopfile = stem(:len_trim(stem))//'.irm'
         call WriteLoopData(['B'], [B_str])

         ! Tecplot file
         write (ZStr, '(A, F14.4, A)') 'IRM ', Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

         !> @todo add flexibility for data output types?

      end do

    !!!!!!!!!!!!!!!
    !! DCD Steps !!
    !!!!!!!!!!!!!!!
      write (*, *) 'Commencing DC demagnetization'
      ! Set the output field to zero
      Btmp = 0
      write (B_str, '(F10.4)') Btmp/1000
      Bvals(1) = Btmp

      write (*, *) 'Copying zero-field saturation remanence'
      call Get_MdotB(MdotB_IF(1))
      call Get_MdotB(MdotB_ZF(1))

      ! Write the infield data for IRM(Bmax)
      ! Since field is zero same a zerofield data (just determined)
      stem = orig_stem(:len_trim(orig_stem))//'_DCD_InField'
      loopfile = stem(:len_trim(stem))//'.dcd'
      call WriteLoopData(['B'], [B_str])

      ! Tecplot file
      write (ZStr, '(A, F14.4, A)') 'DCD ', Btmp, ' mT' ! The zone name
      call compact(ZStr)
      call WriteTecplot(zoneflag, stem, i)

      ! Write zerofield data for IRM(Bmax)
      stem = orig_stem(:len_trim(orig_stem))//'_DCD_ZeroField'
      loopfile = stem(:len_trim(stem))//'.dcd'
      call WriteLoopData(['B'], [B_str])

      ! Tecplot file
      write (ZStr, '(A, F14.4, A)') 'DCD ', Btmp, ' mT' ! The zone name
      call compact(ZStr)
      call WriteTecplot(zoneflag, stem, i)

      ! loop from first non-zero field
      do i = 2, Nstep

         ! Set the external field
         Btmp = -(i - 1)*Bstep
         write (B_str, '(F10.4)') Btmp/1000
         extapp = Btmp/1000. ! Default assume mT
         Bvals(i) = Btmp

         write (*, *) 'Setting in-field step @ ', Btmp, 'mT'

         ! Randomize the moments
         call randomchange('Magnetization', '5')
         ! Do the minimization
         call EnergyMinMult()

         ! Write out the results
         stem = orig_stem(:len_trim(orig_stem))//'_DCD_InField'
         loopfile = stem(:len_trim(stem))//'.dcd'
         call WriteLoopData(['B'], [B_str])

         ! Tecplot file
         write (ZStr, '(A, F14.4, A)') 'DCD ', Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

         ! Get the in-field momenet and normalize it to [0,1]
         call Get_MdotB(MdotB_IF(i))
         Dot_Diff(i) = abs((MdotB_IF(i) - MdotB_IF(1))/(-MAX_MdotB - MdotB_IF(1)))

         write (*, *) 'Normalized in-field momement = ', 100.*Dot_Diff(i), '%'

         ! Check if we need to ramp the field down slowly
         if (Dot_Diff(i) .ge. 0.75) then
            ! Stop at 75%, 50% and 25% of saturation before zero field
            do j = 1, 3

               if (j == 1) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 75.d-2, Binterp)
               else if (j == 2) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 50.d-2, Binterp)
               else
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
               end if

               extapp = Binterp/1000.
               write (*, *) 'Ramping field down @ ', Binterp, 'mT'
               ! Randomize the moments
               call randomchange('Magnetization', '5')
               ! Do the minimization
               call EnergyMinMult()

            end do

         else if (Dot_Diff(i) .ge. 0.5) then
            ! Stop at 50% and 25% of saturation before zero field

            do j = 1, 2
               if (j == 1) then
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 50.d-2, Binterp)
               else
                  call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)
               end if

               extapp = Binterp/1000.
               write (*, *) 'Ramping field down @ ', Binterp, 'mT'
               ! Randomize the moments
               call randomchange('Magnetization', '5')
               ! Do the minimization
               call EnergyMinMult()

            end do

         else if (Dot_Diff(i) .ge. 0.25) then
            ! Stop at 25% of saturation before zero field
            call interp_field(Dot_Diff(1:i), Bvals(1:i), 25.d-2, Binterp)

            extapp = Binterp/1000.
            write (*, *) 'Ramping field down @ ', Binterp, 'mT'
            ! Randomize the moments
            call randomchange('Magnetization', '5')
            ! Do the minimization
            call EnergyMinMult()

         end if

         ! Set for the zero field step
         write (*, *) 'Zero field step after ', Btmp, 'mT in field step'
         extapp = 0

         ! Randomize the moments
         call randomchange('Magnetization', '5')
         ! Do the minimization
         call EnergyMinMult()
         call Get_MdotB(MdotB_ZF(i))

         ! Write out the results
         stem = orig_stem(:len_trim(orig_stem))//'_DCD_ZeroField'
         loopfile = stem(:len_trim(stem))//'.dcd'
         call WriteLoopData(['B'], [B_str])

         ! Tecplot file
         write (ZStr, '(A, F14.4, A)') 'DCD ', Btmp, ' mT' ! The zone name
         call compact(ZStr)
         call WriteTecplot(zoneflag, stem, i)

      end do

   end subroutine IRMDCD

!
!> Function to simulate a simple FORC measurement
! All fields are in mT
! SimpleFORC(Bmax, Bmin, Bstep)
! Bmax the peak field
! Bmin to minimum field
! Bstep the field step size
! save_tec - Flag for saving the FORC multizone tecplot file (0/1) (no/yes)
! save_last - Flag for saving the last complete state to a .dat file (0/1) (no/yes)
! flag_restart - Flag for restarting a FORC model (0/1) (no/yes)
! iStart, jStart - Integer index for the Br and Bb fields, respectively, when restarting a FORC
!> @todo Add status output to STDOUT
   subroutine SimpleFORC(Bmax, Bmin, Bstep, save_tec, save_last, flag_restart, iStart, jStart)
      use Minimizing_Loops, only: EnergyMinMult
      implicit none

      real(KIND=DP) :: Bmax, Bmin, Bstep, Br, Bb

      integer :: Nr, i, Nb, j, save_tec, save_last, flag_restart, iStart, jStart, restart_forc
      integer(KIND=8) :: CLOCKCOUNT, newtime, oldtime! millisec count since Jan 1,1970
      integer(KIND=8) :: elaspetime, WriteInterval
      character(len=10) :: Br_str
      character(len=1) :: AB

      AB = 'A'
      oldtime = 0
      clockcount = 0
      restart_forc = flag_restart
      WriteInterval = 5*3600000 !(5 hours in milliseconds)
      !WriteInterval= 30000 !(30sec s in milliseconds)
      forcfile = stem(:len_trim(stem))//'.frc'

      packing = 'block' ! default to block format ONLY
      !> @todo Add option to change packing ?

      Nr = floor(abs((Bmax - Bmin)/Bstep)) + 1

      if (Bstep == 0) then
         Nr = 1
      end if

      ! The tecplot file with the hysteresis loop data
      ZoneInFile = stem(:len_trim(stem) - 5)//'_Hysteresis_mult.tec'

      ! Loop through the fields - Br reversal fields
      do i = iStart, Nr

         ! Set the external field
         Br = Bmax + (i - 1)*Bstep
         extapp = Br/1000. ! Default assume mT

         if (restart_forc .eq. 0) then
            ! Read in the hysteresis zone
            izone = i
            call ReadTecplotZone()
            jStart = 1
         end if

         Nb = floor(abs((Bmax - Br)/Bstep)) + 1

         do j = jStart, Nb ! the back fields

            Bb = Br - (j - 1)*Bstep
            extapp = Bb/1000. ! Default assume mT

            if (j > 1) then

               write (*, '(A, F10.3 ,  F10.3)') 'SimpleFORC commencing m(Br, Bb) = ', Br, Bb

               ! Randomize the moments
               call randomchange('Magnetization', '5')

               ! Do the minimization
               call EnergyMinMult()

            end if

            ! Write out the results
            write (Br_str, '(F10.4)') Br/1000
            call WriteLoopData(['Br'], [Br_str])

            call WriteGenericFORC()

            ! Tecplot file
            write (ZStr, '(A, F14.4, A, F14.4, A)') 'Br = ', Br, ' mT; Bb = ', Bb, ' mT'  ! The zone name
            call compact(ZStr)

            if (save_tec .eq. 1) then
               call WriteTecplot(zoneflag, stem, i)
            end if

            call system_clock(CLOCKCOUNT)
            elaspetime = CLOCKCOUNT - oldtime
            if ((save_last .eq. 1) .and. (elaspetime .ge. WriteInterval)) then
               if (AB .eq. 'A') then
                  datafile = stem(:len_trim(stem))//'_LastStateA.dat' ! save filename
                  call system_clock(CLOCKCOUNT)
                  oldtime = CLOCKCOUNT
                  AB = 'B'
                  !print*, 'AB, clock', AB, clockcount
               else
                  datafile = stem(:len_trim(stem))//'_LastStateB.dat' ! save filename
                  call system_clock(CLOCKCOUNT)
                  oldtime = CLOCKCOUNT
                  AB = 'A'
                  !print*, 'AB, clock', AB, clockcount
               end if
               call WriteMagForc(clockcount, Bb, Br, Bmax, Bmin, Bstep)
               !CALL execute_command_line('wc -l < file.txt > wc.txt' )
            end if

            ! reset the restart flag
            if (j .eq. Nb) then
               restart_forc = 0
            end if

         end do ! Sweep field loop

         call WriteGenericFORC('SPACE')

         !> @todo add flexibility for data output types?

      end do ! Reversal field loop

      !CALL WriteGenericFORC('SPACE')
      call WriteGenericFORC('END')

   end subroutine SimpleFORC

!> Read the mesh, data, and restarting fields to restart a FORC model
!
   subroutine RestartSimpleFORC()

      implicit none

      character(len=1024) :: old_stem, hyststatefile
      character(len=3) :: flag_save_tec
      character(len=6) :: header
      character(len=100), dimension(:), allocatable :: Hyst_Header, FORC_Header
      real(KIND=DP), dimension(:, :), allocatable :: Hyst_DataVals, FORC_DataVals
      real(KIND=DP), dimension(:), allocatable :: tmp_fields

      real(KIND=DP) :: Bsat, Bmin, Bstep, last_Bb, last_Br
      real(KIND=DP) :: pass(5)
      integer :: ios, save_tec, save_last, nFORC, nMeas, Hyst_Start
      integer :: idx_Bb, idx_Br, idx_good, dunit
      integer(KIND=8)  :: clockcount, clockcountA, clockcountB
      logical :: exist1, exist2, exist3, existA, existB

    !! DEFINE THE FILE NAMES TO SAVE TO

      old_stem = stem ! Save the original input name
      loopfile = stem(:len_trim(stem))//'.loop'
      hyststatefile = stem(:len_trim(stem))//'_Hysteresis_mult.tec'

      ! Read in the hystersis loop data - Hyst_DataVals has header stripped
      call ReadLoopData(loopfile, Hyst_DataVals, Hyst_Header)
      ! set the field direction from columns 2,3,4 of Hyst_DataVals
      hz(1) = Hyst_DataVals(1, 2)
      hz(2) = Hyst_DataVals(1, 3)
      hz(3) = Hyst_DataVals(1, 4)

      ! Redefine the loop file for the FORC
      stem = old_stem
      stem = stem(:len_trim(stem))//'_FORC' ! Reinstate the original name for the FORC
      datafile = stem(:len_trim(stem))//'_LastState.dat' ! file to save last complete state
      loopfile = stem(:len_trim(stem))//'.loop'

      ! Read in the FORC loop data - FORC_DataVals has header stripped
      ! if this does not exist then FORC simulations had not yet started
      ! Check done inside ReadLoopFile
      call ReadLoopData(loopfile, FORC_DataVals, FORC_Header)

      !WRITE(*,*) FORC_Header
      !WRITE(*,*)
      !WRITE(*,*) FORC_DataVals

      ! check if the Hysteresis state file exists
      ! this has the mes that we want to read
      inquire (FILE=hyststatefile, EXIST=exist1)

      if (exist1 .eqv. .false.) then
         write (*, '(A35 A50)') 'Hysteresis state file not found: ', hyststatefile
         stop
      end if

      ! Check the files for reading/saving output
      save_last = 1 ! assume by default since this is a restart

      ! Flag for using last save state (0) or major hysteresis (1) for restart
      Hyst_Start = 0

      ! Check to see which of the last complete state file exists
      ! there are three possibilities , LastState, LastStateA or LastStateB
      ! LastState needed for backward compatability - should be deprectaed at some point
      ! If any one of these exists we shoudl try and use it in a restart
      ! with LastState only beig used if LastStateA or LastStateB do not exist
      inquire (FILE=datafile, EXIST=exist1)
      datafile = stem(:len_trim(stem))//'_LastStateA.dat'
      inquire (FILE=datafile, EXIST=existA)
      datafile = stem(:len_trim(stem))//'_LastStateB.dat'
      inquire (FILE=datafile, EXIST=existB)

      if ((exist1 .eqv. .false.) .and. (existA .eqv. .false.) .and. (existB .eqv. .false.)) then
         ! Does not exist
         ! Use the hysteresis for the last state
         write (*, '(A)') 'No last state file found, restarting from Hysteresis'
         Hyst_Start = 1
      end if

      save_tec = 0
      flag_save_tec = 'OFF'
      inquire (FILE=stem//"_mult.tec", EXIST=exist3)

      if (exist3 .eqv. .true.) then
         save_tec = 1
         flag_save_tec = 'ON'
      end if

      ! Read the mesh from the hystesis file
      call ReadTecplotMesh(hyststatefile)

      ! Define the FORC parameters
      nFORC = size(Hyst_DataVals, 1) - 1
      Bsat = Hyst_DataVals(1, 1)

      Bmin = -Bsat
      Bstep = (-2*Bsat)/nFORC

      nMeas = size(FORC_DataVals, 1)

      last_Bb = FORC_DataVals(nMeas, 1)
      last_Br = FORC_DataVals(nMeas, 10)

      ! TESTING
      ! write(*,*) ' nFORC (from Hyst) = ',nFORC
      ! write(*,*) 'Bsat (from Hyst) = ',Bsat, 'BStep .i.e. 2*Bsat/nFORC = ', Bstep
      ! Write(*,*) 'nMeas i.e. number of field in hysteresis loop = ', nMeas
      ! Write(*,*) 'Last Bb = ', last_Bb, '  last Br = ',last_Br

      ! check to see if the FORC is COMPLEATE
      if (last_Bb .eq. Bsat) then
         !write(*,*) 'last Bb step WAS at saturation, we have a complete FORC'
         ! last Bb step was at saturation, we have a complete FORC
         ! so next step is at Br = next Bb
         idx_Br = minloc(abs(Hyst_DataVals(:, 1) - last_Br), DIM=1) + 1

         ! check to see if this index is at the last FORC  - ie all FORCs are complete
         ! idx_Br already has 1 added to it, so if this index is greater than the length of the hyst file we are done.
         if (idx_Br .gt. size(Hyst_DataVals, 1)) then
            write (*, *) ' THIS FORC SIMULATION IS ALREADY COMPLETE - stopping'
            stop
         end if
      end if

      ! Set the magnetization
      if (Hyst_Start .ne. 1) then ! restart from one of the last saved file

         ios = -999 ! default read file status is fail

      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
         ! if both laststateA and laststateB exist then read in most recentc complete state
         if ((existA .eqv. .true.) .and. (existB .eqv. .true.)) then ! the usual senario

            write (*, *) 'ATTEMPTING TO RESTART FORC FROM LAST SAVED STATE'
            clockcountA = 0
            clockcountB = 0

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            ! try reading from one of the LastStateA or LastStateB files  !!
            ! read the most recent , if fails try the older               !!
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (existA .eqv. .true.) then
               datafile = stem(:len_trim(stem))//'_LastStateA.dat'
               open (NEWUNIT=dunit, FILE=datafile, STATUS='unknown')
               read (dunit, *) header, clockcountA
               close (dunit)
            end if

            if (existB .eqv. .true.) then
               datafile = stem(:len_trim(stem))//'_LastStateB.dat'
               open (NEWUNIT=dunit, FILE=datafile, STATUS='unknown')
               read (dunit, *) header, clockcountB
               close (dunit)
            end if

            ! try reading the most recent
            if (clockcountB .gt. clockcountA) then !  state B is most recent
               datafile = stem(:len_trim(stem))//'_LastStateB.dat'
               write (*, '(A13 A50)') ' Try reading ', datafile
               call ReadMagFORC(datafile, pass, ios) ! pass contains: Bb, Br, Bmax , Bmin ,  Bstep
               last_Bb = pass(1)/1000.
               last_Br = pass(2)/1000.
               if (ios .eq. 0) write (*, '(A20 A50)') '- Successfully read ', datafile
            else
               datafile = stem(:len_trim(stem))//'_LastStateA.dat'
               write (*, '(A13 A50)') ' Try reading ', datafile
               call ReadMagFORC(datafile, pass, ios) ! pass contains: Bb, Br, Bmax , Bmin ,  Bstep
               last_Bb = pass(1)/1000.
               last_Br = pass(2)/1000.
               if (ios .eq. 0) write (*, '(A20 A50)') '- Successfully read ', datafile
            end if

            if (ios < 0) then !  the most recet file is incomplete, try reading from older file
               if (clockcountB .lt. clockcountA) then !  state B is older file
                  datafile = stem(:len_trim(stem))//'_LastStateB.dat'
                  write (*, '(A50 A50)') '** Most recent saved file is incomplete, trying ', datafile
                  !write(*,'(A13 A50)') ' Try reading ',datafile
                  call ReadMagFORC(datafile, pass, ios) ! pass contains: Bb, Br, Bmax , Bmin ,  Bstep
                  last_Bb = pass(1)/1000.
                  last_Br = pass(2)/1000.
                  if (ios .eq. 0) write (*, '(A20 A50)') '- Successfully read ', datafile
               else
                  datafile = stem(:len_trim(stem))//'_LastStateA.dat'
                  write (*, '(A50 A50)') '** Most recent saved file is incomplete, trying ', datafile
                  !write(*,'(A13 A50)') ' Try reading ',datafile
                  call ReadMagFORC(datafile, pass, ios) ! pass contains: Bb, Br, Bmax , Bmin ,  Bstep
                  last_Bb = pass(1)/1000.
                  last_Br = pass(2)/1000.
                  if (ios .eq. 0) write (*, '(A20 A50)') '- Successfully read ', datafile
               end if
            end if
         end if ! for when both A and B exist
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

         ! If above failed its possible that only state A exists, then ios will still be set to 999
         if ((ios < 0) .and. ((existA .eqv. .true.))) then ! try reading state A
            write (*, '(A13 A50)') ' Try reading ', datafile
            datafile = stem(:len_trim(stem))//'_LastStateA.dat'
            call ReadMagFORC(datafile, pass, ios) ! pass contains: Bb, Br, Bmax , Bmin ,  Bstep
            last_Bb = pass(1)/1000.
            last_Br = pass(2)/1000.
            if (ios .eq. 0) write (*, '(A20 A50)') '- Succsessfully read ', datafile
         end if

         ! If above failed its  still possible that only state B exists, then ios will still be set to 999
         if ((ios < 0) .and. ((existB .eqv. .true.))) then ! try reading state A
            write (*, '(A13 A50)') ' Try reading ', datafile
            datafile = stem(:len_trim(stem))//'_LastStateB.dat'
            call ReadMagFORC(datafile, pass, ios) ! pass contains: Bb, Br, Bmax , Bmin ,  Bstep
            last_Bb = pass(1)/1000.
            last_Br = pass(2)/1000.
            if (ios .eq. 0) write (*, '(A20 A50)') '- Sucscessfully read ', datafile
         end if

         ! if failed to read LastStateA or LastStateB its possible legacy data is being restarted, try to
         ! read from LastState
         if (ios < 0) then !  neither A or B are any good
            datafile = stem(:len_trim(stem))//'_LastState.dat'
            write (*, '(A13 A50)') ' Try reading ', datafile
            call ReadMag(datafile, ios)  ! this file has no header so use ReadMag
            if (ios .eq. 0) write (*, '(A20 A50)') '- Successfully read ', datafile
         end if

         if (ios < 0) then ! all attempts read of last saved data file failed - start from Hyst file instead
            Hyst_Start = 1
            write (*, *) 'Attempt to read last saved state failed - forcing restart from MAJOR HYSTERESIS BRANCH'
            goto 11
         end if

         !print*, ' in restsrt' ,pass(1), pass(2), pass(3), pass(4), pass(5)
         if (last_Bb .eq. Bsat) then
            !write(*,*) 'last Bb step WAS at saturation, we have a complete FORC'
            ! last Bb step was at saturation, we have a complete FORC
            ! so next step is at Br = next Bb
            idx_Br = minloc(abs(Hyst_DataVals(:, 1) - last_Br), DIM=1) + 1

            ! check to see if this index is at the last FORC  - ie all FORCs are complete
            ! idx_Br already has 1 added to it, so if this index is greater than the length of the hyst file we are done.
            if (idx_Br .gt. size(Hyst_DataVals, 1)) then
               write (*, *) ' THIS FORC SIMULATION IS ALREADY COMPLETE - stopping'
               stop
            end if

            tmp_fields = Hyst_DataVals(idx_Br:1:-1, 1)
            idx_Bb = 1
            !write (*,*) 'idx_Br (Br Index) , VAL= ',idx_Br, Hyst_DataVals(idx_Br,1)*1000,'idx_Bb (Bb Index) = ', idx_Bb
         else
            !write(*,*) 'last Bb step WAS NOT at saturation, we have an incomplete FORC'
            ! last Bb step was not at saturation, we have an incomplete FORC
            ! Next step is at current Br, and next Bb
            idx_Br = minloc(abs(Hyst_DataVals(:, 1) - last_Br), DIM=1)
            tmp_fields = Hyst_DataVals(idx_Br:1:-1, 1)
            idx_Bb = minloc(abs(tmp_fields - last_Bb), DIM=1) + 1

         end if
11       continue
      end if ! end of attempts to read a restart file

      if (Hyst_Start .eq. 1) then ! no saved file last state - restart from major loop
         ! Read the hysteresis state
         ! First find the relavant field index

         write (*, *) 'RESTARTING FORC FROM MAJOR HYSTERESIS BRANCH'

         if (last_Bb .eq. Bsat) then
            !write(*,*) 'last Bb step was at saturation, we have a complete FORC'
            ! last Bb step was at saturation, we have a complete FORC
            ! so next step is at Br = next Bb
            idx_Br = minloc(abs(Hyst_DataVals(:, 1) - last_Br), DIM=1) + 1
            tmp_fields = Hyst_DataVals(idx_Br:1:-1, 1)
            idx_Bb = 1
         else
            !write(*,*) 'last Bb step was not at saturation, we have an incomplete FORC'
            ! last Bb step was not at saturation, we have an incomplete FORC
            ! Restart at current Br and first Bb
            idx_Br = minloc(abs(Hyst_DataVals(:, 1) - last_Br), DIM=1)
            tmp_fields = Hyst_DataVals(idx_Br:1:-1, 1)
            idx_Bb = 1

            !set last_Br and last_Bb to get the corrext index get to correct point in the FORC loop point
            last_Br = last_Br - Bstep
            last_Bb = Bsat

         end if

         ! Read in the magnetization
         izone = idx_Br
         ZoneInFile = hyststatefile
         call ReadTecplotZone()
         write (*, *) ' Data read from Hyseresis File ', ZoneInFile

      end if

      !WRITE(*,*) tmp_fields

      !> @todo up date actual field starts
      ! Write out confirmation
      write (*, *)
      write (*, *) 'SimpleFORC restarting with: '
      write (*, '(A, F10.3, A)') ' Bsat = ', Bsat*1000, ' mT'
      write (*, *) 'nFORCs = ', nFORC
      write (*, *) 'Save Tecplot file ', flag_save_tec
      write (*, *) 'Save last ON'
      write (*, *)
      ! commented out below since this is printed out withing SimpleForc subroutine
      ! WRITE(*,*) 'Restarting from: '
      !WRITE(*,*)
      !WRITE(*,*) 'START: ', idx_Br, idx_Bb
      !WRITE(*,*)
      ! WRITE(*,'(A, F10.3, A, F10.3, A)') 'START: Br = ',Hyst_DataVals(idx_Br,1)*1000,' mT;  Bb = ',tmp_fields(idx_Bb)*1000,' mT'
      ! WRITE(*,*)
      !WRITE(*,*) last_Br, last_Bb

      !STOP ! for testing
      ! Actually do the restart

      ! calualte teh index of the last good data point in teh forloop file
      idx_good = minloc((abs(FORC_DataVals(:, 1) - last_Bb) + abs(FORC_DataVals(:, 10) - last_Br)), DIM=1)
      call FORCLOOP_rewrite(idx_good)
      call SimpleFORC(Bsat*1000, Bmin*1000, Bstep*1000, save_tec, save_last, 1, idx_Br, idx_Bb)

      ! 3000 FORMAT(7(f10.6, ' '))
      ! 3001 FORMAT(A6, I16)
      ! 3002 FORMAT(5A14)
      ! 3003 FORMAT(5(f14.4, ' '))
   end subroutine RestartSimpleFORC

   subroutine FORCLOOP_rewrite(idx_good)

      implicit none
      integer :: aunit, bunit, idx_good, i, counter
      character(len=1024) :: old_stem, back_loopfile
      character(len=1024) :: line
      old_stem = stem ! Save the original input name
      back_loopfile = stem(:len_trim(stem))//'_BACK.loop'
      loopfile = stem(:len_trim(stem))//'.loop'
      counter = 0

      open (NEWUNIT=aunit, FILE=loopfile, STATUS='unknown')
      open (NEWUNIT=bunit, FILE=back_loopfile, STATUS='unknown')
      ! copy file to back up (n.b. dont use system commands since may vary with operating system)
      do while (.true.)
         counter = +1
         read (aunit, '(A)', end=99) line! read line from input file
         write (bunit, '(A)') trim(line)
      end do

      !close/rewind files
99    continue
      close (aunit)
      rewind (bunit)

      ! overwrite loops file frombackup and stop at the correct point
      open (NEWUNIT=aunit, FILE=loopfile, STATUS='unknown')
      !OPEN(NEWUNIT=bunit, FILE=loopfile, STATUS='unknown')
      do i = 1, idx_good + 1   ! extra one to include the header being written
         read (bunit, '(A)') line ! read line from input file
         write (aunit, '(A)') trim(line)
      end do

      close (aunit)
      close (bunit)

   end subroutine FORCLOOP_rewrite

   subroutine WriteGenericFORC(args)

      implicit none

      character(len=*), optional :: args
      real(KIND=DP) :: mhdot, mag_av(3), InterpolationSum
      logical :: forcexist
      integer :: forcunit, i, l

      inquire (FILE=forcfile, EXIST=forcexist)

      open (NEWUNIT=forcunit, FILE=forcfile, STATUS='unknown', POSITION='append')

      if (present(args)) then
         ! Take specific space or end commands
         select case (args)
         case ("END")
            write (forcunit, '(A)', ADVANCE='no') trim('END')
            close (forcunit)
         case Default
            write (forcunit, *)
            close (forcunit)
         end select

      else
         ! Write out data

         mhdot = 0 ! Moment in field direction

         ! Calculate the average magnetisation per node over neighbouring elements which can be of
         ! different material
         do i = 1, NNODE
            mag_av(:) = 0
            InterpolationSum = 0 ! The summation of the interpolation matrix for a given node
            do l = CNR_IM(i), CNR_IM(i + 1) - 1
               mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
               InterpolationSum = InterpolationSum + InterpolationMatrix(l)
            end do
            mhdot = mhdot + (mag_av(1)*Hz(1) + mag_av(2)*Hz(2) + mag_av(3)*Hz(3))*vbox(i)/(sqrt(Ls)**3)/InterpolationSum
         end do

         write (forcunit, '(F7.3, A, ES16.8)') extapp, ', ', mhdot
         close (forcunit)

      end if

   end subroutine

   subroutine Get_MdotB(mhdot)
      ! Calculate the dot(B, M)
      real(KIND=DP) :: mhdot, mag_av(3), InterpolationSum
      integer :: j, l

      mhdot = 0 ! Moment in field direction

      ! calculate the average magnetisation per node over neighbouring elements which can be of
      ! different material
      do j = 1, NNODE
         mag_av(:) = 0
         InterpolationSum = 0 ! The summation of the interpolation matrix for a given node
         do l = CNR_IM(j), CNR_IM(j + 1) - 1
            mag_av(:) = mag_av(:) + Ms(SDNR_IM(l))*m(RNR_IM(l), :)*InterpolationMatrix(l)
            InterpolationSum = InterpolationSum + InterpolationMatrix(l)
         end do
         mhdot = mhdot + (mag_av(1)*Hz(1) + mag_av(2)*Hz(2) + mag_av(3)*Hz(3))*vbox(j)/(sqrt(Ls)**3)/InterpolationSum
      end do

   end subroutine

end module Prebuilt_Measurements
