module slatec_avint
   implicit none
contains

!DECK AVINT
   subroutine AVINT(X, Y, N, XLO, XUP, ANS, IERR)
!***BEGIN PROLOGUE  AVINT
!***PURPOSE  Integrate a function tabulated at arbitrarily spaced
!            abscissas using overlapping parabolas.
!***LIBRARY   SLATEC
!***CATEGORY  H2A1B2
!***TYPE      SINGLE PRECISION (AVINT-S, DAVINT-D)
!***KEYWORDS  INTEGRATION, QUADRATURE, TABULATED DATA
!***AUTHOR  Jones, R. E., (SNLA)
!***DESCRIPTION
!
!     Abstract
!         AVINT integrates a function tabulated at arbitrarily spaced
!         abscissas.  The limits of integration need not coincide
!         with the tabulated abscissas.
!
!         A method of overlapping parabolas fitted to the data is used
!         provided that there are at least 3 abscissas between the
!         limits of integration.  AVINT also handles two special cases.
!         If the limits of integration are equal, AVINT returns a result
!         of zero regardless of the number of tabulated values.
!         If there are only two function values, AVINT uses the
!         trapezoid rule.
!
!     Description of Parameters
!         The user must dimension all arrays appearing in the call list
!              X(N), Y(N).
!
!         Input--
!         X    - real array of abscissas, which must be in increasing
!                order.
!         Y    - real array of functional values. i.e., Y(I)=FUNC(X(I)).
!         N    - the integer number of function values supplied.
!                N .GE. 2 unless XLO = XUP.
!         XLO  - real lower limit of integration.
!         XUP  - real upper limit of integration.
!                Must have XLO .LE. XUP.
!
!         Output--
!         ANS  - computed approximate value of integral
!         IERR - a status code
!              --normal code
!                =1 means the requested integration was performed.
!              --abnormal codes
!                =2 means XUP was less than XLO.
!                =3 means the number of X(I) between XLO and XUP
!                   (inclusive) was less than 3 and neither of the two
!                   special cases described in the Abstract occurred.
!                   No integration was performed.
!                =4 means the restriction X(I+1) .GT. X(I) was violated.
!                =5 means the number N of function values was .LT. 2.
!                ANS is set to zero if IERR=2,3,4,or 5.
!
!     AVINT is documented completely in SC-M-69-335
!     Original program from "Numerical Integration" by Davis &
!     Rabinowitz.
!     Adaptation and modifications for Sandia Mathematical Program
!     Library by Rondall E. Jones.
!
!***REFERENCES  R. E. Jones, Approximate integrator of functions
!                 tabulated at arbitrarily spaced abscissas,
!                 Report SC-M-69-335, Sandia Laboratories, 1969.
!***ROUTINES CALLED  XERMSG
!***REVISION HISTORY  (YYMMDD)
!   690901  DATE WRITTEN
!   890831  Modified array declarations.  (WRB)
!   890831  REVISION DATE from Version 3.2
!   891214  Prologue converted to Version 4.0 format.  (BAB)
!   900315  CALLs to XERROR changed to CALLs to XERMSG.  (THJ)
!   900326  Removed duplicate information from DESCRIPTION section.
!           (WRB)
!   920501  Reformatted the REFERENCES section.  (WRB)
!***END PROLOGUE  AVINT
!
      double precision, intent(IN) :: X(*), Y(*), XLO, XUP
      double precision, intent(OUT) :: ANS
      integer, intent(IN)  :: N
      integer, intent(OUT) :: IERR

      double precision :: R3, RP5, SUM, SYL, SYL2, SYL3, SYU, SYU2, SYU3, X1, X2, X3 &
         , X12, X13, X23, TERM1, TERM2, TERM3, A, B, C, CA, CB, CC
      double precision SLOPE, FL, FR
      integer :: I, INLFT, INRT, ISTART, ISTOP
!***FIRST EXECUTABLE STATEMENT  AVINT
      IERR = 1
      ANS = 0.0

      CA = 0
      CB = 0
      CC = 0

      !IF (XLO-XUP) 3,100,200
      if ((XLO - XUP) .lt. 0) then
         GO TO 3
      else if (.not. abs(XLO - XUP) .gt. 0) then
         GO TO 100
      else
         GO TO 200
      end if

3     if (N .lt. 2) GO TO 215
      do 5 I = 2, N
         if (X(I) .le. X(I - 1)) GO TO 210
         if (X(I) .gt. XUP) GO TO 6
5        continue
6        continue
         if (N .ge. 3) GO TO 9
!
!     SPECIAL N=2 CASE
         SLOPE = (Y(2) - Y(1))/(X(2) - X(1))
         FL = Y(1) + SLOPE*(XLO - X(1))
         FR = Y(2) + SLOPE*(XUP - X(2))
         ANS = 0.5*(FL + FR)*(XUP - XLO)
         return
9        continue
         if (X(N - 2) .lt. XLO) GO TO 205
         if (X(3) .gt. XUP) GO TO 205
         I = 1
10       if (X(I) .ge. XLO) GO TO 15
         I = I + 1
         GO TO 10
15       INLFT = I
         I = N
20       if (X(I) .le. XUP) GO TO 25
         I = I - 1
         GO TO 20
25       INRT = I
         if ((INRT - INLFT) .lt. 2) GO TO 205
         ISTART = INLFT
         if (INLFT .eq. 1) ISTART = 2
         ISTOP = INRT
         if (INRT .eq. N) ISTOP = N - 1
!
         R3 = 3.0d0
         RP5 = 0.5d0
         SUM = 0.0
         SYL = XLO
         SYL2 = SYL*SYL
         SYL3 = SYL2*SYL
!
         do 50 I = ISTART, ISTOP
            X1 = X(I - 1)
            X2 = X(I)
            X3 = X(I + 1)
            X12 = X1 - X2
            X13 = X1 - X3
            X23 = X2 - X3
            TERM1 = dble(Y(I - 1))/(X12*X13)
            TERM2 = -dble(Y(I))/(X12*X23)
            TERM3 = dble(Y(I + 1))/(X13*X23)
            A = TERM1 + TERM2 + TERM3
            B = -(X2 + X3)*TERM1 - (X1 + X3)*TERM2 - (X1 + X2)*TERM3
            C = X2*X3*TERM1 + X1*X3*TERM2 + X1*X2*TERM3
            !IF (I-ISTART) 30,30,35
            if ((I - ISTART) .le. 0) then
               GO TO 30
            else
               GO TO 35
            end if
30          CA = A
            CB = B
            CC = C
            GO TO 40
35          CA = 0.5*(A + CA)
            CB = 0.5*(B + CB)
            CC = 0.5*(C + CC)
40          SYU = X2
            SYU2 = SYU*SYU
            SYU3 = SYU2*SYU
            SUM = SUM + CA*(SYU3 - SYL3)/R3 + CB*RP5*(SYU2 - SYL2) + CC*(SYU - SYL)
            CA = A
            CB = B
            CC = C
            SYL = SYU
            SYL2 = SYU2
            SYL3 = SYU3
50          continue
            SYU = XUP
            ANS = SUM + CA*(SYU**3 - SYL3)/R3 + CB*RP5*(SYU**2 - SYL2) &
                  + CC*(SYU - SYL)
100         return
200         IERR = 2
            write (*, *) 'SLATEC', 'AVINT', &
               'THE UPPER LIMIT OF INTEGRATION WAS NOT GREATER THAN THE '// &
               'LOWER LIMIT.'
            return
205         IERR = 3
            write (*, *) 'SLATEC', 'AVINT', &
               'THERE WERE LESS THAN THREE FUNCTION VALUES BETWEEN THE '// &
               'LIMITS OF INTEGRATION.'
            return
210         IERR = 4
            write (*, *) 'SLATEC', 'AVINT', &
               'THE ABSCISSAS WERE NOT STRICTLY INCREASING.  MUST HAVE '// &
               'X(I-1) .LT. X(I) FOR ALL I.'
            return
215         IERR = 5
            write (*, *) 'SLATEC', 'AVINT', &
               'LESS THAN TWO FUNCTION VALUES WERE SUPPLIED.'
            return
         end

         end module slatec_avint
