!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_sparsematrix

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      subroutine f_addeval_sparsematrix_avector(alpha, a, src, trg) bind(c, name="addeval_sparsematrix_avector")
         use iso_c_binding, only: c_ptr, c_double
         real(kind=c_double), value :: alpha
         type(c_ptr), value:: a
         type(c_ptr), value:: src
         type(c_ptr), value:: trg
      end subroutine
   end interface

   interface
      subroutine f_addevaltrans_sparsematrix_avector(alpha, a, src, trg) bind(c, name="addevaltrans_sparsematrix_avector")
         use iso_c_binding, only: c_ptr, c_double
         real(kind=c_double), value :: alpha
         type(c_ptr), value:: a
         type(c_ptr), value:: src
         type(c_ptr), value:: trg
      end subroutine
   end interface

   interface
      integer(c_int) function f_solve_cg_sparsematrix_avector(A, b, x, eps, maxiter) bind(c, name="solve_cg_sparsematrix_avector")
         use iso_c_binding, only: c_ptr, c_int, c_double
         type(c_ptr), value :: A
         type(c_ptr), value :: b
         type(c_ptr), value :: x
         real(c_double), value :: eps
         integer(c_int), value :: maxiter !TODO handle uint maybe
      end function
   end interface

   interface
      subroutine f_del_sparsematrix(x) bind(c, name="del_sparsematrix")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: x
      end subroutine
   end interface

   type, bind(c) :: f_sparsematrix
      integer(c_int) :: rows, cols, nz
      type(c_ptr) :: row, col
      real(c_double) :: coeff
   end type

end module H2Lib_sparsematrix
