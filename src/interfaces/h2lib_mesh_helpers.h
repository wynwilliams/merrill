#ifndef H2LIB_MESH_HELPERS_H
#define H2LIB_MESH_HELPERS_H

// C functions helping to use H2Lib
// Related to contruction of 3d tetrahedral mesh and surfaces mesh objects
// from merrill fortran array data
// jklebes 2024

// transferring mesh data from arrays to tet3d objects
//see implementation of new_unitcube for example.
// recieve c_loc of fortran data VCL, transfer to tb->vertices
void modify_tet3dbuilder(tet3dbuilder* tb, real* VCL, int NNODE);

// replace degrees of freedom lists in a tet3dp1 object
void modify_tet3dp1(tet3dp1* dc, uint ndof, uint nfix, bool* is_dof, uint* idx_dof);

void free_idx(cluster* root);

psurface3d tet3d_to_surface3d(ptet3d gr);
