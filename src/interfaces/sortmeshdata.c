#include "H2Lib/Library/settings.h"
#include "H2Lib/Library/tet3d.h"
#include "H2Lib/Library/cluster.h"
#include "H2Lib/Library/tet3dp1.h"
#include "H2Lib/Library/surface3d.h"
#include <stdio.h>
#include <assert.h>

// TODO header file?
// TODO rename file - it's really a collection of various functions
// enhancing H2Lib

//see implementation of new_unitcube for example.
// recieve c_loc of fortran data VCL, transfer to tb->vertices
void modify_tet3dbuilder(tet3dbuilder* tb, real* VCL, int NNODE){
    real(*x)[3];
    x = getx_tet3dbuilder(tb); //get ptr to tb->vertices
    for (int i=0; i<NNODE; i++){
        x[i][0] = VCL[i] ; //rearrange from col major 1D data to x
        x[i][1] = VCL[NNODE+i] ;
        x[i][2] = VCL[2*NNODE+i] ;
    }
}

// replace degrees of freedom lists in a tet3dp1 object
void modify_tet3dp1(tet3dp1* dc, uint ndof, uint nfix, bool* is_dof, uint* idx_dof){
    freemem(dc->is_dof); //make sure to not leak the old, automatically
    freemem(dc->idx2dof);// filled arrays
    dc->ndof = ndof;
    dc->nfix = nfix;
    dc->is_dof = is_dof; // replace arrays w data at my pointers
    dc->idx2dof = idx_dof;
}

void free_idx(cluster* root){
    freemem(root->idx);
}

// initate a surface3d struct and fill its data from tet3d mesh
psurface3d tet3d_to_surface3d(ptet3d gr){
    psurface3d sf;
    //count vertices, edges, triangles marked as surface in gc
    uint vertices=0;
    uint edges = 0;
    uint triangles = 0 ;
    uint* xb = gr->xb;
    uint* eb = gr->eb;
    uint* fb = gr->fb;
    real(*tet3dvertices)[3];
    tet3dvertices  = gr->x ;
    uint(*tet3dedges)[2];
    tet3dedges  = gr->e ;
    uint(*tet3dtriangles)[3];
    tet3dtriangles  = gr->f ;
    //indices in vertex list of 6 endpoints of 3 edges of a triangle.
    uint vidx00, vidx01, vidx10, vidx11, vidx20, vidx21;
    for (int i =0; i< gr->vertices; i++){
        if (xb[i] == 1){
            vertices +=1;
        }
    }
    for (int i =0; i< gr->edges; i++){
        if (eb[i] == 1){
            edges +=1;
        }
    }
    for (int i =0; i< gr->faces; i++){
        if (fb[i] == 1){
            triangles +=1;
        }
    }

    sf = new_surface3d(vertices, edges, triangles);

    uint * idxboundaryvertices = (uint *) allocmem(sizeof(uint) * gr->vertices); //maps index of vertex, if it is on boundary, to index of same vertex in surface3d's lists
    //otherwise uninitialized/zero entries
    uint * idxboundaryedges = (uint *) allocmem(sizeof(uint) * gr->edges); // similar for indices of edges in edge lists

    //fill coordinate lists
    int j=0;
    for (int i =0; i< gr->vertices; i++){
        if (xb[i] == 1){
            sf->x[j][0]= tet3dvertices[i][0];
            sf->x[j][1]= tet3dvertices[i][1];
            sf->x[j][2]= tet3dvertices[i][2];
            idxboundaryvertices[i]=j;
            j++;
        }
    }
    j=0;
    for (int i =0; i< gr->edges; i++){
        if (eb[i] == 1){
                //tet3dedges[i][n] - an index in tet3dvertices of a vertex coord
                //convert to an index in boundaryvertices of same vertex coord
            sf->e[j][0]= idxboundaryvertices[tet3dedges[i][0]];
            sf->e[j][1]= idxboundaryvertices[tet3dedges[i][1]];
            //assert(sf->e[j][0]<vertices);
            //assert(sf->e[j][1]<vertices);
            idxboundaryedges[i] = j;
            j++;
        }
    }
    j=0;
    for (int i =0; i< gr->faces; i++){
        if (fb[i] == 1){
            // in tet3d data the faces are composed of edges
            // tet3dtriangles[i][0] an index of an edge in tet3dedges
            //convert to index of same edge in boundaryedges
            sf->s[j][0]= idxboundaryedges[tet3dtriangles[i][0]];
            sf->s[j][1]= idxboundaryedges[tet3dtriangles[i][1]];
            sf->s[j][2]= idxboundaryedges[tet3dtriangles[i][2]];

            // get vertex opposite the edges:
            // get index of neighboring edge in the triangle.
            // get its farther, non-shared point
            // get index of a all edge's vertices in vertexcoords - convert to index of
            // vertex in boundaryvertices
            vidx00 = idxboundaryvertices[tet3dedges[tet3dtriangles[i][0]][0]];
            vidx01 = idxboundaryvertices[tet3dedges[tet3dtriangles[i][0]][1]];
            vidx10 = idxboundaryvertices[tet3dedges[tet3dtriangles[i][1]][0]];
            vidx11 = idxboundaryvertices[tet3dedges[tet3dtriangles[i][1]][1]];
            vidx20 = idxboundaryvertices[tet3dedges[tet3dtriangles[i][2]][0]];
            vidx21 = idxboundaryvertices[tet3dedges[tet3dtriangles[i][2]][1]];
            // use the vertex index on the neighboring edge not matching either index on edge0
            sf->t[j][0]= ((vidx10 == vidx00 || vidx10 == vidx01) ? vidx11: vidx10);
            sf->t[j][1]= ((vidx20 == vidx10 || vidx20 == vidx11) ? vidx21: vidx20);
            sf->t[j][2]= ((vidx00 == vidx20 || vidx00 == vidx21) ? vidx01: vidx00);
            //should result in not having the same vertex index twice in a triangle
            //assert(sf->t[j][0]!= sf->t[j][1]);
            //assert(sf->t[j][0]!= sf->t[j][2]);
            //assert(sf->t[j][1]!= sf->t[j][2]);
            j++;
        }
    }

    //check_surface3d(sf);
    freemem(idxboundaryvertices);
    freemem(idxboundaryedges);
    return sf;
}
