!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_truncation

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_new_abseucl_truncmode() bind(c, name="new_abseucl_truncmode")
         use iso_c_binding, only: c_ptr
      end function
   end interface

   interface
      subroutine f_del_truncmode(x) bind(c, name="del_truncmode")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: x
      end subroutine
   end interface

end module H2Lib_truncation
