!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_clusterbasis

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_build_from_cluster_clusterbasis(t) bind(c, name="build_from_cluster_clusterbasis")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: t
      end function
   end interface

end module H2Lib_clusterbasis
