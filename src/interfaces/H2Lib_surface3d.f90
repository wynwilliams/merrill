!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_surface3d

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      subroutine f_prepare_surface3d(sf) bind(c, name="prepare_surface3d")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: sf  !c_ptr to surface3d
      end subroutine
   end interface

   interface
      subroutine f_check_surface3d(sf) bind(c, name="check_surface3d")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: sf
      end subroutine
   end interface

   interface
      subroutine f_del_surface3d(sf) bind(c, name="del_surface3d")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: sf
      end subroutine
   end interface

   !types
   type, bind(c) :: f_surface3d
      integer(c_int)::vertices, edges, triangles
      type(c_ptr) :: x
      type(c_ptr) :: e
      type(c_ptr) :: t
      type(c_ptr) :: s
      type(c_ptr) :: n
      type(c_ptr)  ::g
      real(c_double) :: hmin, hmax
   end type

end module H2Lib_surface3d
