!> This function is almost like dsiccg.f90, but user-supplied IWORK and
!> RWORK matrices are used to re-use preconditioning work.  It calls slatec DCG.

module DCG_Preconditioned

   !> Calls slatec DCG with user-supplied preconditioning matrices
   !> to solve matrix-vector system Ax = b for x.
   !> See DCGICCG and DCG
contains
   subroutine DCGpreconditioned( &
      N, b, x, nze, RNR, CNR, A, &
      ISYM, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
      RWORK, LENW, IWORK, LENIW)
      use Utils, only: DP
      use slatec_dcg, only: DCG  ! Preconditioned Conjugate Gradient Sparse Ax = b Solver. !TODO wrap up the slatec call
      use slatec_dsmtv, only: DSMTV  ! Routine to calculate the sparse matrix vector product:  Y = A'*X
      use slatec_dsllti, only: DSLLTI  ! SLAP MSOLVE for LDL' (IC) Factorization.
      implicit none

      integer, intent(IN):: N  ! number elements
      real(kind=DP), intent(IN):: b(:)  ! Vector, RHS of problem Ax = b
      real(kind=DP), intent(inout):: x(:)  ! Vector, output: solution x of Ax = b.  In: Initial guess.
      integer, intent(IN):: nze  ! number nonzero elements in sparse matrices
      integer, intent(IN)  :: RNR(nze), CNR(nze)  ! sparse matrices, length nze
      real(kind=DP), intent(IN):: A(nze)  ! matrix A in sparse SLAP format
      real(kind=DP), intent(IN):: ERR, TOL
      integer, intent(in):: IERR, ISYM, ITER, ITMAX, ITOL, IUNIT
      integer, intent(in):: LENW, LENIW  ! length rwork, iwork
      real(kind=DP), intent(IN)  ::  rwork(LENW)
      integer, intent(IN)  ::  iwork(LENIW)

      integer, parameter:: LOCRB = 1, LOCIB = 11
      integer:: LOCEL, LOCDIN, LOCR, LOCZ, LOCP, LOCDZ

      ! numbers
      LOCEL = LOCRB
      LOCDIN = LOCEL + nze
      LOCR = LOCDIN + N
      LOCZ = LOCR + N
      LOCP = LOCZ + N
      LOCDZ = LOCP + N

      call DCG( &
         N, b, x, nze, RNR, CNR, A, &
         ISYM, DSMTV, DSLLTI, ITOL, TOL, ITMAX, ITER, ERR, IERR, IUNIT, &
         RWORK(LOCR), &
         RWORK(LOCZ), &
         RWORK(LOCP), &
         RWORK(LOCDZ), RWORK(1), IWORK(1) &
         )
   end subroutine DCGpreconditioned
end module
