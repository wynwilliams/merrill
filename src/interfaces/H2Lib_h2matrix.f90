!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_h2matrix

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_build_from_block_h2matrix(b, rb, cb) bind(c, name="build_from_block_h2matrix")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: b, rb, cb ! pcblock, 2x pcclusterbasis
      end function
   end interface

   interface
      subroutine f_addeval_h2matrix_avector(alpha, a, src, trg) bind(c, name="addeval_h2matrix_avector")
         use iso_c_binding, only: c_ptr, c_double
         real(kind=c_double), value :: alpha
         type(c_ptr), value:: a
         type(c_ptr), value:: src
         type(c_ptr), value:: trg
      end subroutine
   end interface

   interface
      subroutine f_addevaltrans_h2matrix_avector(alpha, a, src, trg) bind(c, name="addevaltrans_h2matrix_avector")
         use iso_c_binding, only: c_ptr, c_double
         real(kind=c_double), value :: alpha
         type(c_ptr), value:: a
         type(c_ptr), value:: src
         type(c_ptr), value:: trg
      end subroutine
   end interface

   interface
      subroutine f_del_h2matrix(x) bind(c, name="del_h2matrix")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: x
      end subroutine
   end interface
#if H2LIB_USE_NETCDF
   interface
      subroutine f_write_cdfcomplete_h2matrix(G, filename) bind(c, name="write_cdfcomplete_h2matrix")
         use iso_c_binding, only: c_ptr, c_char
         type(c_ptr), value:: G ! constant pointer to h2matrix object
         character(kind=c_char), dimension(*), intent(in) :: filename
      end subroutine
   end interface

   interface
      type(c_ptr) function f_read_cdfcomplete_h2matrix (filename) bind(c, name="read_cdfcomplete_h2matrix")
         use iso_c_binding, only: c_ptr, c_char
         ! returns : c_ptr to h2matrix object
         character(kind=c_char), dimension(*):: filename
      end function
   end interface
#endif
end module H2Lib_h2matrix
