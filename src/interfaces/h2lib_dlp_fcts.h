#ifndef H2LIB_DLP_FCTS_H
#define H2LIB_DLP_FCTS_H
#endif

// C functions helping to use H2Lib
// Setting functions and kernels of bem3d object to these 
// function, which ultimately call our function BAMatrix::Lindholm 
// to fill values of each element of the BEM matrix
// as hierarchically constructed Hmatrix
//
// Largely taken from tetmag h2interface.c :
//
// Copyright (C) 2016-2023 CNRS and Université de Strasbourg
//
// Author: Riccardo Hertel
//
// GPL3 Licence
//
// modifications jklebes 2024:
// - modified arguments for compatibility with newest H2Lib library
// - c_Lindholm now refers to our fortran function BA_matrx::Lindholm from merrill
// - new_dlp_collocation_laplace_bem3d() modified to set_functions()
// - removed compiler macros relating to OMP

// declared here, defined in fortran at BAmatrix.f90: 
// subroutine Lindholm(...) bind(c, name=c_Lindholm) ...
void c_Lindholm(double* x0, double* p1, double* p2, double* p3, double* res); 

static void fill_dlp_l_collocation_near_laplacebem3d(const uint *ridx,
													 const uint *cidx, pcbem3d bem,
													 bool ntrans, pamatrix N);

void fill_nearfield_collocation_bem3d(pcbem3d bem, pamatrix A);

void assemble_fundamental_collocation_row_bem3d(const uint *idx,
												const real (*Z)[3], pcbem3d bem, pamatrix A);

void assemble_dnz_fundamental_collocation_row_bem3d(const uint *idx,
													const real (*Z)[3], const real (*N)[3], pcbem3d bem,  pamatrix A);

void assemble_lagrange_collocation_row_bem3d(const uint *idx, pcrealavector px,
											 pcrealavector py, pcrealavector pz, pcbem3d bem, pamatrix A);

void set_functions(pbem3d bem);

