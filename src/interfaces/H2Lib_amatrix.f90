!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_amatrix

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_new_amatrix(N1, N2) bind(c, name="new_amatrix")
         use iso_c_binding, only: c_int, c_ptr
         integer(kind=c_int), value :: N1, N2
      end function
   end interface

   interface
      type(c_ptr) function f_new_pointer_amatrix(ptr, N1, N2) bind(c, name="new_pointer_amatrix")
         use iso_c_binding, only: c_int, c_ptr
         type(c_ptr), value :: ptr
         integer(kind=c_int), value :: N1, N2
      end function
   end interface

   interface
      subroutine f_addeval_amatrix_avector(alpha, a, src, trg) bind(c, name="addeval_amatrix_avector")
         use iso_c_binding, only: c_ptr, c_double
         real(kind=c_double), value :: alpha
         type(c_ptr), value:: a
         type(c_ptr), value:: src
         type(c_ptr), value:: trg
      end subroutine
   end interface

   interface
      subroutine f_addevaltrans_amatrix_avector(alpha, a, src, trg) bind(c, name="addevaltrans_amatrix_avector")
         use iso_c_binding, only: c_ptr, c_double
         real(kind=c_double), value :: alpha
         type(c_ptr), value:: a
         type(c_ptr), value:: src
         type(c_ptr), value:: trg
      end subroutine
   end interface

   interface
      subroutine f_del_amatrix(x) bind(c, name="del_amatrix")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: x
      end subroutine
   end interface

   type, bind(c) :: f_amatrix
      type(c_ptr) :: a
      integer(c_int) :: id_, rows, cols
      type(c_ptr) :: owner
   end type

end module H2Lib_amatrix
