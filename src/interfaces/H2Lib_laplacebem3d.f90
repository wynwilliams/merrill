!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_laplacebem3d

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_new_dlp_laplace_bem3d(gr, q_regular, q_singular, &
              &row_basis, col_basis, alpha) bind(c, name="new_dlp_laplace_bem3d")
         use iso_c_binding, only: c_ptr, c_int, c_double, c_char
         type(c_ptr), intent(in), value:: gr !ptr to surface3d
         integer(c_int), intent(in), value :: q_regular, q_singular
         character(c_char), intent(in), value:: row_basis, col_basis !basisfunctionbem3d:choice from enum: 'c' or 'l'
         real(c_double), intent(in), value:: alpha
      end function
   end interface

   interface
      subroutine f_del_laplace_bem3d(x) bind(c, name="del_laplace_bem3d")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: x !ptr to a bem3d
      end subroutine
   end interface

end module H2Lib_laplacebem3d
