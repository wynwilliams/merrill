!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_block
   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_build_strict_block(rc, cc, data_, admis) bind(c, name="build_strict_block")
         use iso_c_binding, only: c_ptr, c_bool, c_double, c_funptr
         type(c_ptr), intent(in), value:: rc, cc !roots
         real(c_double), intent(in) :: data_ ! ptr to data , here eta - but pass by ptr, not value
         type(c_funptr), intent(in), value :: admis !ptr to a function
      end function
   end interface

   interface
      logical(c_bool) function f_admissible_2_cluster(rc, cc, data_) bind(c, name="admissible_2_cluster")
         use iso_c_binding, only: c_ptr, c_bool, c_double
         type(c_ptr), intent(in), value:: rc, cc !roots
         real(c_double), intent(in) :: data_ ! ptr to data , here eta - but pass by ptr, not value
      end function
   end interface

   interface
      subroutine f_del_block(x) bind(c, name="del_block")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: x
      end subroutine
   end interface

end module H2Lib_block
