!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_tet3dp1

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_new_tet3dp1(gr) bind(c, name="new_tet3dp1")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: gr !cptr to tet3dp1 struct
      end function
   end interface

   interface
      type(c_ptr) function f_build_tet3dp1_sparsematrix(dc) bind(c, name="build_tet3dp1_sparsematrix")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: dc ! c_ptr to tet3dp1 struct
      end function
   end interface

   interface
      type(c_ptr) function f_build_tet3dp1_interaction_sparsematrix(dc) bind(c, name="build_tet3dp1_interaction_sparsematrix")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: dc ! c_ptr to tet3dp1 struct
      end function
   end interface

   interface
      subroutine f_assemble_tet3dp1_laplace_sparsematrix(dc, A, Af) bind(c, name="assemble_tet3dp1_laplace_sparsematrix")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: dc !c_ptr to tot3d1d
         type(c_ptr), intent(in), value:: A, Af ! cptrs to sparsematrix
      end subroutine
   end interface

   interface
      subroutine f_del_tet3dp1(dc) bind(c, name="del_tet3dp1")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value :: dc ! ptr to tet3dp1
      end subroutine
   end interface

   type, bind(c) :: f_tet3dp1
      type(c_ptr) :: gr
      integer(c_int) :: ndof, nfix !TODO restrict range to match uint
      type(c_ptr) :: is_dof !ptr to bool
      type(c_ptr) :: idx2dof !ptr to uint
   end type

end module H2Lib_tet3dp1
