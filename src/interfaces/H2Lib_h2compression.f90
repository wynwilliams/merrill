!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_h2compression

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_compress_amatrix_h2matrix(g, b, tm, eps) bind(c, name="compress_amatrix_h2matrix")
         use iso_c_binding, only: c_double, c_ptr
         type(c_ptr), intent(in), value :: g, b, tm
         real(c_double), value :: eps
      end function
   end interface

end module H2Lib_h2compression
