!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_avector

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_new_avector(dim_) bind(c, name="new_avector")
         use iso_c_binding, only: c_ptr, c_int
         integer(c_int), value:: dim_
      end function
   end interface

   interface
      type(c_ptr) function f_new_pointer_avector(data_, dim_) bind(c, name="new_pointer_avector")
         use iso_c_binding, only: c_ptr, c_int
         type(c_ptr), value :: data_
         integer(kind=c_int), value :: dim_
      end function
   end interface

   interface
      subroutine f_clear_avector(v) bind(c, name="clear_avector")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: v
      end subroutine
   end interface

   interface
      subroutine f_del_avector(v) bind(c, name="del_avector")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value :: v
      end subroutine
   end interface

   type, bind(c) :: f_avector
      type(c_ptr) :: v
      integer(c_int) :: dim_
      type(c_ptr) :: owner
   end type

end module H2Lib_avector
