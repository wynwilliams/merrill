!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_basic

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      subroutine f_init_h2lib(argc, argv) bind(c, name="init_h2lib")
         use iso_c_binding, only: c_int, c_char, c_ptr
         integer(c_int), intent(in) :: argc !pass ptr to
         type(c_ptr), intent(in):: argv
      end subroutine
   end interface

   interface
      subroutine f_init_h2lib_noargs(argc) bind(c, name="init_h2lib")
         use iso_c_binding, only: c_int, c_ptr
         integer(c_int), intent(in) :: argc !passes ptr to int (zero)
      end subroutine
   end interface

   interface
      subroutine f_uninit_h2lib() bind(c, name="uninit_h2lib")
      end subroutine
   end interface

end module H2Lib_basic
