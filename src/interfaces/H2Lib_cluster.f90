!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_cluster

   use iso_c_binding, only: c_ptr
   implicit none

   interface
      subroutine f_del_cluster(x) bind(c, name="del_cluster")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: x
      end subroutine
   end interface

end module H2Lib_cluster
