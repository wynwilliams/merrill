#include "H2Lib/Library/settings.h"
#include "H2Lib/Library/tet3d.h"
#include "H2Lib/Library/cluster.h"
#include "H2Lib/Library/tet3dp1.h"
#include "H2Lib/Library/surface3d.h"
#include "H2Lib/Library/bem3d.h"
#include "h2lib_dlp_fcts.h"
#include <stdio.h>
#include <assert.h>

// C functions helping to use H2Lib
// Setting functions and kernels of bem3d object to these 
// function, which ultimately call our function BAMatrix::Lindholm 
// to fill values of each element of the BEM matrix
// as hierarchically constructed Hmatrix
//
// Largely taken from tetmag h2interface.c :
//
// Copyright (C) 2016-2023 CNRS and Université de Strasbourg
//
// Author: Riccardo Hertel
//
// GPL3 Licence
//
// modifications jklebes 2024:
// - modified arguments for compatibility with newest H2Lib library
// - c_Lindholm now refers to our fortran function BA_matrx::Lindholm from merrill
// - new_dlp_collocation_laplace_bem3d() modified to set_functions()
// - removed compiler macros relating to OMP

static void fill_dlp_l_collocation_near_laplacebem3d(const uint *ridx,
													 const uint *cidx, pcbem3d bem,
													 bool ntrans, pamatrix N)
{
	pcsurface3d gr = bem->gr;
	real(*gr_x)[3] = (real(*)[3])gr->x;
	uint(*gr_t)[3] = (uint(*)[3])gr->t;
	plistnode *v2t = bem->v2t;
	field *aa = N->a;
	uint rows = ntrans ? N->cols : N->rows;
	uint cols = ntrans ? N->rows : N->cols;
	longindex ld = N->ld;
	uint vertices = gr->vertices;
	uint triangles = gr->triangles;
	ptri_list tl_c;

		ptri_list tl1_c;
		pvert_list vl_c;
		plistnode v;
		uint j, jj, i, ii, l, cj, vv;
		uint *tri_j;
		real res[3];

//identical to loop in  assemble_cl_simd_near_bem3d except for possible OMP wrapper,
// variable name tl1, tl <-> tl1_c, tl_c
// make a tree structure
			clear_amatrix(N);
			tl_c = NULL;
			cj = 0;
			for (j = 0; j < cols; ++j)
			{
				jj = (cidx == NULL ? j : cidx[j]);
				for (v = v2t[jj], vv = v->data; v->next != NULL;
					 v = v->next, vv = v->data)
				{

					tl1_c = tl_c;
					while (tl1_c && tl1_c->t != vv)
					{
						tl1_c = tl1_c->next;
					}

					if (tl1_c == NULL)
					{
						tl1_c = tl_c = new_tri_list(tl_c);
						tl_c->t = vv;
						cj++;
					}

					tl1_c->vl = new_vert_list(tl1_c->vl);
					tl1_c->vl->v = j;
				}
			}

		for (i = 0; i < rows; ++i)
		{
			//    	printf("completed: %d %\r", (int)((double)i / (double)rows * 100)); // not suitable for H-matrix calculation
			ii = (ridx == NULL ? i : ridx[i]);
			assert(ii < vertices);
			for (tl1_c = tl_c; tl1_c != NULL; tl1_c = tl1_c->next)
			{
				jj = tl1_c->t;
				assert(jj < triangles);

				tri_j = gr_t[jj];

                // back to merrill fortran subroutine
                // Lindholm : coordinate triples x0, p1, p2, p3 -> res, three values to be added to relevant matrix elements
				c_Lindholm(gr_x[ii], gr_x[tri_j[0]], gr_x[tri_j[1]],
								 gr_x[tri_j[2]], res); // fills res from geometry

				vl_c = tl1_c->vl;
				while (vl_c)
				{
					j = vl_c->v;
					if (j < cols)
					{
						jj = (cidx == NULL ? j : cidx[j]);
						for (l = 0; l < 3; ++l)
						{
							if (jj == tri_j[l])
							{
								if (ntrans)
								{
									aa[j + i * ld] += res[l];
									//printf("aa1 %f \n" , aa[i + j * ld]);
								}
								else
								{
									aa[i + j * ld] += res[l];

                                //printf("aa2 %f \n" , aa[i + j * ld]);
								}
							}
						}
					}
					vl_c = vl_c->next;
				}
			}
		}

	del_tri_list(tl_c);
}

void fill_nearfield_collocation_bem3d(pcbem3d bem, pamatrix A)
{
	bem->kernels->kernel_col(NULL, bem->gr->x, bem, A);
}

void assemble_fundamental_collocation_row_bem3d(const uint *idx,
												const real (*Z)[3], pcbem3d bem, pamatrix A)
{
	real(*X)[3] = (real(*)[3])allocreal(3 * A->rows);
	uint i, ii;
	for (i = 0; i < A->rows; ++i)
	{
		ii = (idx != NULL ? idx[i] : i);
		X[i][0] = bem->gr->x[ii][0];
		X[i][1] = bem->gr->x[ii][1];
		X[i][2] = bem->gr->x[ii][2];
	}
	bem->kernels->fundamental(bem, X, Z, A);
	freemem(X);
}

void assemble_dnz_fundamental_collocation_row_bem3d(const uint *idx,
													const real (*Z)[3], const real (*N)[3], pcbem3d bem,  pamatrix A)
{
	real(*X)[3] = (real(*)[3])allocreal(3 * A->rows);
	uint i, ii;

	for (i = 0; i < A->rows; ++i)
	{
		ii = (idx != NULL ? idx[i] : i);
		X[i][0] = bem->gr->x[ii][0];
		X[i][1] = bem->gr->x[ii][1];
		X[i][2] = bem->gr->x[ii][2];
	}
	bem->kernels->dny_fundamental(bem, X, Z, N, A);
	freemem(X);
}

void assemble_lagrange_collocation_row_bem3d(const uint *idx, pcrealavector px,
											 pcrealavector py, pcrealavector pz, pcbem3d bem, pamatrix A)
{

	real(*X)[3] = (real(*)[3])allocreal(3 * A->rows);
	uint i, ii;

	for (i = 0; i < A->rows; ++i)
	{
		ii = (idx != NULL ? idx[i] : i);
		X[i][0] = bem->gr->x[ii][0];
		X[i][1] = bem->gr->x[ii][1];
		X[i][2] = bem->gr->x[ii][2];
	}

	assemble_bem3d_lagrange_amatrix(X, px, py, pz, bem, A);

	freemem(X);
}

// Set the various callback functions and kernels in bem object
// to those that correctly fill the desired matrix ,
// following tetmag.
void set_functions(pbem3d bem){
  bem->nearfield = fill_dlp_l_collocation_near_laplacebem3d;
  bem->nearfield_far = fill_dlp_l_collocation_near_laplacebem3d;
  bem->kernels->fundamental_row = assemble_fundamental_collocation_row_bem3d;
  bem->kernels->dnz_fundamental_row = assemble_dnz_fundamental_collocation_row_bem3d;
  bem->kernels->lagrange_row = assemble_lagrange_collocation_row_bem3d;

}


