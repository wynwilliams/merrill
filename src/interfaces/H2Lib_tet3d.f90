!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_tet3d
   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_new_tet3dbuilder(N) bind(c, name="new_tet3dbuilder")
         use iso_c_binding, only: c_ptr, c_int
         integer(c_int), intent(in), value:: N !number of vertices
      end function
   end interface

   interface
      type(c_ptr) function f_new_tet3d(vertices, edges, faces, tetrahedra) bind(c, name="new_tet3d")
         use iso_c_binding, only: c_ptr, c_int
         integer(c_int), intent(in), value:: vertices, edges, faces, tetrahedra !number of, for
         ! storage initialization
      end function
   end interface

   interface
      type(c_ptr) function f_buildmesh_tet3dbuilder(tb) bind(c, name="buildmesh_tet3dbuilder")
         use iso_c_binding, only: c_ptr, c_int
         type(c_ptr), intent(in), value::tb !tet3dbuilder ptr
      end function
   end interface

   interface
      subroutine f_addtetrahedron_tet3dbuilder(tb, v0, v1, v2, v3) bind(c, name="addtetrahedron_tet3dbuilder")
         use iso_c_binding, only: c_ptr, c_int
         type(c_ptr), intent(in), value::tb !tet3dbuilder ptr
         integer(c_int), intent(in), value:: v0, v1, v2, v3 !vertex indices of coords already in tb->vertices
      end subroutine
   end interface

   interface
      subroutine f_check_tet3d(gr) bind(c, name="check_tet3d")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value:: gr  !c_ptr to tet3d struct
      end subroutine
   end interface

   interface
      integer(c_int) function f_fixnormals_tet3d(gr) bind(c, name="fixnormals_tet3d")
         use iso_c_binding, only: c_ptr, c_int
         type(c_ptr), intent(in), value:: gr
      end function
   end interface

   interface
      subroutine f_del_tet3d(gr) bind(c, name="del_tet3d")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value :: gr ! ptr to tet3d
      end subroutine
   end interface

   interface
      subroutine f_del_tet3dbuilder(tb) bind(c, name="del_tet3dbuilder")
         use iso_c_binding, only: c_ptr
         type(c_ptr), intent(in), value :: tb
      end subroutine
   end interface

   type, bind(c) :: f_tet3d
      integer(c_int)::vertices, edges, faces, tetrahedra
      type(c_ptr) :: x
      type(c_ptr) :: e
      type(c_ptr) :: f
      type(c_ptr) :: t
      type(c_ptr) :: xb, fb, eb
   end type

end module H2Lib_tet3d
