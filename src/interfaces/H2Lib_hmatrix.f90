!> fortran interfaces to H2Lib functions and structs.
!> See http://www.h2lib.org/doc/index.html for doc.
module H2Lib_hmatrix

   use iso_c_binding, only: c_ptr, c_int, c_double
   implicit none

   interface
      type(c_ptr) function f_build_from_block_hmatrix(b, k) bind(c, name="build_from_block_hmatrix")
         use iso_c_binding, only: c_ptr, c_int
         type(c_ptr), intent(in), value:: b
         integer(c_int), value :: k
      end function
   end interface

   interface
      subroutine f_addeval_hmatrix_avector(alpha, a, src, trg) bind(c, name="addeval_hmatrix_avector")
         use iso_c_binding, only: c_ptr, c_double
         real(kind=c_double), value :: alpha
         type(c_ptr), value:: a
         type(c_ptr), value:: src
         type(c_ptr), value:: trg
      end subroutine
   end interface

   interface
      subroutine f_del_hmatrix(x) bind(c, name="del_hmatrix")
         use iso_c_binding, only: c_ptr
         type(c_ptr), value:: x
      end subroutine
   end interface

   interface
      function f_norm2_hmatrix(H) bind(c, name="norm2_hmatrix")
         use iso_c_binding, only: c_ptr, c_double
         real(c_double) :: f_norm2_hmatrix
         type(c_ptr), value:: H
      end function
   end interface

end module H2Lib_hmatrix
