Set MaxMeshNumber 1
ReadMesh 1 TM_CubOct_100nm_8nm.pat

Set MaxEnergyEvaluations 10000
Set ExchangeCalculator 1

! Set TM60 properties
TM 60 20 C

Cubic Anisotropy
! Rotate easy axis tp [1 0 0]
CubicRotation 0 0.6154 0.7854

!! Set field and intial guess
Uniform Magnetization -0.757 -0.521 -0.394
External Field Direction -0.757 -0.521 -0.394

Loop field 100 0 -20
External Field Strength $field$ mT
Randomize Magnetization 5
Minimize
WriteLoopData ./Normal_Loop_Hyst_1
WriteHyst ./Normal_Loop_Hyst_1
ZoneName $field$ mT
AppendTecPLotZone ./Normal_Loop_Hyst_1
EndLoop


Set MaxMeshNumber 1
ReadMesh 1 TM_CubOct_100nm_8nm.pat

Set MaxEnergyEvaluations 10000
Set ExchangeCalculator 1

! Set TM60 properties
TM 60 20 C

Cubic Anisotropy
! Rotate easy axis tp [1 0 0]
CubicRotation 0 0.6154 0.7854


!! Set field and intial guess
Uniform Magnetization -0.757 -0.521 -0.394
External Field Direction -0.757 -0.521 -0.394

HystLoop 100 100 0 My_Loop_Hyst_1






END
