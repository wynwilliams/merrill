Set MaxMeshNumber 1
ReadMesh 1 TM60_Cube_120nm.pat

Set MaxEnergyEvaluations 100
Set ExchangeCalculator 1

! Set TM60 properties
TM 60 20 C

Cubic Anisotropy
! Rotate easy axis tp [1 0 0]
CubicRotation 0 0.6154 0.7854

Uniform Magnetization -0.757 -0.521 -0.394
External Field Direction -0.757 -0.521 -0.394

Loop field 100 0 -20

TimeStamp

External Field Strength $field$ mT

Randomize Magnetization 5

Minimize
WriteLoopData ./TimeStamp_Loop

EndLoop


END
