\section{MERRILL Overview}\label{sec:Considerations}

MERRILL is the acronym for the highly contrived  title 'Micromagnetic Earth Related Rapid Interpreted Language Laboratory', chosen to salute the piloting work of Ronald T. Merrill in applying micromagnetic modeling to rock magnetism \url{https://honors.agu.org/winners/ronald-t-merrill/}.

MERRILL is a finite-element three-dimensional micromagnetic modeling software tailored for rock magnetic applications \cite{Conbhui:2018}. This is expressed in several defining features:
\begin{enumerate}
    \item Ease of use for non-specialists in micromagnetism
    \item Modeling of complex three-dimensional geometries
    \item Handling several meshes for the same geometry to enable stepwise structure refinement, for example in very large magnetic particles
    \item Focus on fast local energy minimization  (Hubert-Minimizer) to explore magnetization structures
    \item Calculation of energy barriers using a refined nudged-elastic-band (NEB) method \cite{Fabian:2018a}
    \item Scripting language that simplifies high-level magnetization curve calculations
    \item Predefined material constants for many natural magnetic minerals
\end{enumerate}

A number of examples and tutorials for MERRILL can be found at \url{https://blogs.ed.ac.uk/rockmag/}

\subsection{New in Merrill 2.0.0}
\subsubsection{H2 library for demagnetizing energy}
The calculation of the demagnetizing energy and its gradient in MERRILL~1  is based on a boundary element method (BEM), where size and computation time scales with $K^2$, where $K$ is the number of boundary elements in the finite element tetrahedral mesh. 

The H2Lib is an open source software library for hierarchical matrices and H$^2$-matrices that is being developed in the Scientific Computing Group at Kiel University.\\
\url{http://www.h2lib.org/}

Compression of the BEM boundary matrix in micromagnetic modelling using H2Lib has been pioneered by the {\em tetmag} code of R.~Hertel \url{https://github.com/R-Hertel/tetmag}.

Since version  2.0.0, Merrill depends on H2Lib and works with a compressed H2matrix representation of the boundary element matrix, reducing memory footprint from $K^2$ in number of surface elements to $K \log(K)$ during initialization and $K$ during use.  This enable simulation of larger grain sizes.  

\subsection{General structure of MERRILL }
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{Figures/MERRILL-Sketch.png}
    \caption{Structure of MERRILL.}
    \label{fig:Sketch}
\end{figure}
MERRILL requires as input a three-dimensional volume mesh consisting exclusively of tetrahedral cells. This input meshes must be provided by the user and can be generated in several ways:
\begin{enumerate}
\item Professional 3D-mesh generators like TecPlot, SolidWorks, AutoCAD. These are expensive, but sometimes available at universities. Open-source tools like gmsh, CGal, or tetgen are extremely good, but more difficult to learn and use.
    \item CAD programs can be used to generate three-dimensional shapes. Open-source codes are for example OpenSCAD or FreeCAD or Blender. These programs provide computational geometry engines and can easily produce spheres, cylinders, cuboids and many complex derived shapes. Sometimes they only export to STL files for 3D printers. Then these STL surface meshes need to be converted to volume meshes.
    \item Iso2Mesh is a MATLAB based software library that uses tetgen and CGal to easily transform STL files, or image slices (e.g. from FIB-nt)  to tetrahedral meshes. Iso2Mesh also works with the open-source MATLAB clone Octave. We provide a simple program merrillsave.m to export iso2mesh results in a MERRILL-readable file format.
    An overview of how to use Iso2Mesh can be found at \url{https://nanopaleomag.esc.cam.ac.uk/mesh-generation-iso2mesh/}
    \item  Download existing particle meshes for MERRILL from the example website: ?? %TODO
\end{enumerate}
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{Figures/MERRILL-FEM.png}
    \caption{In MERRILL a magnetic particle is represented by a tight mesh of tetrahedra. The magnetization structure is defined by two angles $\phi, \theta$ for each node. }
    \label{fig:FEM}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{Figures/MERRILL-Meshes.png}
    \caption{  MERRILL can work with several meshes for the same particle geometry.}
    \label{fig:Meshes}
\end{figure}

Besides the input meshes, MERRILL requires the intrinsic material parameters (E.g. $M_s$, $K_1$,$A_{\rm ex}$)  and external forces (e.g. stress fields, external magnetic fields)  defining the micromagnetic energy terms. It   also requires   an initial magnetization structure, which either can be defined internally (e.g. single-domain, vortex, random), or read from an input file.

 MERRILL  expects a script file that contains the description of the inputs, the required minimization steps, and the output formats of the results.

 The output of MERRILL is finally analyzed by external programs. A good open source 3D visualization program is for example ParaView.

\subsection{Micromagnetic energy calculation and minimization }

MERRILL is a numerical program to minimize the micromagnetic energy functional.
It is based on the continuum theory of micromagnetics \cite{Brown:1957} 





\subsection{Energy barriers}

\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{Figures/MERRILL-Path.png}
    \caption{  MERRILL can calculate energy barriers between LEM states.}
    \label{fig:Path}
\end{figure}



\subsection{Limits of MERRILL}


The mesh related discretization error can be estimated by comparing results on two separate meshes with similar number of nodes.
To test whether a minimum is really achieved one can monitor largest angular variation along the minimization route. Close to a supposed minimum one can perturb the current state m$_0$ into a state m$_1$ which is a distance d away from m$_0$. If after k minimization steps the result m2 is within $||$m2 - m0$||$ $<$ d/k then m$_0$ is assumed to represent an LEM.

Energy minimization is limited by physical, numerical and algorithmic constraints.
\textbf{Numerical constraints} The different minimization algorithms have a  limited  precision.  Minimization below this precision can lead to unwanted repetitions due to numerical noise. This can be related to the conjugate gradient method for the sparse matrices or to limited precision in internal functions, e.g. trigonometric functions used for transforming polar to cartesian coordinates.

\textbf{Algorithmic constraints} The finite mesh size or finite distances introduce grid errors and artifacts. The weak FEM solutions are elements of a finite dimensional space approximating a real physical solution. Minimizing below the corresponding approximation error is physically meaningless, even if mathematically correct.

\textbf{Physical constraints} The micromagnetic model only represents a part of the physically relevant energies. Some energy terms are neglected, like magnetostriction, elastic, and electric energies. Most notably thermal activation is disregarded. Minimization of the energy to higher precision than the real variability of the energy is physically irrelevant.

Sometimes it may be mathematically useful to perform higher precision minimizations. This could be necessary to find a complex minimization route that finally leads to a also physically better minimum.

To benchmark different algorithms it also can be useful to compare their output to unphysical degrees of precision.

In most cases it is useful to have a constantly updated estimate of the different accuracy requests to avoid unnecessary minimization steps. The thermal energy per degree of freedom is 1/2kT . In the FEM model the number of degrees of freedom (DOF) is 2 $\times$ NNODE where NNODE is the number of nodes in the
mesh, because each unit vector has two DOF. The total variability in energy density $\Delta$E is then

\[\Delta E = \frac{{kT\;NNODE}}{{{V_{tot}}}}\]

\subsection{Obtaining}

% TODO 

Download newest binary from \url{https://blogs.ed.ac.uk/rockmag/} or \url{https://bitbucket.org/wynwilliams/merrill/downloads/}.  

Since version 2.0.0, Linux and MacOs executables are available.  If working on Windows, run the Linux executable in WSL.

\subsection{Using}

Write/obtain a script file as detailed below.  

Generate/obtain a mesh file, if used.


\subsubsection{Linux}
%TODO possibly need to chmod the executable?

From bash shell, call the executable with the script file as first argument.  
% TODO style for bash
\begin{lstlisting}
./merrill example_script.txt
\end{lstlisting}

\subsubsection{Mac OS}

\subsubsection{Windows}
% TODO presumably, open one of the windows command lines and do the same
