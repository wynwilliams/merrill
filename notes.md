Changes 
jklebes 2024

## Getting
- git clone, switch to this branch ``H2lib-dev``
- H2Lib subproject should populate automatically on first clone.  On older git versions or on an existing
 repository, run 
 ```
 git submodule update --init --recursive
 ```
 to populate H2Lib.

### development build
- Different compiler flags for Debug and Release were added to the main CMake file.  To build with all
 compiler flags (warnings, -pg to generate files for profiler, etc) use
``cd build; cmake .. -DCMAKE_BUILD_TYPE=Debug; make -f Makefile``
- For Release use ``cd build; cmake .. -DCMAKE_BUILD_TYPE=Release; make -f Makefile``
- Runs as if with ``-DMERRILL_USE_H2LIB=ON`` by default, use cmake flag ``-DMERRILL_USE_H2LIB=OFF`` for the old version.
- should build (``make``) included H2Lib when main cmake and make is run
  - If not, Try main Cmake first because it writes matching ``options.inc`` to H2Lib .  If it did not trigger build of H2Lib 
( make error: libh2.a not found ) go to /src/interfaces/H2Lib and run ``make`` .  Then go back to build directory.

#### mac
- pgprof does not exist (error gcrt.o not found) Take out ``-pg`` flag or only run release build.
- Use build type ``DEBUGMAC``

### Style
- Line comments with ``!>`` and above-function comment blocks with ``!-------`` can be picked up by ford to generate
  html doc
  - Regular comment with ``!``.  Comments to be picked up for automatic documentation:  Before-code line comments (predocmark) with ``!>``.  Before-code block comments (predocmark-alt) with ``!----``.  Before code line comments to be picked up for documenation generation (docmark) with ``!!``.  After-code  block comments (docmark-alt) with ``!######``.
- explicit ``USE ... , ONLY: ...`` per procedure.   Functions and parameters imported (explicitly) to the parent module 
  for use in its data declarations may be used without redundant explicit USE declarations per procedure.
- Everything is set to ``IMPLICIT NONE``
	- It's not necessary to have ``IMPLICIT NONE`` statements in each procedure within a module after the ``CONTAINS``,
  but following the vast majority of procedures already present I'm keeping it.
- I don't think its necessary to set the module to private and every member of the module explicitly to public.
- Fotran keywords are caps, variables and functions are mixed case.
- contents of SLATEC files should not be touched; remain in old style conventions.


### Changes
- H2Lib (external) added.  H2Lib interfaces added.
- Cmake additions to include c objects.  New cmake flag to enable/disable use of H2Lib 
- Analogously to kstiff and kstiffnonzero, boundmata is "sparsified" during initialization using H2lib.
- files were sorted into folders by conceptual role, and corresponding changes in Cmake
- modules were renamed and/or split
-
