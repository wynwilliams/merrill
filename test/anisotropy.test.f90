!
! Test CalcAnisExt conforms to expectations
!
PROGRAM Anisotropy_Test
  USE Merrill
  !$ USE omp_lib
  IMPLICIT NONE

  CHARACTER(len=1024) :: program_name
  CHARACTER(len=1024) :: sphere_mesh_filename

  CHARACTER(len=1024) :: argument

  LOGICAL :: test_passed

  REAL(KIND=DP) :: sphere_radius
  REAL(KIND=DP) :: old_Ms


  CALL GET_COMMAND_ARGUMENT(0, program_name)

  ! Parse command line arguments
  IF(COMMAND_ARGUMENT_COUNT() .NE. 1) THEN
    WRITE(*,*) "USAGE: ", TRIM(program_name), " SPHERE_MESH_FILE"
    STOP 1
  END IF

  ! SPHERE_MESH_FILE
  CALL GET_COMMAND_ARGUMENT(1, argument)
  sphere_mesh_filename = TRIM(argument)


  CALL InitializeMerrill()

  CALL Magnetite(20.0d0)

  CALL ReadPatranMesh(TRIM(sphere_mesh_filename))

  ! Find sphere radius
  sphere_radius = MAXVAL(NORM2(VCL(:,1:3), DIM=2))


  !
  ! Run single-phase sphere anisotropy tests
  !

  anisform = ANISFORM_CUBIC
  K1 = (/ 1.2 /)
  K2 = (/ 2.3 /)
  CALL TestCubicAnisotropy()

  anisform = ANISFORM_UNIAXIAL
  K1 = (/ 1.4 /)
  K2 = (/ 2.5 /)
  EasyAxis(:,1) = (/ 1.0, 0.0, 0.0 /)
  CALL TestUniaxialAnisotropy()

  !
  ! Run multi-phase sphere anisotropy tests
  !

  CALL TestMultiphaseAnisotropy()
CONTAINS

  SUBROUTINE TestCubicAnisotropy()
    INTEGER :: el
    REAL(KIND=DP), ALLOCATABLE :: expected_h(:,:)
    REAL(KIND=DP) :: expected_e1, expected_e2, expected_e
    REAL(KIND=DP) :: m_i(3), vv
    INTEGER :: sd

    INTEGER :: i

    ALLOCATE(expected_h(NNODE,3))

    test_passed = .TRUE.

    ! Randomize magnetization
    DO i=1,NNODE
      m(i,:) = 1 - 2*(/ DRAND(), DRAND(), DRAND() /)
      IF(NORM2(m(i,:)) .GT. 0) THEN
        m(i,:) = m(i,:) / NORM2(m(i,:))
      ELSE
        m(i,:) = 1/SQRT(3.0_DP)
      END IF
    END DO

    CALL CalcAnisExt()

    expected_h = 0
    expected_e = 0
    expected_e1 = 0
    expected_e2 = 0

    DO el=1,NTRI
      DO i=1,4
        m_i = m(TIL(el,i),:)
        vv = vol(el) / 4
        sd = TetSubDomains(el)

        expected_h(TIL(el,i),:) = expected_h(TIL(el,i),:) &
          + K1(sd) * (/ &
            2*m_i(1)*m_i(2)**2 + 2*m_i(1)*m_i(3)**2, &
            2*m_i(2)*m_i(1)**2 + 2*m_i(2)*m_i(3)**2, &
            2*m_i(3)*m_i(1)**2 + 2*m_i(3)*m_i(2)**2  &
          /) * vv &
          + K2(sd) * (/ &
            2*m_i(1)*m_i(2)**2*m_i(3)**2, &
            2*m_i(2)*m_i(1)**2*m_i(3)**2, &
            2*m_i(3)*m_i(1)**2*m_i(2)**2  &
          /) * vv

        expected_e1 = expected_e1 &
          + K1(sd) * ( &
              m_i(1)**2*m_i(2)**2 &
            + m_i(2)**2*m_i(3)**2 &
            + m_i(3)**2*m_i(1)**2 &
          ) * vv

        expected_e2 = expected_e2 &
          + K2(sd) * ( &
            m_i(1)**2*m_i(2)**2*m_i(3)**2 &
          ) * vv

      END DO
    END DO

    expected_e = expected_e1 + expected_e2

    DO i=1,NNODE
      IF(&
        NORM2(hanis(i,:) - expected_h(i,:)) &
        .GT. &
        NORM2(expected_h(i,:))*1e-3 &
      ) THEN
        WRITE(*,*) "TestCubicAnisotropy failed for h: i=", i
        WRITE(*,*) "Expected: ", expected_h(i,:)
        WRITE(*,*) "Found:    ", hanis(i,:)
        WRITE(*,*) "e/h:      ", expected_h(i,:)/hanis(i,:)
        test_passed = .FALSE.
      END IF
    END DO

    IF(ABS(AnisEnerg1 - expected_e1) .GT. ABS(expected_e1)*1e-3) THEN
      WRITE(*,*) "TestCubicAnisotropy failed for e1"
      WRITE(*,*) "Expected: ", expected_e1
      WRITE(*,*) "Found:    ", AnisEnerg1
      WRITE(*,*) "e/h:      ", expected_e1/AnisEnerg1
      test_passed = .FALSE.
    END IF

    IF(ABS(AnisEnerg2 - expected_e2) .GT. ABS(expected_e2)*1e-3) THEN
      WRITE(*,*) "TestCubicAnisotropy failed for e2"
      WRITE(*,*) "Expected: ", expected_e2
      WRITE(*,*) "Found:    ", AnisEnerg2
      WRITE(*,*) "e/h:      ", expected_e2/AnisEnerg2
      test_passed = .FALSE.
    END IF

    IF(ABS(AnisEnerg - expected_e) .GT. ABS(expected_e)*1e-3) THEN
      WRITE(*,*) "TestCubicAnisotropy failed for e"
      WRITE(*,*) "Expected: ", expected_e
      WRITE(*,*) "Found:    ", AnisEnerg
      WRITE(*,*) "e/h:      ", expected_e/AnisEnerg
      test_passed = .FALSE.
    END IF

    IF(test_passed) THEN
      WRITE(*,*) "Test Passed"
    ELSE
      WRITE(*,*) "Test Failed!"
      STOP 1
    END IF

  END SUBROUTINE TestCubicAnisotropy

  SUBROUTINE TestUniaxialAnisotropy()
    INTEGER :: el
    REAL(KIND=DP), ALLOCATABLE :: expected_h(:,:)
    REAL(KIND=DP) :: expected_e
    REAL(KIND=DP) :: m_i(3), vv
    INTEGER :: sd

    INTEGER :: i

    ALLOCATE(expected_h(NNODE,3))

    test_passed = .TRUE.

    ! Randomize magnetization
    DO i=1,NNODE
      !m(i,:) = 1 - 2*(/ DRAND(), DRAND(), DRAND() /)
      m(i,:) = (/ 1.0, 1.1, 1.0 /)
      IF(NORM2(m(i,:)) .GT. 0) THEN
        m(i,:) = m(i,:) / NORM2(m(i,:))
      ELSE
        m(i,:) = 1/SQRT(3.0_DP)
      END IF
    END DO

    CALL CalcAnisExt()

    expected_h = 0
    expected_e = 0

    DO el=1,NTRI
      DO i=1,4
        m_i = m(TIL(el,i),:)
        vv = vol(el) / 4
        sd = TetSubDomains(el)

        ! Uniaxial gradient
        expected_h(TIL(el,i),:) = expected_h(TIL(el,i),:) &
          -2*K1(sd)*( &
              EasyAxis(1, sd)*m_i(1) &
            + EasyAxis(2, sd)*m_i(2) &
            + EasyAxis(3, sd)*m_i(3) &
          ) * EasyAxis(:, sd) * vv

        ! Energy
        expected_e = expected_e &
          + K1(sd)*( &
            1 - ( &
                EasyAxis(1, sd)*m_i(1) &
              + EasyAxis(2, sd)*m_i(2) &
              + EasyAxis(3, sd)*m_i(3) &
            )**2 &
          ) * vv

      END DO
    END DO

    DO i=1,NNODE
      IF(&
        NORM2(hanis(i,:) - expected_h(i,:)) &
        .GT. &
        NORM2(expected_h(i,:))*1e-3 &
      ) THEN
        WRITE(*,*) "TestUniaxialAnisotropy failed for h: i=", i
        WRITE(*,*) "Expected: ", expected_h(i,:)
        WRITE(*,*) "Found:    ", hanis(i,:)
        WRITE(*,*) "e/h:      ", NORM2(expected_h(i,:))/NORM2(hanis(i,:))
        test_passed = .FALSE.
      END IF
    END DO

    IF(ABS(AnisEnerg - expected_e) .GT. ABS(expected_e)*1e-3) THEN
      WRITE(*,*) "TestUniaxialAnisotropy failed for e"
      WRITE(*,*) "Expected: ", expected_e
      WRITE(*,*) "Found:    ", AnisEnerg
      WRITE(*,*) "e/h:      ", expected_e/AnisEnerg
      test_passed = .FALSE.
    END IF

    IF(test_passed) THEN
      WRITE(*,*) "Test Passed"
    ELSE
      WRITE(*,*) "Test Failed!"
      STOP 1
    END IF

  END SUBROUTINE TestUniaxialAnisotropy

  SUBROUTINE TestMultiphaseAnisotropy()
    INTEGER :: i

    NMaterials = 2
    TetSubDomains = 1
    DO i=1,NTRI
      IF(MOD(i,3) .EQ. 0) TetSubDomains(i) = 2
    END DO

    CALL BuildTetrahedralMeshData()
    CALL BuildFiniteElement()
    CALL BuildMaterialParameters()
    CALL BuildEnergyCalculator()

    anisform = ANISFORM_CUBIC
    K1 = (/ 1.2, 2.3 /)
    K2 = (/ 3.4, 4.5 /)
    CALL TestCubicAnisotropy()

    anisform = ANISFORM_UNIAXIAL
    K1 = (/ 1.2, 2.3 /)
    K2 = 0
    CALL TestUniaxialAnisotropy()

    anisform = ANISFORM_CUBIC
    K1 = (/ 5.6, 0.0 /)
    K2 = (/ 6.7, 0.0 /)
    CALL TestCubicAnisotropy()

    anisform = ANISFORM_UNIAXIAL
    K1 = (/ 5.6, 0.0 /)
    K2 = 0
    CALL TestUniaxialAnisotropy()
  END SUBROUTINE TestMultiphaseAnisotropy

END PROGRAM Anisotropy_Test
