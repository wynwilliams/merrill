!
! Test that YA and PoissonDirichletMatrix are symmetric.
! This is justification for swapping DSMV for DSMTV in the DCG call
! in CalcDemagEx.
!
PROGRAM Symmetric_Matrices_Test
    USE Merrill
    !$ USE omp_lib
    IMPLICIT NONE

    CHARACTER(len=1024) :: program_name
    CHARACTER(len=1024) :: sphere_mesh_filename

    CHARACTER(len=1024) :: argument

    INTEGER :: I, J


    CALL GET_COMMAND_ARGUMENT(0, program_name)

    ! Parse command line arguments
    IF(COMMAND_ARGUMENT_COUNT() .NE. 1) THEN
        WRITE(*,*) "USAGE: ", TRIM(program_name), " SPHERE_MESH_FILE"
        STOP 1
    END IF

    ! SPHERE_MESH_FILE
    CALL GET_COMMAND_ARGUMENT(1, argument)
    sphere_mesh_filename = TRIM(argument)


    CALL InitializeMerrill()

    CALL Magnetite(20.0d0)
    Ms = 1
    mu = 1
    Ls = 1

    !meshfile=TRIM(sphere_mesh_filename)
    CALL ReadPatranMesh(TRIM(sphere_mesh_filename))

    CALL TestSparseMatrix(PoissonNeumannMatrix, RNR_PNM, CNR_PNM, NNODE)
    CALL TestSparseMatrix(PoissonDirichletMatrix, RNR_PDM, CNR_PDM, NNODE)
CONTAINS
    SUBROUTINE TestSparseMatrix(YA, IA, JA, NNODE)
        REAL(KIND=DP), INTENT(IN) :: YA(:)
        INTEGER, INTENT(IN) :: IA(:), JA(:), NNODE
        REAL(KIND=DP), ALLOCATABLE :: A(:,:)

        ALLOCATE(A(NNODE, NNODE))
        A = 0

        DO I=1,NNODE
            DO J=JA(I),JA(I+1)-1
                A(I,IA(J)) = YA(J)
            END DO
        END DO

        DO I=1,NNODE
            DO J=1,NNODE
                IF( NONZERO(A(I,J)) ) THEN
                    IF( ABS((A(I,J) - A(J,I)) / A(I,J)) > 1e-3 ) THEN
                        WRITE(*,*) "NOT SYMMETRIC!"
                        WRITE(*,*) I, J, A(I,J), A(J,I)
                        STOP 1
                    END IF
                END IF
            END DO
        END DO

        DEALLOCATE(A)
    END SUBROUTINE TestSparseMatrix

END PROGRAM Symmetric_Matrices_Test
