!
! Test routines taken from slatec/chk
!
PROGRAM SLATEC_TEST
    USE slatec_dsiccg
    USE slatec_xsetun
    USE slatec_xsetf
    USE slatec_xermax
    USE slatec_i1mach
    USE slatec_dlapqc
    USE slatec_blachk

!     .. Local Scalars ..
      INTEGER IPASS, KPRINT, LIN, LUN, NFAIL

!
! Test body from slatec/chk/test26.f
!
!***FIRST EXECUTABLE STATEMENT  TEST26
      LUN = I1MACH(2)
      LIN = I1MACH(1)
      NFAIL = 0
!
!     Read KPRINT parameter
!
!      READ (LIN, '(I1)') KPRINT
      KPRINT=3
      CALL XSETUN(LUN)
      IF (KPRINT .LE. 1) THEN
         CALL XSETF(0)
      ELSE
         CALL XSETF(1)
      ENDIF
      CALL XERMAX(1000)
!
!     Test SLAP (double precision)
!
      WRITE(*,*)
      WRITE(*,*) "RUNNING DLAPQC"
      WRITE(*,*)
      CALL DLAPQC(LUN,KPRINT,IPASS)

!
! Test body from slatec/chk/test17.f
!
      WRITE(*,*)
      WRITE(*,*) "RUNNING BLACHK"
      WRITE(*,*)
      CALL BLACHK(LUN,KPRINT,IPASS)
END PROGRAM SLATEC_TEST
