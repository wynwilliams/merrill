MODULE slatec_check1
CONTAINS
!DECK CHECK1
      SUBROUTINE CHECK1 (SFAC, DFAC, KPRINT)
        USE slatec_dnrm2
        USE slatec_dtest
!***BEGIN PROLOGUE  CHECK1
!***PURPOSE  (UNKNOWN)
!***LIBRARY   SLATEC
!***AUTHOR  Lawson, C. L., (JPL)
!***DESCRIPTION
!
!     THIS SUBPROGRAM TESTS THE INCREMENTING AND ACCURACY OF THE LINEAR
!     ALGEBRA SUBPROGRAMS 26 - 38 (SNRM2 TO ICAMAX). STORED RESULTS ARE
!     COMPARED WITH THE RESULT RETURNED BY THE SUBPROGRAM.
!
!     THESE SUBPROGRAMS REQUIRE A SINGLE VECTOR ARGUMENT.
!
!     ICASE            DESIGNATES WHICH SUBPROGRAM TO TEST.
!                      26 .LE. ICASE .LE. 38
!     C. L. LAWSON, JPL, 1974 DEC 10, MAY 28
!
!***ROUTINES CALLED  CSCAL, CSSCAL, DASUM, DNRM2, DSCAL, DTEST, ICAMAX,
!                    IDAMAX, ISAMAX, ITEST, SASUM, SCASUM, SCNRM2,
!                    SNRM2, SSCAL, STEST
!***COMMON BLOCKS    COMBLA
!***REVISION HISTORY  (YYMMDD)
!   741210  DATE WRITTEN
!   890911  Removed unnecessary intrinsics.  (WRB)
!   890911  REVISION DATE from Version 3.2
!   891214  Prologue converted to Version 4.0 format.  (BAB)
!***END PROLOGUE  CHECK1
      COMMON /COMBLA/ NPRINT, ICASE, N, INCX, INCY, MODE, PASS
      LOGICAL          PASS
      INTEGER          ITRUE2(5),ITRUE3(5)
      DOUBLE PRECISION DA,DX(8)
      DOUBLE PRECISION DV(8,5,2)
      DOUBLE PRECISION DFAC
      !DOUBLE PRECISION DNRM2,DASUM
      DOUBLE PRECISION DTRUE1(5),DTRUE3(5),DTRUE5(8,5,2)
      REAL             STRUE2(5),STRUE4(5),STRUE(8),SX(8)
      COMPLEX          CA,CV(8,5,2),CTRUE5(8,5,2),CTRUE6(8,5,2),CX(8)
!
      DATA SA, DA, CA        / .3, .3D0, (.4,-.7)    /
      DATA DV/.1D0,2.D0,2.D0,2.D0,2.D0,2.D0,2.D0,2.D0, &
              .3D0,3.D0,3.D0,3.D0,3.D0,3.D0,3.D0,3.D0, &
              .3D0,-.4D0,4.D0,4.D0,4.D0,4.D0,4.D0,4.D0, &
              .2D0,-.6D0,.3D0,5.D0,5.D0,5.D0,5.D0,5.D0, &
              .1D0,-.3D0,.5D0,-.1D0,6.D0,6.D0,6.D0,6.D0, &
              .1D0,8.D0,8.D0,8.D0,8.D0,8.D0,8.D0,8.D0, &
              .3D0,9.D0,9.D0,9.D0,9.D0,9.D0,9.D0,9.D0, &
              .3D0,2.D0,-.4D0,2.D0,2.D0,2.D0,2.D0,2.D0, &
              .2D0,3.D0,-.6D0,5.D0,.3D0,2.D0,2.D0,2.D0, &
               .1D0,4.D0,-.3D0,6.D0,-.5D0,7.D0,-.1D0, 3.D0 /
!     COMPLEX TEST VECTORS
      DATA CV/ &
      (.1,.1),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.), &
      (.3,-.4),(3.,4.),(3.,4.),(3.,4.),(3.,4.),(3.,4.),(3.,4.),(3.,4.), &
      (.1,-.3),(.5,-.1),(5.,6.),(5.,6.),(5.,6.),(5.,6.),(5.,6.),(5.,6.), &
      (.1,.1),(-.6,.1),(.1,-.3),(7.,8.),(7.,8.),(7.,8.),(7.,8.),(7.,8.), &
      (.3,.1),(.1,.4),(.4,.1),(.1,.2),(2.,3.),(2.,3.),(2.,3.),(2.,3.), &
      (.1,.1),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.), &
      (.3,-.4),(6.,7.),(6.,7.),(6.,7.),(6.,7.),(6.,7.),(6.,7.),(6.,7.), &
      (.1,-.3),(8.,9.),(.5,-.1),(2.,5.),(2.,5.),(2.,5.),(2.,5.),(2.,5.), &
      (.1,.1),(3.,6.),(-.6,.1),(4.,7.),(.1,-.3),(7.,2.),(7.,2.),(7.,2.), &
      (.3,.1),(5.,8.),(.1,.4),(6.,9.),(.4,.1),(8.,3.),(.1,.2),(9.,4.) /
!
      DATA STRUE2/.0,.5,.6,.7,.7/
      DATA STRUE4/.0,.7,1.,1.3,1.7/
      DATA DTRUE1/.0D0,.3D0,.5D0,.7D0,.6D0/
      DATA DTRUE3/.0D0,.3D0,.7D0,1.1D0,1.D0/
      DATA DTRUE5/.10D0,2.D0,2.D0,2.D0,2.D0,2.D0,2.D0,2.D0, &
                  .09D0,3.D0,3.D0,3.D0,3.D0,3.D0,3.D0,3.D0, &
                  .09D0,-.12D0,4.D0,4.D0,4.D0,4.D0,4.D0,4.D0, &
                  .06D0,-.18D0,.09D0,5.D0,5.D0,5.D0,5.D0,5.D0, &
                  .03D0,-.09D0,.15D0,-.03D0,6.D0,6.D0,6.D0,6.D0, &
                  .10D0,8.D0,8.D0,8.D0,8.D0,8.D0,8.D0,8.D0, &
                  .09D0,9.D0,9.D0,9.D0,9.D0,9.D0,9.D0,9.D0, &
                  .09D0,2.D0,-.12D0,2.D0,2.D0,2.D0,2.D0,2.D0, &
                  .06D0,3.D0,-.18D0,5.D0,.09D0,2.D0,2.D0,2.D0, &
                  .03D0,4.D0, -.09D0,6.D0, -.15D0,7.D0, -.03D0,  3.D0/
!
      DATA CTRUE5/ &
      (.1,.1),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.), &
      (-.16,-.37),(3.,4.),(3.,4.),(3.,4.),(3.,4.),(3.,4.),(3.,4.), &
                                                               (3.,4.), &
      (-.17,-.19),(.13,-.39),(5.,6.),(5.,6.),(5.,6.),(5.,6.),(5.,6.), &
                                                               (5.,6.), &
      (.11,-.03),(-.17,.46),(-.17,-.19),(7.,8.),(7.,8.),(7.,8.),(7.,8.), &
                                                               (7.,8.), &
      (.19,-.17),(.32,.09),(.23,-.24),(.18,.01),(2.,3.),(2.,3.),(2.,3.), &
                                                               (2.,3.), &
      (.1,.1),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.), &
      (-.16,-.37),(6.,7.),(6.,7.),(6.,7.),(6.,7.),(6.,7.),(6.,7.), &
                                                               (6.,7.), &
      (-.17,-.19),(8.,9.),(.13,-.39),(2.,5.),(2.,5.),(2.,5.),(2.,5.), &
                                                               (2.,5.), &
      (.11,-.03),(3.,6.),(-.17,.46),(4.,7.),(-.17,-.19),(7.,2.),(7.,2.), &
                                                               (7.,2.), &
      (.19,-.17),(5.,8.),(.32,.09),(6.,9.),(.23,-.24),(8.,3.),(.18,.01), &
                                                               (9.,4.) /
!
      DATA CTRUE6/ &
      (.1,.1),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.),(1.,2.), &
      (.09,-.12),(3.,4.),(3.,4.),(3.,4.),(3.,4.),(3.,4.),(3.,4.), &
                                                               (3.,4.), &
      (.03,-.09),(.15,-.03),(5.,6.),(5.,6.),(5.,6.),(5.,6.),(5.,6.), &
                                                               (5.,6.), &
      (.03,.03),(-.18,.03),(.03,-.09),(7.,8.),(7.,8.),(7.,8.),(7.,8.), &
                                                               (7.,8.), &
      (.09,.03),(.03,.12),(.12,.03),(.03,.06),(2.,3.),(2.,3.),(2.,3.), &
                                                               (2.,3.), &
      (.1,.1),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.),(4.,5.), &
      (.09,-.12),(6.,7.),(6.,7.),(6.,7.),(6.,7.),(6.,7.),(6.,7.), &
                                                               (6.,7.), &
      (.03,-.09),(8.,9.),(.15,-.03),(2.,5.),(2.,5.),(2.,5.),(2.,5.), &
                                                               (2.,5.), &
      (.03,.03),(3.,6.),(-.18,.03),(4.,7.),(.03,-.09),(7.,2.),(7.,2.), &
                                                               (7.,2.), &
      (.09,.03),(5.,8.),(.03,.12),(6.,9.),(.12,.03),(8.,3.),(.03,.06), &
                                                               (9.,4.) /
!
!
      DATA ITRUE2/ 0, 1, 2, 2, 3/
      DATA ITRUE3/ 0, 1, 2, 2, 2/
!***FIRST EXECUTABLE STATEMENT  CHECK1
      JUMP=ICASE-25
         DO 520 INCX=1,2
            DO 500 NP1=1,5
            N=NP1-1
            LEN= 2*MAX(N,1)
!                                                  SET VECTOR ARGUMENTS.
                    DO 22 I = 1, LEN
                    SX(I) = DV(I,NP1,INCX)
                    DX(I) = DV(I,NP1,INCX)
   22               CX(I) = CV(I,NP1,INCX)
!
!                        BRANCH TO INVOKE SUBPROGRAM TO BE TESTED.
!
               GO TO (260,270,280,290,300,310,320, &
                      330,340,350,360,370,380),JUMP
!                                                             26. SNRM2
  !260       STEMP = DTRUE1(NP1)
  !          CALL STEST(1,SNRM2(N,SX,INCX),STEMP,STEMP,SFAC,KPRINT)
  260       CONTINUE
            GO TO 500
!                                                             27. DNRM2
  270       CALL DTEST(1,(/ DNRM2(N,DX,INCX) /),DTRUE1(NP1),DTRUE1(NP1),DFAC, &
                       KPRINT)
            GO TO 500
!                                                             28. SCNRM2
  !280       CALL STEST(1,SCNRM2(N,CX,INCX),STRUE2(NP1),STRUE2(NP1), &
  !                     SFAC,KPRINT)
  280       CONTINUE
            GO TO 500
!                                                             29. SASUM
  !290       STEMP = DTRUE3(NP1)
  !          CALL STEST(1,SASUM(N,SX,INCX),STEMP,STEMP,SFAC,KPRINT)
  290       CONTINUE
            GO TO 500
!                                                             30. DASUM
  !300       CALL DTEST(1,DASUM(N,DX,INCX),DTRUE3(NP1),DTRUE3(NP1),DFAC, &
  !                     KPRINT)
  300       CONTINUE
            GO TO 500
!                                                             31. SCASUM
  !310       CALL STEST(1,SCASUM(N,CX,INCX),STRUE4(NP1),STRUE4(NP1),SFAC, &
  !                     KPRINT)
  310       CONTINUE
            GO TO 500
!                                                             32. SSCALE
  !320       CALL SSCAL(N,SA,SX,INCX)
  !             DO 322 I = 1, LEN
  !322          STRUE(I) = DTRUE5(I,NP1,INCX)
  !          CALL STEST(LEN,SX,STRUE,STRUE,SFAC,KPRINT)
  320       CONTINUE
            GO TO 500
!                                                             33. DSCALE
  !330       CALL DSCAL(N,DA,DX,INCX)
  !         CALL DTEST(LEN,DX,DTRUE5(1,NP1,INCX),DTRUE5(1,NP1,INCX), &
  !                     DFAC,KPRINT)
  330       CONTINUE
            GO TO 500
!                                                             34. CSCALE
  !340       CALL CSCAL(N,CA,CX,INCX)
  !      CALL STEST(2*LEN,CX,CTRUE5(1,NP1,INCX),CTRUE5(1,NP1,INCX), &
  !                     SFAC,KPRINT)
  340       CONTINUE
            GO TO 500
!                                                             35. CSSCAL
  !350       CALL CSSCAL(N,SA,CX,INCX)
  !       CALL STEST(2*LEN,CX,CTRUE6(1,NP1,INCX),CTRUE6(1,NP1,INCX), &
  !                     SFAC,KPRINT)
  350       CONTINUE
            GO TO 500
!                                                             36. ISAMAX
  !360       CALL ITEST(1,ISAMAX(N,SX,INCX),ITRUE2(NP1),KPRINT)
  360       CONTINUE
            GO TO 500
!                                                             37. IDAMAX
  !370       CALL ITEST(1,IDAMAX(N,DX,INCX),ITRUE2(NP1),KPRINT)
  370       CONTINUE
            GO TO 500
!                                                             38. ICAMAX
  !380       CALL ITEST(1,ICAMAX(N,CX,INCX),ITRUE3(NP1),KPRINT)
  380       CONTINUE
!
  500       CONTINUE
  520    CONTINUE
      RETURN
      END
END MODULE slatec_check1
