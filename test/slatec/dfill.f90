MODULE slatec_dfill
CONTAINS

!DECK DFILL
      SUBROUTINE DFILL (N, V, VAL)
!***BEGIN PROLOGUE  DFILL
!***SUBSIDIARY
!***PURPOSE  Fill a vector with a value.
!***LIBRARY   SLATEC (SLAP)
!***TYPE      DOUBLE PRECISION (VFILL-S, DFILL-D)
!***AUTHOR  Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (510) 423-3141
!             seager@llnl.gov
!***DESCRIPTION
!
! *Usage:
!     INTEGER  N
!     DOUBLE PRECISION V(N), VAL
!
!     CALL DFILL( N, V, VAL )
!
! *Arguments:
! N      :IN       Integer.
!         Length of the vector
! V      :OUT      Double Precision V(N).
!         Vector to be set.
! VAL    :IN       Double Precision.
!         Value to seed the vector with.
!***REFERENCES  (NONE)
!***ROUTINES CALLED  (NONE)
!***REVISION HISTORY  (YYMMDD)
!   890404  DATE WRITTEN
!   890920  Converted prologue to SLATEC 4.0 format.  (FNF)
!   920511  Added complete declaration section.  (WRB)
!***END PROLOGUE  DFILL
!     .. Scalar Arguments ..
      DOUBLE PRECISION VAL
      INTEGER N
!     .. Array Arguments ..
      DOUBLE PRECISION V(*)
!     .. Local Scalars ..
      INTEGER I, IS, NR
!     .. Intrinsic Functions ..
      INTRINSIC MOD
!***FIRST EXECUTABLE STATEMENT  DFILL
      IF (N .LE. 0) RETURN
      NR=MOD(N,4)
!
!         The following construct assumes a zero pass do loop.
!
      IS=1
      GOTO(1,2,3,4), NR+1
    4   IS=4
        V(1)=VAL
        V(2)=VAL
        V(3)=VAL
        GOTO 1
    3   IS=3
        V(1)=VAL
        V(2)=VAL
        GOTO 1
    2   IS=2
        V(1)=VAL
    1 DO 10 I=IS,N,4
        V(I)  =VAL
        V(I+1)=VAL
        V(I+2)=VAL
        V(I+3)=VAL
 10   CONTINUE
      RETURN
!------------- LAST LINE OF DFILL FOLLOWS -----------------------------
      END

END MODULE slatec_dfill
