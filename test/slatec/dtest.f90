MODULE slatec_dtest
CONTAINS
!DECK DTEST
      SUBROUTINE DTEST (LEN, DCOMP, DTRUE, DSIZE, DFAC, KPRINT)
        USE slatec_d1mach
!***BEGIN PROLOGUE  DTEST
!***PURPOSE  Compare arrays DCOMP and DTRUE.
!***LIBRARY   SLATEC
!***TYPE      DOUBLE PRECISION (STEST-S, DTEST-D)
!***KEYWORDS  QUICK CHECK
!***AUTHOR  Lawson, C. L., (JPL)
!***DESCRIPTION
!
!   This subroutine compares arrays DCOMP and DTRUE of length LEN to
!   see if the term by term differences, multiplied by DFAC, are
!   negligible.  In the case of a significant difference, appropriate
!   messages are written.
!
!***ROUTINES CALLED  D1MACH
!***COMMON BLOCKS    COMBLA
!***REVISION HISTORY  (YYMMDD)
!   741210  DATE WRITTEN
!   891214  Prologue converted to Version 4.0 format.  (BAB)
!   900820  Modified IF test to use function DDIFF and made cosmetic
!           changes to routine.  (WRB)
!   901005  Removed usage of DDIFF in favour of D1MACH.  (RWC)
!   910501  Added TYPE record.  (WRB)
!   920211  Code restructured and information added to the DESCRIPTION
!           section.  (WRB)
!***END PROLOGUE  DTEST
      DOUBLE PRECISION DCOMP(*), DTRUE(*), DSIZE(*), DFAC, DD, &
             RELEPS
      LOGICAL PASS
      COMMON /COMBLA/ NPRINT, ICASE, N, INCX, INCY, MODE, PASS
      SAVE RELEPS
      DATA RELEPS /0.0D0/
!***FIRST EXECUTABLE STATEMENT  DTEST
      IF (RELEPS .EQ. 0.0D0) RELEPS = D1MACH(4)
      DO 100 I = 1,LEN
        DD = ABS(DCOMP(I)-DTRUE(I))
        IF (DFAC*DD .GT. ABS(DSIZE(I))*RELEPS) THEN
!
!         Here DCOMP(I) is not close to DTRUE(I).
!
          IF (PASS) THEN
!
!           Print FAIL message and header.
!
            PASS = .FALSE.
            IF (KPRINT .GE. 3) THEN
              WRITE (NPRINT,9000)
              WRITE (NPRINT,9010)
            ENDIF
          ENDIF
          IF (KPRINT .GE. 3) WRITE (NPRINT,9020) ICASE, N, INCX, INCY, &
                             MODE, I, DCOMP(I), DTRUE(I), DD, DSIZE(I)
        ENDIF
  100 CONTINUE
      RETURN
 9000 FORMAT ('+', 39X, 'FAIL')
 9010 FORMAT ('0CASE  N INCX INCY MODE  I', 29X, 'COMP(I)', 29X, &
              'TRUE(I)', 2X, 'DIFFERENCE', 5X, 'SIZE(I)' / 1X)
 9020 FORMAT (1X, I4, I3, 3I5, I3, 2D36.18, 2D12.4)
      END
END MODULE slatec_dtest
