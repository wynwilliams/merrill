MODULE slatec_duterr
CONTAINS

!DECK DUTERR
      SUBROUTINE DUTERR (METHOD, IERR, IOUT, NFAIL, ISTDO, ITER, ERR)
!***BEGIN PROLOGUE  DUTERR
!***SUBSIDIARY
!***PURPOSE  Output error messages for the SLAP Quick Check.
!***LIBRARY   SLATEC (SLAP)
!***TYPE      DOUBLE PRECISION (OUTERR-S, DUTERR-D)
!***AUTHOR  Seager, Mark K., (LLNL)
!             Lawrence Livermore National Laboratory
!             PO BOX 808, L-300
!             Livermore, CA 94550 (510) 423-3141
!             seager@llnl.gov
!***ROUTINES CALLED  (NONE)
!***REVISION HISTORY  (YYMMDD)
!   890404  DATE WRITTEN
!   890920  Converted prologue to SLATEC 4.0 format.  (FNF)
!   920511  Added complete declaration section.  (WRB)
!   921021  Changed E's to 1P,D's in output formats.  (FNF)
!***END PROLOGUE  DUTERR
!     .. Scalar Arguments ..
      DOUBLE PRECISION ERR
      INTEGER IERR, IOUT, ISTDO, ITER, NFAIL
      CHARACTER METHOD*6
!***FIRST EXECUTABLE STATEMENT  DUTERR
      IF( IERR.NE.0 ) NFAIL = NFAIL + 1
      IF( IOUT.EQ.1 .AND. IERR.NE.0 ) THEN
         WRITE(ISTDO,1000) METHOD
      ENDIF
      IF( IOUT.EQ.2 ) THEN
         IF( IERR.EQ.0 ) THEN
            WRITE(ISTDO,1010) METHOD
         ELSE
            WRITE(ISTDO,1020) METHOD,IERR,ITER,ERR
         ENDIF
      ENDIF
      IF( IOUT.GE.3 ) THEN
         IF( IERR.EQ.0 ) THEN
            WRITE(ISTDO,1030) METHOD,IERR,ITER,ERR
         ELSE
            WRITE(ISTDO,1020) METHOD,IERR,ITER,ERR
         ENDIF
      ENDIF
      RETURN
 1000 FORMAT( 1X,A6,' : **** FAILURE ****')
 1010 FORMAT( 1X,A6,' : **** PASSED  ****')
 1020 FORMAT(' **************** WARNING ***********************'/ &
             ' **** ',A6,' Quick Test FAILED: IERR = ',I5,' ****'/ &
             ' **************** WARNING ***********************'/ &
             ' Iteration Count = ',I3,' Stop Test = ',1P,D12.6)
 1030 FORMAT(' ***************** PASSED ***********************'/ &
             ' **** ',A6,' Quick Test PASSED: IERR = ',I5,' ****'/ &
             ' ***************** PASSED ***********************'/ &
             ' Iteration Count = ',I3,' Stop Test = ',1P,D12.6)
!------------- LAST LINE OF DUTERR FOLLOWS ----------------------------
      END

END MODULE slatec_duterr
