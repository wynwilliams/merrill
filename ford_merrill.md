---
project: merrill
docmark: !
docmark_alt: #
predocmark: >
predocmark_alt: - 
graph: true
exclude_dir: src/interfaces/H2Lib
exclude_dir: src/interfaces/slatec
exclude_dir: src/thirdparty
output_dir: .doc/ford/
display: public
display: protected
display: private
extra_filetypes: c //
---
