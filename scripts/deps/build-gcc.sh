#!/usr/bin/env bash

#
# Build a gfortran suitable for building MERRILL on this platform
#


# Exit on first error
set -e

# Directory containing this script
script_dir="$(cd "$(dirname "$(which ${BASH_SOURCE[0]})")"; echo $PWD)"

function help_message() {
    echo "Usage $(basename $0) [--prefix INSTALL_DIR] [--build BUILD_DIR] [--source SOURCE_DIR] [-- [CMake args]]"
    exit 0
}

function error_echo {
    echo "$(basename $0):" "$@" >&2
    exit 1
}

function arg_error() {
    error_echo "Expected argument to $1"
}


while :; do
    case $1 in
        -h|--help)
            help_message
            ;;

        # Prefix argument
        -p|--prefix)
            if [ -n "$2" ]; then
                install_dir="$2"
                shift
            else
                arg_error --prefix
            fi
            ;;
        --prefix=?*)
            install_dir="${1#*=}"
            ;;
        --prefix=)
            arg_error --prefix
            ;;

        # Build argument
        -b|--build)
            if [ -n "$2" ]; then
                build_dir="$2"
                shift
            else
                arg_error --build
            fi
            ;;
        --build=?*)
            build_dir="${1#*=}"
            ;;
        --build=)
            arg_error --build
            ;;

        # Source argument
        -b|--source)
            if [ -n "$2" ]; then
                source_dir="$2"
                shift
            else
                arg_error --source
            fi
            ;;
        --source=?*)
            source_dir="${1#*=}"
            ;;
        --source=)
            arg_error --source
            ;;


        # URL arguments

        # GCC
        --gcc-url)
            if [ -n "$2" ]; then
                gcc_url="$2"
                shift
            else
                arg_error --gcc-url
            fi
            ;;
        --gcc-url=?*)
            gcc_dir="${1#*=}"
            ;;
        --gcc-url=)
            arg_error --gcc-url
            ;;

        # Platform argument
        --platform)
            if [ -n "$2" ]; then
                source_dir="$2"
                shift
            else
                arg_error --platform
            fi
            ;;
        --platform=?*)
            source_dir="${1#*=}"
            ;;
        --platform=)
            arg_error --source
            ;;

        # Rest of arguments passed to CMake
        --)
            shift
            break
            ;;

        # Pass from first unknown argument to CMake.
        *)
            break
            ;;
    esac

    shift
done


#
# Argument defaults
#

# Default build/install dirs
: ${build_dir:=$PWD/build/build/gcc}
: ${install_dir:=$PWD/build/install}
: ${source_dir:=$PWD/build/src/gcc}
: ${download_dir:=$PWD/build}


# Make build/install dirs absolute
mkdir -p "$build_dir"
mkdir -p "$install_dir"
mkdir -p "$source_dir"

build_dir="$(cd "$build_dir"; echo $PWD)"
install_dir="$(cd "$install_dir"; echo $PWD)"
source_dir="$(cd "$source_dir"; echo $PWD)"


# Setup default gcc URL
: ${gcc_url:=ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-7.1.0/gcc-7.1.0.tar.gz}
: ${gcc_tarball:=${download_dir}/$(basename "${gcc_url}")}
: ${gcc_tarball_md5:=b3d733ad75fdaf88009b52c0cce0ad4c}

# Setup default platform
: ${platform:=$(uname)}

# Setup md5 program. md5 on OSX, md5sum elsewhere.
case $platform in
    Darwin)
        : ${MD5_EXECUTABLE:=md5}
        ;;

    *)
        : ${MD5_EXECUTABLE:=md5sum}
        ;;
esac


# Setup platform specific default compilers and flags
: ${CC:=$(which gcc)}
: ${CXX:=$(which g++)}

# Export compiler paths
export CC
export CXX


# Download GCC if tarball doesn't match md5
if ! $MD5_EXECUTABLE "${gcc_tarball}" | grep -q ${gcc_tarball_md5}
then
    curl -L "${gcc_url}" > "${gcc_tarball}"
fi

# Extract tarballs
mkdir -p "${source_dir}/gcc"
tar -C "${source_dir}/gcc" --strip-components=1 -xf "${gcc_tarball}"


# Configure / Make / Install
mkdir -p "${build_dir}/gcc"
cd "${build_dir}/gcc"
"${source_dir}"/gcc/configure \
    --prefix=${install_dir} \
    --enable-languages=fortran \
    --disable-multilib \
    --disable-bootstrap

make -j 4
make install


#
# Copy this build script to share/merrill
#
mkdir -p "${install_dir}/share/merrill"

script_name=$(basename ${BASH_SOURCE[0]})
cat > "${install_dir}/share/merrill/${script_name}" <<EOF
#!/usr/bin/env bash

# Setting defaults from original build arguments

# Default build/install dirs
: \${build_dir:=$build_dir}
: \${install_dir:=$install_dir}
: \${source_dir:=$source_dir}


# Setup default compilers
: \${FC:=$FC}
: \${AR:=$AR}
: \${RANLIB:=$RANLIB}


# MERRILL build stript:
EOF
cat "${BASH_SOURCE[0]}" >> "${install_dir}/share/merrill/${script_name}"
