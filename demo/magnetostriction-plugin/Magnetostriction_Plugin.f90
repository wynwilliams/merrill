MODULE Magnetostriction_Plugin
    USE Magnetostriction
CONTAINS
    SUBROUTINE InitializeMerrillPlugin() BIND(C, NAME="InitializeMerrillPlugin")
        CALL InitializeMagnetostriction
    END SUBROUTINE InitializeMerrillPlugin
END MODULE Magnetostriction_Plugin
