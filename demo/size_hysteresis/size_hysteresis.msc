! NOTE: This script uses the mesh_0.04um_0.003_um.neu
!       and mesh_0.04um_0.0015um.neu files, which are
!       provided in this folder in a zipped format.
!       These need to be unzipped before running this
!       script.

! Use magnetite material parameters
Iron 20 C

! Ensure we can load at least 2 meshes at a time
Set MaxMeshNumber 2

! Load two meshes of two different node spacings
! to save time solving the smaller grains.
! The widths of the meshes need to be the same
! so the Remesh command, used later, works as
! expected.

! Load 0.1 um mesh with 0.003 um node spacing,
! suitable for scaling up to 0.1 um,
! into slot 1
ReadMesh 1 mesh_0.04um_0.003um.neu

! Load 0.1 um mesh with 0.0015 um node spacing
! suitable for scaling up to 0.2 um,
! into slot 2
ReadMesh 2 mesh_0.04um_0.0015um.neu


! Set reference size of the meshes to 100
define refsize 40

! Make sure mesh 1 is loaded
LoadMesh 1

! Set initial magnetization to [111]
Uniform Magnetization 1 1 1

! Loop from 10 to 100 in steps of 10 for
! the 0.005 um mesh
Loop meshsize 10 40 2
    ! Resize our mesh to the current %meshsize
    ! For #meshsize 20, for example, the 0.1 um
    ! mesh is scaled to 0.02 um.
    Resize #refsize #meshsize

    ! Give the magnetization a small kick
    Randomize Magnetization 5

    ! Run the minimization
    Minimize

    ! Write the output to a file
    WriteMagnetization up_0.$meshsize$um

    External Field Strength #meshsize mT                                         
    WriteHyst size_hysteresis                                                    
    External Field Strength 0 mT 

    ! Resize the mesh back to its original size,
    ! for the next loop iteration
    Resize #meshsize #refsize
EndLoop

!
! Hand off to the 0.0025 um mesh
!

! Interpolate the current magnetization to
! mesh 2 and load it
Remesh 2
WriteMagnetization test_remesh

! Loop from 110 to 200 in steps of 10 for
! the 0.0025 um mesh
Loop meshsize 40 80 2
    Resize #refsize #meshsize
    Randomize Magnetization 5
    Minimize
    WriteMagnetization up_0.$meshsize$um

    External Field Strength #meshsize mT                                         
    WriteHyst size_hysteresis                                                    
    External Field Strength 0 mT 

    Resize #meshsize #refsize
EndLoop

! Small to large done. Now do large to small.

! Loop over 0.0025 um mesh
Loop meshsize 80 40 -2
    Resize #refsize #meshsize
    Randomize Magnetization 5
    Minimize
    WriteMagnetization down_0.$meshsize$um

    External Field Strength #meshsize mT                                         
    WriteHyst size_hysteresis                                                    
    External Field Strength 0 mT 

    Resize #meshsize #refsize
EndLoop

! Hand off to 0.1 um mesh
Remesh 1

! Loop over 0.1 um mesh
Loop meshsize 40 10 -2
    Resize #refsize #meshsize
    Randomize Magnetization 5
    Minimize
    WriteMagnetization down_0.$meshsize$um

    External Field Strength #meshsize mT                                         
    WriteHyst size_hysteresis                                                    
    External Field Strength 0 mT 

    Resize #meshsize #refsize
EndLoop
