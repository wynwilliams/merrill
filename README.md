# README #

This is the source code for the Micromagnetic modelling programme using the
finite element method.

Micromagnetic Earth Related Robust Interpreter Language Laboratory
Finite element solver was developed by:

- Wyn Williams
- Phil Ridley
- Karl Fabian
- Pádraig Ó Conbhuí
- Les Nagy
- Greig Paterson
- Jason Klebes

 Open source routines  by:
 
- G. Benthien -   string module
- NAG library  -   sparse matrix solver

This software uses, as a submodule, H2Lib https://github.com/H2Lib/H2Lib/

## About
MERRILL is an open source software package for three-dimensional micromagnetics.
MERRILL has a simple scripting user interface that requires little computational knowledge to use, and it is straightforward 
to simulate typical rock magnetic observations.
It uses a finite element/boundary element numerical method, optimally suited for
calculating magnetization structures of local energy minima (LEM) in irregular grain geometries that are of
interest to the rock and paleomagnetic community. MERRILL is able to simulate the magnetic characteristics
of LEM states in both single grains, and small assemblies of interacting grains, including saddle-point paths
between nearby LEMs.


See publications :

...

## Citations
Conbhuí, P. Ó., Williams, W., Fabian, K., Ridley, P., Nagy, L., & Muxworthy, A. R. (2018). MERRILL: Micromagnetic Earth Related Robust Interpreted Language Laboratory. Geochemistry Geophysics Geosystems, 19(4), 1080 -- 1106. 



## How do I get set up? 

### Binary

Binary versions of MERRILL along with tutorial examples are available from [rock.org](https://www.rockmag.org/)

### Source

The code is configured using CMake and can be compiled with a standard
compliant fortran compiler.
A script used to compile binary distributions is in
`scripts/merrill-static/unix.sh`.
It has been compiled with the GNU fortran compilers.
It can be compiled in Linux and OSX using the standard fortran compilers.
Windows development not supported - use WSL2 .

Full details can be found in the manual: /doc/LaTex/main.pdf

Git clone and git clone recursive submodules to populate H2Lib/

``
git clone --recurse-submodules https://bitbucket.org/wynwilliams/merrill
``

cmake and make:

```
mkdir build
cd build
cmake .. 
make
```

Relevant Cmake options include:
- ``-DCMAKE_BUILD_TYPE=Release`` (default), ``DEBUG``
- ``-DMERRILL_USE_H2LIB=ON`` (default) , ``OFF`` for when H2Lib cannot be built

### Use

Run the merrill executable with a merrill input script file. 

``./merrill path/to/script.msc``

See documentation and examples for scripting language.

### Troubleshooting
#### H2Lib... no makefile found
```
[  1%] Creating directories for 'H2Lib'
[  2%] No download step for 'H2Lib'
[  4%] No update step for 'H2Lib'
[  5%] No patch step for 'H2Lib'
[  7%] No configure step for 'H2Lib'
[  8%] Performing build step for 'H2Lib'
make[3]: *** No targets specified and no makefile found. Stop.
make[2]: *** [CMakeFiles/H2Lib.dir/build.make:86: ../src/interfaces/H2Lib/stamp/H2Lib-build] Error 2
make[1]: *** [CMakeFiles/Makefile2:156: CMakeFiles/H2Lib.dir/all] Error 2
make: *** [Makefile:136: all] Error 2
```

Is `src/interfaces/H2Lib/` populated?  Use

``git submodule update --init --recursive``

to fetch H2Lib source code.

#### gctr1.o not found
``ld: library 'gcrt1.o' not found`` 

gprof doesn't exist on mac, but flags were requested via ``-DCMAKE_BUILD_TYPE=DEBUG``.  Use ``-DCMAKE_BUILD_TYPE=DEBUGMAC`` on mac.


## Contribution guidelines 

File issues, bugs, and enhancement proposals on the issues board of https://bitbucket.org/wynwilliams/merrill/ .

Submit patches and code contributions as a pull request to merrill-development branch.

## Git history

This is the main repository for MERRILL including releases and ongoing development.

Main development was hosted here originally.  Between release 1.3.6 and 1.8.6p this bitbucket repository merrill/ held only releases;
development branches were hosted at private bitbucket repository merrill-development/ .  For release 2.0.0 Feb 2025 all branches from both repositories
were uploaded and made public to merrill/ .  The git history in merrill/ looks like two trees with seperate root points, merging tips of master branches.

master is for releases
merrill-development is the main branch for ongoing development

## Who do I talk to? ###

Further information can be obtained from:<br>
Wyn Williams at wyn.williams@ed.ac.uk<br>
Karl Fabian at karl.fabian@ngu.no<br>
Les Nagy at lesleis.nagy@liverpool.ac.uk<br>
Greig Paterson at greig.paterson@liverpool.ac.uk

## License
MERRILL is open source we welcome contributors. MERRILL is shared under a CC BY-SA 4.0 license<br>
https://creativecommons.org/licenses/by-sa/4.0/